﻿#include"XQZipPack_zipInfo.h"
#include"XQAlgorithm.h"
QString XQZipPack_zipInfo::curfileSizeToString()const
{
	return std::move(readableFileSize(fileSize));
}

QString XQZipPack_zipInfo::fileSizeToString()const
{
	return std::move(readableFileSize(fileSumSize));
}

void XQZipPack_zipInfo::setFile(const QFileInfo& file)
{
	File = QFileInfo(file.absoluteFilePath());
}

void XQZipPack_zipInfo::addSize(const size_t size)
{
	fileSize += size;
	if (fileSumSize != 0)
		fileSizePercent = (qreal)(fileSize) / fileSumSize;
}

void XQZipPack_zipInfo::setIndexes(size_t nSel)
{
	this->nSel = nSel;
	if (fileCount != 0)
		fileCountPercent = (qreal)(nSel) / fileCount;
}

void XQZipPack_zipInfo::addIndexes()
{
	setIndexes(nSel + 1);
}

void XQZipPack_zipInfo::clear()
{
	nSel = 0;
	fileCount = 0;
	fileSumSize = 0;
	fileSizePercent = 0;
	fileCountPercent = 0;
	fileSize = 0;
}
