﻿#include"XQZipPack.hpp"
#include"XQAlgorithm.h"
#include<QStack>
#include<QDir>
#include<QDebug>
size_t XQZipPack::StatisticalDirFileSize(QFileInfoList& list)
{
	size_t size = 0;
	for (auto& info : list)
	{
		size += info.size();
	}
	return size;
}

QString XQZipPack::StatisticalDirFileSizeToString()
{
	//StatisticalDirFileSize();
	return std::move(readableFileSize(m_zipInfo.fileSumSize));
}
void XQZipPack::sendProgress()
{
	QDateTime endTime = QDateTime::currentDateTime();
	if (m_startTime.msecsTo(endTime) > m_ProgressTime)
	{
		emit zipProgress(m_zipInfo);//时间到了发送信号
		m_startTime = endTime;
	}
}
size_t XQZipPack::fileListSize(const QString& dirName, QFileInfoList* list)
{
	size_t count = 0;//统计数量
	QStack<QFileInfo> stack;
	stack.push(QFileInfo(dirName));
	QFileInfoList infoList;
	while (!stack.isEmpty())
	{
		QDir dir(stack.top().filePath());
		stack.pop();
		infoList = dir.entryInfoList(QDir::Files);
		count += infoList.count();
		if(list!=nullptr)
			list->append(infoList);
		emit fileListProgress(infoList);//通知进度
		//emit progressInfo(QString("获取到文件数量:") + QString::number(list.count()));
		stack.append(dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot));
	}
	return count;
}
QFileInfoList XQZipPack::fileList(const QString& dirName)
{
	QFileInfoList fileList;//文件列表
	fileListSize(dirName, &fileList);
	emit fileListFinish(fileList);
	return std::move(fileList);
}
void XQZipPack::clear()
{
}
XQZipPack::XQZipPack(QObject* parent)
	:QObject(parent)
{

}

XQZipPack::~XQZipPack()
{

}

void XQZipPack::addDir(const QString& dir)
{
	m_dirs << dir;
}

void XQZipPack::addDir(const QStringList& dir)
{
	m_dirs.append(dir);
}

void XQZipPack::addFile(const QString& path)
{
	m_files << path;
}

void XQZipPack::addFile(const QStringList& path)
{
	m_files.append(path);
}

void XQZipPack::setProgressTime(int time)
{
	m_ProgressTime = time;
}
