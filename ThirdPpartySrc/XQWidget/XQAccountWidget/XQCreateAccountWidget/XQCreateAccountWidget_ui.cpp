﻿#include"XQCreateAccountWidget.h"
#include"XQTitleBarWidget.h"
#include"XQSurfacePlotWidget.h"
#include"XQUserInfo.h"
#include<QBoxLayout>
#include<QLabel>
#include<QLineEdit>
#include<QRadioButton>
#include<QButtonGroup>
#include<QPushButton>
#include<QIntValidator>
#include<QRegExp>
void XQCreateAccountWidget::init_UI()
{
	setWindowTitle("注册账号");
	//设置自定义标题栏
	auto title = new XQTitleBarWidget(this);
	title->setBtnSpacing(20);
	title->setButtons(XQTBWType::top | XQTBWType::minimized | XQTBWType::close);
	title->setMouseStretchWidget(false);
	//设置内容布局
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom, this);
	layout->addWidget(title);
	layout->addLayout(init_headPortrait());
	layout->addLayout(init_accountEdit());
	layout->addLayout(init_passwordEdit());
	layout->addLayout(init_emailEdit());
	layout->addLayout(init_phoneEdit());
	layout->addLayout(init_nameEdit());
	layout->addLayout(init_genderButtons()); 
	layout->addLayout(init_verify());
	layout->addLayout(init_registerAndClose());
	layout->addStretch(1);
	setLayout(layout); 
}

QBoxLayout* XQCreateAccountWidget::init_headPortrait()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("头像:", this);
	LeftLayout->addWidget(label, 0, Qt::AlignVCenter | Qt::AlignLeft);

	m_headPortrait = new XQSurfacePlotWidget(this);
	m_headPortrait->setFixedHeight(150);//设置高度
	//surfacePlot->setImageDir(imageDir());
	LeftLayout->addWidget(m_headPortrait, 1);
	//链接选择头像信号

	return LeftLayout;
}

QBoxLayout* XQCreateAccountWidget::init_accountEdit()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("账号:", this);
	LeftLayout->addWidget(label, 0, Qt::AlignVCenter | Qt::AlignLeft);
	m_accountEdit = new QLineEdit(this);
	LeftLayout->addWidget(m_accountEdit);
	return LeftLayout;
}

QBoxLayout* XQCreateAccountWidget::init_passwordEdit()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("密码:", this);
	LeftLayout->addWidget(label, 0, Qt::AlignVCenter | Qt::AlignLeft);
	m_passwordEdit = new QLineEdit(this);
	m_passwordEdit->setEchoMode(QLineEdit::Password);
	LeftLayout->addWidget(m_passwordEdit);
	return LeftLayout;
}

QBoxLayout* XQCreateAccountWidget::init_emailEdit()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("邮箱:", this);
	LeftLayout->addWidget(label, 0, Qt::AlignVCenter | Qt::AlignLeft);
	m_emailEdit = new QLineEdit(this);
	LeftLayout->addWidget(m_emailEdit);
	return LeftLayout;
}

QBoxLayout* XQCreateAccountWidget::init_phoneEdit()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("手机:", this);
	LeftLayout->addWidget(label, 0, Qt::AlignVCenter | Qt::AlignLeft);
	m_phoneEdit = new QLineEdit(this);
	m_phoneEdit->setValidator(new QRegularExpressionValidator(QRegularExpression("\\d{11}"), m_phoneEdit));
	LeftLayout->addWidget(m_phoneEdit);
	return LeftLayout;
}

QBoxLayout* XQCreateAccountWidget::init_nameEdit()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("名字:", this);
	LeftLayout->addWidget(label, 0, Qt::AlignVCenter | Qt::AlignLeft);
	m_nameEdit = new QLineEdit(this);
	LeftLayout->addWidget(m_nameEdit);
	return LeftLayout;
}

QBoxLayout* XQCreateAccountWidget::init_genderButtons()
{
	m_gender =new QButtonGroup(this);
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto boy = new QRadioButton("男", this);
	auto girl = new QRadioButton("女", this);
	auto Hideyoshi = new QRadioButton("秀吉", this);
	LeftLayout->addStretch(1);
	m_gender->addButton(boy, genderType::boy);
	LeftLayout->addWidget(boy);
	m_gender->addButton(girl, genderType::girl);
	LeftLayout->addWidget(girl);
	m_gender->addButton(Hideyoshi, genderType::Hideyoshi);
	LeftLayout->addWidget(Hideyoshi);
	LeftLayout->addStretch(1);
	boy->setChecked(true);
	return LeftLayout;
}

QBoxLayout* XQCreateAccountWidget::init_verify()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("邮箱验证码:", this);
	
	LeftLayout->addWidget(label, 0, Qt::AlignVCenter | Qt::AlignLeft);
	m_verifyEdit = new QLineEdit(this);//验证码输入框
	m_verifyEdit->setValidator(new QIntValidator(m_verifyEdit));
	LeftLayout->addWidget(m_verifyEdit);

	m_sendVerifyBtn = new QPushButton("发送",this);//验证码发送按钮
	m_sendVerifyBtn->setObjectName("sendBtn");
	LeftLayout->addWidget(m_sendVerifyBtn);
	
	return LeftLayout;
}

QBoxLayout* XQCreateAccountWidget::init_registerAndClose()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	m_registerBtn = new QPushButton("注册", this);//注册
	m_registerBtn->setObjectName("registerBtn");
	LeftLayout->addWidget(m_registerBtn);

	auto closeBtn = new QPushButton("关闭", this);//关闭
	closeBtn->setObjectName("closeBtn");
	LeftLayout->addWidget(closeBtn);
	//链接关闭窗口信号
	connect(closeBtn, &QPushButton::pressed, this, &XQCreateAccountWidget::close);
	//链接注册账号信号
	connect(m_registerBtn, &QPushButton::pressed, this, &XQCreateAccountWidget::registerAccountPressed);
	return LeftLayout;
}
