﻿#include"XQRetrievePasswordWidget.h"
#include"XQTitleBarWidget.h"
#include"XQAlgorithm.h"
#include<QBoxLayout>
#include<QLabel>
#include<QLineEdit>
#include<QPushButton>
#include<QDebug>
void XQRetrievePasswordWidget::init()
{
	setCssFile(this, ":/XQRetrievePasswordWidget/style.css");
	init_UI();
}

void XQRetrievePasswordWidget::init_UI()
{
	setWindowTitle("注册账号");
	//设置自定义标题栏
	auto title = new XQTitleBarWidget(this);
	title->setBtnSpacing(20);
	title->setButtons(XQTBWType::top | XQTBWType::minimized | XQTBWType::close);
	title->setMouseStretchWidget(false);
	//设置内容布局
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom, this);
	layout->addWidget(title);
	layout->addLayout(init_emailEdit());
	layout->addLayout(init_verify());
	layout->addLayout(init_passwordEdit());
	layout->addLayout(init_RedoPasswordEdit());
	layout->addLayout(init_ModifyAndClose());
	layout->addStretch(1);
	setLayout(layout);
}

QBoxLayout* XQRetrievePasswordWidget::init_emailEdit()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("邮箱:", this);
	LeftLayout->addWidget(label, 0, Qt::AlignVCenter | Qt::AlignLeft);
	m_emailEdit = new QLineEdit(this);
	LeftLayout->addWidget(m_emailEdit);
	return LeftLayout;
}

QBoxLayout* XQRetrievePasswordWidget::init_verify()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("邮箱验证码:", this);

	LeftLayout->addWidget(label, 0, Qt::AlignVCenter | Qt::AlignLeft);
	m_verifyEdit = new QLineEdit(this);//验证码输入框
	LeftLayout->addWidget(m_verifyEdit);

	m_sendVerifyBtn = new QPushButton("发送", this);//验证码发送按钮
	m_sendVerifyBtn->setObjectName("sendBtn");
	LeftLayout->addWidget(m_sendVerifyBtn);
	//链接发送验证码信号
	connect(m_sendVerifyBtn, &QPushButton::pressed, this, &XQRetrievePasswordWidget::sendVerify);
	return LeftLayout;
}

QBoxLayout* XQRetrievePasswordWidget::init_passwordEdit()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("输入密码:", this);
	LeftLayout->addWidget(label, 0, Qt::AlignVCenter | Qt::AlignLeft);
	m_passwordEdit = new QLineEdit(this);
	LeftLayout->addWidget(m_passwordEdit);
	return LeftLayout;
}

QBoxLayout* XQRetrievePasswordWidget::init_RedoPasswordEdit()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("再次输入:", this);
	LeftLayout->addWidget(label, 0, Qt::AlignVCenter | Qt::AlignLeft);
	m_RedoPasswordEdit = new QLineEdit(this);
	LeftLayout->addWidget(m_RedoPasswordEdit);
	return LeftLayout;
}

QBoxLayout* XQRetrievePasswordWidget::init_ModifyAndClose()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto modifyBtn = new QPushButton("修改", this);//修改
	modifyBtn->setObjectName("modifyBtn");
	LeftLayout->addWidget(modifyBtn);

	auto closeBtn = new QPushButton("关闭", this);//关闭
	closeBtn->setObjectName("closeBtn");
	LeftLayout->addWidget(closeBtn);
	//链接关闭窗口信号
	connect(closeBtn, &QPushButton::pressed, this, &XQRetrievePasswordWidget::close);
	//链接修改账号信号
	connect(modifyBtn, &QPushButton::pressed, this, &XQRetrievePasswordWidget::modifyAccount);
	return LeftLayout;
}