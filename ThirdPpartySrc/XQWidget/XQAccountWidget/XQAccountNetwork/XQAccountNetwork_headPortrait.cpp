﻿#include"XQAccountNetwork.h"
#include"XQAlgorithm.h"
#include"XQMySql.hpp"
#include"XQUserInfo.h"
#include"XMD5.h"
#include"XQSettingsInfo.h"
#ifdef THIRDPARTY
#include"XQFtp.hpp"
#endif
#include"XQAppInfo.h"
#include<QFileInfo>
//获取头像信息
QStringList XQAccountNetwork::getheadPortraitInfo(const QString& account)
{
	if (ISQNULL(appInfo(), "appInfo")) { emit loginVerifyResult(login::error, "XQAccountNetwork::appInfo()是null"); return QStringList(); }
	auto mysql = appInfo()->mysql()->cloneDatabase_ScopedPointer("getheadPortraitInfo");
	auto userInfo = appInfo()->userInfo();
	auto settingsInfo = appInfo()->settingsInfo();
	if (mysql->Query("SELECT account FROM user WHERE account='%1';", account) && !mysql->result())
	{//账户不存在
		/*emit loginVerifyResult(login::userNameNotExist);;*/
		return QStringList();
	}
	//通过用户名查询信息
	if (!mysql->Query("SELECT headPortraitPath,headPortraitPathMd5 FROM user WHERE account='%1';", account))
	{
		emit loginVerifyResult(login::error, "数据库查询失败");
		return QStringList();
	}
	QStringList info=mysql->result_oneRow_toString();
	if (info.isEmpty())
	{//没有这个账号
		return QStringList();
	}
	return std::move(info);
}
bool XQAccountNetwork::getheadPortraitInfo(const QString& account,QString& remoteHeadPortraitPath, QString& headPortraitPathMd5)
{

	QStringList info = getheadPortraitInfo(account);
	if (info.isEmpty())
		return false;
	remoteHeadPortraitPath = info[0];
	headPortraitPathMd5 = info[1];
	return true;
}
QPixmap XQAccountNetwork::findHeadPortrait(const QString& account)
{
	if (ISQNULL(appInfo(), "appInfo")) { emit loginVerifyResult(login::error, "XQAccountNetwork::appInfo()是null"); return QPixmap(); }
	auto mysql = appInfo()->mysql()->cloneDatabase_ScopedPointer("findHeadPortrait");
	auto userInfo = appInfo()->userInfo();
	auto settingsInfo = appInfo()->settingsInfo();
	QStringList info = getheadPortraitInfo(account);
	if (info.isEmpty())
		return QPixmap();
	//读取本地的头像信息
	QString headPortraitPath = settingsInfo->bufferPath() + "/user/" + account + "/data" + "/headPortrait.png";
	if (QFile(headPortraitPath).exists()&& XMD5::file(headPortraitPath.toLocal8Bit().toStdString()).c_str() == info[1])
	{//文件存在并且MD5值也对
		return QPixmap(headPortraitPath);
	}
	//本地缓存的头像路径
	auto path = settingsInfo->bufferPath() + "/temp/headPortrait";
	QDir().mkpath(path);//递归创建路径
	headPortraitPath = path + "/"+ account+".png";
	if (!QFile(headPortraitPath).exists()|| XMD5::file(headPortraitPath.toLocal8Bit().toStdString()).c_str() != info[1])
	{
		//文件不存在或md5值不对
		QFile(headPortraitPath).remove();
#ifdef THIRDPARTY
		auto ftp = appInfo()->ftp();
		ftp->Download(headPortraitPath, info[0]);
#endif
	}
	return QPixmap(headPortraitPath);
}

QPair<QString, QString> XQAccountNetwork::uploadUserheadPortrait(const QPixmap& pixmap)
{
	if (ISQNULL(appInfo(), "appInfo")) { emit loginVerifyResult(login::error, "XQAccountNetwork::appInfo()是null"); return QPair<QString, QString>(); }
	auto mysql = appInfo()->mysql()->cloneDatabase_ScopedPointer("uploadUserheadPortrait");
	auto userInfo = appInfo()->userInfo();
	auto settingsInfo = appInfo()->settingsInfo();
	
	if (userInfo->headPortrait().isNull())//不存在头像
	{
		return QPair<QString, QString>();
	}
	if (settingsInfo == nullptr)
		return QPair<QString, QString>("error", "settingsInfoisNull");
	if (userInfo == nullptr)
		return QPair<QString, QString>("error", "userInfoisNull");
	//缓存路径/用户账号/data
	auto path = settingsInfo->bufferPath() + "/user/" + userInfo->account() + "/data";
	QString headPortraitPath = path + "/headPortrait.png";//本地缓存的头像路径
	QDir().mkpath(path);//递归创建路径
	//缩放100*100保存头像到本地
	if (!pixmap/*.scaled(100, 100)*/.save(headPortraitPath))//失败的话
	{
		return QPair<QString, QString>("error", "headPortraitPath fail to save");
	}
	//获取头像md5值
	QString md5 = XMD5::file(headPortraitPath.toLocal8Bit().toStdString()).c_str();
	//开始上传ftp
	QString ftpPath = "user/" + userInfo->account() + "/data";
	QString ftpheadPortraitPath = ftpPath + "/headPortrait.png";//ftp路径
#ifdef THIRDPARTY
	auto ftp = appInfo()->ftp();
	ftp->mkpath(ftpPath);//创建目录
	ftp->Upload(headPortraitPath, ftpheadPortraitPath);
#endif
	return QPair<QString, QString>(ftpheadPortraitPath, md5);
}

void XQAccountNetwork::saveUserheadPortrait(const QString& account,  QString remoteHeadPortraitPath,  QString headPortraitPathMd5, const QPixmap& pixmap)
{
	if (ISQNULL(appInfo(), "appInfo")) { emit loginVerifyResult(login::error, "XQAccountNetwork::appInfo()是null"); return; }
	auto mysql = appInfo()->mysql()->cloneDatabase_ScopedPointer("saveUserheadPortrait");
	auto userInfo = appInfo()->userInfo();
	auto settingsInfo = appInfo()->settingsInfo();
	
	if (account.isEmpty())
		return;
	//先看看本地文件在不在，对不对
	auto path = settingsInfo->bufferPath() + "/user/" + account + "/data";
	QDir().mkpath(path);//递归创建路径
	QString localHeadPortraitPath = path + "/headPortrait.png";//本地用户的头像路径
	if (!pixmap.isNull())
	{
		pixmap.save(localHeadPortraitPath);
		return;
	}
	//没有传入md5值联网查询
	if(headPortraitPathMd5.isEmpty()&& !getheadPortraitInfo(account,remoteHeadPortraitPath, headPortraitPathMd5))
		return;
	//先看看本地文件在不在，对不对
	if (QFile(localHeadPortraitPath).exists() && XMD5::file(localHeadPortraitPath.toLocal8Bit().toStdString()).c_str() == headPortraitPathMd5)
	{//文件存在并且MD5值也对
		return;
	}
	//看看缓存目录有没有对不对
	QString headPortraitPath = settingsInfo->bufferPath() + "/temp/headPortrait" + "/" + account + ".png";
	if (QFile(headPortraitPath).exists() && XMD5::file(headPortraitPath.toLocal8Bit().toStdString()).c_str() == headPortraitPathMd5)
	{//文件存在并且MD5值也对拷贝文件到本地用户目录
		QFile(localHeadPortraitPath).remove();
		QFile::copy(headPortraitPath, localHeadPortraitPath);
		return;
	}
	//两处都不存在我只能下载了
	QFile(localHeadPortraitPath).remove();
	//没有远程地址联网查询
	if (remoteHeadPortraitPath.isEmpty() && !getheadPortraitInfo(account, remoteHeadPortraitPath, headPortraitPathMd5))
		return;
#ifdef THIRDPARTY
	auto ftp = appInfo()->ftp();
	ftp->Download(localHeadPortraitPath, remoteHeadPortraitPath);
#endif
}
