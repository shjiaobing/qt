﻿#include"XQAccountNetwork.h"
#include"XQMySql.hpp"
#include"XQUserInfo.h"
#include"XQSettingsInfo.h"
#include"XMD5.h"
//#include"XQZipPack.hpp"
//#include"XQFtp.hpp"
#include"XQAlgorithm.h"
#include"XQAppInfo.h"
#include<QDir>
#include<QDebug>
#include<QEventLoop>
#include<QThread>
void XQAccountNetwork::registerAccountNetwork()
{
	if (ISQNULL(appInfo(), "appInfo")) { emit loginVerifyResult(login::error, "XQAccountNetwork::appInfo()是null"); return; }
	auto mysql = appInfo()->mysql()->cloneDatabase_ScopedPointer("registerAccountNetwork");
	auto userInfo = appInfo()->userInfo();
	auto settingsInfo = appInfo()->settingsInfo();
	//qInfo() << "验证注册的账号是否合法";
	//判断用户表是否存在不存在创建表
	//userTableExists();

	if (mysql->Query("SELECT account FROM user WHERE account='%1';", userInfo->account()) && mysql->result())
	{//有存在同名账户
		//qInfo() << "存在同名账户" << userInfo()->account();
		emit createVerifyResult(::create::userNameRepeat);
		return;
	}
	
	QPair<QString, QString> ret;//返回值
	auto LPret = &ret;
	//向窗口发送上传头像到数据库的信息
	emit createVerifyResult(::create::UploadheadPortrait);
	auto thread = QThread::create([=] {*LPret = uploadUserheadPortrait(userInfo->headPortrait());});
	thread->start();//开始执行
	QEventLoop eventLoop;
	connect(thread, &QThread::finished, &eventLoop, &QEventLoop::quit);
	eventLoop.exec();//进入时间循环，等到线程结束
	if (ret.first == "error")
	{
		emit createVerifyResult(::create::error, ret.second);
		qWarning() << ret.second;
		return;
	}
	auto ctime = mysql->ctime("@time");
	//插入用户信息
	if (!mysql->Query("INSERT INTO `user` (`account`,`password`,`email`,`phone`,`name`,`gender`,`permission`,`headPortraitPath`,`headPortraitPathMd5`,`ctime`,`mtime`) VALUES('%1','%2','%3','%4','%5',%6,%7,'%8','%9',@time,@time);", userInfo->account(), userInfo->password(),userInfo->email(), userInfo->phone(), userInfo->name(), userInfo->gender(), userInfo->permission(), ret.first,ret.second))
	{//插入失败
		emit createVerifyResult(::create::error);
	}
	userInfo->setCTime(ctime);
	userInfo->setMTime(ctime);
	//插入成功
	emit createVerifyResult(::create::yes);
}
void XQAccountNetwork::userTableExists(const QString& tableName)
{
	if (ISQNULL(appInfo(), "appInfo")) { emit loginVerifyResult(login::error, "XQAccountNetwork::appInfo()是null"); return; }
	auto mysql = appInfo()->mysql()->cloneDatabase_ScopedPointer("userTableExists");
	auto userInfo = appInfo()->userInfo();
	auto settingsInfo = appInfo()->settingsInfo();
	qInfo() << "查询表是否存在";
	if (mysql->Query("SELECT table_name FROM information_schema.tables WHERE table_schema='note' AND table_name='%1';", tableName) && !mysql->result())
	{
		qInfo() <<"用户表不存在";
		//创建表
		auto ret=mysql->Query("CREATE TABLE `%1` ("
			"account VARCHAR(30) PRIMARY KEY,"//账号
			"ctime VARCHAR(32) ,"//创建时间
			"mtime VARCHAR(32) ,"//修改时间
			"password VARCHAR(32) ,"//密码
			"email VARCHAR(30) ,"//邮箱
			"phone VARCHAR(20) ,"//手机号
			"name VARCHAR(10) ,"//名字
			"gender INT ,"//性别
			"permission INT ,"//权限
			"headPortraitPath VARCHAR(512),"//存储头像的ftp服务器路径
			"headPortraitPathMd5 VARCHAR(32)"//存储头像在ftp服务器的md5值
		")", tableName);
		if (ret && !mysql->result())
			qInfo() << "用户表创建成功";
	}
}
