﻿#include"XQAccountNetwork.h"
#include"XQAlgorithm.h"
#include"XQMySql.hpp"
#include"XQUserInfo.h"
#include"XMD5.h"
#include"XQSettingsInfo.h"
//#include"XQFtp.hpp"
#include"XQAppInfo.h"
#include<QFileInfo>
void XQAccountNetwork::loginNetwork()
{
	if (ISQNULL(appInfo(), "appInfo")) { emit loginVerifyResult(login::error, "XQAccountNetwork::appInfo()是null"); return; }
	auto mysql = appInfo()->mysql()->cloneDatabase_ScopedPointer("loginNetwork");
	auto userInfo = appInfo()->userInfo();
	auto settingsInfo = appInfo()->settingsInfo();
	if (ISQNULL(userInfo, "userInfo")) {emit loginVerifyResult(login::error, "XQAccountNetwork::userInfo是null");return;}
	//if (ISQNULL(mysql, "mysql")) { emit loginVerifyResult(login::error, "XQAccountNetwork::mysql是null"); return; }
	if (ISQNULL(settingsInfo, "settingsInfoisNull")) { emit loginVerifyResult(login::error, "XQAccountNetwork::settingsInfo是null"); return; }
	if (mysql->Query("SELECT account FROM user WHERE account='%1';", userInfo->account()) && !mysql->result())
	{//账户不存在
		emit loginVerifyResult(login::userNameNotExist);;
		return;
	}
	//通过密码查询信息
	if (!mysql->Query("SELECT `account`,`password`,`email`,`phone`,`name`,`gender`,`permission`,`headPortraitPath`,`headPortraitPathMd5`,`ctime`,`mtime` FROM `user` WHERE `account`='%1' AND `password`='%2';", userInfo->account(), userInfo->password()))
	{
		emit loginVerifyResult(login::error,"数据库查询失败");
		return;
	}
	QStringList info = mysql->result_oneRow_toString();
	if (info.isEmpty())
	{//密码错误
		emit loginVerifyResult(login::passwordError);
		return;
	}
	//读取写入基本信息
	auto user = userInfo;
	user->setAccount(info[0]);
	user->setPassword(info[1]);
	user->setEmail(info[2]);
	user->setPhone(info[3]);
	user->setName(info[4]);
	user->setGender(genderType(info[5].toInt()));
	user->setPermission(Permission(info[6].toInt()));
	user->setCTime(QDateTime::fromString(info[9], Qt::ISODate));
	user->setMTime(QDateTime::fromString(info[10], Qt::ISODate));
	//写入头像信息
	saveUserheadPortrait(info[0],info[7],info[8]);
	QString headPortraitPath = settingsInfo->bufferPath() + "/user/" + userInfo->account() + "/data" + "/headPortrait.png";//本地缓存的头像路径
	user->setheadPortrait(headPortraitPath);//设置用户头像
	QFile(settingsInfo->bufferPath() + "/temp/headPortrait" + "/" + info[0] + ".png").remove();//删除缓存目录的头像节约空间
	emit loginVerifyResult(login::yes);
}


