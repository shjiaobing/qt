﻿#include"XExcel.h"

XExcel::XExcel()
{
	//// 获取默认的工作表
	auto sheet=m_wb.active_sheet();
	auto name = sheet.title();
	m_sheets.insert(QString::fromLocal8Bit(name.c_str(), name.size()), sheet);
	m_worksheet = sheet;
}

XExcel::XExcel(const QString& file)
{
	load(file);
	for (auto sheet:m_wb)
	{
		auto name = sheet.title();
		m_sheets.insert(QString::fromLocal8Bit(name.c_str(),name.size()),sheet);
	}
	m_worksheet = m_wb.active_sheet();
}

XExcel::~XExcel()
{
}

void XExcel::load(const QString& file)
{
	m_wb.load(file.toStdString());
	emit loadFinish();
}

xlnt::worksheet& XExcel::addSheet(const QString& name)
{
	// 添加一个新的工作表
	xlnt::worksheet newWs = m_wb.create_sheet();
	newWs.title(name.toStdString());
	m_sheets[name] = newWs;
	return m_sheets[name];
}

xlnt::worksheet XExcel::currentSheet()
{
	return m_worksheet;
}

bool XExcel::switchSheet(const QString& name)
{
	if (m_sheets.contains(name))
	{
		m_worksheet = m_sheets[name];
		return true;
	}
	return false;
}

bool XExcel::switchSheet(const xlnt::worksheet& sheet)
{
	if (m_sheets.key(sheet).isEmpty())
		return false;
	m_worksheet = sheet;
	return true;
}

void XExcel::setSheetName(const QString& name)
{
	std::string t=  name.toStdString();
	m_worksheet.title(t);
}

QString XExcel::sheetName(const QString& name)
{
	return QString::fromLocal8Bit(m_worksheet.title().c_str(), m_wb.active_sheet().title().size());
}

void XExcel::writeRow(size_t row, const QStringList list)
{
	for (int i = 0; i < list.size(); i++)
	{
		// 将数据插入到单元格中
		//m_worksheet.cell(xlnt::cell_reference(i + 1, row)).value(list[i].toLocal8Bit().data());
		setItemData(i+1,row,list[i]);
		emit writeOneRow(row,list, m_worksheet);
	}
}

void XExcel::writeRow(const QStringList list)
{
	writeRow(m_worksheet.next_row(),std::move(list));
}

void XExcel::writeAll(const QList<QStringList> list)
{
	for (auto& stringList:list)
	{
		writeRow(stringList);
	}
	emit writeAllFinish();
}

QStringList XExcel::readRow(size_t row)
{
	QStringList list;
	for (int i = 0; i < max_column(); i++)
	{
		// 将数据插入到单元格中
		//list << m_worksheet.cell(xlnt::cell_reference(i + 1, row)).to_string().c_str();
		list << itemData(i+1,row);
	}
	emit readOneRow(row,list,m_worksheet);
	return std::move(list);
}

void XExcel::readAll(QList<QStringList>& list)
{
	for (size_t i = 0; i < max_row(); i++)
	{
		list<<readRow(i+1);//添加一行数据
	}
	emit readAllFinish();
}

QString XExcel::itemData(size_t column, size_t row)
{
	std::string data=m_worksheet.cell(xlnt::cell_reference(column, row)).to_string();
	return QString(data.c_str());
}

void XExcel::setItemData(size_t column, size_t row, const QString& data)
{
	auto SourceText = itemData(column, row);
	m_worksheet.cell(xlnt::cell_reference(column, row)).value(data.toStdString());
	emit TextChange(column, row, SourceText, data);
}

void XExcel::save(const QString& file)
{
	m_wb.save(file.toStdString());
	emit saveFinish();
}

size_t XExcel::max_column()
{
	return m_worksheet.highest_column().index;
}

size_t XExcel::max_row()
{
	return m_worksheet.highest_row();
}
