﻿#ifndef XEXCEL_H
#define XEXCEL_H
#include<QOBject>
#include<QMap>
#include"xlnt/xlnt.hpp"
#ifdef _DEBUG
#pragma comment(lib,"x64-Debug/xlntd.lib")
#else
#pragma comment(lib,"x64-Release/xlnt.lib")
#endif // _DEBUG
//表格类
class XExcel:public QObject
{
	Q_OBJECT
public:
	//默认创建新表打开
	XExcel();
	//从文件打开一个工作簿
	XExcel(const QString& file);
	~XExcel();
public slots:
	//从文件打开一个工作簿
	void load(const QString& file);
	//添加一个工作表
	xlnt::worksheet& addSheet(const QString& name);
	//获取当前工作表
	xlnt::worksheet currentSheet();
	//切换工作表
	bool switchSheet(const QString& name);
	bool switchSheet(const xlnt::worksheet& sheet);
	//设置工作表名字
	void setSheetName(const QString& name);
	//获取工作表名
	QString sheetName(const QString& name);
	//表写入一行
	void writeRow(size_t row,const QStringList list);
	//表写入一行,自动往下
	void writeRow(const QStringList list);
	//写入一个二维数组的内容
	void writeAll(const QList<QStringList> list);
	//表读取一行
	QStringList readRow(size_t row);
	//读取全部数据到一个二维数组
	void readAll(QList<QStringList>& list);
	//获取当前工作表一个单元格
	QString itemData(size_t column, size_t row);
	//设置当前工作表的一个单元格数据
	void setItemData(size_t column, size_t row, const QString& data);
	//保存工作簿到文件
	void save(const QString& file);
	//获取表列数
	size_t max_column();
	//获取表行数
	size_t max_row();
signals://信号
	//当前写一行数据
	void writeOneRow(size_t row,const QStringList& list, xlnt::worksheet& sheet);
	//全部写入完成
	void writeAllFinish();
	//当前读取一行数据
	void readOneRow(size_t row, const QStringList& list, xlnt::worksheet& sheet);
	//全部读取完成
	void readAllFinish();
	//保存完成
	void saveFinish();
	//读取打开文件结束
	void loadFinish();
	//内容/文本改变
	void TextChange(size_t column, size_t row, const QString& SourceText, const QString& newText);
private:
	xlnt::workbook m_wb;//工作簿
	QMap<QString, xlnt::worksheet> m_sheets;//表集合
	xlnt::worksheet m_worksheet;//当前工作表
};
#endif // ! XExcel_H
