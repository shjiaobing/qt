﻿#ifndef GZIP_H
#define GZIP_H
#include"zlib.h"
/* 压缩 data 原数据 ndata 原数据长度 zdata 压缩后数据 nzdata 压缩后长度 */
int gzCompress(const char* src, int srcLen, char* dest, int destLen);
/*解压*/
int gzDecompress(const char* src, int srcLen, const char* dst, int dstLen);
#endif // !GZIP_H


