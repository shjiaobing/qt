﻿#include"XQStackedTitleWidget.h"
#include<QLabel>
#include<QBoxLayout>
void XQStackedTitleWidget::init_ui()
{
	m_background = new QWidget(this);
	m_background->setObjectName("background");
	m_buttonCount = new QLabel("0", this);
	m_buttonCount->setObjectName("count");
	m_buttonCount->setAlignment(Qt::AlignHCenter);
	{//m_background布局设置
		//设置内容布局
		auto layout = new QBoxLayout(m_dirButton, m_background);
		layout->setContentsMargins(5, 5, 5, 5);
		layout->setSpacing(10);
		layout->addStretch(1);
		layout->addWidget(m_buttonCount);
		m_background->setLayout(layout);
	}
	{//主界面背景布局
		auto layout = new QBoxLayout(QBoxLayout::LeftToRight, this);//从左到右
		layout->setContentsMargins(0, 0, 0, 0);
		layout->addWidget(m_background);
		setLayout(layout);
	}
}