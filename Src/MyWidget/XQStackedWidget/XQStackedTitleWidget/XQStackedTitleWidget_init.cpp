﻿#include"XQStackedTitleWidget.h"
#include"XQLabelButton.h"
#include"XQAlgorithm.h"
#include"XQStackedWidget.h"
#include"XQDragStackedWidgetEventFilter.hpp"
#include<QLabel>
#include<QMouseEvent>
#include<QDebug>
void XQStackedTitleWidget::init()
{
	init_ui();
	setCssFile(this, ":/XQStackedTitleWidget/style.css");

	//链接数量改变信号
	connect(this, &XQStackedTitleWidget::countChange, [this](size_t count) {m_buttonCount->setText(QString("%1").arg(count)); });
	setMouseTracking(true);
}

void XQStackedTitleWidget::mousePressEvent(QMouseEvent* event)
{
	if(m_selectBtn)
	{
		auto rect = m_selectBtn->geometry();
		m_pressPos = event->pos();
		if (rect.contains(m_pressPos))
		{
			m_selectWidgetPos = m_selectBtn->pos();
			m_moveBtn = true;
			//qInfo() << "按下坐标" << m_pressPos << "按钮的pos" << m_selectBtn->pos();
		}
	}
}

void XQStackedTitleWidget::mouseReleaseEvent(QMouseEvent* event)
{
	if(m_IndependentWidget|| m_moveWidget)
	{
		m_moveBtn = false;
		//m_selectBtn = nullptr;
		auto layout = (QBoxLayout*)m_background->layout();
		layout->invalidate();
	}
	
}

void XQStackedTitleWidget::mouseMoveEvent(QMouseEvent* event)
{
	auto layout = (QBoxLayout*)m_background->layout();
	if (m_moveWidget&&m_moveBtn&&m_selectBtn && (m_dirButton == QBoxLayout::LeftToRight|| m_dirButton == QBoxLayout::RightToLeft))
	{
		int x = event->pos().x() - m_pressPos.x()+ m_selectWidgetPos.x();
		if(x >0&&x+ m_selectBtn->width()< m_buttonCount->pos().x())
			m_selectBtn->move(x, m_selectBtn->pos().y());
		int index = layout->indexOf(m_selectBtn);
		int count = layout->count();
		auto fron = index - 1;
		if (fron >= 0)
		{
			auto fronBtn = layout->itemAt(fron)->widget();
			if(fronBtn&&fronBtn->geometry().intersects(m_selectBtn->geometry()))
			{
				layout->insertWidget(fron, m_selectBtn);
				/*qInfo() << "需要向前交换";*/
			}
			//qInfo() <<  "检测是否需要往前交换";
		}
		auto last = index + 1;
		if (last < count - 2)
		{
			auto lastBtn = layout->itemAt(last)->widget();
			if (lastBtn && !lastBtn->geometry().intersects(m_selectBtn->geometry())
				/*&& m_selectBtn->pos().x()> lastBtn->pos().x()*/)
			{
				layout->insertWidget(last, m_selectBtn);
				/*qInfo() << "需要向后交换";*/
			}
			//qInfo()<<"检测是否需要往后交换";
		}
		//qInfo() << x << "鼠标移动" << QPoint(x, m_selectBtn->pos().y());
	}
	if (m_IndependentWidget&& m_selectBtn&&!rect().contains(event->pos()) && stackedWidget())
	{
		auto widget = stackedWidget()->widget(m_selectBtn);
		if (widget)
		{
			//widget->setParent(this);
			if (!(widget->windowFlags() & Qt::Window))
			{
				stackedWidget()->IndependentWidget(widget);
				//widget->installEventFilter(new XQDragStackedWidgetEventFilter(this,widget));
				centerShow(widget, stackedWidget());
				//widget->move(QCursor::pos());
			}
		}
		//qInfo() << "移动到边框外";
	}
	//else if (rect().contains(event->pos()) && stackedWidget())
	//{
	//	auto widget = stackedWidget()->widget(m_selectBtn);
	//	if (widget)
	//	{
	//		if (widget->windowFlags() & Qt::Window)
	//		{
	//			stackedWidget()->addWidget(widget);
	//		}
	//	}
	//	//qInfo() << "移动到边框内";
	//}
}
