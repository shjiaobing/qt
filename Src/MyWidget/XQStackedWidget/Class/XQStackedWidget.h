﻿#ifndef XQSTACKEDWIDGET_H
#define XQSTACKEDWIDGET_H
#include<QWidget>
#include<QMap>
#include<QBoxLayout>
#include"XQHead.h"
//堆栈小部件
class XQStackedWidget:public QWidget
{
	Q_OBJECT
public:
	XQStackedWidget(QWidget* parent = nullptr);
	XQStackedWidget(QBoxLayout::Direction dirWindow, QBoxLayout::Direction dirButton, QWidget* parent = nullptr);
	~XQStackedWidget();
	//获取标签标题文本
	QString titleText(QWidget* widget)const;
	//获取标签的图标
	QPixmap icon(QWidget* widget)const;
	//获取小部件数量
	size_t count()const;
	//获取标题栏按钮
	XQLabelButton* labelButton(QWidget* widget)const;
	//获取widget
	QWidget* widget(XQLabelButton* btn)const;
	//获取标题栏小部件
	XQStackedTitleWidget* titleWidget()const;
public slots:
	//切换当前显示的QWidget
	void setCurrentWidget(QWidget* widget);
	void setCurrentWidget(XQLabelButton* btn);
	void setCurrentIndex(int index);
	//独立小部件
	void IndependentWidget(QWidget* widget);
	//添加widget
	void addWidget(QWidget* widget);
	void addWidget(QWidget* widget, const QString& title,const QPixmap& icon= QPixmap());
	//删除widget和对应的按钮
	void removeWidget(QWidget* widget);
	void removeWidget(XQLabelButton* btn);
	//断开关闭按钮的链接
	void disconnectClose(QWidget* widget);
	//清空
	void clearWidget();
	//链接关闭
	void connectClose(QWidget* widget,std::function<bool()>func);
	//设置标题文本
	void setTitleText(const QString& text, QWidget* widget);
	//设置图标
	void setIcon(QPixmap icon, QWidget* widget);
	//设置窗口方向
	void setWindowDirection(QBoxLayout::Direction dir);
	//设置按钮方向
	void setButtonDirection(QBoxLayout::Direction dir);
signals://信号
	//释放小部件，widget已经释放了，通知将其删除，请不要对其操作
	void deleteWidget(QWidget* widget);
protected://初始化
	//初始化
	void init();
	void init_ui();
	//仅仅删除小部件
	void removeOnlyWidget(QWidget* widget,bool AutoDelete=true);
	void removeOnlyWidget(XQLabelButton* btn);
protected://事件
	void closeEvent(QCloseEvent* event)override;
protected://变量
	QBoxLayout* m_windowLayout = nullptr;
	//QBoxLayout::Direction m_dirWindow = QBoxLayout::TopToBottom;//窗口方向
	QBoxLayout::Direction m_dirButton = QBoxLayout::LeftToRight;//按钮方向
	XQStackedTitleWidget* m_StackedTitleWidget = nullptr;//堆栈标题小部件
	QStackedLayout* m_StackedLayout = nullptr;//堆栈布局
	QMap<QWidget*, XQLabelButton*> m_TitleBtns;//标题栏管理按钮
	QMap<XQLabelButton*, QWidget*> m_widgets;//窗口管理
};
#endif // ! XQStackedWidget_H
