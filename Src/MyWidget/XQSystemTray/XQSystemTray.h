﻿#ifndef XQSYSTEMTRAY_H
#define XQSYSTEMTRAY_H
#include<QSystemTrayIcon>
#include"XQHead.h"
//系统托盘菜单
class XQSystemTray:public QSystemTrayIcon
{
	Q_OBJECT
public:
	XQSystemTray(const QIcon& icon, QObject* parent = nullptr);
	XQSystemTray(QObject* parent = nullptr);
	virtual~XQSystemTray();
public://菜单设置
	//设置默认的右键菜单函数
	void setContextMenu();
	//菜单添加显示隐藏窗口
	QAction* menuAddShowHideWindow(QMenu* menu,QWidget* receive,const QString&showText="显示窗口", const QString& hideText = "隐藏窗口");
	//菜单添加关闭窗口
	static QAction* menuAddCloseWindow(QMenu* menu, QWidget* receive, const QString& text = "关闭窗口");
	//菜单添加退出窗口
	static QAction* menuAddQuit(QMenu* menu, const QString& text = "退出程序");
protected://隐藏的函数
	//初始化
	virtual void init();
protected://事件
	/*void showEvent(QShowEvent* event)override;*/
protected://变量
	
};
#endif
