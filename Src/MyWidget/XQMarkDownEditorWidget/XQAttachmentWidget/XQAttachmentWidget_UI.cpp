﻿#include"XQAttachmentWidget.h"
#include"XQAnimationButton.h"
#include"XQFileTableWidget.h"
#include<QStackedLayout>
#include<QSplitter>
#include<QBoxLayout>
#include<QTableWidget>
#include<QHeaderView>
#include<QLabel>
void XQAttachmentWidget::uiInit()
{
	setStyleSheet(QString("background: qlineargradient(x1:0, y1:0, x2:1, y2:1, m_stop:0 #ffffff, m_stop:0.4 #00FFC8, m_stop:0.7 #00C8C8, m_stop:1 #00FFFF);"));//初始化背景渐变色
	setWindowTitle("文件管理");//设置标题

	
	m_StackedLayout = new QStackedLayout();//堆栈布局
	m_StackedLayout->addWidget(imageWidgetInit());
	m_StackedLayout->addWidget(fileWidgetInit());
	m_ImageDisplay = new QLabel("图片", this);

    auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	LeftLayout->addLayout(leftButtonsInit());
	LeftLayout->addLayout(m_StackedLayout);

	auto TopLayout = new QBoxLayout(QBoxLayout::Direction::TopToBottom,this);//从上到下
	TopLayout->addLayout(topButtonsInit());
	TopLayout->addLayout(LeftLayout);
	
	setLayout(TopLayout);
}
QBoxLayout* XQAttachmentWidget::leftButtonsInit()
{
	QSize size = QSize(50, 50);
	auto Layout = new QBoxLayout(QBoxLayout::Direction::TopToBottom/*,this*/);//从上到下
	//图片按钮
	auto imageBtn = new XQAnimationButton(":/sidebar/image/sidebar/imageOne.png",":/sidebar/image/sidebar/imageTwo.png",this);
	imageBtn->setFixedSize(size);
	imageBtn->setBubbleText("图片");
	m_ImageDisplay->setPixmap(imageBtn->getPixmap());//设置默认显示的图片
	connect(imageBtn, &QPushButton::pressed, [=] {toggleWidget(0, imageBtn->getPixmap());});

	//文件按钮
	auto fileBtn = new XQAnimationButton(":/sidebar/image/sidebar/fileOne.png", ":/sidebar/image/sidebar/fileTwo.png",this);
	fileBtn->setFixedSize(size);
	fileBtn->setBubbleText("文件");
	connect(fileBtn, &QPushButton::pressed, [=] {toggleWidget(1, fileBtn->getPixmap()); });

	Layout->addWidget(imageBtn);
	Layout->addWidget(fileBtn);
	return Layout;
}
QBoxLayout* XQAttachmentWidget::topButtonsInit()
{
	QSize size = QSize(50, 50);
	auto leftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右

	m_addFileBtn = new XQAnimationButton(":/sidebar/image/sidebar/addFileOne.png", ":/sidebar/image/sidebar/addFileTwo.png", this);
	m_addFileBtn->setFixedSize(size);
	m_addFileBtn->setBubbleText("添加文件");
	leftLayout->addWidget(m_addFileBtn);
	
	//connect(imageBtn, &QPushButton::pressed, [this] {m_StackedLayout->setCurrentIndex(0); });

	m_deleteFileBtn= new XQAnimationButton(":/sidebar/image/sidebar/removeFileOne.png", ":/sidebar/image/sidebar/removeFileTwo.png", this);
	m_deleteFileBtn->setFixedSize(size);
	m_deleteFileBtn->setBubbleText("删除选中文件");
	leftLayout->addWidget(m_deleteFileBtn);

	leftLayout->setSpacing(size.height());
	leftLayout->addStretch(1);

	m_info = new QLabel("信息展示", this);
	m_info->setFont(QFont("微软雅黑", 20));
	m_info->setWindowFlags(Qt::FramelessWindowHint);  // 将窗口去除边框
	m_info->setAttribute(Qt::WA_TranslucentBackground);  // 设置背景透明
	m_info->setWindowOpacity(1);  // 设置透明度
	leftLayout->addWidget(m_info);

	
	m_ImageDisplay->setWindowFlags(Qt::FramelessWindowHint);  // 将窗口去除边框
	m_ImageDisplay->setAttribute(Qt::WA_TranslucentBackground);  // 设置背景透明
	m_ImageDisplay->setWindowOpacity(1);  // 设置透明度
	m_ImageDisplay->setFixedSize(size);
	m_ImageDisplay-> setScaledContents(true);  // 缩放到完整显示
	m_ImageDisplay->setAlignment(Qt::AlignRight);
	leftLayout->addWidget(m_ImageDisplay);

	return leftLayout;
}
QWidget* XQAttachmentWidget::imageWidgetInit()
{
	m_imageTable = new XQFileTableWidget(this);
	m_imageTable->setHorizontalHeading({ "预览","名字", "大小", "本地","云端" });
	m_imageTable->setNameFilters(IMAGENAMEFILTERS);

	return m_imageTable;
}
QWidget* XQAttachmentWidget::fileWidgetInit()
{
	m_fileTable = new XQFileTableWidget(this);
	m_fileTable->setHorizontalHeading({ "预览","名字", "大小", "本地","云端" });
	m_fileTable->setNameFilters({"*"});
	////列宽自动调整的方法如下
	//m_fileTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	//// 去掉水平和垂直边框线
	//m_fileTable->setStyleSheet("QTableView::verticalHeader {border: none;}"
	//	"QTableView::horizontalHeader {border: none;}"
	//	"QTableView::item:selected {border: none;}");
	//m_fileTable->setColumnCount(4); // 设置列数为4
	////设置标题
	//m_fileTable->setHorizontalHeaderLabels({ "名字", "大小", "状态","进度" });
	//m_fileTable->insertRow(1);
	return m_fileTable;
}