﻿#include "XQAttachmentWidget.h"
#include "XQFileTableWidget.h"
#include<QCloseEvent>
#include<QDragEnterEvent>
#include<QMimeData>
#include<QApplication>
#include<QStackedLayout>
static QRect getScreenRect(QWidget* widget)
{
    QPoint globalPos = widget->mapToGlobal(QPoint(0, 0));  // 获取控件左上角在屏幕上的坐标
    int width = widget->width();  // 获取控件宽度
    int height = widget->height();  // 获取控件高度
    return QRect(globalPos, QSize(width, height));  // 构造矩形并返回
}

void XQAttachmentWidget::showEvent(QShowEvent* event)
{
    updateInfo((XQFileTableWidget*)(m_StackedLayout->currentWidget()));//窗口显示时更新信息
}

void XQAttachmentWidget::closeEvent(QCloseEvent* event)
{
	event->ignore();
	setVisible(false);
}

void XQAttachmentWidget::dragEnterEvent(QDragEnterEvent* ev)
{
    if (ev->mimeData()->hasUrls()) {
        ev->acceptProposedAction();
       // qInfo() << "主窗口有文件进入控件";
    }
}

void XQAttachmentWidget::dropEvent(QDropEvent* ev)
{
    auto rect = getScreenRect(m_StackedLayout->currentWidget());
    if (rect.contains(mapToGlobal(ev->pos())))//表格控件
    {
       // 获取拖拽的文件名列表
       QList<QUrl> urls = ev->mimeData()->urls();
       QList<QString> files;
       for (int i = 0; i < urls.count(); i++)
       {
           files.append(urls.at(i).toLocalFile());
       }
      // qInfo() << files;
       ((XQFileTableWidget*)m_StackedLayout->currentWidget())->addFile(files);
    }
}
