﻿#include"XQAttachmentWidget.h"
#include"XQFileTableWidget.h"
#include"XQAlgorithm.h"
#include"XQMarkDownEditorDataInfo.h"
#include<QFileSystemWatcher>
#include<QStackedLayout>
#include<QLabel>
XQAttachmentWidget::XQAttachmentWidget(QWidget* parent, bool AutoUi)
	:QWidget(parent)
{
	if(AutoUi)
		init();
}
void XQAttachmentWidget::setFilePath(const QString& dir)
{
	m_fileTable->setDirPath(dir);
}
void XQAttachmentWidget::setImageDir(const QString& dir)
{
	m_imageTable->setDirPath(dir);
}

void XQAttachmentWidget::toggleWidget(int nSel, const QPixmap& pixmap)
{
	m_StackedLayout->setCurrentIndex(nSel); //设置堆栈显示的小部件
	m_ImageDisplay->setPixmap(pixmap); //设置要显示的图标
	updateInfo((XQFileTableWidget*)m_StackedLayout->currentWidget());//更新统计信息
}
void XQAttachmentWidget::updateInfo(XQFileTableWidget* table)
{
	auto currentTable = (XQFileTableWidget*)m_StackedLayout->currentWidget();
	if (currentTable != table)
		return;//当前的表不是正在显示的表不用更新
	QString Text = QString("文件:%1个 合计大小:%2").arg(table->rowCount()).arg(readableFileSize(table->tableFileSizeSum()));
	m_info->setText(Text);
}
