﻿#include "XQMarkDownEditorDataInfo.h"
#include"XMD5.h"
#include <fstream>
XQMarkDownEditorDataInfo::XQMarkDownEditorDataInfo(QObject* parent)
	:QObject(parent)
{
}
XQMarkDownEditorDataInfo::XQMarkDownEditorDataInfo(editModel model, QObject* parent)
	:QObject(parent)
	,m_model(model)
{
}

XQMarkDownEditorDataInfo::~XQMarkDownEditorDataInfo()
{
}

editModel XQMarkDownEditorDataInfo::model()const
{
	return m_model;
}

editWidget XQMarkDownEditorDataInfo::editType()const
{
	return m_editType;
}

QString XQMarkDownEditorDataInfo::typoraExePath() const
{
	return m_typoraExePath;
}

QString XQMarkDownEditorDataInfo::dirPath() const
{
	return std::move(m_path.path());
}

QString XQMarkDownEditorDataInfo::mdFilePath() const
{
	return m_mdFilePath;
}

QString XQMarkDownEditorDataInfo::imageDir() const
{
	return std::move(m_ImagePath.path());
}

QString XQMarkDownEditorDataInfo::filePath() const
{
	return std::move(m_filePath.path());
}

bool XQMarkDownEditorDataInfo::isMdFileModify() const
{
	return m_mdFileMD5 != mdFileOfMD5();
}

void XQMarkDownEditorDataInfo::setModel(editModel model)
{
	m_model= model;
	emit modelChanged(m_model);
}
void XQMarkDownEditorDataInfo::setEditType(editWidget type)
{
	m_editType = type;
	emit editTypeChanged(m_editType);
}
void XQMarkDownEditorDataInfo::setTyporaExePath(const QString& path)
{
	if (QFile(path).exists())//文件存在的情况下
		m_typoraExePath = path;
	emit typoraExePathChanged(m_typoraExePath);
}

void XQMarkDownEditorDataInfo::setDirPath(const QString& path)
{
	m_path = path;
	emit DirChanged(path);
}

void XQMarkDownEditorDataInfo::setMdFilePathName(const QString& name)
{
	QString Path = dirPath() + "/" + name;
	//if (QFile(Path).exists())//文件存在的情况下
		m_mdFilePath = Path;
	emit mdFileChanged(m_mdFilePath);
}

void XQMarkDownEditorDataInfo::setMdFilePath(const QString& path)
{
	m_mdFilePath = path;
	emit mdFileChanged(m_mdFilePath);
}

void XQMarkDownEditorDataInfo::setImageDirName(const QString& name)
{
	m_ImagePath = dirPath() + "/" + name;
	emit ImageDirChanged(imageDir());
}

void XQMarkDownEditorDataInfo::setImageDirPath(const QString& path)
{
	m_ImagePath = path;
	emit ImageDirChanged(imageDir());
}

void XQMarkDownEditorDataInfo::setFileDirName(const QString& name)
{
	m_filePath = dirPath() + "/" + name;
	emit fileDirChanged(filePath());
}

void XQMarkDownEditorDataInfo::setFileDirPath(const QString& path)
{
	m_filePath = path;
	emit fileDirChanged(filePath());
}

QString XQMarkDownEditorDataInfo::mdFileOfMD5()const
{
	return XMD5::file(m_mdFilePath.toLocal8Bit().toStdString()).c_str();
}

void XQMarkDownEditorDataInfo::upDataMd5()
{
	m_mdFileMD5 = mdFileOfMD5();//更新md5值
}
