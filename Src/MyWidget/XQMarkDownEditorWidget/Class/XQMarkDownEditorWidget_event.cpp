﻿#include"XQMarkDownEditorWidget.h"
#include"XQAttachmentWidget.h"
#include<QMessageBox>
#include<QCloseEvent>
void XQMarkDownEditorWidget::resizeEvent(QResizeEvent* event)
{
}

void XQMarkDownEditorWidget::moveEvent(QMoveEvent* event)
{
}

void XQMarkDownEditorWidget::closeEvent(QCloseEvent* event)
{
	if (!(m_editData->model() == editModel::Read) && m_editData->isMdFileModify())
	{
		auto ret = QMessageBox::information(this, "提醒", "监测到文本被修改未保存\n确认关闭吗？？？", QMessageBox::No | QMessageBox::Yes);
		if (ret == QMessageBox::No)//不退出
		{
			event->ignore();//不执行事件
			return;
		}
	}
}

void XQMarkDownEditorWidget::changeEvent(QEvent* event)
{
}

void XQMarkDownEditorWidget::showEvent(QShowEvent* event)
{
}

void XQMarkDownEditorWidget::keyPressEvent(QKeyEvent* event)
{
}