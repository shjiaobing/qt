﻿#include"XQSettingsWidget.h"
#include"XQSurfacePlotWidget.h"
#include"XQLabelWidget.h"
#include<QLineEdit>
#include<QTextEdit>
#include<QDebug>
XQSettingsWidget::XQSettingsWidget(QWidget* parent)
	:QWidget(parent)
{
	init();
}

XQSettingsWidget::~XQSettingsWidget()
{

}
QString XQSettingsWidget::title() const
{
	return m_title->text();
}
QPixmap XQSettingsWidget::image() const
{
	return m_surfacePlot->image();
}
QString XQSettingsWidget::imageDir()
{
	return m_imageDir;
}
QString XQSettingsWidget::abstract() const
{
	return m_abstract->toPlainText();
}
QStringList XQSettingsWidget::labelList()
{
	return std::move(m_labelWidget->labelList());
}
QStringList& XQSettingsWidget::labelListData()
{
	return m_labelWidget->labelListData();
}
void XQSettingsWidget::setTitle(const QString& text)
{
	m_title->setText(text);
}
void XQSettingsWidget::setImage(const QPixmap& pixmap)
{
	m_surfacePlot->setImage(pixmap);
}
void XQSettingsWidget::setAbstract(const QString& text)
{
	m_abstract->setText(text);
}
void XQSettingsWidget::setLabelList(const QStringList& list)
{
	m_labelWidget->setLabelList(list);
}
void XQSettingsWidget::setLabelListData(const QStringList& list)
{
	m_labelWidget->setLabelListData(list);
}
void XQSettingsWidget::setImageDir(const QString& path)
{
	m_imageDir = path;
	if (m_surfacePlot != nullptr)
		m_surfacePlot->setImageDir(m_imageDir);
}