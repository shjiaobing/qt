﻿#include"XQSurfacePlotWidget.h"
#include"XQAnimationButton.h"
#include"XQFileSelectionWidget.h"
#include"XQGraphicsViewWidget.h"
#include<QDebug>
#include<QMenu>
#include<QContextMenuEvent>
#include<QFileDialog>
#include<QFileInfo>
XQSurfacePlotWidget::XQSurfacePlotWidget(QWidget* parent)
{
	init();
}

XQSurfacePlotWidget::~XQSurfacePlotWidget()
{
}
QString XQSurfacePlotWidget::imageDir()
{
	return m_imageDir;
}
QString XQSurfacePlotWidget::imagePath()
{
	return m_imageFile;
}
QPixmap XQSurfacePlotWidget::image()
{
	if (imageNull)
		return QPixmap();
	return m_ImageBtn->getPixmap(mouseState::mouseLeave);
}
void XQSurfacePlotWidget::setImageDir(const QString& path)
{
	m_imageDir = path;
	if (m_fileList != nullptr)
		m_fileList->setDirPath(m_imageDir);
}
void XQSurfacePlotWidget::setImage(const QString& image)
{
	if (image.isEmpty()||!QFileInfo(image).exists())//文件不存在返回
		return;
	m_imageFile = image;
	m_ImageBtn->clearPixmap();
	m_ImageBtn->move(0, 0);
	m_ImageBtn->resize(this->size());
	m_ImageBtn->setMouseLeavePixmap(image);
	imageNull = false;
	if(m_fileList!=nullptr)
		m_fileList->deleteLater();
	emit selectFile(m_imageFile);
}
void XQSurfacePlotWidget::setImage(const QPixmap& image)
{
	if (image.isNull())
		return;
	m_ImageBtn->clearPixmap();
	m_ImageBtn->move(0, 0);
	m_ImageBtn->resize(this->size());
	m_ImageBtn->setMouseLeavePixmap(image);
	imageNull = false;
}
void XQSurfacePlotWidget::openList()
{
	if(!imageDir().isEmpty())
	{
		if (m_fileList == nullptr)
		{
			m_fileList = new XQFileSelectionWidget();
			m_fileList->setActivityAutoClose(true);
			connect(m_fileList, &XQFileSelectionWidget::destroyed, [=] {m_fileList = nullptr; });//关闭时自动释放
			connect(m_fileList, &XQFileSelectionWidget::selectFile, this, QOverload<const QString&>::of(&XQSurfacePlotWidget::setImage));//选择图片后设置图片
		}
		m_fileList->move(((QWidget*)parent())->mapToGlobal(QPoint(50, 50)));
		m_fileList->resize(((QWidget*)parent())->size() - QSize(100, 100));
		m_fileList->setDirPath(m_imageDir);//设置图片目录
		if (parentWidget()->windowFlags() & Qt::WindowStaysOnTopHint)
			m_fileList->setWindowFlag(Qt::WindowStaysOnTopHint);
		m_fileList->show();
		m_fileList->activateWindow();
	}
	else
	{
		//获取数据
		QString options = QString("Image Files(%1)").arg(QStringList(IMAGENAMEFILTERS).join(" "));//图片文件过滤器
		auto filename = QFileDialog::getOpenFileName(this, "打开文件", "", options);
		if (filename.isEmpty())
			return;
		setImage(filename);
	}
	
}
void XQSurfacePlotWidget::contextMenu()
{
	if(menu==nullptr)
	{
		menu = new QMenu(this);
		menu->setFont(QFont("黑体", 15));
	}
	menu->clear();

	QString options = QString("Image Files(%1)").arg(QStringList(IMAGENAMEFILTERS).join(" "));//图片文件过滤器
	menu->addAction("上传文件", [=] {
		auto file = QFileDialog::getOpenFileName(this, "选择图片", QString(), options);
	if (!file.isEmpty())
		setImage(file);
		//	QFile::copy(file, m_imageDir + "/" + QFileInfo(file).fileName());//拷贝到缓存目录
	 });
	if (!imageNull)
	{
		menu->addAction("预览图片", [=] {
		auto imageWidget = new XQGraphicsViewWidget();
		imageWidget->setAttribute(Qt::WA_DeleteOnClose); //关闭时自动删除实例，防止内存泄漏
		//imageWidget->setAutoDelete(true);
		imageWidget->setPixmap(this->image());
		imageWidget->setWindowIcon(this->image());
		imageWidget->show();});
		menu->addSeparator();
		menu->addAction("选择图片", this, &XQSurfacePlotWidget::openList);
		menu->addAction("清空图片", [=] {m_imageFile.clear(); init_addPixmap(); imageNull = true; });
	}
	menu->popup(QCursor::pos());
}
