﻿#include"XQLabelWidget.h"
#include"XQBubbleWidget.h"
#include"XQListSearchWidget.h"
#include<QPushButton>
#include<QTableWidget>
#include<QDebug>
XQLabelWidget::XQLabelWidget(QWidget* parent)
	:QWidget(parent),
	m_parent(parent)
{
	init();
}

XQLabelWidget::~XQLabelWidget()
{
	if (m_Bubble != nullptr)
		m_Bubble->deleteLater();
	if (m_ListSearch != nullptr)
		m_ListSearch->deleteLater();
}
void XQLabelWidget::setRowCount(size_t row)
{
	m_row = row;
	updataTable();
}
void XQLabelWidget::setColumnCount(size_t column)
{
	m_column = column;
	updataTable();
}

void XQLabelWidget::removeButton(QPushButton* btn)
{
	qint32 nSel=m_btns.indexOf(btn);
	m_btns.remove(nSel);
	btn->deleteLater();
	updataTable();
}
void XQLabelWidget::updataTable(bool copy)
{
	//先全部拷贝一遍按钮//没法解决setCellWidget自动释放的问题
	if(copy)
	{
		for (auto& btn : m_btns)
		{
			auto temp = btn;
			btn = init_btn(temp->text());
			temp -> deleteLater();
		}
	}
	m_addBtn = init_addBtn();

	size_t count= m_btns.count();//数量
	size_t nSel = 0;//当前索引
	for (size_t row = 0; row < m_row; row++)
	{
		for (size_t column = 0; column < m_column; column++)
		{
			if (nSel < count)
			{
				m_Layout->setCellWidget(row, column, m_btns[nSel]);
				++nSel;
			}
			else if (nSel == count)
			{
				m_Layout->setCellWidget(row, column, m_addBtn);
				++nSel;
				
			}
			else
			{
				m_Layout->setCellWidget(row, column, nullptr);
			}
		}
	}
}

void XQLabelWidget::setLabelList(const QStringList& list)
{
	m_btns.clear();
	for (auto&Text: list)
	{
		auto btn = init_btn(Text);
		m_btns.push_back(btn);
	}
	updataTable(false);
}

void XQLabelWidget::setLabelListData(const QStringList& list)
{
	m_ListSearch->setDatabase(list);
}

void XQLabelWidget::addButton()
{
	//窗口设置文本
	/*if(m_ListSearch==nullptr)
	{
	
	}*/
	
	m_ListSearch->move(m_parent->mapToGlobal(QPoint(50, 50)));
	m_ListSearch->resize(m_parent->size() - QSize(100, 100));
	m_ListSearch->show();
	m_ListSearch->activateWindow();
	m_ListSearch->setSelectedData(labelList());
	//qDebug() << labelList();
}

QStringList XQLabelWidget::labelList()
{
	QStringList list;
	for (auto& btn: m_btns)
	{
		list << btn->text();
	}
	return std::move(list);
}

QStringList& XQLabelWidget::labelListData()
{
	return m_ListSearch->database();
}
