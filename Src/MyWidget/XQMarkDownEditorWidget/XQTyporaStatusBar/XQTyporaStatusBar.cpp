﻿#include"XQTyporaStatusBar.h"
#include"XQBubbleWidget.h"
#include"XQAnimationButton.h"
#include<QHBoxLayout>
#include<QProgressBar>
#include<QTimer>
XQTyporaStatusBar::XQTyporaStatusBar(QWidget* parent)
	:QWidget(parent)
{
	init();
}
XQTyporaStatusBar::~XQTyporaStatusBar()
{
	if (m_saveButton != nullptr)
		 m_saveButton->deleteLater();
	if (m_DownloadImageButton != nullptr)
		m_DownloadImageButton->deleteLater();
	if (m_openImageDirButton != nullptr)
		 m_openImageDirButton->deleteLater();
	if (m_info != nullptr)
		m_info->deleteLater();
	if (m_Bubble != nullptr)
		m_Bubble->deleteLater();
}

void XQTyporaStatusBar::showInfo(const QString& info, const size_t delay)
{
	m_info->setText(info);
	if(delay!=0)
		QTimer::singleShot(delay,this,&XQTyporaStatusBar::clearInfoText);
}

void XQTyporaStatusBar::setProgress(const qint16 val, const size_t delay)
{
	m_ProgressBar->setValue(val);
	if(val!=100)
	{
		m_ProgressBar->setVisible(true);
	}
	else if (delay != 0)
	{
		QTimer::singleShot(delay, [this]() {m_ProgressBar->setVisible(false); });
	}
}

void XQTyporaStatusBar::clearInfoText()
{
	if (m_info != nullptr)m_info->setText("");
}
