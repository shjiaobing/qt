﻿#ifndef XQTYPORASTATUSBAR_H
#define XQTYPORASTATUSBAR_H
#include<QLabel>
class XQAnimationButton;
class XQBubbleWidget;
class QProgressBar;
class QBoxLayout;
class QPushButton;
//编辑框状态栏
class XQTyporaStatusBar :public QWidget
{
	Q_OBJECT
public:
	XQTyporaStatusBar(QWidget* parent = nullptr);
	~XQTyporaStatusBar();
public slots:
	//显示信息,显示时间毫秒
	void showInfo(const QString& info, const size_t delay=0);
	//设置进度,到达100时延迟隐藏毫秒
	void setProgress(const qint16 val,const size_t delay);
	//清空信息提示框
	void clearInfoText();
signals://信号
	//保存上传数据
	void saveData(QPushButton* btn);
	//下载图片
	void DownloadImage(QPushButton* btn);
	//打开图片目录信号
	void openImageDir(QPushButton* btn);
	//打开设置窗口
	void openSettings(QPushButton* btn);
	//打开附件窗口
	void openAttachment(QPushButton* btn);
protected:
	void init();
	void init_saveButton();
	void init_DownloadImageButton();
	void init_openImageDirButton();
	void init_settings();
	void init_openAttachment();
private:
	XQAnimationButton*	 m_saveButton = nullptr;//保存按钮
	XQAnimationButton* m_DownloadImageButton = nullptr;//下载图片按钮
	XQAnimationButton* m_openImageDirButton = nullptr;//打开图片目录按钮
	XQAnimationButton* m_settings = nullptr;//打开设置按钮
	XQAnimationButton* m_openAttachment = nullptr;//打开附件窗口
	QLabel*			 m_info = nullptr;//信息
	QProgressBar* m_ProgressBar = nullptr;//进度条
	XQBubbleWidget* m_Bubble=nullptr;//悬浮气泡提示文字
};

#endif // !EDITSTATUSBAR_H