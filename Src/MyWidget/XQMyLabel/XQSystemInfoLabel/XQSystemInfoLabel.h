﻿#ifndef XQSYSTEMINFOLABEL_H
#define XQSYSTEMINFOLABEL_H
#include"XQHead.h"
#include<QLabel>
#include<QDateTime>
class XQSystemInfoLabel :public QLabel
{
	//Q_OBJECT
public:
	XQSystemInfoLabel(QWidget* parent = nullptr);
	~XQSystemInfoLabel();
	QString runTime()const;
	void showAll();
protected://初始化
	void init();
	void showText();
protected://事件
	//表格弹出菜单
	virtual void contextMenuRequested();
protected:
	static QDateTime m_startTime;
	QTimer* m_timer = nullptr;
	bool m_cpu=true;
	bool m_memory = true;
	bool m_runTime = true;
};
#endif // !XQSystemInfoLabel_H
