﻿#include"XQCircleLabel.h"
#include"DrawMulticoloredCircle.h"
#include<QPaintEvent>
XQCircleLabel::XQCircleLabel(QWidget* parent)
	:QLabel(parent)
{
}
XQCircleLabel::XQCircleLabel(const QString& text, QWidget* parent)
	:QLabel(text,parent)
{
	init();
}

XQCircleLabel::~XQCircleLabel()
{

}

void XQCircleLabel::setLineWidth(int width)
{
	DrawMulticoloredCircle::setWidth(width);
}

void XQCircleLabel::setCircleHz(int Hz)
{
	DrawMulticoloredCircle::setHz(Hz);
}

void XQCircleLabel::updateColor()
{
	DrawMulticoloredCircle::updateColor();
	QWidget::update();
}

void XQCircleLabel::init()
{
	int w = width(), h = height();
	int r = qMin(w, h) / 2;
	Initia(QPoint(w / 2, h / 2), r,3);
	m_Timer.callOnTimeout([this]() {this->QWidget::update(); });
	setHz(10);
	setAlignment(Qt::AlignCenter);
	//setText("频率");
}

void XQCircleLabel::paintEvent(QPaintEvent* event)
{
	QPainter painter(this);
	draw(&painter);
	QLabel::paintEvent(event);
}

void XQCircleLabel::resizeEvent(QResizeEvent* event)
{
	int LineWidth= DrawMulticoloredCircle::getWidth();
	int w = width(),h=height();
	int r = qMin(w,h)/2- LineWidth;
	reSize(QPoint(w / 2, h / 2), r);
}
