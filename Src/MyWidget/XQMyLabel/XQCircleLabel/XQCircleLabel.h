﻿#ifndef XQCIRCLELABEL_H
#define XQCIRCLELABEL_H
//class DrawMulticoloredCircle;
#include"DrawMulticoloredCircle.h"
#include<QLabel>
//彩虹标签
class XQCircleLabel:public QLabel,protected DrawMulticoloredCircle
{
	Q_OBJECT
public:
	XQCircleLabel(QWidget* parent = nullptr);
	XQCircleLabel(const QString& text, QWidget* parent = nullptr);
	~XQCircleLabel();
public:
	//设置线宽
	void setLineWidth(int width);
	//设置频率
	void setCircleHz(int Hz);
	//更新颜色
	void updateColor();
protected://隐藏的函数
	void init();
protected://事件
	void paintEvent(QPaintEvent* event);
	void resizeEvent(QResizeEvent* event);
protected://
	DrawMulticoloredCircle* m_Circle = nullptr;
};
#endif // !XQCircleLabel_H
