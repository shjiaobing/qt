﻿#ifndef BubbleWidget_H
#define BubbleWidget_H
//#include <QWidget>
#include <QGraphicsDropShadowEffect>
#include <QLabel>
#include<QPen>
#include<QBrush>
const int SHADOW_WIDTH = 30;                 // 窗口阴影宽度;
const int TRIANGLE_WIDTH = 30;               // 小三角的宽度;
const int TRIANGLE_HEIGHT = 10;              // 小三角的高度;
const int BORDER_RADIUS = 15;                 // 窗口边角的弧度;
enum class Derection {
    left,
    right,
    up,
    down
};
//悬浮气泡提示框
class XQBubbleWidget : public QLabel
{
    Q_OBJECT
public:
   
    XQBubbleWidget(QWidget* parent = 0);
    XQBubbleWidget(QWidget* widget, QString* pStr);
    XQBubbleWidget(QWidget* widget, std::function<QString()>func);
    XQBubbleWidget(const QPoint&pos,const QString& Text,QWidget* parent = 0);
    ~XQBubbleWidget();
   //设置边距
    void setBackGauge(size_t DIST);
   //获取边距
    const size_t backGauge();
     //设置展示文本
    void setText(const QPoint& pos, const QString& Text);
    //显示文本
    void showText(const QPoint& pos, const QString& Text, Derection derect= Derection::up,int msec=-1);
    //设置画笔
    void setPen(const QPen& pen);
    //获取画笔
    QPen pen();
    //设置画刷
    void setBrush(const QBrush& brush);
    //获取画刷
    QBrush brush();
    //绑定在一个widget上
    void bind(QWidget* widget,QString* pStr);
    void bind(QWidget* widget,std::function<QString()>func);
protected:
    void init();
    void setSize();
    void paintEvent(QPaintEvent*);
private:
    size_t m_BackGauge=4;//边距
    QPen m_pen;//画笔
    QBrush m_brush;//画刷
    QWidget* m_bindWidget = nullptr;
    //QString* m_bindStr = nullptr;
    QObject* m_bindFilter_enter = nullptr;
    QObject* m_bindFilter_Leave = nullptr;
};
#endif // !BubbleWidget_H
