﻿#include"XQBubbleWidget.h"
#include"XQMouseEventFilter_Enter.hpp"
#include"XQMouseEventFilter_Leave.hpp"
#include<QEvent>
#include<QWidget>
#include<QHBoxLayout>
#include<QPoint>
#include<QPainter>
#include<QPainterPath>
#include<QImage>
#include<QVariant>
#include<QTimer>
//获取字符串的宽度
int getContentWidth(const QFont& font,const QString& str)
{
    QFontMetrics drugNameFmContent(font);
    QRect recContent_drugName = drugNameFmContent.boundingRect(str);
    int nDrugNameWidth = recContent_drugName.width();
    return nDrugNameWidth;
}

//获取字符串的高度
int getContentHeight(const QFont& font, const QString& str)
{
    QFontMetrics drugNameFmContent(font);
    QRect recContent_drugName = drugNameFmContent.boundingRect(str);
    int nDrugNametHeight = recContent_drugName.height();
    return nDrugNametHeight;
}

XQBubbleWidget::XQBubbleWidget(QWidget* parent) 
    :QLabel(parent)
{
    init();  
    setText(QPoint(0, 0), "init");
    setSize();
    hide();
}

XQBubbleWidget::XQBubbleWidget(QWidget* widget, QString* pStr)
    :XQBubbleWidget()
{
    bind(widget,pStr);
    hide();
}

XQBubbleWidget::XQBubbleWidget(QWidget* widget, std::function<QString()> func)
    :XQBubbleWidget()
{
    bind(widget, func);
    hide();
}

XQBubbleWidget::XQBubbleWidget(const QPoint& pos,const QString& Text, QWidget* parent)
    :QLabel(Text,parent)
{
    init();
    setText(pos, Text);
    setSize();
    hide();
}

XQBubbleWidget::~XQBubbleWidget()
{
    //qInfo() << "释放";
}

void XQBubbleWidget::setBackGauge(size_t DIST)
{
    m_BackGauge = DIST;
    update();
}

const size_t XQBubbleWidget::backGauge()
{
    return m_BackGauge;
}

void XQBubbleWidget::setText(const QPoint& pos, const QString& Text)
{
    QLabel::setText(Text);
    setSize();
    move(pos.x(), pos.y());
}
void XQBubbleWidget::showText(const QPoint& pos, const QString& Text, Derection derect, int msec)
{
    if (Text.isEmpty())
        return;
    if (msec > 0)
    {//延迟隐藏
        QTimer::singleShot(msec, this, &QWidget::hide);
    }
    QLabel::setText(Text);
    setSize();
    QPoint p;
    if (derect == Derection::up)
    {
        p = pos+ QPoint(0, -height());
    }
    else  if (derect == Derection::down)
    {
        p = pos;
    }
    auto parent = parentWidget();
    if (parent)
    {
        if(parent->width()<pos.x()+width())
            p= pos + QPoint(-width(), 0);//超出右边界向左偏移
    }
    move(p);
    show();
}
void XQBubbleWidget::setPen(const QPen& pen)
{
    m_pen = pen;
}
 QPen XQBubbleWidget::pen()
{
    return m_pen;
}
 void XQBubbleWidget::setBrush(const QBrush& brush)
 {
     m_brush = brush;
 }
 QBrush XQBubbleWidget::brush()
 {
     return m_brush;
 }
 void XQBubbleWidget::bind(QWidget* widget, QString* pStr)
 {
     bind(widget, [=] {return *pStr; });
 }
 void XQBubbleWidget::bind(QWidget* widget, std::function<QString()> func)
 {
     if (widget == nullptr || func == nullptr)
         return;
     if (m_bindWidget != nullptr)
     {
         if (m_bindFilter_enter)
         {
             m_bindWidget->removeEventFilter(m_bindFilter_enter);
             m_bindFilter_enter->deleteLater();
         }
         if (m_bindFilter_Leave)
         {
             m_bindWidget->removeEventFilter(m_bindFilter_Leave);
             m_bindFilter_Leave->deleteLater();
         }
         disconnect(m_bindWidget, &QWidget::destroyed, this, &QWidget::deleteLater);
     }
     connect(widget, &QWidget::destroyed, this, &QWidget::deleteLater);
     m_bindFilter_enter = new XQMouseEventFilter_Enter([=] {
         auto pos = m_bindWidget->mapToGlobal(QPoint(0, 0));
         showText(pos, func());
         }, m_bindWidget);
     m_bindFilter_Leave = new XQMouseEventFilter_Leave([this] {
         hide();
         }, m_bindWidget);
     widget->installEventFilter(m_bindFilter_enter);
     widget->installEventFilter(m_bindFilter_Leave);
     m_bindWidget = widget;
 }
void XQBubbleWidget::init()
{
    setWindowFlags(Qt::FramelessWindowHint| Qt::WindowStaysOnTopHint);//设置无窗口框架边界
    setAttribute(Qt::WA_TranslucentBackground);//设置背景透明
    setAlignment(Qt::AlignCenter);//设置居中
    setWordWrap(true);//设置自动换行
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
   /* setStyleSheet("QLabel { text-overflow: ellipsis; }");*/
    QFont font("黑体", 14);
    setFont(font);

    //画笔设置
    m_pen.setColor(Qt::lightGray);
    m_pen.setWidth(4);

    //画刷设置
    m_brush=QBrush(Qt::cyan);
}

void XQBubbleWidget::setSize()
{
    adjustSize();
    size_t PenWidth = m_pen.width();
    size_t add = (m_BackGauge + PenWidth*2) * 2;
    resize(width()+ add,height() + add);
}

void XQBubbleWidget::paintEvent(QPaintEvent*ev)
{
  //  setSize();
    size_t PenWidth = m_pen.width();
    QPainter painter(this);
    painter.setPen(m_pen);
    painter.setBrush(m_brush);
    //painter.setRenderHint(QPainter::Antialiasing, true);

    painter.drawRoundedRect(QRect(QPoint(PenWidth, PenWidth), size()-QSize(PenWidth*2, PenWidth*2)), 10, 10);
    QLabel::paintEvent(ev);
}


