﻿#ifndef XQSTATUSBARWIDGET_H
#define XQSTATUSBARWIDGET_H
#include<QStatusBar>
#include<QTimer>
#include<QDateTime>
#include"XQHead.h"
//自定义的状态栏
class XQStatusBarWidget:public QStatusBar
{
	Q_OBJECT
public:
	XQStatusBarWidget(QWidget* parent=nullptr);
	~XQStatusBarWidget();
	QProgressBar* progressBar()const;
	QWidget* logWidget()const;
	XQSystemInfoLabel* systemInfoLabel();
	int updataTime()const;
	bool isVisible_logButton()const;
public:
	void setUpdataTime(int msec);
public:
	//设置进度
	void setProgressValueSize(size_t val, size_t maxSize, size_t delay = 5000);
	void setProgressValueCount(size_t val, size_t maxSize, size_t delay = 5000);
	void setProgressValue(size_t val, size_t maxSize, const QString& format,size_t delay = 5000);
	//设置开启日志窗口
	void setOpenLogWidget(bool open,const QString& title=QString());
	void setOpenSystemInfoWidget(bool open);
	void setLogWidget(QWidget* widget);
	void setVisible_logButton(bool visible);
public:
	//设置菜单方法
	void setMenuFunc(std::function<void(QMenu*)>func);
	void addMenu_SystemInfoWidge(QMenu* menu);
	void addMenu_LogWidge(QMenu*menu);
	void addMenu_LogButton(QMenu* menu);
signals://信号
	void setProgressRange(int min, int max);
	void setProgressValue(int val);
protected://初始化
	void init();
	void init_ui();
	void init_ProgressBar();
protected://事件
	//表格弹出菜单
	virtual void contextMenuRequested();
protected://变量
	std::function<void(QMenu*)> m_menuFunc = nullptr;
	QPushButton* m_logBtn = nullptr;
	XQSystemInfoLabel* m_systemInfo=nullptr;
	QWidget* m_logWidget = nullptr;//日志小部件
	QProgressBar* m_progressBar = nullptr;//进度条
	QTimer m_progressDelay;//进度条的延迟定时器
	QDateTime m_time = QDateTime::currentDateTime();//记录的刷新时间
	int m_updataTime = 500;//刷新时间毫秒
};
#endif