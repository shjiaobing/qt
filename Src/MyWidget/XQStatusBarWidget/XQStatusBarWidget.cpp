﻿#include"XQStatusBarWidget.h"
#include"XQFuncEvent.h"
#include"XQAlgorithm.h"
#include"XQLogWidget.h"
#include"XQAppInfo.h"
#include"XQSystemInfoLabel.h"
#include<QProgressBar>
#include<QPushButton>
#include<QMenu>
XQStatusBarWidget::XQStatusBarWidget(QWidget* parent)
	:QStatusBar(parent)
{
	init();
}

XQStatusBarWidget::~XQStatusBarWidget()
{
}
QProgressBar* XQStatusBarWidget::progressBar() const
{
	return m_progressBar;
}
QWidget* XQStatusBarWidget::logWidget() const
{
	return m_logWidget;
}
XQSystemInfoLabel* XQStatusBarWidget::systemInfoLabel()
{
	return m_systemInfo;
}
int XQStatusBarWidget::updataTime() const
{
	return m_updataTime;
}
bool XQStatusBarWidget::isVisible_logButton() const
{
	if (m_logBtn)
		return m_logBtn->isVisible();
	return false;
}
void XQStatusBarWidget::setUpdataTime(int msec)
{
	if(msec>=0)
		m_updataTime = msec;
}
void XQStatusBarWidget::setProgressValueSize(size_t val, size_t maxSize, size_t delay)
{
	setProgressValue(val, maxSize, readableFileSize(val) + "/" + readableFileSize(maxSize) + "  %p%", delay);
}

void XQStatusBarWidget::setProgressValueCount(size_t val, size_t maxSize, size_t delay)
{
	setProgressValue(val, maxSize, QString("%1/%2  %p%").arg(val).arg(maxSize), delay);
}

void XQStatusBarWidget::setProgressValue(size_t val, size_t maxSize, const QString& format, size_t delay)
{
	QDateTime time = QDateTime::currentDateTime();
	if (val != maxSize&&m_time.msecsTo(time) < m_updataTime)
		return;
	m_time = time;
	new XQFuncEvent(this,[=] {
		m_progressBar->setRange(0, 100);
		m_progressBar->setValue(((double)val / (double)maxSize) * 100);
		m_progressBar->setFormat(format);
		if (m_progressBar->value() == m_progressBar->maximum())
		{
			disconnect(&m_progressDelay, &QTimer::timeout, nullptr, nullptr);
			m_progressDelay.callOnTimeout(m_progressBar, &QProgressBar::hide);
			if (delay != 0)
				m_progressDelay.start(delay);
		}
		else if (!m_progressBar->isVisible())
		{
			m_progressBar->show();
			m_progressDelay.stop();
		}
		else
		{
			m_progressDelay.stop();
		}
		
		});
}

void XQStatusBarWidget::setOpenLogWidget(bool open, const QString& title)
{
	if (open)
	{
		if(m_logBtn==nullptr)
		{
			m_logBtn = new QPushButton("日志",this);
			connect(m_logBtn, &QPushButton::pressed, [=]
				{
					if (m_logWidget->isVisible())
						m_logWidget->setVisible(false);
					else
						centerShow(m_logWidget, (QWidget*)XQAppInfo::mainWindow());
				});
			insertPermanentWidget(1,m_logBtn);
		}
		if (m_logWidget == nullptr)
		{
			m_logWidget = new XQLogWidget;
			connect(this, &QWidget::destroyed, m_logWidget, &QWidget::deleteLater);
			if (!title.isEmpty())
				m_logWidget->setWindowTitle(title);
		}
	}
	else
	{
		if (m_logWidget != nullptr)
		{
			m_logWidget->close();
			m_logWidget->deleteLater();
			m_logWidget = nullptr;
		}

		if (m_logBtn != nullptr&& m_logWidget == nullptr)
		{
			removeWidget(m_logBtn);
			m_logBtn->deleteLater();
			m_logBtn = nullptr;
		}
		
	}
}

void XQStatusBarWidget::setOpenSystemInfoWidget(bool open)
{
	if (open)
	{
		if (m_systemInfo==nullptr)
		{
			m_systemInfo = new XQSystemInfoLabel(this);
			addPermanentWidget(m_systemInfo);
		}
	}
	else if (m_systemInfo != nullptr)
	{
		removeWidget(m_systemInfo);
		m_systemInfo->deleteLater();
		m_systemInfo = nullptr;
	}
}

void XQStatusBarWidget::setLogWidget(QWidget* widget)
{
	if (widget == nullptr && m_logBtn != nullptr)
	{//删除日志按钮
		removeWidget(m_logBtn);
		m_logBtn->deleteLater();
		m_logBtn = nullptr;
	}
	if (m_logBtn == nullptr)
	{
		m_logBtn = new QPushButton("日志", this);
		connect(m_logBtn, &QPushButton::pressed, [=]
			{
				if (m_logWidget == nullptr)
					return;
				if (m_logWidget->isVisible())
					m_logWidget->setVisible(false);
				else if(m_logWidget->parentWidget())
					m_logWidget->setVisible(true);
				else
					centerShow(m_logWidget, (QWidget*)XQAppInfo::mainWindow());
			});
		insertPermanentWidget(1, m_logBtn);
	}
	if (m_logWidget == nullptr)
	{
		m_logWidget = widget;
		/*connect(this, &QWidget::destroyed, m_logWidget, &QWidget::deleteLater);*/
	}
}

void XQStatusBarWidget::setVisible_logButton(bool visible)
{
	if(m_logBtn)
	m_logBtn->setVisible(visible);
}

void XQStatusBarWidget::setMenuFunc(std::function<void(QMenu*)> func)
{
	m_menuFunc = func;
}

void XQStatusBarWidget::addMenu_SystemInfoWidge(QMenu* menu)
{
	//系统信息
	if (systemInfoLabel() && !systemInfoLabel()->isVisible())
		menu->addAction("打开系统信息", systemInfoLabel(), &QWidget::show);
	else if (systemInfoLabel() && systemInfoLabel()->isVisible())
		menu->addAction("隐藏系统信息", systemInfoLabel(), &QWidget::hide);
	else if(systemInfoLabel()==nullptr)
		menu->addAction("打开系统信息", [=] {setOpenSystemInfoWidget(true); });
}

void XQStatusBarWidget::addMenu_LogWidge(QMenu* menu)
{
	//日志窗口
	if (logWidget() && !logWidget()->isVisible())
		menu->addAction("打开日志", logWidget(), &QWidget::show);
	else if (logWidget() && logWidget()->isVisible())
		menu->addAction("隐藏日志", logWidget(), &QWidget::hide);
	
}

void XQStatusBarWidget::addMenu_LogButton(QMenu* menu)
{
	//日志按钮
	if (logWidget() && !m_logBtn->isVisible())
		menu->addAction("显示日志按钮", m_logBtn, &QWidget::show);
	else if (logWidget() && m_logBtn->isVisible())
		menu->addAction("隐藏日志按钮", m_logBtn, &QWidget::hide);
}

