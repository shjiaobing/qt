﻿#ifndef XQTIMESELECTDOCKWIDGET_H
#define XQTIMESELECTDOCKWIDGET_H
#include"XQHead.h"
#include"XQDockWidget.h"
//时间间隔
enum class TimeInterval
{
	null,//没选中
	oneWeek,//一周
	oneMonth,//一月
	threeMonth,//三月
	sixMonth,//六月
	oneYear,//一年
	threeYear,//三年
	fiveYear,//近五年
	thisYear,//今年来
	hitherto,//至今
	userDefined//用户自定义的
};
//时间选择浮动小部件
class XQTimeSelectDockWidget :public XQDockWidget
{
	Q_OBJECT
public:
	XQTimeSelectDockWidget(QWidget* parent = nullptr);
	XQTimeSelectDockWidget(const QString& title, QWidget* parent = nullptr);
	~XQTimeSelectDockWidget();
public://获取一些信息
	QDateTimeEdit* startDateTimeEdit()const;
	QDateTimeEdit* endDateTimeEdit()const;
	TimeInterval selectTimeType()const;
	QDateTime startDateTime()const;
	QDateTime endDateTime()const;
	//根据传入的时间根据选中的时间间隔类型往前推QDateTime
	QDateTime selectDateTime(QDateTime time);
public://设置 
	void timeButtonPressed(TimeInterval time);

	//设置时间
	void setStartTime(const QDateTime& startTime);
	void setEndTime(const QDateTime& endTime);
signals://信号
	void selectTime(TimeInterval time);
protected://初始化
	virtual void init();
	virtual void init_ui();
	virtual void init_timeGroup();
	virtual void init_userGroup();
protected://信号事件处理
	//事件按钮按下
	void timeButtonPressed(int id);
	//表格弹出菜单
	//virtual void contextMenuRequested();
	//停靠位置改变
	virtual void locationChanged(Qt::DockWidgetArea area);
protected:
	QBoxLayout* m_timeLayout = nullptr;//时间选择组
	QBoxLayout* m_userLayout = nullptr;//用户选择组
	QPushButton* m_selectTimeBtn = nullptr;//当前选中的时间按钮
	QButtonGroup* m_timeButtons = nullptr;//按钮组
	QDateTimeEdit* m_startDateTime = nullptr;//开始日期选择
	QDateTimeEdit* m_endDateTime = nullptr;//开始日期选择
	QPushButton* m_userDefinedBtn = nullptr;//自定义统计按钮
};
#endif // !XQTimeSelectDockWidget_H
