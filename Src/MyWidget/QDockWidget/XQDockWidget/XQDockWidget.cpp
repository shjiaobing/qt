﻿#include"XQDockWidget.h"
#include"XQTitleBarWidget.h"
#include"XQMouseEventFilter_Press.hpp"
XQDockWidget::XQDockWidget(QWidget* parent)
	:QDockWidget(parent)
{

}

XQDockWidget::XQDockWidget(const QString& title, QWidget* parent) :QDockWidget(title, parent)
{
}

XQDockWidget::~XQDockWidget()
{
}

void XQDockWidget::setChildWidget(bool child)
{
	m_title->setAutoHide(child);
	m_title->setMouseStretchWidget(child);
	m_title->setMouseDoubleClickmaxWidget(child);
	if (child)
	{
		//setTitleBarWidget(nullptr);
		m_title->setButtons(15);
		m_title->setMouseMoveWidget(true);
	}
	else
	{
		//setTitleBarWidget(m_title);
	}
}

void XQDockWidget::init()
{
	resize(500, 400);
	init_ui();
	topChanged();
	connect(this, &QDockWidget::topLevelChanged, this,&XQDockWidget::topChanged);
	connect(this, &QDockWidget::dockLocationChanged, this, &XQDockWidget::locationChanged);
	//setWindowFlags(Qt::Widget);
	//setFloating(true);
}

void XQDockWidget::init_ui()
{
	m_title = new XQTitleBarWidget(this);
	m_title->setAutoHide(false);
	//m_title->setMouseMoveWidget(false);
	m_title->setMouseStretchWidget(false);
	m_title->setMouseDoubleClickmaxWidget(false);
	setTitleBarWidget(m_title);
}

void XQDockWidget::locationChanged(Qt::DockWidgetArea area)
{
	if (area == Qt::LeftDockWidgetArea || area == Qt::RightDockWidgetArea)
	{
		/*layout->setDirection(QBoxLayout::TopToBottom);
		setFixedWidth(300);
		setMaximumHeight(16777215);*/
	}
	else if (area == Qt::TopDockWidgetArea || area == Qt::BottomDockWidgetArea)
	{
		/*layout->setDirection(QBoxLayout::LeftToRight);
		setMaximumWidth(16777215);
		setFixedHeight(100);*/
	}
	else
	{
		setMinimumSize(0, 0);
		setMaximumSize(16777215, 16777215);
	}
}

void XQDockWidget::topChanged()
{
	//qDebug() << "浮动改变";
	if (!isTopLevel())
	{//不是悬浮的时候
		m_title->setButtons(0);
		m_title->setMouseMoveWidget(false);
	}
	else
	{//浮动的时候
		m_title->setButtons(15);
		m_title->setMouseMoveWidget(true);
	}
}
