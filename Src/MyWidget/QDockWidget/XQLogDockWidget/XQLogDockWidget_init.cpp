﻿#include"XQLogDockWidget.h"
#include"XQTitleBarWidget.h"
#include"XQMouseEventFilter_Press.hpp"
#include"XQLogWidget.h"
#include<QTableWidget>
#include<QHeaderView>
#include<QBoxLayout>
#include<QCloseEvent>
#include<QDateTime>
#include<QLabel>
void XQLogDockWidget::init()
{
	tableWidget_init();
	XQDockWidget::init();
}

void XQLogDockWidget::init_ui()
{
	XQDockWidget::init_ui();
	setWidget(m_tableWidget);
	
}

void XQLogDockWidget::tableWidget_init()
{
	m_tableWidget = new XQTablePagingWidget(this);
	m_tableWidget->setTableUpdataTime(1);
	m_tableWidget->setPaletteVisible(false);
	m_tableWidget->setDirection(QBoxLayout::RightToLeft);
	m_tableWidget->setHorizontalTitle({ "名字","类型","时间","信息" });
	m_tableWidget->tableWidget()->horizontalHeader()->setSectionResizeMode(m_tableWidget->columnCount() - 1, QHeaderView::Stretch);
	m_tableWidget->setSortFunc<int>(1,std::less<int>());
	m_tableWidget->setSortFunc<QDateTime>(2, std::less<QDateTime>());
}

void XQLogDockWidget::showEvent(QShowEvent*)
{
	/*if (m_tableWidget->rowCount() == 0)
		showDataAll();*/
}

void XQLogDockWidget::closeEvent(QCloseEvent* event)
{
	/*event->ignore();
	hide();*/
}

void XQLogDockWidget::resizeEvent(QResizeEvent* event)
{
	
}
