﻿#ifndef XQGraphicsViewWidget_H
#define XQGraphicsViewWidget_H
#include"XQWidget.h"
#include"XQHead.h"
//图片查看器小部件
class XQGraphicsViewWidget:public XQWidget
{
	Q_OBJECT
public:
	XQGraphicsViewWidget(QWidget* parent = nullptr);
	~XQGraphicsViewWidget();
public slots://槽函数
	void setPixmap(const QPixmap& pixmap);
	void setPixmap(const QString name,const QPixmap& pixmap);
	void setPixmap(const QString& pixmap);
signals://信号

protected://初始化
	void init();
	void init_ui();
protected://事件
	void showEvent(QShowEvent* ev)override;
	//窗口大小改变事件
	void resizeEvent(QResizeEvent* ev)override;
	//按键按下
	void keyPressEvent(QKeyEvent* ev) override;
	//按键释放
	void keyReleaseEvent(QKeyEvent* ev)override;
	//滚轮事件
	void wheelEvent(QWheelEvent* ev)override;
	//窗口关闭事件
	void closeEvent(QCloseEvent* event)override;
	//右键菜单
	void contextMenuRequested();
protected://变量
	QGraphicsScene* m_scene = nullptr;//图片管理
	QGraphicsView* m_view = nullptr;//视图
	Qt::AspectRatioMode m_AspectRatioMode = Qt::KeepAspectRatio;//缩放时长宽比策略
	bool m_ctrl_Press = false;//ctrl按键按下
};
#endif // !XQGraphicsViewWidget_H
