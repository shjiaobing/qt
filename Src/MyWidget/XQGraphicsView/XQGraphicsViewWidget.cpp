﻿#include"XQGraphicsViewWidget.h"
#include"XQTitleBarWidget.h"
#include<QGraphicsView>
#include<QGraphicsScene>
#include<QBoxLayout>
#include<QFileInfo>
#include<QResizeEvent>
#include<QWheelEvent>
#include<QKeyEvent>
#include<QMenu>
#include<QDebug>
XQGraphicsViewWidget::XQGraphicsViewWidget(QWidget* parent)
	:XQWidget(parent)
{
	init();
}

XQGraphicsViewWidget::~XQGraphicsViewWidget()
{

}
void XQGraphicsViewWidget::setPixmap(const QString name, const QPixmap& pixmap)
{
	setPixmap(pixmap);
	setWindowTitle(name);
}
void XQGraphicsViewWidget::setPixmap(const QString& pixmap)
{
	setPixmap(QFileInfo(pixmap).fileName(),QPixmap(pixmap));
}
void XQGraphicsViewWidget::init()
{
	m_scene = new QGraphicsScene(this);
	m_view = new QGraphicsView(this);
	m_view->setScene(m_scene);
	m_view->setDragMode(QGraphicsView::ScrollHandDrag);//可拖拽
	m_view->setRenderHint(QPainter::Antialiasing);  // 反走样
	m_view->setRenderHint(QPainter::SmoothPixmapTransform);  // 平滑缩放
	m_view->setResizeAnchor(QGraphicsView::AnchorUnderMouse);  // 设置锚点
	//设置自定义菜单
	setContextMenuPolicy(Qt::CustomContextMenu);
	m_view->connect(this, &QWidget::customContextMenuRequested, this, &XQGraphicsViewWidget::contextMenuRequested);
	setWindowTitle("图像查看");
	XQWidget::init();
}
void XQGraphicsViewWidget::setPixmap(const QPixmap& pixmap)
{
	m_scene->clear();
	m_scene->addPixmap(pixmap);
	m_view->fitInView(m_scene->sceneRect(), m_AspectRatioMode);
	setWindowIcon(pixmap);
}

void XQGraphicsViewWidget::init_ui()
{
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom, this);//从左到右
	layout->addWidget(m_title);
	layout->addWidget(m_view);
	setLayout(layout);
}
void XQGraphicsViewWidget::showEvent(QShowEvent* ev)
{
	m_view->fitInView(m_scene->sceneRect(), m_AspectRatioMode);
}
void XQGraphicsViewWidget::resizeEvent(QResizeEvent* event)
{
	//qDebug() << event->size()<<size();
	m_view->fitInView(m_scene->sceneRect(), m_AspectRatioMode);
}
void XQGraphicsViewWidget::keyPressEvent(QKeyEvent* ev)
{
	if (ev->key() == Qt::Key_Control)//ctrl
	{
		m_ctrl_Press=true;
	}
}
void XQGraphicsViewWidget::keyReleaseEvent(QKeyEvent* ev)
{
	if (ev->key() == Qt::Key_Control)//ctrl
	{
		m_ctrl_Press = false;
	}
}
void XQGraphicsViewWidget::wheelEvent(QWheelEvent* ev)
{
	int delta = ev->angleDelta().y();
	//ctrl按下时才进行缩放
	if(m_ctrl_Press)
	{
		// 计算缩放因子
		qreal scaleFactor = qPow(2.0, delta / 240.0);
		scaleFactor = qMin(scaleFactor, 10.0);     // 缩放因子最大为10倍
		scaleFactor = qMax(scaleFactor, 0.1);       // 缩放因子最小为0.1倍
		m_view->scale(scaleFactor, scaleFactor);
	}
	//qDebug() << scaleFactor << scaleFactor;
}

void XQGraphicsViewWidget::closeEvent(QCloseEvent* event)
{
	
}

void XQGraphicsViewWidget::contextMenuRequested()
{
	QMenu* menu = new QMenu(this);
	menu->setAttribute(Qt::WA_DeleteOnClose);
	menu->addAction("铺满窗口", [this] {m_view->fitInView(m_scene->sceneRect(), m_AspectRatioMode); });
	{
		QMenu* AspectRatioMode_menu = new QMenu("缩放策略");
		{
			auto IgnoreAspectRatio = AspectRatioMode_menu->addAction("大小可以自由缩放,长宽比没有保留", [this] {m_AspectRatioMode = Qt::IgnoreAspectRatio; });
			if (m_AspectRatioMode == Qt::IgnoreAspectRatio)
			{
				IgnoreAspectRatio->setCheckable(true); IgnoreAspectRatio->setChecked(true);
			}
			auto KeepAspectRatio = AspectRatioMode_menu->addAction("该大小在给定矩形内被缩放为尽可能大的矩形，同时保留长宽比", [this] {m_AspectRatioMode = Qt::KeepAspectRatio; });
			if (m_AspectRatioMode == Qt::KeepAspectRatio)
			{
				KeepAspectRatio->setCheckable(true); KeepAspectRatio->setChecked(true);
			}
			auto KeepAspectRatioByExpanding = AspectRatioMode_menu->addAction("在保留长宽比的情况下，尺寸被缩放为给定矩形外尽可能小的矩形", [this] {m_AspectRatioMode = Qt::KeepAspectRatioByExpanding; });
			if (m_AspectRatioMode == Qt::KeepAspectRatioByExpanding)
			{
				KeepAspectRatioByExpanding->setCheckable(true); KeepAspectRatioByExpanding->setChecked(true);
			}
		}
		menu->addMenu(AspectRatioMode_menu);
	}
	
	menu->popup(QCursor::pos());
}
