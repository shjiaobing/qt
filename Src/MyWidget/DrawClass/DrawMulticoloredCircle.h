﻿#ifndef __DRAWMULTICOLOREDCIRCLE_H__
#define __DRAWMULTICOLOREDCIRCLE_H__
//画圆圈，画时钟外部的圆边框
#include<QPoint>
#include<QColor>
#include<QPainter>
#include<QTimer>
//绘制七彩圆
class DrawMulticoloredCircle
{
public:
	DrawMulticoloredCircle() = default;
	DrawMulticoloredCircle(QPoint o, qint32 r, qint32 width=5, QColor color=Qt::black);
	//初始化圆心，半径，线宽，颜色，画家
	virtual void Initia(QPoint o, qint32 r, qint32 width = 5, QColor color = Qt::black);
	//增加大小
	virtual void addSize(qint32 size);
	//重新设置圆心和半径，线宽
	void reSize(QPoint o, qint32 r, qint32 width =-1);
	//设置圆心坐标或起点坐标
	void setPointO(QPoint o);
	void setPointO(qint32 x,qint32 y);
	//设置线宽度
	void setWidth(qint32 width);
	//设置颜色
	virtual void setColor(QColor color);
	//设置频率
	void setHz(int Hz);
	const int getHz();
	//获取颜色
	const QColor& getColor();
	//获取半径r
	const qint32 getR();
	//获取圆心
	const QPoint getO();
	//获取线宽
	qint32 getWidth();
	//画图
	virtual void draw(QPainter* painter);
	virtual void update();
	virtual void updateColor();
protected:
	QColor m_color;//初始颜色
	//int m_Hz=50;//旋转频率
	QTimer m_Timer;//定时器
	QPen m_pen;//画笔
	qint32 m_r=10;//中心到外部端点的半径
	QPoint m_o=QPoint(10,10);//中心坐标
	//QPainter* m_Pain;//画家
};
#endif