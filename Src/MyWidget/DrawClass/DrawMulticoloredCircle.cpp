﻿#include"DrawMulticoloredCircle.h"
#include<qmath.h>
#include<QPen>
#include<qdebug.h>
DrawMulticoloredCircle::DrawMulticoloredCircle(QPoint o, qint32 r, qint32 width, QColor color)
{
    Initia(o, r, width, color);
}

void DrawMulticoloredCircle::Initia(QPoint o, qint32 r, qint32 width, QColor color)
{
    m_o = o, m_r = r;
    setWidth(width);
    setColor(color);
    m_color = color;
    m_Timer.callOnTimeout([this]() { updateColor(); });
}

const QColor& DrawMulticoloredCircle::getColor()
{
    return m_pen.color();
}

const qint32 DrawMulticoloredCircle::getR()
{
    return m_r;
}

const QPoint DrawMulticoloredCircle::getO()
{
    return m_o;
}

qint32 DrawMulticoloredCircle::getWidth()
{
    return m_pen.width();
}

void DrawMulticoloredCircle::addSize(qint32 size)
{
    m_r = getR() + size;
    //setWidth(getWidth() + size / 15);
    qint32 width = getWidth();
    m_o = QPoint(m_r + width, m_r + width);
    
}

void DrawMulticoloredCircle::reSize(QPoint o, qint32 r, qint32 width)
{
    m_o = o;
    m_r = r;
    if(width!=-1)
    setWidth(width);
}

void DrawMulticoloredCircle::setPointO(QPoint o)
{
    m_o = o;
}

void DrawMulticoloredCircle::setPointO(qint32 x, qint32 y)
{
    m_o.setX(x);
    m_o.setY(y);
}

void DrawMulticoloredCircle::setWidth(qint32 width)
{
    m_pen.setWidth(width);
}

void DrawMulticoloredCircle::setColor(QColor color)
{
    m_pen.setColor(color);
}

void DrawMulticoloredCircle::setHz(int Hz)
{
    if (Hz == 0)
        m_Timer.stop();
    else
        m_Timer.start(1000.0/Hz);
}

const int DrawMulticoloredCircle::getHz()
{
    return 1000.0/m_Timer.interval();
}


void DrawMulticoloredCircle::draw(QPainter* painter)
{
    if (painter == nullptr)
            throw "QPainter* m_Pain==nullptr";
    painter->setPen(m_pen);
   
    qint32 r, g, b;

    m_color.getRgb(&r, &g, &b);
    qint32 n = 360;
    for (size_t i = 0; i < n; i++)
    {
        r++, g += 2, b += 3;
        r %= 255, g %= 255, b %= 255;
        setColor(qRgb(r, g, b));
        painter->setPen(m_pen);
        QRect rect(m_o.x() - m_r, m_o.y() - m_r, 2 * m_r, 2 * m_r);
        //qDebug() << rect;
        painter->drawArc(rect, i * 360 / n * 16, 360 / n * 16);
    }
}

void DrawMulticoloredCircle::update()
{
   
    //m_Pain->drawEllipse(m_o, m_r, m_r);

}

void DrawMulticoloredCircle::updateColor()
{
    m_color = getColor();
}
