﻿#include "XQListSearchWidget.h"
#include"XQAnimationButton.h"
#include<QLineEdit>
#include<QListWidget>
#include<QBoxLayout>
#include<QLabel>
#include<QDateTime>
#include<QMessageBox>
void XQListSearchWidget::init()
{
	init_UI();
	m_startTime = new QDateTime;//初始化
	*m_startTime= QDateTime::currentDateTime();//初始化时间
	//搜索框变化
	connect(m_search, &QLineEdit::textChanged, this, &XQListSearchWidget::setChangeList);
	//搜索框回车按下信号
	connect(m_search, &QLineEdit::returnPressed, m_okBtn, &QPushButton::pressed);
	//按钮确认
	connect(m_okBtn, &QPushButton::pressed, [this] {
		if (m_search->text().isEmpty())
		{
			QMessageBox::information(this, "提醒", "输入框不能为空");
			return;//输入框是空的不返回
		}
		if (m_selectedData.indexOf(m_search->text()) != -1)//我已经选择过了
		{
			QMessageBox::information(this, "提醒", QString("%1 已经选择了").arg(m_search->text()));
			return;
		}
		emit selectText(m_search->text());});
	//列表框单机选择
	connect(m_listWidget, &QListWidget::itemSelectionChanged, [=] {
		auto text=m_listWidget->currentItem()->text();
	disconnect(m_search, &QLineEdit::textChanged, nullptr, nullptr);
	m_search->setText(text);
	connect(m_search, &QLineEdit::textChanged, this, &XQListSearchWidget::setChangeList);
		});
	//列表框双击选择
	connect(m_listWidget, &QListWidget::itemDoubleClicked, [=](QListWidgetItem* item) {
		auto text = item->text();
	disconnect(m_search, &QLineEdit::textChanged, nullptr, nullptr);
	m_search->setText(text);
	connect(m_search, &QLineEdit::textChanged, this, &XQListSearchWidget::setChangeList);
	emit m_okBtn->pressed();//发送确认信号
		});
}

void XQListSearchWidget::init_UI()
{
	setWindowFlags(Qt::FramelessWindowHint);  // 将窗口去除边框
	setStyleSheet(QString("background: qlineargradient(x1:0, y1:0, x2:1, y2:1, m_stop:0 #ffffff, m_stop:0.4 #00FFC8, m_stop:0.7 #00C8C8, m_stop:1 #00FFFF);"));//初始化背景渐变色

	auto TopLayout = new QBoxLayout(QBoxLayout::Direction::TopToBottom, this);//从上到下
	TopLayout->addLayout(init_SearchBox(),0);
	TopLayout->addWidget(init_ListWidget(), 1);
	setLayout(TopLayout);
}

QBoxLayout* XQListSearchWidget::init_SearchBox()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("搜索", this);
	label->setWindowFlags(Qt::FramelessWindowHint);  // 将窗口去除边框
	label->setAttribute(Qt::WA_TranslucentBackground);  // 设置背景透明
	label->setWindowOpacity(1);  // 设置透明度
	label->setFont(QFont("微软雅黑", 15));
	LeftLayout->addWidget(label, 0);

	m_search = new QLineEdit(this);
	m_search->setFont(QFont("微软雅黑", 15));
	//m_search->setPlaceholderText("");
	LeftLayout->addWidget(m_search, 1);

	m_okBtn = new XQAnimationButton(":/sidebar/image/sidebar/okOne.png",":/sidebar/image/sidebar/okTwo.png",this);
	m_okBtn->setFixedSize(40, 40);
	LeftLayout->addWidget(m_okBtn, 0);
	return LeftLayout;
}

QListWidget* XQListSearchWidget::init_ListWidget()
{
	m_listWidget = new QListWidget(this);
	m_listWidget->setFont(QFont("微软雅黑", 15));
	m_listWidget->setViewMode(QListWidget::IconMode);
	m_listWidget->setSpacing(10);//设置间距
	m_listWidget->setDragEnabled(false);//设置禁止拖拽
	m_listWidget->setAlternatingRowColors(true); // 启用交替行颜色
	m_listWidget->setStyleSheet("QListWidget::item:nth-child(odd) { background-color: #00FFC8; }"); // 设置奇数行颜色
	// 创建 QListWidgetItem 对象
	//QListWidgetItem* item = new QListWidgetItem(m_listWidget);
	//// 创建需要插入的 widget
	//QLabel* btn = new QLabel("Click me", m_listWidget);
	//// 将 widget 和 QListWidgetItem 关联起来
	//m_listWidget->setItemWidget(item, btn);
	//// 添加 QListWidgetItem 到 QListWidget 中
	//m_listWidget->addItem(item);
	return m_listWidget;
}
