﻿#include"XQMainSetupWidget.h"
#include"XQStackedWidget.h"
#include"XQSystemSetupWidget.h"
#include"XQRobotSetupWidget.h"
#include"XQTimerTaskSetupWidget.h"
#include"XQAppInfo.h"
XQMainSetupWidget::XQMainSetupWidget(QWidget* parent, bool AutoInit)
	:QWidget(parent)
{
	if (AutoInit)
	{
		init();
	}
}

XQMainSetupWidget::~XQMainSetupWidget()
{
}

XQSettingsInfo* XQMainSetupWidget::settingsInfo() const
{
	return XQAppInfo::settingsInfo();
}

XQLabelButton* XQMainSetupWidget::addWidget(QWidget* widget, const QString& title)
{
	if (widget == nullptr)
		return nullptr;
	m_StackedWidget->addWidget(widget, title);
	return m_StackedWidget->labelButton(widget);;
}

QWidget* XQMainSetupWidget::addWidget(SetupWidgetType type)
{
	QWidget* widget=nullptr;
	switch (type)
	{
	case XQMainSetupWidget::System:
		if (m_SystemSetup)m_SystemSetup->deleteLater();
		addWidget(widget=new XQSystemSetupWidget(m_StackedWidget), "基本");
		m_SystemSetup = (XQSystemSetupWidget*)widget;
		break;
	case XQMainSetupWidget::Robot:
		if (m_RobotSetup)m_RobotSetup->deleteLater();
		addWidget(widget=new XQRobotSetupWidget(m_StackedWidget), "机器人");
		m_RobotSetup = (XQRobotSetupWidget*)widget;
		break;
	case XQMainSetupWidget::Timer:
		if (m_TimerTaskSetup)m_TimerTaskSetup->deleteLater();
		addWidget(widget = new XQTimerTaskSetupWidget(m_StackedWidget), "定时器");
		m_TimerTaskSetup = (XQTimerTaskSetupWidget*)widget;
		break;
	default:
		break;
	}
	return widget;
}

void XQMainSetupWidget::clearWidget()
{
	m_StackedWidget->clearWidget();//清理下窗口
}

void XQMainSetupWidget::setCurrentWidget(SetupWidgetType type)
{
	if(type)
		m_StackedWidget->setCurrentIndex(type);
}
