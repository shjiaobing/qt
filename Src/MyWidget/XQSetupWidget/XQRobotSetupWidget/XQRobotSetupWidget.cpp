﻿#include"XQRobotSetupWidget.h"
#include"XQSettingsInfo.h"
#include"XQAppInfo.h"
#include"XQRobotDing.h"
#include<QComboBox>
#include<QCheckBox>
#include<QLineEdit>
#include<QLabel>
#include<QPushButton>
#include<QMessageBox>
#include<QSettings>
#include<QDebug>
XQRobotSetupWidget::XQRobotSetupWidget(QWidget* parent, bool AutoInit)
	:QScrollArea(parent)
{
	if (AutoInit)
	{
		init();
	}
}

XQRobotSetupWidget::~XQRobotSetupWidget()
{
}

QString XQRobotSetupWidget::tokenDingDing()
{
	if (XQAppInfo::settingsInfo() == nullptr)
		return QString();
	return std::move(XQAppInfo::settingsInfo()->Settings()->value("RobotDingDingToken").toString());
}

QString XQRobotSetupWidget::secretDingDing()
{
	if (XQAppInfo::settingsInfo() == nullptr)
		return QString();
	return std::move(XQAppInfo::settingsInfo()->Settings()->value("RobotDingDingSecret").toString());
}

XQSettingsInfo* XQRobotSetupWidget::settingsInfo() const
{
	return XQAppInfo::settingsInfo();
}

XQAppInfo* XQRobotSetupWidget::appInfo()
{
	return XQAppInfo::Global();
}

void XQRobotSetupWidget::testSend()
{
	QString text = m_testText->text();
	switch (RobotType(m_RobotBox->currentData().toInt()))
	{
	case RobotType::DingDing:sendTextDingDing(text);
		break;
	default:
		break;
	}
}

void XQRobotSetupWidget::sendTextDingDing(const QString& text)
{
	XQRobotDing robot;
	robot.setToken(m_tokenDingDing->text().trimmed());
	robot.setSecret(m_secretDingDing->text().trimmed());
	if(robot.sendText(text))
		QMessageBox::information(this, "钉钉机器人", "发送成功\r\n信息:" + text);
	else
		QMessageBox::information(this, "钉钉机器人", "发送失败\r\n信息:" + text);
}

void XQRobotSetupWidget::readRobotEnabled()
{
	if (m_Enabled == nullptr || m_RobotBox == nullptr)
		return;
	auto settings = settingsInfo();
	auto key = QString("RobotEnabled-%1").arg(m_RobotBox->currentData().toInt());
	auto Enabled = settings->value(key).toBool();
	m_Enabled->setChecked(Enabled);
}

void XQRobotSetupWidget::readTokenDingDing()
{

	if (m_tokenDingDing == nullptr)
		return;
	m_tokenDingDing->setText(tokenDingDing());
}

void XQRobotSetupWidget::readSecretDingDing()
{
	if (m_secretDingDing == nullptr)
		return;
	auto settings = settingsInfo();
	auto text = settings->value("RobotDingDingSecret").toString();
	m_secretDingDing->setText(text);
}
void XQRobotSetupWidget::writeRobotEnabled(RobotType type, bool Enabled)
{
	auto settings = settingsInfo();
	settings->setValue("RobotEnabled-" + QString::number(type), Enabled);
	auto robot=XQAppInfo::robot(type);
	if (robot)//设置其启用属性
		robot->setEnabled(Enabled);
	else
		qWarning() << QStringList{"钉钉"}[type] << "机器人不存在无法设置启用属性";
}

void XQRobotSetupWidget::writeTokenDingDing(const QString& text)
{
	auto settings = settingsInfo();
	settings->setValue("RobotDingDingToken", text);
	auto dingding =(XQRobotDing*) appInfo()->robot(DingDing);
	if (dingding)
		dingding->setToken(text);
}

void XQRobotSetupWidget::writeSecretDingDing(const QString& text)
{
	auto settings = settingsInfo();
	settings->setValue("RobotDingDingSecret", text);
	auto dingding = (XQRobotDing*)appInfo()->robot(DingDing);
	if (dingding)
		dingding->setSecret(text);
}