﻿#ifndef XQROBOTSETUPWIDGET_H
#define XQROBOTSETUPWIDGET_H
#include"XQHead.h"
#include"XQRobot.h"
#include<QScrollArea>
//机器人设置小部件
class XQRobotSetupWidget:public QScrollArea
{
	Q_OBJECT
public:
	XQRobotSetupWidget(QWidget* parent = nullptr, bool AutoInit = true);
	virtual ~XQRobotSetupWidget();
	static QString tokenDingDing();
	static QString secretDingDing();
public:
	//获取配置信息
	XQSettingsInfo* settingsInfo()const;
	XQAppInfo* appInfo();
public:
	void testSend();//发送测试文本
signals://信号
	//机器人是否启用
	void robotEnabledChange(RobotType type,bool Enabled);
	//钉钉
	void tokenDingDingChange(const QString& text);
	void secretDingDingChange(const QString& text);
protected://隐藏的函数
	//初始化
	virtual void init();
	//绑定函数
	virtual void bindFunc();
	//初始化ui数据
	virtual void init_UiData();
protected://需要的一些槽函数(机器人发送信息)
	void sendTextDingDing(const QString& text);
protected://需要的一些槽函数(读取配置文件)
	void readRobotEnabled();
	void readTokenDingDing();
	void readSecretDingDing();
protected://需要的一些槽函数(写入配置文件)
	void writeRobotEnabled(RobotType type, bool Enabled);
	void writeTokenDingDing(const QString& text);
	void writeSecretDingDing(const QString& text);
protected://初始化ui
	virtual void init_ui();
	virtual void init_StackLayout();
	virtual QBoxLayout* init_OneRow();//第一行
	QWidget* init_DingDingWidget();//
	QWidget* init_test();//测试设置
protected://变量
	QCheckBox* m_Enabled = nullptr;//是否启用
	QComboBox* m_RobotBox = nullptr;//选择机器人类型
	QStackedLayout* m_StackLayout = nullptr;//堆栈布局
	//钉钉
	QLineEdit* m_tokenDingDing = nullptr;//令牌
	QLineEdit* m_secretDingDing = nullptr;//密钥
	QLineEdit* m_testText = nullptr;//测试文本
	QPushButton* m_testSend = nullptr;//测试发送
};

#endif // !XQRobotSetupWidget_h
