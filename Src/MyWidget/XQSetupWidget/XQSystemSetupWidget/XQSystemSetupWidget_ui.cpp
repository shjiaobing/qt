﻿#include"XQSystemSetupWidget.h"
#include"XQStackedWidget.h"
#include"XQLabelButton.h"
#include<QCoreApplication>
#include<QBoxLayout>
#include<QButtonGroup>
#include<QRadioButton>
#include<QLabel>
#include<QCheckBox>
#include<QPushButton>
#include<QlineEdit>
void XQSystemSetupWidget::init_ui()
{
	auto widget = new QWidget(this);
	//设置内容布局
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom, widget);//从上到下
	layout->setContentsMargins(10, 10, 10, 10);
	layout->addLayout(init_autoStart());
	layout->addLayout(init_systemTray());
	layout->addLayout(init_suspensionWindow());
	layout->addLayout(init_saveMainWindowPosition());
	layout->addLayout(init_bufferPath());
	layout->addLayout(init_checkUpdates());
	layout->addStretch(1);
	widget->setLayout(layout);


	/*m_StackedWidget = new XQStackedWidget(QBoxLayout::LeftToRight,QBoxLayout::TopToBottom, this);
	m_StackedWidget->addWidget(widget, "基本");
	m_StackedWidget->labelButton(widget)->setCloseButtonShow(false);
	m_StackedWidget->setCurrentWidget(widget);*/
	//设置滚动条
	setWidget(widget);
	setWidgetResizable(true);
}

QBoxLayout* XQSystemSetupWidget::init_autoStart()
{
	m_autorunGroup = new QButtonGroup(this);
	auto btnShow = new QRadioButton("显示", this);
	auto btnHide = new QRadioButton("隐藏", this);
	auto btnMinimized = new QRadioButton("最小化", this);
	m_autorunGroup->addButton(btnShow, int(AutoStartType::show));
	m_autorunGroup->addButton(btnHide, int(AutoStartType::hide));
	m_autorunGroup->addButton(btnMinimized, int(AutoStartType::minimized));
	m_autoStart = new QCheckBox("开机自启");
	auto layout = new QBoxLayout(QBoxLayout::LeftToRight);//从左到右
	layout->addWidget(m_autoStart);
	layout->addWidget(new QLabel("启动方式:"));
	layout->addWidget(btnShow);
	layout->addWidget(btnHide);
	layout->addWidget(btnMinimized);
	layout->addStretch(1);
    return layout;
}

QBoxLayout* XQSystemSetupWidget::init_systemTray()
{
	m_systemTray = new QCheckBox("显示托盘图标");
	auto layout = new QBoxLayout(QBoxLayout::LeftToRight);//从左到右
	layout->addWidget(m_systemTray);
	return layout;
}

QBoxLayout* XQSystemSetupWidget::init_suspensionWindow()
{
	m_suspension = new QCheckBox("打开悬浮窗");
	auto layout = new QBoxLayout(QBoxLayout::LeftToRight);//从左到右
	layout->addWidget(m_suspension);
	return layout;
}

QBoxLayout* XQSystemSetupWidget::init_bufferPath()
{
	m_bufferPath = new QLineEdit();
	m_bufferPath->setReadOnly(true);
	auto label = new QLabel("缓存路径:");
	label->setFixedWidth(m_leftLabelWidth);
	auto layout = new QBoxLayout(QBoxLayout::LeftToRight);//从左到右
	layout->addWidget(label);
	layout->addWidget(m_bufferPath);
	return layout;
}

QBoxLayout* XQSystemSetupWidget::init_saveMainWindowPosition()
{
	m_saveMainWindowPosition = new QCheckBox("记住主窗口位置和大小");
	auto layout = new QBoxLayout(QBoxLayout::LeftToRight);//从左到右
	layout->addWidget(m_saveMainWindowPosition);
	return layout;
}

QBoxLayout* XQSystemSetupWidget::init_checkUpdates()
{
	m_version = new QLabel(QCoreApplication::applicationVersion());
	m_checkUpdates = new QPushButton("检查更新");
	auto label = new QLabel("版本号:");
	label->setFixedWidth(m_leftLabelWidth);
	auto layout = new QBoxLayout(QBoxLayout::LeftToRight);//从左到右
	layout->addWidget(label);
	layout->addWidget(m_version);
	layout->addStretch(1);
	layout->addWidget(m_checkUpdates);
	return layout;
}
