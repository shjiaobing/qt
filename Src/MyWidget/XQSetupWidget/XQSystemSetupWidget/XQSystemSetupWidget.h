﻿#ifndef XQSYSTEMSETUPWIDGET_H
#define XQSYSTEMSETUPWIDGET_H
#include<QScrollArea>
#include"XQHead.h"
#include"XQArguments.h"
//系统基本设置小部件
class XQSystemSetupWidget:public QScrollArea
{
	Q_OBJECT
public:
	XQSystemSetupWidget(QWidget* parent = nullptr, bool AutoInit = true);
	virtual ~XQSystemSetupWidget();
public:
	int leftLabelWidth()const;
	//获取配置信息
	XQSettingsInfo *settingsInfo()const;
	//获取是否开机自启
	bool isAutoStartEnabled()const;
public slots:
	//设置左边标签宽度
	void setLeftLabelWidth(int width);
signals://信号
	//自启状态改变
	void autorunChange(bool Auto);
	//托盘菜单显示/隐藏
	void systemTrayChange(bool show);
	//自启启动方式改变
	void autorunStartTypeChange(AutoStartType type);
	//悬浮窗显示/隐藏
	void suspensionChange(bool show);
	//记住？主窗口位置
	void saveMainWindowPositionChange(bool yes);
	//缓存路径改变
	void bufferPathChange(const QString& path);
protected://隐藏的函数
	//初始化
	virtual void init();
	//绑定函数
	virtual void bindFunc();
	//初始化ui数据
	virtual void init_UiData();
	//设置缓存路径
	virtual void setBufferPath();
	//设置开机自启
	void setAutorun(bool Auto);
protected://初始化ui
	virtual void init_ui();
	virtual QBoxLayout* init_autoStart();//开机自启
	virtual QBoxLayout* init_systemTray();//托盘菜单
	virtual QBoxLayout* init_suspensionWindow();//打开悬浮窗
	virtual QBoxLayout* init_bufferPath();//缓存路径
	virtual QBoxLayout* init_saveMainWindowPosition();//保存主窗口位置
	virtual QBoxLayout* init_checkUpdates();//检查更新
protected://事件
	/*void showEvent(QShowEvent* event)override;*/
	void closeEvent(QCloseEvent* event)override;
protected://变量
	int m_leftLabelWidth = 150;//左边标签宽度
	QCheckBox* m_autoStart = nullptr;//开机自启
	QButtonGroup* m_autorunGroup = nullptr;//自动启动单选框按钮组
	QCheckBox* m_systemTray = nullptr;//打开托盘菜单
	QCheckBox* m_suspension = nullptr;//打开悬浮窗
	QCheckBox* m_saveMainWindowPosition = nullptr;//保存主窗口位置
	QLineEdit* m_bufferPath = nullptr;//缓存路径
	QLabel* m_version = nullptr;//版本号
	QPushButton* m_checkUpdates= nullptr;//检查更新
};
#endif // !XQSystemSetupWidget_H
