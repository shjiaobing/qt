﻿#include"XQTimerTaskSetupWidget.h"
#include"XQAppInfo.h"
#include"XQTimerTaskWidget.h"
#include"XQMouseEventFilter_Press.hpp";
#include"XQCircleLabel.h"
#include<QBoxLayout>
#include<QMessageBox>
#include<QPushButton>
#include<QLabel>
XQTimerTaskSetupWidget::XQTimerTaskSetupWidget(QWidget* parent, bool AutoInit)
	:QScrollArea(parent)
{
	if (AutoInit)
	{
		init();
	}
}

XQTimerTaskSetupWidget::~XQTimerTaskSetupWidget()
{
}


XQSettingsInfo* XQTimerTaskSetupWidget::settingsInfo() const
{
	return XQAppInfo::settingsInfo();
}

XQTimerGroup* XQTimerTaskSetupWidget::timerGroup() const
{
	return XQAppInfo::timerGroup();
}

XQTimerTaskWidget* XQTimerTaskSetupWidget::taskWidget(int id)
{
	if (id == -1)
		return nullptr;
	XQTimerTaskWidget* widget = nullptr;
	for (auto&task: m_taskList)
	{
		if (task->timerId() == id)
			widget = task;
	}
	return widget;
}

void XQTimerTaskSetupWidget::addTaskRoot(const QString& name, std::function<void()> func)
{
	m_taskGroup_root[name] = func;
}

void XQTimerTaskSetupWidget::addTaskWidget(XQTimerTaskWidget* widget)
{
	if (widget == nullptr)
		return;
	m_layout->insertWidget(m_layout->count()-1,widget);
	m_taskList << widget;
	setShowCountText();
	widget->installEventFilter(new XQMouseEventFilter_Press([=] {
		if (m_selectTask != nullptr)
			m_selectTask->setBackgroundBorderColour();
		widget->setBackgroundBorderColour(qRgb(202, 148, 247));
		m_selectTask = widget;
		}, widget));
	widget->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(widget, &QWidget::customContextMenuRequested, this, &XQTimerTaskSetupWidget::showTaskContextMenu);
	for (auto&name:m_taskGroup_root.keys())
	{
		widget->addTaskRoot(name,&m_taskGroup_root);
	}
	m_clearBtn->setEnabled(true);
}

void XQTimerTaskSetupWidget::removeTaskWidget(XQTimerTaskWidget* widget)
{
	if (widget == nullptr)
		return;
	int nSel = m_taskList.indexOf(widget);
	if (nSel == -1)
		return;
	if (m_selectTask == widget)
		m_selectTask = nullptr;
	m_layout->removeWidget(widget);
	widget->deleteLater();
	m_taskList.remove(nSel);
	setShowCountText();
	if (m_taskList.isEmpty())
		m_clearBtn->setEnabled(false);
}

void XQTimerTaskSetupWidget::clearTaskWidget()
{
	m_selectTask = nullptr;
	for (auto& widget: m_taskList)
	{
		m_layout->removeWidget(widget);
		widget->deleteLater();
	}
	m_taskList.clear();
	setShowCountText();
	m_clearBtn->setEnabled(false);
}
void XQTimerTaskSetupWidget::addTask()
{
	addTaskWidget(new XQTimerTaskWidget(this));
}

void XQTimerTaskSetupWidget::removeTask()
{
	if (m_selectTask == nullptr)
		return;
	auto name = m_selectTask->taskName();
	if (QMessageBox::Yes == QMessageBox::information(this, "删除 "+ name, "删除后将无法恢复", QMessageBox::Yes | QMessageBox::No, QMessageBox::No))
		removeTaskWidget(m_selectTask);
}

void XQTimerTaskSetupWidget::clearTask()
{
	if (m_taskList.isEmpty())
		return;
	if (QMessageBox::Yes == QMessageBox::information(this, "清空", "删除后将无法恢复", QMessageBox::Yes | QMessageBox::No, QMessageBox::No))
	{
		clearTaskWidget();
	}
}

void XQTimerTaskSetupWidget::setShowCountText()
{
	m_taskCount->setText(QString("%1").arg(m_taskList.size()));
}

void XQTimerTaskSetupWidget::timerState(TimerPollState state, int id)
{
	auto widget = taskWidget(id);
	switch (state)
	{
	case TimerPollState::poll:m_taskCount->updateColor();
		break;
	case TimerPollState::sleep:setInfoText("定时器线程休眠");
		break;
	case TimerPollState::wake:setInfoText("定时器线程唤醒");
		break;
	case TimerPollState::run:if (widget)setInfoText(widget->taskName()+"定时器任务运行");
		break;
	case TimerPollState::remove:if (widget)setInfoText(widget->taskName() + "定时器任务删除");
		break;
	default:
		break;
	}
}

void XQTimerTaskSetupWidget::setInfoText(const QString& text)
{
	m_info->setText(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss")+"  "+text);
}
