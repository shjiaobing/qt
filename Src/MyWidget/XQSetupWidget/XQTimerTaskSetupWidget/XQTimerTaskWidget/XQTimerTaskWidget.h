﻿#ifndef XQTIMERTASKWIDGET_H
#define XQTIMERTASKWIDGET_H
#include"XQHead.h"
#include<QWidget>
#include<functional>
#include"XQTimerTask.h"
#include"XEnum.h"

//定时器任务小部件
class XQTimerTaskWidget:public QWidget
{
	Q_OBJECT
public:
	XQTimerTaskWidget(QWidget* parent = nullptr,bool Auto=true);
	virtual ~XQTimerTaskWidget();
	XQTimerGroup* timerGroup()const;
public:
	virtual int timerId()const;
	virtual bool isTaskEnable()const;
	virtual QString taskName()const;
	virtual QDateTime startDateTime()const;
	virtual int runCount()const;
	virtual TimerType timerType()const;
	virtual QTime timerTime_time()const;
	virtual TimeType timeType()const;
	virtual //选择的天数索引从0开始
	virtual QList<int> daySelect()const;
	virtual QTime timerTime_date()const;
	virtual TaskType taskType()const;
	virtual int taskRoot()const;
	virtual QString taskUser()const;
	virtual QList<int> robotSelect()const;
public:
	void setTimerId(int id);
	//定时器启用
	void setTaskEnable(bool enable);
	void setTaskName(const QString& name);
	void setStartDateTime(const QDateTime& dateTime);
	void setRunCount(int count);
	void setTimerType(TimerType type);
	void setTimerTime_time(const QTime& time);
	void setTimeType(TimeType type);
	void setDaySelect(const QList<int>&select);
	void setTimerTime_date(const QTime& time);
	void setTaskType(TaskType type);
	void setTaskRoot(int index);
	void setTaskUser(const QString& user);
	void setRobotSelect(const QList<int>& select);
public:
	//添加一个内置的预设任务
	void addTaskRoot(const QString& name, QMap<QString, std::function<void()>>* taskGroup);
	//设置背景边框颜色
	void setBackgroundBorderColour(QRgb rgb);
	//设置背景边框颜色恢复
	void setBackgroundBorderColour();
	
protected://隐藏的函数
	//初始化
	virtual void init();
	//信息处理 下一次运行时间
	virtual void nextRunTime(int id, QDateTime dataTime);
	//加入定时器
	virtual bool joinTimer(int id=-1);
	virtual std::function<void()> taskFunc();
	virtual std::function<QDateTime(QDateTime)> nextTime();
	virtual std::function<void(XQTimerTask*)>robotPush(std::function<QDateTime(QDateTime)> nextTime);
protected://初始化ui
	virtual void init_ui();
	virtual QBoxLayout* init_task();
	virtual QBoxLayout* init_timerType();
	virtual QBoxLayout* init_taskType();
	//初始化堆栈里面的小部件
	virtual void init_timerStackWidget();
	virtual void init_taskStackWidget();
	//时间类型改变
	void timeTypeComBox_dateChanged(int index);
protected://事件
	void mousePressEvent(QMouseEvent* ev);
	/*void showEvent(QShowEvent* event)override;*/
	//void closeEvent(QCloseEvent* event)override;
protected://变量
	/*friend class XQTimerTaskSetupWidget;*/
	int m_timerId = -1;//定时器id添加成功后会有的
	QWidget* m_background = nullptr;//背景
	//第一行公共的
	QCheckBox* m_enableCheck = nullptr;//启用多选框
	QLineEdit* m_taskName = nullptr;//任务名字
	QDateTimeEdit* m_startDateTime=nullptr;//第一次的开始时间
	QLineEdit* m_countEdit = nullptr;//运行次数
	QLabel* m_timerInfo = nullptr;//定时器信息
	//第二行
	QComboBox* m_timerTypeComBox = nullptr;//以选择定时的方式
	QStackedLayout* m_timerStackLayout = nullptr;//堆栈布局
	//第二行时间
	QTimeEdit* m_timerEdit_time = nullptr;//定时时间
	//第二行日期
	QComboBox* m_timeTypeComBox_date = nullptr;//时间类型
	XQComboCheckButton* m_daySelectComBox_date = nullptr;//天数选择
	QTimeEdit* m_timerEdit_date = nullptr;//定时时间
	//第三行
	QComboBox* m_taskTypeComBox = nullptr;//任务类型
	QStackedLayout* m_taskStackLayout = nullptr;//堆栈布局
	QComboBox* m_taskRootComBox = nullptr;//任务预设
	QLineEdit* m_taskUserEdit = nullptr;//任务用户
	XQComboCheckButton* m_robot = nullptr;//机器人推送
};
#endif