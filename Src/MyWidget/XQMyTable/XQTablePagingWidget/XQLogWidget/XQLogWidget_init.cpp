﻿#include"XQLogWidget.h"
#include"XQTitleBarWidget.h"
#include"XQMouseEventFilter_Press.hpp"
#include<QTableWidget>
#include<QHeaderView>
#include<QBoxLayout>
#include<QCloseEvent>
#include<QDateTime>
void XQLogWidget::init()
{
	XQTablePagingWidget::init();
	setMinimumSize(700,400);
	setWindowTitle("日志");
	//设置自动刷新频率
	setTableUpdataTime(1);
	//超过最大数量清理
	connect(this, &XQTablePagingWidget::dataCountChange, [=] (qint64 count)
		{
			if (count > m_maxSize)
				m_data.remove(m_data.size()-1,1);

		});
}

void XQLogWidget::init_ui()
{
	XQTitleBarWidget* title = nullptr;
	if (parentWidget() == nullptr)
	{
		title = new XQTitleBarWidget(this);
		title->setButtons(15);
	}
	//设置内容布局
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom, this);//从上到下
	layout->setContentsMargins(10, 10, 10, 10);
	layout->setSpacing(0);
	if (title != nullptr)
		layout->addWidget(title);
	layout->addWidget(init_paging());
	layout->addWidget(m_tableWidget,1);
	setLayout(layout);
}

void XQLogWidget::tableWidget_init()
{
	XQTablePagingWidget::tableWidget_init();
	setHorizontalTitle({ "名字","类型","时间","信息" });
	m_tableWidget->horizontalHeader()->setSectionResizeMode(m_tableWidget->columnCount() - 1, QHeaderView::Stretch);
	setSortFunc<int>(1,std::less<int>());
	setSortFunc<QDateTime>(2, std::less<QDateTime>());
}

void XQLogWidget::showEvent(QShowEvent*)
{
	if (m_tableWidget->rowCount() == 0)
		showDataAll();
}

void XQLogWidget::closeEvent(QCloseEvent* event)
{
	event->ignore();
	hide();
}

void XQLogWidget::resizeEvent(QResizeEvent* event)
{
	
}
