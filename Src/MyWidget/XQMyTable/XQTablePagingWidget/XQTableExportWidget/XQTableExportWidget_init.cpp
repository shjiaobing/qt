﻿#include"XQTableExportWidget.h"
#include"XQTitleBarWidget.h"
#include"XQStatusBarWidget.h"
#include"XQTaskPool.h"
#include"XQMouseEventFilter_Press.hpp"
#include<QBoxLayout>
#include<QThreadPool>
#include<QDir>
#include<QCheckBox>
#include<QLineEdit>
#include<QPushButton>
#include<QLabel>
#include<QFileDialog>
#include<QDebug>
void XQTableExportWidget::init()
{
	setWindowTitle("表格导出到本地Excel(*.xlsx)");
	init_taskPool();
	init_statusbar();
	init_ui();
	resize(800, 150);
}
void XQTableExportWidget::init_ui()
{
	XQTitleBarWidget* title = nullptr;
	if (parentWidget() == nullptr)
	{
		title = new XQTitleBarWidget(this);
		title->setButtons(15);
	}

	auto layout = new QBoxLayout(QBoxLayout::TopToBottom, this);
	layout->setContentsMargins(10, 10, 10, 0);
	if (title != nullptr)
		layout->addWidget(title);
	layout->addLayout(init_oneRow());
	layout->addWidget(m_statusbar);
	setLayout(layout);
}

void XQTableExportWidget::init_statusbar()
{
	m_statusbar = new XQStatusBarWidget(this);
	connect(this, &XQTableExportWidget::showMessage, m_statusbar, &XQStatusBarWidget::showMessage, Qt::QueuedConnection);
}

void XQTableExportWidget::init_taskPool()
{
	m_taskPool = new XQTaskPool(this);
	connect(m_taskPool, QOverload<>::of(&XQTaskPool::start), [=] {m_startBtn->setEnabled(false); m_stopBtn->setEnabled(true); });
	connect(m_taskPool, QOverload<>::of(&XQTaskPool::finish), [=] {m_startBtn->setEnabled(true); m_stopBtn->setEnabled(false); });
}

QBoxLayout* XQTableExportWidget::init_oneRow()
{
	m_filePathEdit = new QLineEdit(QDir::currentPath()+"/table.xlsx", this);
	m_filePathEdit->setReadOnly(true);
	m_ExportTitle = new QCheckBox("导出标题栏", this);
	m_ExportTitle->setChecked(true);
	m_startBtn = new QPushButton("开始", this);
	m_stopBtn = new QPushButton("结束", this);
	m_stopBtn->setEnabled(false);
	auto layout = new QBoxLayout(QBoxLayout::LeftToRight);
	layout->addWidget(new QLabel("保存文件路径:"));
	layout->addWidget(m_filePathEdit,1);
	layout->addWidget(m_ExportTitle);
	layout->addWidget(m_startBtn);
	layout->addWidget(m_stopBtn);

	//设置目录编辑框单机选择保存的文件
	m_filePathEdit->installEventFilter(new XQMouseEventFilter_Press([=] {
		auto path = QFileDialog::getSaveFileName(this,"保存文件","","Excel(*.xlsx)");
		if (path.isEmpty())
			return;
		m_filePathEdit->setText(path);
		}, m_filePathEdit));
	//绑定按钮槽函数
	connect(m_startBtn, &QPushButton::pressed, this, &XQTableExportWidget::start, Qt::QueuedConnection);
	connect(m_stopBtn, &QPushButton::pressed, this, &XQTableExportWidget::stop, Qt::QueuedConnection);
	return layout;
}
