﻿#include"XQFileTableWidget.h"
#include"XQAlgorithm.h"
#include"XMD5.h"
#include<QProgressBar>
#include<QFileSystemWatcher>
#include<QApplication>
#include<QClipboard>
#include<QDir>
#include<QMenu>
XQFileTableWidget::XQFileTableWidget(QWidget* parent)
	:QTableWidget(parent)
{
	
	init();
}

XQFileTableWidget::~XQFileTableWidget()
{
}

QString XQFileTableWidget::dirPath() const
{
	return m_DirPath;
}

QStringList XQFileTableWidget::localityFiles()
{
	QStringList list;
	// 遍历行
	for (int row = 0; row < rowCount(); row++) {
		// 获取单元格指针
		QTableWidgetItem* item = this->item(row, Type::name);
		if (item) {
			// 获取单元格数据
			list << item->data(Qt::UserRole).toString();
		}
	}
	return std::move(list);
}

QMenu* XQFileTableWidget::menu()
{
	return m_menu;
}
void XQFileTableWidget::setMenu(QMenu* menu)
{
	if (m_menu != nullptr)
	{
		m_menu->deleteLater();
	}
	m_menu = menu;
}
void XQFileTableWidget::setProgressBar(int row, Type LocaType, size_t val, size_t maxSize, const QString& text)
{
	int w = 150;
	auto widget =(QProgressBar*)cellWidget(row, LocaType);
	if(widget==nullptr)
	{
		widget = new QProgressBar(this);
		widget->setAlignment(Qt::AlignCenter);
		setCellWidget(row, LocaType, widget);
		this->setColumnWidth(Type::name, columnWidth(Type::name)-w);//设置列宽
		this->setColumnWidth(LocaType,w+ columnWidth(LocaType));//设置列宽
		SetColumnWidth(Type::size);
	}
	else if (val >= maxSize)//到达100了
	{
		widget->deleteLater();
		setCellWidget(row, LocaType, nullptr);
		this->setColumnWidth(LocaType, columnWidth(LocaType) - w);//设置列宽
		this->setColumnWidth(Type::name, columnWidth(Type::name)+ w);//设置列宽
	}
	else
	{
		widget->setRange(0, 100);
		widget->setValue(((double)val / (double)maxSize) * 100);
		widget->setFormat(readableFileSize(val) + "/" + readableFileSize(maxSize) + "  %p% " + text);
	}
	
}
void XQFileTableWidget::renamelocalFile(QTableWidgetItem* item)
{
	QFile file(item->data(Qt::UserRole).toString());//打开文件
	if (file.rename(QFileInfo(file).path() + "/" + item->text()))//重命名文件
	{
		item->setData(Qt::UserRole, QFileInfo(file).filePath());//成功更新数据
	}
	else//重命名失败
	{
		item->setText(QFileInfo(file).fileName());//失败写回原先的名字
	}
}
void XQFileTableWidget::renameremoteFile(QTableWidgetItem* item)
{
	for (auto& file: m_cloudFile)
	{
		if (file.md5 == item->data(FILEMD5).toString());
		{
			emit renameRemoteFile(this, &file, item->text());
			return;
		}
	}
	
}
void XQFileTableWidget::updataTabel()
{
	fileChange();
}

void XQFileTableWidget::copyLinkPath(int row)
{
	auto fileName = this->item(row, Type::name)->text();
	QString dirName = QDir(dirPath()).dirName();

	// 获取系统剪切板
	QClipboard* clipboard = QApplication::clipboard();
	// 复制文本到剪切板
	QString text = dirName+"/"+fileName;
	clipboard->setText(text);

}

void XQFileTableWidget::setDirPath(const QString& dir)
{
	if (dir.isEmpty())
		return;
	m_DirPath = dir;
	if (!m_DirPath.isEmpty())
		m_Watcher->removePath(m_DirPath);
	m_Watcher->addPath(dir);
	m_DirPath = dir;
	tableFileInit();
}

void XQFileTableWidget::setNameFilters(const QStringList& Filters)
{
	m_nameFilters = Filters;
}
size_t XQFileTableWidget::tableFileSizeSum()
{
	size_t size = 0;
	// 遍历行
	for (int row = 0; row < rowCount(); row++)
	{
		// 获取单元格指针
		QTableWidgetItem* item = this->item(row, XQFileTableWidget::Type::size);
		if (item)
		{
			size += item->data(Qt::UserRole).toLongLong();
		}
	}
	return size;
}
size_t XQFileTableWidget::fileMaxSize()
{
	return m_fileMaxSize;
}
size_t XQFileTableWidget::fileSumMaxSize()
{
	return m_fileSumMaxSize;
}
bool XQFileTableWidget::changeNameFlag()
{
	return m_changeNameFlag;
}
bool XQFileTableWidget::isLocalFile(int row)const
{
	auto item = this->item(row, Type::locality);
	if (item == nullptr)
		return false;
	return item->data(FILEEXIST).toBool();
}
bool XQFileTableWidget::isRemoteFile(int row)const
{
	auto item = this->item(row, Type::cloud);
	if (item == nullptr)
		return false;
	return item->data(FILEEXIST).toBool();
}
int XQFileTableWidget::nameToRow(const QString& name) const
{
	// 遍历行
	for (int row = 0; row < rowCount(); row++) {
		// 获取单元格指针
		QTableWidgetItem* item = this->item(row, XQFileTableWidget::Type::name);
		if (item!=nullptr)
		{
			if (name == item->text())//找到了
				return row;
		}
	}
	return -1;
}
int XQFileTableWidget::md5ToRow(const QString& md5) const
{
	// 遍历行
	for (int row = 0; row < rowCount(); row++) {
		// 获取单元格指针
		QTableWidgetItem* item = this->item(row, Type::name);
		if (item != nullptr)
		{
			if (md5 == item->data(FILEMD5).toString())//找到了
				return row;
		}
	}
	return -1;
}
QString XQFileTableWidget::rowToMd5(int row) const
{
	QTableWidgetItem* item = this->item(row, Type::name);
	if (item != nullptr)
	{
		return item->data(FILEMD5).toString();
	}
	return QString();
}
void XQFileTableWidget::setFileMaxSize(const size_t size)
{
	m_fileMaxSize = size;
}

void XQFileTableWidget::setFileSumMaxSize(const size_t size)
{
	m_fileSumMaxSize = size;
}

void XQFileTableWidget::setChangeNameFlag(bool flag)
{
	m_changeNameFlag = flag;
}
