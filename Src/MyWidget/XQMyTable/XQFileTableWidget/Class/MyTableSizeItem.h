﻿#ifndef MYTABLESIZEITEM_H
#define MYTABLESIZEITEM_H
#include<QTableWidgetItem>
//重载了排序根据用户数值排序
class MyTableSizeItem : public QTableWidgetItem
{
public:
    MyTableSizeItem(const QString& text) : QTableWidgetItem(text) {}

    bool operator<(const QTableWidgetItem& other) const override
    {
        // 自定义排序准则
        auto otherSize = other.data(Qt::UserRole).toLongLong();
        auto size = data(Qt::UserRole).toLongLong();
        return otherSize < size;
    }
};
#endif // !MYTABLESIZEITEM_H


