﻿#ifndef XQFILESELECTIONWIDGET_H
#define XQFILESELECTIONWIDGET_H
#include"XQFileTableWidget.h"
#include"XQHead.h"
//文件选择小部件
class XQFileSelectionWidget:public XQFileTableWidget
{
	Q_OBJECT
public:
	XQFileSelectionWidget(QWidget* parent = nullptr);
	~XQFileSelectionWidget();
	//返回当窗口不是活动窗口时自动关闭窗口
	bool activityAutoClose()const;
public slots:
	//设置当窗口不是活动窗口时自动关闭窗口
	void setActivityAutoClose(bool flag);
signals://信号
	
	//选择信号
	void selectFile(const QString& file);
protected:
	void init();
	//窗口状态改变了
	void changeEvent(QEvent* event)override;
	void closeEvent(QCloseEvent* event)override;
	
protected:
	XQGraphicsViewWidget* m_GraphicsViewWidget = nullptr;//图片查看器
	bool m_AutoClose = false;//是否自动关闭窗口
};
#endif // !XQFileSelectionWidget_H
