﻿#ifndef XQLISTEXHIBITWIDGET_H
#define XQLISTEXHIBITWIDGET_H
#include<QWidget>
#include"XQHead.h"
//展示的列表Widget
class XQListExhibitWidget:public QWidget
{
	Q_OBJECT
public:
	XQListExhibitWidget(QWidget* parent);
	~XQListExhibitWidget();
	//获取数据
	XQListData* data()const;
public slots://槽函数
	//设置数据
	virtual void setData(XQListData* data);
signals://信号
	void free();//释放自己
protected://隐藏的函数
	//更新数据重载数据对qwidget的画面更新
	virtual void updata();
protected://初始化
protected://事件
protected://数据
	XQListData* m_data=nullptr;//数据
};
#endif // !XQListExhibitWidget_H
