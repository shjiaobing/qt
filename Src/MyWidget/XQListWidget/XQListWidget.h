﻿#ifndef XQLISTWIDGET_H
#define XQLISTWIDGET_H
#include<QWidget>
#include"XQHead.h"
class XQListWidget :public QWidget
{
	Q_OBJECT
public:
	XQListWidget(QWidget* parent=nullptr);
	~XQListWidget();
	//获取间距
	int spacing()const;
	//获取展示的小部件大小
	QSize exhibitWidgetSize()const;
	QListWidget* listWidget()const;
	//返回全部数据
	const QList<XQListData*>& datas()const;
	//返回查询按钮
	XQComboCheckButton* findButton()const;
public slots:
	//添加数据
	void addData(XQListData* data);
	//当前页第一个插入
	void insertCurrentPage(XQListData* data);
	//删除数据
	void removeData(XQListData* data);
	//清空数据
	void clearData();
	//显示全部数据
	void showDataAll();
	//显示搜索的数据
	void showSearchData();
	//显示当前页的小部件,更新
	void showListExhibitWidget(size_t paging);
	void showListExhibitWidget();
	//设置创建展示控件的方法函数
	void setExhibitWidgetFunc(std::function<XQListExhibitWidget* ()> func);
	//设置展示的小部件的大小
	void setExhibitWidgetSize(QSize size);
	//设置间距
	void setSpacing(int Spacing);
	//清空列表显示
	void clearList();
	//返回QListWidget
	QListWidget* listWidget();
	//signals://信号
signals://信号
	//数据数量改变
	void dataCountChange(size_t count);
protected://初始化
	//初始化
	void init();
	void init_ui();
	QBoxLayout* init_paging();
protected://隐藏的函数
	//添加一个小部件
	bool addWidget(QWidget* widget);
	void clearWidget();
	//在已有的数据中搜索 
	void searchData();
	//计算当前一页数量
	int curNumberPages();
	//设置页数
	void setNumberPages(int min,int max,int curVal,const QString& text);
protected://事件
	void resizeEvent(QResizeEvent* event)override;
	void showEvent(QShowEvent* event)override;
protected://变量
	QComboBox* m_findEdit = nullptr;//搜索组合框
	XQComboCheckButton* m_findBtn = nullptr;//查询按钮
	QLabel* m_count= nullptr;//数量
	QLabel* m_Pages = nullptr;//页数
	QListWidget* m_listWidget=nullptr;
	QSpinBox* m_paging = nullptr;//分页微调框
	size_t m_onePagesCount = 0;//一页数量
	size_t m_nSel = 0;//记录的当前页码
	size_t m_sumCount = 0;//记录的总数量
	QList<XQListData*>* m_showData=nullptr;//显示的数据
	QList<XQListData*> m_data;//数据
	QList<XQListData*> m_SearchData;//搜索的数据
	std::function<XQListExhibitWidget* ()> m_newExhibitWidget=nullptr;//展示控件的方法
	QSize m_exhibitSize=QSize(100,100);//展示的小部件大小
};
#endif