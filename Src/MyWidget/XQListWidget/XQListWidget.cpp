﻿#include"XQListWidget.h"
#include"XQListExhibitWidget.h"
#include"XQListData.h"
#include<QListWidgetItem>
#include<QCoreApplication>
#include<QSpinBox>
#include<QLabel>
#include<QDebug>
XQListWidget::XQListWidget(QWidget* parent)
{
	init();
}

XQListWidget::~XQListWidget()
{
	clearList();
}
int XQListWidget::spacing() const
{
	if (m_listWidget != nullptr)
		return m_listWidget->spacing();
	return 0;
}
QSize XQListWidget::exhibitWidgetSize() const
{
	return m_exhibitSize;
}
QListWidget* XQListWidget::listWidget() const
{
	return m_listWidget;
}
const QList<XQListData*>& XQListWidget::datas() const
{
	return m_data;
}
XQComboCheckButton* XQListWidget::findButton() const
{
	return m_findBtn;
}
void XQListWidget::addData(XQListData* data)
{
	if(data!=nullptr)
	{
		m_data << data;
		data->setParent(this);
		emit dataCountChange(m_data.count());
	}
}
void XQListWidget::insertCurrentPage(XQListData* data)
{
	if (data != nullptr)
	{
		int num = curNumberPages();//一页数量
		int Pages = m_paging->value();//当前第几页
		if (Pages == 0)
			Pages = 1;
		int nSel = num * (Pages - 1);

		m_data.insert(nSel,data);
		data->setParent(this);
		emit dataCountChange(m_data.count());
	}
}
void XQListWidget::removeData(XQListData* data)
{
	if (data != nullptr)
	{
		--m_sumCount;
		data->deleteLater();
		m_data.remove(m_data.indexOf(data));

		QList<QListWidgetItem*> itemList = m_listWidget->findItems("*", Qt::MatchWildcard);
		for (auto& item : itemList)
		{
			auto widget = (XQListExhibitWidget*)m_listWidget->itemWidget(item);
			if(widget->data()== data)
			{
				m_listWidget->removeItemWidget(item);
				widget->deleteLater();
				delete item;
				break;
			}
		}
		emit dataCountChange(m_data.count());
	}
	
}
void XQListWidget::clearData()
{
	if (m_data.isEmpty())
		return;
	clearList();
	for (auto&data:m_data)
	{
		data->deleteLater();
	}
	m_data.clear();
	m_sumCount = 0;
	m_nSel = 0;
	emit dataCountChange(m_data.count());
}
void XQListWidget::showDataAll()
{
	m_showData = &m_data;
	showListExhibitWidget();
}
void XQListWidget::showSearchData()
{
	m_showData = &m_SearchData;
	showListExhibitWidget();
}
int XQListWidget::curNumberPages()
{
	if (m_listWidget == nullptr || m_exhibitSize.isNull())
		return 0;
	QSize exhiSize = m_exhibitSize;// 展示的小部件的大小
	QRect listRect = m_listWidget->viewport()->rect();// 获取 QListWidget 的当前可视区域
	int spacing=m_listWidget->spacing();//间距
	int rowCount = (listRect.width() - spacing-1) / (exhiSize.width()+ spacing);
	int columnCount = (listRect.height() ) / (exhiSize.height() + spacing);
	/*qInfo() << exhiSize << listRect<< spacing;
	qInfo() << rowCount<< columnCount<< rowCount* columnCount;*/
	return  (rowCount * columnCount);
}
void XQListWidget::setNumberPages(int min, int max, int curVal, const QString& text)
{
	if (m_paging == nullptr || m_Pages == nullptr)
		return;
	m_paging->setRange(min, max);
	if(curVal>0)
	m_paging->setValue(curVal);
	m_Pages->setText(text);
}
void XQListWidget::resizeEvent(QResizeEvent* event)
{
	int num = curNumberPages();
	if(isVisible()&& num != 0&& m_showData!=nullptr)
	{
		if ((m_sumCount == m_showData->count() && m_paging->value() == m_nSel && (m_onePagesCount == num)))
			return;
		//尽量在其附近
		int nSel = m_onePagesCount * (m_nSel) / num;
		showListExhibitWidget(nSel);
	}
}
void XQListWidget::showEvent(QShowEvent* event)
{
	showListExhibitWidget();
}
void XQListWidget::showListExhibitWidget(size_t paging)
{
	int num = curNumberPages();
	if (paging == 0 || ((m_sumCount == m_showData->count()&&m_listWidget->count()== num) && paging == m_nSel && (num <= m_onePagesCount)))
	{
		m_listWidget->sortItems();
		return;
	}
	m_onePagesCount= curNumberPages();
	m_nSel = paging;
	size_t MaxPaging = 0;//页数
	//切换页码
	disconnect(m_paging, &QSpinBox::valueChanged, this, QOverload<size_t>::of(&XQListWidget::showListExhibitWidget));
	m_listWidget->setBatchSize(num);
	clearList();
	if(m_showData != nullptr&&!m_showData->isEmpty()&& m_onePagesCount!=0)
	{
		size_t nSel = (paging-1) * m_onePagesCount;
		for (size_t count=0; nSel < m_showData->count()&& count< m_onePagesCount; ++nSel,++count)
		{
			XQListData* data = (*m_showData)[nSel];
			auto widget = data->newWidget();
			if (widget == nullptr)
			{
				if (m_newExhibitWidget != nullptr)
					widget = m_newExhibitWidget();
			}
			if (widget == nullptr)
				return;
			{
				widget->setData(data);
				widget->setFixedSize(m_exhibitSize);
				addWidget(widget);
			}
		}
	}
	else//数据是空的情况
	{
		setNumberPages(0, 0, 0, QString("页数:%0").arg(MaxPaging));
		return;
	}
	m_sumCount = m_showData->count();
	{
		int MaxPaging = m_showData->count() / m_onePagesCount;
		if (m_showData->count() % m_onePagesCount != 0)
			++MaxPaging;
		setNumberPages(1, MaxPaging, paging, QString("页数:%1").arg(MaxPaging));
	}
	//切换页码
	connect(m_paging, &QSpinBox::valueChanged, this, QOverload<size_t>::of(&XQListWidget::showListExhibitWidget));
	m_listWidget->repaint();
}
void XQListWidget::showListExhibitWidget()
{
	if (m_showData == nullptr)
		return;
	if (m_paging->value() == 0)
	{
		showListExhibitWidget(1);
	}
	else
	{
		showListExhibitWidget(m_paging->value());
	}
		
}
void XQListWidget::setExhibitWidgetFunc(std::function<XQListExhibitWidget* ()> func)
{
	m_newExhibitWidget = func;
}
void XQListWidget::setExhibitWidgetSize(QSize size)
{
	m_exhibitSize = size;
}
void XQListWidget::setSpacing(int Spacing)
{
	if(m_listWidget!=nullptr)
		m_listWidget->setSpacing(Spacing);
}
void XQListWidget::clearList()
{
	// 遍历 QListWidget 中的项
	QList<QListWidgetItem*> itemList = m_listWidget->findItems("*", Qt::MatchWildcard);
	for (auto& item: itemList)
	{
		auto widget = m_listWidget->itemWidget(item);
		m_listWidget->removeItemWidget(item);
		widget->close();
		widget->deleteLater();
		delete item;
	}
	/*QCoreApplication::processEvents(QEventLoop::AllEvents);*/
}
QListWidget* XQListWidget::listWidget()
{
	return m_listWidget;
}
bool XQListWidget::addWidget(QWidget* widget)
{
	widget->setParent(m_listWidget);
	auto item = new QListWidgetItem(m_listWidget);
	item->setSizeHint(widget->size());
	m_listWidget->addItem(item);
	m_listWidget->setItemWidget(item, widget);
	return true;
	//// 获取 QListWidgetItem 的当前位置和大小
	//QRect itemRect = m_listWidget->visualItemRect(item);
	//// 获取 QListWidget 的当前可视区域
	//QRect visibleRect = m_listWidget->viewport()->rect();
	//// 判断是否有滚动条
	//bool maxVal = visibleRect.contains(itemRect);
	//if (maxVal) {
	//	return true;
	//}
	//else {
	//	auto widget = m_listWidget->itemWidget(item);
	//	m_listWidget->removeItemWidget(item);
	//	widget->deleteLater();
	//	delete item;
	//	return false;
	//}
}

void XQListWidget::clearWidget()
{
	int count=m_listWidget->count();
	for (size_t i = 0; i < count; i++)
	{
		auto item = m_listWidget->item(i);
		auto widget = m_listWidget->itemWidget(item);
		if (widget != nullptr)
			widget->deleteLater();
	}
}
