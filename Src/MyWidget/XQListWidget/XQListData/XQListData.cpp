﻿#include"XQListData.h"
#include"XQListExhibitWidget.h"
#include<QDebug>
XQListData::XQListData(QObject* parent)
	:QObject(parent)
{
}

XQListData::~XQListData()
{
}
XQListExhibitWidget* XQListData::newWidget()
{
	if(m_newExhibitWidget==nullptr)
		return nullptr;
	m_widget = m_newExhibitWidget();
	connect(m_widget, &QWidget::destroyed, [this] {m_widget = nullptr; });
	emit widgetGenerate(m_widget);
	/*qInfo() << "产生了" << m_widget;*/
	return m_widget;
}
XQListExhibitWidget* XQListData::widget() const
{
	return m_widget;
}

bool XQListData::textExists(const QString& text, const QList<int>& items)
{
	return false;
}

bool XQListData::textExists(const QString& text, const QStringList& items)
{
	return false;
}

void XQListData::setExhibitWidgetFunc(std::function<XQListExhibitWidget* ()> func)
{
	m_newExhibitWidget = func;
}
