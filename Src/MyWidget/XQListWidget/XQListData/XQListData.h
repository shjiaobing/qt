﻿#ifndef  XQLISTDATA_H
#define XQLISTDATA_H
#include<QObject>
#include"XQHead.h"
class XQListData:public QObject
{
	Q_OBJECT
public:
	XQListData(QObject* parent = nullptr);
	virtual ~XQListData();
	//获取新的widget
	XQListExhibitWidget* newWidget();
	//获取widget
	XQListExhibitWidget* widget()const;
//public slots://槽函数
//字符串是否存在在当前数据中--用在搜索框
virtual	bool textExists(const QString& text,const QList<int>& items);
virtual	bool textExists(const QString& text, const QStringList& items);
//设置创建展示控件的方法函数
void setExhibitWidgetFunc(std::function<XQListExhibitWidget* ()> func);
signals://信号
	//释放请求
	void freeRequest();
	//有新的widget产生
	void widgetGenerate(XQListExhibitWidget* widget);
protected://

protected://初始化
protected://事件
protected://数据
	XQListExhibitWidget*  m_widget = nullptr;
	std::function<XQListExhibitWidget* ()> m_newExhibitWidget = nullptr;//展示控件的方法
};
#endif // ! XQListData_H
