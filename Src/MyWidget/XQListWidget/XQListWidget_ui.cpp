﻿#include"XQListWidget.h"
#include"XQComboCheckButton.h"
#include<QListWidget>
#include<QBoxLayout>
#include<QSpinBox>
#include<QLabel>
#include<QComboBox>
#include<QPushButton>
void XQListWidget::init_ui()
{
	m_listWidget = new QListWidget(this);
	//m_listWidget->setFont(QFont("微软雅黑", 15));
	// 设置 QListWidget 控件的布局方式为 Batched（尽量填满一行再换行）
	m_listWidget->setLayoutMode(QListView::SinglePass);
	m_listWidget->setViewMode(QListWidget::IconMode);
	m_listWidget->setSpacing(30);//设置间距
	m_listWidget->setDragEnabled(false);//设置禁止拖拽
	 // 开启统一尺寸
	m_listWidget->setUniformItemSizes(true);
    // 开启分组渲染
	m_listWidget->setResizeMode(QListView::Fixed);
	//m_listWidget->setAlternatingRowColors(true); // 启用交替行颜色
	m_listWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);//禁用垂直滚动条
	//设置内容布局
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom, this);//从上到下
	layout->setContentsMargins(0, 0, 0, 0);
	layout->addLayout(init_paging());
	layout->addWidget(m_listWidget);
	setLayout(layout);
}

QBoxLayout* XQListWidget::init_paging()
{
	m_count = new QLabel("数量:0");
	m_Pages = new QLabel("页数:0");
	m_paging = new  QSpinBox(this);
	m_paging->setRange(0,0);

	m_findEdit = new QComboBox(this);
	m_findEdit->setMinimumWidth(500);
	m_findEdit->setEditable(true);
	m_findBtn = new XQComboCheckButton("筛选",this);
	m_findBtn->setMinimumWidth(100);
	//设置内容布局
	auto layout = new QBoxLayout(QBoxLayout::LeftToRight);//从左到右
	layout->addWidget(new QLabel("列表搜索", this));
	layout->addWidget(m_findEdit);
	layout->addWidget(m_findBtn);
	layout->addStretch(1);
	layout->addWidget(m_count);
	layout->addWidget(m_paging);
	layout->addWidget(m_Pages);
	return layout;
}

