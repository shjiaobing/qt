﻿#include "XQAnimationButton.h"

void XQAnimationButton::setPixmaps(const QVector<QPixmap>& LPvector)
{
    m_pixmatps = LPvector;
    update();
}

void XQAnimationButton::setMouseLeavePixmap(const QPixmap& pixmap)
{

    m_pixmatps[int(mouseState::mouseLeave)] = pixmap;
    if (m_State == mouseState::mouseLeave)
        update();
}

void XQAnimationButton::setMouseEnterPixmap(const QPixmap& pixmap)
{
    m_pixmatps[int(mouseState::mouseEnter)] = pixmap;
    if (m_State == mouseState::mouseEnter)
        update();
}

void XQAnimationButton::setMousePressPixmap(const QPixmap& pixmap)
{
    m_pixmatps[int(mouseState::mousePress)] = pixmap;
    if (m_State == mouseState::mousePress)
        update();
}

void XQAnimationButton::setMouseReleasePixmap(const QPixmap& pixmap)
{
    m_pixmatps[int(mouseState::mouseRelease)] = pixmap;
    if (m_State == mouseState::mouseRelease)
        update();
}

void XQAnimationButton::setButtonForbiddenPixmap(const QPixmap& pixmap)
{
    m_pixmatps[int(mouseState::buttonForbidden)] = pixmap;
  /*  if (m_State == buttonForbidden)
        update();*/
}

void XQAnimationButton::setEnabled(bool flag)
{
    QPushButton::setEnabled(flag);
    update();
}
