﻿#ifndef XQCOMBOCHECKBUTTON_H
#define XQCOMBOCHECKBUTTON_H
#include<QPushButton>
#include"XQHead.h"
//组合多选下拉框按钮
class  XQComboCheckButton:public QPushButton
{
public:
	XQComboCheckButton(QWidget* parent=nullptr);
	XQComboCheckButton(const QString& text,QWidget* parent = nullptr);
	~XQComboCheckButton();
	//已选中的项的索引
	QList<int> selectIndexs()const;
	//已选中的文本合集
	QStringList selectTexts()const;
	//全部项的文本
	QStringList textAll()const;
	QStandardItem* item(int index)const;
public slots://槽函数
	void showView();
	void setShowRowCount(int count);
	void addItem(const QStringList& items, bool on = false);
	int addItem(const QString& item, bool on = false);
	int addItem(const QString& item, const QVariant& value, int role=Qt::UserRole, bool on = false);
	void setCheckState(int index, bool on = true);
	void setCheckState(const QString& text, bool on = true);
	void setSelectIndexs(const QList<int>& list);
	void clear();
protected://变量
	void init();
	void init_ui();
protected://变量
	QListView* m_list_view = nullptr;
	QStandardItemModel* m_model = nullptr;
	QPushButton* m_comboBoxBtn= nullptr;
	//QSize m_size;//列表大小
	int m_showRow = 0;
};
#endif