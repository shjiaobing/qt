﻿#include"XQLabelButton.h"
#include"XQAlgorithm.h"
#include"XQMouseEventFilter_enter.hpp"
#include"XQMouseEventFilter_Press.hpp"
#include"XQMouseEventFilter_leave.hpp"
#include"XQBubbleWidget.h"
#include<QLabel>
#include<QMouseEvent>
#include<QPushbutton>
#include<QCoreApplication>
#include<QDebug>
void XQLabelButton::init()
{
	if (m_bubble==nullptr)
		m_bubble = new XQBubbleWidget();
	init_ui();
	/*qInfo() << "准备css文件";*/
	setCssFile(this, ":/XQLabelButton/style.css");
	////测试图标
	//m_icon->setPixmap(QPixmap(":/sidebar/image/sidebar/addOne.png").scaled(height(), height()));

	//鼠标进入后背景变色
	m_text->installEventFilter(new XQMouseEventFilter_Enter([=] {
		if(m_isShowBubble)
			m_bubble->showText(mapToGlobal(QPoint()), m_text->text());//气泡提示显示标题文本
		if (m_checked)
			return;//选中状态下不变色
		QString css = ".QWidget#background {background-color: rgb(240,240,240);border: 1px solid rgb(1,247,227);}";
		m_background->setStyleSheet(css);
		}, m_text));
	//鼠标离开
	m_text->installEventFilter(new XQMouseEventFilter_Leave([=] {
		if (m_isShowBubble)
			m_bubble->hide();//隐藏标题文本
		if (m_checked)
			return;//选中状态下不变色
		m_background->setStyleSheet("");
		}, m_text));
	//鼠标按下
	//发送关闭按钮请求信号
	connect(m_clsBtn, &QPushButton::pressed, this,&XQLabelButton::closeWidgetRequest);
	//在不是选中状态下发送选择信号
	m_text->installEventFilter(new XQMouseEventFilter_Press([=](QMouseEvent* ev) {if (!m_checked)emit selectWidget(); }, m_text));
}