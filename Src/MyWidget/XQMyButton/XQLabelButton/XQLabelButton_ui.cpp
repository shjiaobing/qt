﻿#include"XQLabelButton.h"
#include<QBoxLayout>
#include<QLabel>
#include<QPushButton>
void XQLabelButton::init_ui()
{
	m_background = new QWidget(this);
	if (m_icon == nullptr)
		m_icon = new QLabel(m_background);
	if(m_text==nullptr)
		m_text = new QLabel(m_background);
	m_clsBtn = new QPushButton(m_background);
	m_background->setObjectName("background");
	m_icon->setObjectName("icon");
	m_icon->hide();
	m_text->setObjectName("text");
	m_clsBtn->setObjectName("close");
	m_icon->setScaledContents(true);
	
	{//背景内容设置
		//设置内容布局
		auto layout = new QBoxLayout(QBoxLayout::LeftToRight, m_background);//从左到右
		layout->setContentsMargins(5, 2, 5, 2);
		layout->setSpacing(0);
		layout->addWidget(m_icon);
		layout->addWidget(m_text);
		layout->addWidget(m_clsBtn,0, Qt::AlignCenter);
		m_background->setLayout(layout);
	}
	{//主界面布局
		auto layout = new QBoxLayout(QBoxLayout::LeftToRight, this);//从左到右
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);
		layout->addWidget(m_background);
		setLayout(layout);
	}
}