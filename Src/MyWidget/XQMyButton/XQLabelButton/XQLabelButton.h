﻿#ifndef XQLABELBUTTON_H
#define XQLABELBUTTON_H
//堆栈小部件里面的标签按钮
#include<QWidget>
#include<QVariant>
#include<QMap>
#include"XQHead.h"
class XQLabelButton:public QWidget
{
	Q_OBJECT
public:
	XQLabelButton(QWidget* parent = nullptr);
	XQLabelButton(const QString& text,QWidget* parent = nullptr);
	~XQLabelButton();
	QString text()const;
	//获取图标
	QPixmap icon()const;
	//获取当前是否被选中
	bool checked()const;
	//获取数据
	QVariant data(int role=0);
	template<typename Role>
	void setData(const QVariant& value, Role role);
	template<typename Role>
	QVariant data(Role role);
	//获取是否显示气泡
	bool isShowBubble()const;
	//是否显示关闭按钮
	bool isCloseButtonShow()const;
public slots:
	//设置文本
	void setText(const QString& text);
	//设置图标
	void setIcon(const QPixmap& pixmap);
	void setIcon(const QIcon& icon);
	//设置选中状态
	void setChecked(bool flag);
	//设置值
	void setData(const QVariant& value, int role=0);
	//设置隐藏关闭按钮
	void setCloseButtonShow(bool show);
	//设置是否显示气泡
	void setShowBubble(bool show);
	//设置固定宽度
	void setFixedWidth(int w);
	//设置文本固定宽度
	void setTextFixedWidth(int w);
	//设置文本对齐
	void setTextAlignment(Qt::Alignment flag);
signals://信号
	//关闭当前小部件请求信号
	void closeWidgetRequest();
	//选择当前小部件信号
	void selectWidget();
protected://初始化
	//初始化
	void init();
	void init_ui();
protected://事件
	void resizeEvent(QResizeEvent* event)override;
protected://变量
	bool m_checked = false;//选中状态
	QWidget* m_background = nullptr;//背景
	QLabel* m_icon= nullptr;//图标
	QLabel* m_text = nullptr;//展示的文本按钮
	QPushButton* m_clsBtn = nullptr;//关闭按钮
	QMap<int, QVariant> m_data;//绑定的数据
	static XQBubbleWidget* m_bubble;//提示气泡
	bool m_isShowBubble = true;//是否显示气泡
};

template<typename Role>
inline QVariant XQLabelButton::data(Role role)
{
	return std::move(data(int(role)));
}

template<typename Role>
inline void XQLabelButton::setData(const QVariant& value, Role role)
{
	setData(value,int(role));
}
#endif // !XQLabelButton_h