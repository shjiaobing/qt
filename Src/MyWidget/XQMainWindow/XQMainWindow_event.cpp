﻿#include"XQMainWindow.h"
#include"XQStackedWidget.h"
#include"XQAppInfo.h"
#include"XQMySql.hpp"
#include"XQuickSort.hpp"
#include"XQSettingsInfo.h"
#include<QCloseEvent>
#include<QDebug>
void XQMainWindow::showEvent(QShowEvent* event)
{
	auto settingsInfo = appInfo()->settingsInfo();
	if(settingsInfo->saveMainWindowPosition())
	{
		//读取上次的坐标和大小
		auto rect = settingsInfo->value("XQMainWindow-rect", QRect(INT_FAST32_MAX, INT_FAST32_MAX, INT_FAST32_MAX, INT_FAST32_MAX)).toRect();
		if (rect != QRect(INT_FAST32_MAX, INT_FAST32_MAX, INT_FAST32_MAX, INT_FAST32_MAX))
		{
			this->move(rect.topLeft());
			this->resize(rect.size());
		}
	}
}

void XQMainWindow::closeEvent(QCloseEvent* event)
{
	auto settingsInfo = appInfo()->settingsInfo();
	if (settingsInfo->saveMainWindowPosition())
	{
		//写入现在的坐标和大小
		settingsInfo->setValue("XQMainWindow-rect", QRect(pos(), size()));
	}
	m_StackedWidget->clearWidget();//清理下窗口
	if (m_StackedWidget->count() != 0)
		event->ignore();
}
