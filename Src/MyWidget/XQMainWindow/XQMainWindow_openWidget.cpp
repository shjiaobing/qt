﻿#include"XQMainWindow.h"
#include"XQStackedWidget.h"
#include"XQMySql.hpp"
#include"XQUserInfo.h"
#include"XQAppInfo.h"
#include"XQMainSetupWidget.h"
#include<QPushButton>
#include<QLabel>
#include<QDebug>
void XQMainWindow::userLogin(bool autoLogin)
{
	
}

void XQMainWindow::openWidget(int type)
{
	systemSetupWidget();
}

void  XQMainWindow::showStackedWidget(int type,QWidget* widget,const QString& title,const QPixmap& icon)
{
	//窗口创建
	if (m_widgets.find(type) == m_widgets.end() && widget != nullptr)
	{
		m_widgets[type] = widget;
		m_widgetTypes[m_widgets[type]] = type;
		m_StackedWidget->addWidget(m_widgets[type],title, icon);
		/*qInfo() << "新建一个";*/
	}
	m_StackedWidget->setCurrentWidget(m_widgets[type]);
	/*qInfo() << m_widgetTypes.count() << m_widgets.count();*/
}
void XQMainWindow::systemSetupWidget()
{
	if (stackedWidget(XQMainWindowSystemSetup) == nullptr)
	{
		auto widget = new  XQMainSetupWidget(this);
		showStackedWidget(XQMainWindowSystemSetup, widget, "系统设置");
	}
	else
	{
		showStackedWidget(XQMainWindowSystemSetup);
	}
	
}
