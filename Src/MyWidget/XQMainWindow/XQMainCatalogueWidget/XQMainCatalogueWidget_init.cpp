﻿#include"XQMainCatalogueWidget.h"
#include"XQAppInfo.h"
#include"XQAlgorithm.h"
#include<QTreeWidget>
#include<QLabel>
#include<QHeaderView>
#include<QBoxLayout>
void XQMainCatalogueWidget::init()
{
	setCssFile(this, ":/XQMainCatalogueWidget/style.css");
	init_ui();
	init_catalogue();
	auto userInfo = appInfo()->userInfo();
	connect(userInfo, &XQUserInfo::stateChange, this, &XQMainCatalogueWidget::userStateChange, Qt::QueuedConnection);
}

void XQMainCatalogueWidget::init_ui()
{
	m_catalogue = new QTreeWidget(this);
	// 隐藏头部控件
	m_catalogue->header()->hide();
	m_catalogue->setAlternatingRowColors(true);
	//设置内容布局
	auto widget = new QWidget(this);
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom, widget);//从上到下
	layout->setContentsMargins(0, 0, 0, 0);
	layout->addLayout(init_headPortrait());
	layout->addWidget(m_catalogue);
	setWidget(widget);
	m_head->hide();//隐藏头像框
	//设置自定义标题栏
	m_title = new QLabel("导航菜单", this);
	m_title->setAlignment(Qt::AlignCenter);
	m_title->setObjectName("title");
	
	setTitleBarWidget(m_title);
	setContextMenuPolicy(Qt::CustomContextMenu);
}
QBoxLayout* XQMainCatalogueWidget::init_headPortrait()
{
	m_head = new QLabel(this);
	m_head->setFixedSize(100, 100);
	//设置内容布局
	auto layout = new QBoxLayout(QBoxLayout::LeftToRight);//从左到右
	layout->addStretch(1);
	layout->addWidget(m_head);
	layout->addStretch(1);
	return layout;
}
void XQMainCatalogueWidget::init_catalogue()
{
	catalogue_user();
	catalogue_system();
	m_catalogue->expandAll();
}


