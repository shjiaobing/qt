﻿#include"XQMainCatalogueWidget.h"
#include"XQAppInfo.h"
#include<QLabel>
XQMainCatalogueWidget::XQMainCatalogueWidget(QWidget* parent, bool AutoInit)
	:QDockWidget(parent)
{
	if(AutoInit)
		init();
}

XQMainCatalogueWidget::~XQMainCatalogueWidget()
{

}

XQAppInfo* XQMainCatalogueWidget::appInfo()
{
	return XQAppInfo::Global();
}

void XQMainCatalogueWidget::setHeadPortrait(const QPixmap& image)
{
	if (m_head == nullptr)
		return;
	if(image.isNull())
	{
		m_head->setPixmap(image);
		return;
	}
	auto pixmap = QPixmap(image);
	pixmap = pixmap.scaled(m_head->size());
	pixmap.setMask(QBitmap::fromImage(QPixmap(":/XQLoginWidget/images/portrait/mask.png").scaled(m_head->size()).toImage()));
	m_head->setPixmap(pixmap);
	m_head->update();
}

void XQMainCatalogueWidget::userStateChange(userState state)
{
	auto userInfo = appInfo()->userInfo();
	switch (state)
	{
	case Login:
	{
		//设置圆形图标
		m_head->show();
		setHeadPortrait(userInfo->headPortrait());
	}
	break;
	case Logout:
	{
		m_head->hide();
	}
	break;
	}
}
