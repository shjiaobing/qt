﻿#ifndef XQCHARTVIEW_XYSERIES_H
#define XQCHARTVIEW_XYSERIES_H
#include"XQChartView.h"
class XQChartView_XYSeries:public XQChartView
{
	Q_OBJECT
public:
	XQChartView_XYSeries(QWidget* parent = nullptr);
	~XQChartView_XYSeries();
	QXYSeries* selectSeries()const;
public:
	void setSelectSeries(QXYSeries* series);
	//设置显示方法
	void setShowToolTip(std::function<QString(QXYSeries*, const QPointF&)>&& showFunc);
protected:
	void init();
protected:
	void mouseMoveEvent(QMouseEvent* ev);
protected:
	int findNearestPointIndex(QXYSeries* series,const QPoint& pos);
protected:
	std::function<QString(QXYSeries*, const QPointF&)> m_showFunc;
	XQBubbleWidget* m_bubble = nullptr;
	QXYSeries* m_selectSeries = nullptr;
};
#endif