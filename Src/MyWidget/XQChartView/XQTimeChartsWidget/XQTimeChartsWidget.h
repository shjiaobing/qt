﻿#ifndef XQTimeChartsWidget_H
#define XQTimeChartsWidget_H
#include"XQHead.h"
#include<QWidget>
#include<QHash>
//时间/Double数据图表
class XQTimeChartsWidget :public QWidget
{
	Q_OBJECT
public:
	XQTimeChartsWidget(QWidget* parent = nullptr);
	XQTimeChartsWidget(const QString& title, QWidget* parent = nullptr);
	~XQTimeChartsWidget();
public://获取一些信息
	XQTimeDoubleDataModel* model(const QString& sign)const;
	XQTimeDoubleDataModel* model(int sign)const;
	size_t modelSize()const;
	size_t seriesSize()const;
	//获取按钮选中的时间和涨跌值
	double getTimeChangePercent(const QString& sign, QDateTime* startOut, QDateTime* endOut);
	XQTimeSelectDockWidget* selectWidget()const;
	QDateTimeAxis* AxisX()const;
	QValueAxis* AxisY()const;
	QMainWindow* mainWindow()const;
	XQStatusBarWidget* statusBar()const;
public://设置 
	void addModel(const QString& sign,XQTimeDoubleDataModel* model);
	void addModel(int sign, XQTimeDoubleDataModel* model);
	void removeModel(const QString& sign, bool Delete=false);
	void removeModel(int sign, bool Delete = false);
	void clearModel();
	void addCharts(const QString& sign, QXYSeries* series);
	void addCharts(int sign, QXYSeries* series);
	void removeCharts(const QString& sign);
	void removeCharts(int sign);
	//用户单机切换时间 图表重新显示
	void selectChartsTime();
	//线程等待
	void wait();
signals://信号
	//状态栏显示提示信息
	void showMessage(const QString& text, int timeout = 0);
	//进度条
	void progressValue(size_t val, size_t maxSize, size_t delay = 5000);
protected://
	//删除清理
	void clearSeries(const QString& sign);
	//新Series属性设置初始化
	void newSeries_setting(const QString& sign, QXYSeries* series, const QDateTime& startDate, const QDateTime& endDate);
	//初始化设置坐标抽范围
	void AxisRange_setting(const QDateTime& startDate, const QDateTime& endDate);
	//显示数据提示文本
	QString ShowDataToolTip(QXYSeries* series, const QPointF& pointf);
protected://初始化
	virtual void init();
	virtual void init_ui();
	virtual void init_MainWindow();
	virtual void init_selectWidget();
	virtual void init_statusbar();//状态栏初始化
	virtual void init_chartView();//图表视图初始化
protected://

protected:
	XQTaskPool* m_task = nullptr;//任务池
	QHash<QString, XQTimeDoubleDataModel*>m_model;//数据模型
	QHash<QString, QXYSeries*>m_Series;//
	QMainWindow* m_mainWindow = nullptr;
	XQChartView_XYSeries* m_chartView = nullptr;//图表视图
	XQTimeSelectDockWidget* m_selectWidget = nullptr;//时间间隔选择浮动小部件
	XQStatusBarWidget* m_statusbar = nullptr;//状态栏
};
#endif // !XQTimeChartsWidget_H
