﻿#include"XQTimeChartsWidget.h"
#include"XQStatusBarWidget.h"
#include"XQTitleBarWidget.h"
#include"XQTaskPool.h"
#include"XQFuncEventFilter.hpp"
#include"XQTimeSelectDockWidget.h "
#include"XQLog.hpp"
#include"XQChartView_XYSeries.h"
#include<QXYSeries>
#include<QMouseEvent>
#include<QMainWindow>
#include<QChartView>
#include<QDateTimeAxis>
#include<QValueAxis>
#include<QBoxLayout>
#include<QMenu>

void XQTimeChartsWidget::init()
{
	installEventFilter(new XQFuncEventFilter(this));
	m_task = new XQTaskPool(this);
	m_mainWindow = new QMainWindow();
	init_chartView();
	init_statusbar();
	init_selectWidget();
	init_MainWindow();
	init_ui();
	resize(1200, 600);
}

void XQTimeChartsWidget::init_ui()
{
	auto title = new XQTitleBarWidget(this);
	title->setButtons(15);
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom, this);
	layout->addWidget(title);
	layout->addWidget(m_mainWindow);
	setLayout(layout);
}

void XQTimeChartsWidget::init_MainWindow()
{
	m_mainWindow->setCentralWidget(m_chartView);
	m_mainWindow->setStatusBar(m_statusbar);
	m_mainWindow->setParent(this);
	m_mainWindow->addDockWidget(Qt::BottomDockWidgetArea, m_selectWidget);
}

void XQTimeChartsWidget::init_selectWidget()
{
	m_selectWidget = new XQTimeSelectDockWidget("时间选择",m_mainWindow);
	m_selectWidget->timeButtonPressed(TimeInterval::oneMonth);
	connect(m_selectWidget,&XQTimeSelectDockWidget::selectTime,this,&XQTimeChartsWidget::selectChartsTime);
}

void XQTimeChartsWidget::init_statusbar()
{
	m_statusbar = new XQStatusBarWidget(this);
	connect(this, &XQTimeChartsWidget::showMessage, m_statusbar, &XQStatusBarWidget::showMessage, Qt::QueuedConnection);
	connect(this, &XQTimeChartsWidget::progressValue, m_statusbar, &XQStatusBarWidget::setProgressValueCount, Qt::QueuedConnection);
}

void XQTimeChartsWidget::init_chartView()
{
	m_chartView = new XQChartView_XYSeries(this);
	
	auto chart = m_chartView->chart();
	//坐标抽
	auto AxisX = new QDateTimeAxis(chart);
	AxisX->setTickCount(10);
	AxisX->setTitleText("日期");
	AxisX->setFormat("yyyy-MM-dd");
	chart->addAxis(AxisX, Qt::AlignBottom);

	auto AxisY = new QValueAxis(chart);
	AxisY->setTitleText("");
	chart->addAxis(AxisY, Qt::AlignLeft);

	auto View = static_cast<XQChartView_XYSeries*>(m_chartView);
	View->setShowToolTip([this](QXYSeries* series, const QPointF& pointf) { return ShowDataToolTip(series, pointf); });
}

