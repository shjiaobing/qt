﻿#include "XQTyporaWidget.hpp"
#include"XQMarkDownEditorDataInfo.h"
#include<QResizeEvent>
#include<QCloseEvent>
#include<Windows.h>
void XQTyporaWidget::resizeEvent(QResizeEvent* event)
{
#ifdef WIN32
	if (!(m_data->model()==editModel::Read))
	{
		MoveWindow((HWND)m_typoraHWND, 0, 0, size().width() * m_scale, (size().height()) * m_scale, TRUE);
	}
#endif // WIN32
}

void XQTyporaWidget::closeEvent(QCloseEvent* event)
{
	deleteLater();
}
