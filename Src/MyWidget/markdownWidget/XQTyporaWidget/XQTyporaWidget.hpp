﻿#ifndef XQTYPORAWIDGET_H
#define XQTYPORAWIDGET_H
#include<QWidget>
class QWindow;
class XQMarkDownEditorDataInfo;
//捕获Typora编辑器
class XQTyporaWidget:public QWidget
{
	Q_OBJECT
public:
	XQTyporaWidget(XQMarkDownEditorDataInfo* data,QWidget* parent = nullptr);
	~XQTyporaWidget();
	//打开Typora
	template <typename ...Args>
	void open_md(Args&& ...args);
public slots:
	//打开Typora
	void open_md();
	//查询窗口句柄
	size_t FindWindowHWND();
signals://信号
	
protected://事件
	//初始化
	void init();
	void init_typora();
protected://事件
	//窗口大小改变事件
	void resizeEvent(QResizeEvent* event)override;
	//窗口关闭事件
	void closeEvent(QCloseEvent* event)override;
protected:
	XQMarkDownEditorDataInfo* m_data = nullptr;
	WId m_typoraHWND = 0;//typora的窗口句柄
	QWindow* m_typoraWindow = nullptr;//typora的窗口对象
	float m_scale = 0;//缩放值
};
template<typename ...Args>
inline void XQTyporaWidget::open_md(Args && ...args)
{
	open_md();
	QObject::connect(this, &XQTyporaWidget::destroyed, std::forward<Args>(args)...);
}
#endif // !XQTyporaWidget_H

