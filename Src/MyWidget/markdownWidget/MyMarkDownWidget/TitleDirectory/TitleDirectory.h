﻿#ifndef TitleDirectory_H
#define MARKDOWNDIR_H
#include<QTreeView>
#include"XDirectory.hpp"

class QStandardItemModel;
class QStandardItem;
class QMenu;
//目录窗口
class TitleDirectory:public QTreeView
{
	Q_OBJECT
public:
	TitleDirectory(QWidget* parent = nullptr);
	~TitleDirectory();
	//根据索引获取QStandardItem指针
	QStandardItem* itemFromIndex(const QModelIndex& index) const;
public slots:
	//设置目录
	void setDirectory(const QString& Text);
	//设置为展开
	void setExpand(const bool spread = true);
protected:
	//初始化菜单
	void init_Menu();
	void mousePressEvent(QMouseEvent* ev)override;
private:
	QStandardItemModel* m_model=nullptr;
	//XDirectory<QStandardItemModel*>* m_dir=nullptr;//目录
	bool m_expand = true;//展开显示

	QMenu* m_menu = nullptr;//右键菜单
};

#endif // !markDownDir_H
