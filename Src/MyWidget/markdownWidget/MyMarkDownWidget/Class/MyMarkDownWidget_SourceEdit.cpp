﻿#include"MyMarkDownWidget.h"
#include"TitleDirectory.h"
#include"XStringOperation.hpp"
void MyMarkdownWidget::SourceEditTextChanged()
{
	auto cursor = m_SourceEdit->textCursor();
	size_t line = cursor.blockNumber();//获取光标所在行的行号
	if (line == m_currentLine)
		return;
	m_currentLine = line;
	QString PlainText = m_SourceEdit->toPlainText();
	//替换正确的图片路径
	PlainText=XStringOperation::regex_replace( "!\\[.+?\\]\\((.+?)/.+?\\.\\w+?\\)", PlainText, { imageDir() });
	PlainText = XStringOperation::regex_replace("<img src=\"(.+?)//.+?\\.\\w+?\"", PlainText, { imageDir() });
	if (m_SourceEdit == focusWidget() && PlainText != m_MarkDownEdit->toMarkdown())
	{
		m_MarkDownEdit->setMarkdown(PlainText);
		m_TitleDirectory->setDirectory(PlainText);
	}

	//auto list = XStringOperation::strtok(PlainText, "\n",true);
	//qInfo() << list.size();

	//auto widget = focusWidget();
	//if (widget == nullptr)return;
	//m_MarkDownEdit->setFocus();
	//auto Cursor = m_MarkDownEdit->textCursor();
	//Cursor.movePosition(QTextCursor::Start);
	//Cursor.movePosition(QTextCursor::EndOfLine, QTextCursor::MoveAnchor, 1);

	//for (auto& data : list)
	//{
	//	qInfo() << data;
	//	//Cursor.movePosition(QTextCursor::EndOfLine, QTextCursor::MoveAnchor, 1);
	//	if (data.isEmpty())
	//	{
	//		Cursor.insertBlock();
	//		Cursor.insertText("\n123");
	//	}
	//	//qInfo() << data;
	//	Cursor.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, 1);
	//}
	//m_MarkDownEdit->setTextCursor(Cursor);
	//widget->setFocus();
	//

}
void MyMarkdownWidget::SourceEditCursorPositionChanged()
{
	//auto SourceCursor = m_SourceEdit->textCursor();
	//size_t line = SourceCursor.blockNumber();//获取光标所在行的行号
	////qInfo() << line;
	//setCursorRow(m_MarkDownEdit,line);
}