﻿#include"MyMarkDownWidget.h"
#include<QFile>
#include<QFileInfo>
bool MyMarkdownWidget::readTextFile(const QString& fileName)
{
	QFile file(fileName);
	if (file.open(QIODeviceBase::ReadOnly))
	{
		m_SourceEdit->setPlainText(file.readAll());
		return true;
	}
	return false;
}
bool MyMarkdownWidget::writeTextFile(const QString& fileName)
{
	QFile file(fileName);
	if (file.open(QIODeviceBase::WriteOnly))
	{
		file.write(text().toStdString().c_str());
		return true;
	}
	return false;
}
