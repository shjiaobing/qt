﻿#include"MyMarkDownWidget.h"
#include<QShortCut>
#include<QKeySequence>
//快捷键设置
void MyMarkdownWidget::init_ShortcutKey()
{
	new QShortcut(QKeySequence("Ctrl+Tab"), this, [this]() {
		auto focus = focusWidget();
		if (focus == m_MarkDownEdit)
		{
			m_SourceEdit->setFocus();
		}
		else if (focus == m_SourceEdit)
		{
			m_MarkDownEdit->setFocus();
		}
		
		 });
}