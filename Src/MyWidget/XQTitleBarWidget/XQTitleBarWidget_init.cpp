﻿#include"XQTitleBarWidget.h"
#include"XQAlgorithm.h"
#include<QLabel>
#include<QBoxLayout>
#include<QPushButton>
void XQTitleBarWidget::init()
{
	setCssFile(this, ":/XQTitleBarWidgetx/style.css");
	//无边框窗口拖动设置
	setWindowFlag(Qt::WindowType::FramelessWindowHint);
	//设置鼠标跟踪
	setMouseTracking(true);
	/*m_buttons.resize(MAXBTNCOUNT);*/

	//开始鼠标拉伸窗口
	setMouseStretchWidget(true);

	//设置默认图标
	m_icon = new QLabel(this);
	if (this->controlWidget() != this)
	{
		m_icon->setPixmap(controlWidget()->windowIcon().pixmap(50, 50));
		//设置默认标题
		m_title = new QLabel(controlWidget()->windowTitle(), this);//设置为父窗口标题
	}
	else
	{
		m_title = new QLabel("标题", this);//设置为父窗口标题
	}
	m_icon->setObjectName("icon");
	m_icon->setScaledContents(true);
	if(m_icon->pixmap().isNull())
		m_icon->hide();
	m_title->setObjectName("title");
	
	if(m_typeAll.isEmpty())//静态成员变量是空的时候
	{
		//初始化参数
		m_typeAll = {
			XQTBWType(XQTBWType::skin, "skin", &XQTitleBarWidget::showSkinWidget),
			XQTBWType(XQTBWType::top, "topOff", &XQTitleBarWidget::topWidget),
			XQTBWType(XQTBWType::minimized, "minimized", &XQTitleBarWidget::minimizedWidget),
			XQTBWType(XQTBWType::maximized, "maximized", &XQTitleBarWidget::maximizedWidget),
			XQTBWType(XQTBWType::close, "close", &XQTitleBarWidget::closeWidget),
		};
	}
	init_ui();
	setFixedHeight(30);
}
void XQTitleBarWidget::init_ui()
{
	auto Layout = new QBoxLayout(QBoxLayout::Direction::LeftToRight, this);//从左到右
	auto LayoutOne = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右-图标和标题
	auto LayoutTwo = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右-按钮
	LayoutOne->addWidget(m_icon);
	LayoutOne->addWidget(m_title);
	LayoutOne->addStretch(1);
	LayoutTwo->setSpacing(10);
	Layout->setContentsMargins(0, 0, 0, 0);//设置边框间距
	Layout->addLayout(LayoutOne);
	Layout->addLayout(LayoutTwo);
	setLayout(Layout);
	m_layoutBtn = LayoutTwo;
	//setButtons(m_types);//设置按钮种类
}