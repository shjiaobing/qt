﻿#include"XQTitleBarWidget.h"
#include"XQAlgorithm.h"
#include"XQEventFilterObject.h"
#include<QPushButton>
#include<QDebug>
void XQTitleBarWidget::showSkinWidget()
{
	emit skinBtn();
	if (m_skinWidget == nullptr)
		return;
	m_skinWidget->show();
	auto skinBtn = QObject::findChild<QPushButton*>("skin");
	if (skinBtn == nullptr)
		return;
	/*qInfo() << "显示皮肤窗口";*/
	
	//居中显示
	size_t w = m_skinWidget->width(), h = m_skinWidget->height();
	m_skinWidget->move(controlWidget()->pos() - (QPoint(w - controlWidget()->width(), h - controlWidget()->height()) / 2));//移动新窗口居中
	//将窗口显示并且激活
	m_skinWidget->show();
	m_skinWidget->activateWindow();
}
void XQTitleBarWidget::topWidget()
{
	emit topBtn();
	QWidget* widget = this->controlWidget();
	QPoint pos = widget->mapToGlobal(QPoint(0,0));
	QSize size = widget->size();
	// 判断窗口是否处于置顶状态
	if (widget->windowFlags() & Qt::WindowStaysOnTopHint)
	{
		// 如果是，去除置顶标志位
		widget->setWindowFlags(widget->windowFlags() & ~Qt::WindowStaysOnTopHint);
		QObject::findChild<QPushButton*>("topOn")->setObjectName("topOff");
	}
	else
	{
		// 如果不是，添加置顶标志位
		widget->setWindowFlags(widget->windowFlags() | Qt::WindowStaysOnTopHint);
		QObject::findChild<QPushButton*>("topOff")->setObjectName("topOn");
	}
	//重新加载以其更新
	setCssFile(this, ":/XQTitleBarWidgetx/style.css");
	widget->removeEventFilter(m_TopHintEventFilter);
	widget->installEventFilter(m_TopHintEventFilter);
	widget->show();
	widget->move(pos);
	widget->resize(size);
}
void XQTitleBarWidget::minimizedWidget()
{
	emit minimizedBtn();
	QWidget* widget = this->controlWidget();
	widget->showMinimized();
	/*qInfo() << "最小化";*/
}
void XQTitleBarWidget::maximizedWidget()
{
	emit maximizedBtn();
	QWidget* widget = this->controlWidget();
	// 判断窗口是否处于最大化状态
	if (widget->isMaximized())
		// 如果是，调用 showNormal() 函数还原窗口
		widget->showNormal();
	else
		widget->showMaximized();//窗口最大化
	/*qInfo() << "最大化";*/
}
void XQTitleBarWidget::closeWidget()
{
	emit closeBtn();
	//向控制窗口发送关闭请求
	QWidget* widget = this->controlWidget();
	widget->close();
	/*if(widget->close())
		widget -> deleteLater();*/
	/*qInfo() << "关闭";*/
}