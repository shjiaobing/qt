﻿#include"XQTitleBarWidget.h"
#include"XQAlgorithm.h"
#include"XQStretchEventFilter.h"
#include"XQActivationChangeEventFilter.hpp"
#include<QPushButton>
#include<QBoxLayout>
#include<QLabel>
#include<QDebug>
QList<XQTBWType> XQTitleBarWidget::m_typeAll;
XQTitleBarWidget::XQTitleBarWidget(QWidget* parent)
	:QWidget(parent),
	m_parentWidget(parent)
{
	if (parent!=nullptr)
	{
		//将父窗口设置为无边框窗口
		parent->setWindowFlag(Qt::FramelessWindowHint);
		//parent->setAttribute(Qt::WA_DeleteOnClose); //# 在关闭时自动删除

		//作为子窗口时隐藏标题栏
		parent->installEventFilter(new XQEventFilterObject([=]
			{
				if (!m_autoHide)
					return;
				if (parent->windowFlags() & Qt::Window)
					show();
				else
					hide();
			}, QEvent::Show, this));
	}
	else
	{
		hide();
	}
	
	//链接图标改变信号
	connect(controlWidget(), &QWidget::windowIconChanged, [=] {
		setWindowIcon(controlWidget()->windowIcon()); });
	//链接标题改变信号
	connect(controlWidget(), &QWidget::windowTitleChanged, [=] {
		m_title->setText(controlWidget()->windowTitle()); });

	//链接窗口状态改变置顶
	m_TopHintEventFilter = new XQEventFilterObject([this] {WindowStaysOnTopHintEventFilter(); }, QEvent::Show, controlWidget());
	controlWidget()->installEventFilter(m_TopHintEventFilter);

	//设置自定义菜单
	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, &QWidget::customContextMenuRequested, this, &XQTitleBarWidget::contextMenuRequested);
	//标题栏初始化
	init();
}

XQTitleBarWidget::~XQTitleBarWidget()
{
	if (m_skinWidget != nullptr)
		m_skinWidget->deleteLater();
	if (m_TopHintEventFilter != nullptr)
		m_TopHintEventFilter->deleteLater();
	if (m_layoutBtn != nullptr)
		m_layoutBtn->deleteLater();
	if (m_icon != nullptr)
		m_icon->deleteLater();
	if (m_title != nullptr)
		m_title->deleteLater();
	for (auto&button: m_buttons)
	{
		if(button!=nullptr)
			button->deleteLater();
	}
}
QWidget* XQTitleBarWidget::controlWidget()
{
	QWidget* pWidget = m_parentWidget;
	if (pWidget == nullptr)
		pWidget = this;
	return pWidget;
}
QString XQTitleBarWidget::windowTitle()const
{
	return std::move(m_title->text());
}
QPixmap XQTitleBarWidget::windowIcon()const
{
	return std::move(m_icon->pixmap());
}
int XQTitleBarWidget::btnSpacing() const
{
	return m_layoutBtn->spacing();
}
int XQTitleBarWidget::buttons() const
{
	return m_types;
}
QWidget* XQTitleBarWidget::skinWidget() const
{
	return m_skinWidget;
}
bool XQTitleBarWidget::mouseMoveWidget() const
{
	return m_mouseMoveWidge;
}
bool XQTitleBarWidget::mouseDoubleClickmaxWidget() const
{
	return m_mouseDoubleClickmaxWidget;
}
bool XQTitleBarWidget::mouseStretchWidget() const
{
	return m_StretchEventFilter!=nullptr;
}
XQStackedWidget* XQTitleBarWidget::stackedWidget()
{
	return m_StackedWidget;
}
void XQTitleBarWidget::setStackedWidget(XQStackedWidget* StackedWidget)
{
	m_StackedWidget = StackedWidget;
}
void XQTitleBarWidget::setControlWidget(QWidget* widget)
{
	m_parentWidget = widget;
}
void XQTitleBarWidget::setWindowTitle(const QString& title)
{
	m_title->setText(title);
}
void XQTitleBarWidget::setWindowIcon(const QPixmap& icon, bool circle)
{
	//m_icon->setPixmap(icon);//:/XQTitleBarWidgetx/images/mask.png
	auto pixmap = QPixmap(icon);
	pixmap = pixmap.scaled(m_icon->size());
	pixmap.setMask(QBitmap::fromImage(QPixmap(":/XQTitleBarWidgetx/images/mask.png").scaled(m_icon->size()).toImage()));
	m_icon->setPixmap(pixmap);
	m_icon->setVisible(!icon.isNull());
}
void XQTitleBarWidget::setWindowIcon(const QIcon& icon, bool circle)
{
	setWindowIcon(icon.pixmap(100, 100),circle);
}
void XQTitleBarWidget::setBtnSpacing(int Spacing)
{
	m_layoutBtn->setSpacing(Spacing);
}
void XQTitleBarWidget::setSkinWidget(QWidget* widget)
{
	if (widget == nullptr)
		return;
	m_skinWidget = widget;
	//无边框窗口
	m_skinWidget->setWindowFlag(Qt::FramelessWindowHint);
	//安装失去焦点的事件过滤器
	m_skinWidget->installEventFilter(new XQActivationChangeEventFilter([=] { if(!m_skinWidget->isActiveWindow())m_skinWidget->hide(); }, m_skinWidget));
}
void XQTitleBarWidget::setMouseMoveWidget(bool flag)
{
	m_mouseMoveWidge = flag;
}
void XQTitleBarWidget::setMouseDoubleClickmaxWidget(bool flag)
{
	m_mouseDoubleClickmaxWidget = flag;
}
void XQTitleBarWidget::setMouseStretchWidget(bool flag)
{
	if(flag&&m_StretchEventFilter==nullptr)//当需要设置拉伸并且还未安装事件过滤器的时候
	{
		m_StretchEventFilter = new XQStretchEventFilter(controlWidget());
		//安装窗口拉伸事件过滤器
		controlWidget()->installEventFilter(m_StretchEventFilter);
	}
	else if (!flag && m_StretchEventFilter != nullptr)//当不需要设置拉伸并且已经安装事件过滤器的时候
	{
		controlWidget()->removeEventFilter(m_StretchEventFilter);
		m_StretchEventFilter->deleteLater();
		m_StretchEventFilter = nullptr;
	}
}

void XQTitleBarWidget::setAutoHide(bool hide)
{
	m_autoHide = hide;
}

