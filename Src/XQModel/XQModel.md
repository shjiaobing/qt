# XQTimeDoubleDataModel

## 宏

```c++
#define getStartEndTime_min 1
#define getStartEndTime_max 2
#define getStartEndTime_all 3
```

## public(静态)

### 获取正确的时间

```c++
static bool getStartEndTime(int type,const QList<QDateTime>&dataTimes , const QDateTime& start, const QDateTime& end, QDateTime* startOut = nullptr, QDateTime* endOut = nullptr);
```

### 计算一个范围内的涨跌幅度

```c++
static double changePercent(int type, const QMap<QDateTime, double>& datas, const QDateTime& start, const QDateTime& end, QDateTime* startOut = nullptr, QDateTime* endOut = nullptr);//涨跌幅度
```

## public(获取)

### 获取正确的时间

```c++
bool getStartEndTime(const QDateTime& start, const QDateTime& end, QDateTime* startOut = nullptr, QDateTime* endOut = nullptr);
```

### 计算指定范围内的涨跌幅

```c++
double changePercent(const QDateTime& start, const QDateTime& end, QDateTime* startOut, QDateTime* endOut)const;
```

### 获取历史趋势

```c++
XQHistoryTrend getHistoryTrend()const;
```

### 获取全部数据

```c++
const QMap<QDateTime, double>& datas()const;
```

### 获取指定时间的值

```c++
double data(QDateTime time)const;
```

### 获取一个时间范围内的最小与最大值

```c++
QPair<double, double> value_min_max(const QDateTime& startDate, const QDateTime& endDate);
```

### 获取最大值

```c++
double value_max()const;
```

### 获取最小值

```c++
double value_min()const;
```

### 获取最晚的时间

```c++
QDateTime time_max()const;
```

### 获取最早的时间

```c++
QDateTime time_min()const;
```

## public(设置)

### 插入

```c++
virtual void insert(QDateTime time, double value);
```

### 删除

```c++
virtual void remove(QDateTime time);
```

### 清空

```c++
virtual	void clear();
```

### 更新计算最大最小值

```c++
virtual	void updateMaxMin();
```

# XQUserInfo

> 用户信息

## public(获取)

### 获取头像

```c++
QPixmap headPortrait()const;
```

### 获取头像路径

```c++
QString headPortrait_toPath()const;
```

### 获取账号

```c++
QString account()const;
```

### 获取密码

```c++
QString password()const;
```

### 获取邮箱

```c++
QString email()const;
```

### 获取手机号

```c++
QString phone()const;
```

### 获取名字

```c++
QString name()const;
```

### 获取性别

```c++
genderType gender()const;
```

### 获取权限

```c++
Permission permission()const;
```

### 获取用户当前状态

```c++
userState state()const;
```

### 获取创建时间

```c++
QDateTime ctime()const;
```

### 获取修改时间

```c++
QDateTime mtime()const;
```

## public(设置)

### 设置头像路径

```c++
void setheadPortrait(const QString&path);
```

### 设置头像

```c++
void setheadPortrait(const QPixmap& pixmap);
```

### 设置账号

```c++
void setAccount(const QString& account);
```

### 设置密码

```c++
void setPassword(const QString&password );
```

### 设置邮箱

```c++
void setEmail(const QString& email);
```

### 设置手机号

```c++
void setPhone(const QString& phone);
```

### 设置名字

```c++
void setName(const QString& name);
```

### 设置性别

```c++
void setGender(const genderType& gender);
```

### 设置权限

```c++
void setPermission(const Permission& permission);
```

### 设置用户当前状态

```c++
void setState(userState state);
```

### 设置创建时间

```c++
void setCTime(const QDateTime& time);
```

### 设置修改时间

```c++
void setMTime(const QDateTime& time);
```

## signals

### 头像缓存路径改变

```c++
void headPortraitPathChange(const QString& path);
```

### 头像改变

```c++
void headPortraitChange(const QPixmap& pixmap);
```

### 账号改变

```c++
void accountChange(const QString& account);
```

### 密码改变

```c++
void passwordChange(const QString& password);
```

### 邮箱改变

```c++
void emailChange(const QString& email);
```

### 手机号改变

```c++
void phoneChange(const QString& phone);
```

### 名字改变

```c++
void nameChange(const QString& name);
```

### 性别改变

```c++
void genderChange(const genderType& gender);
```

### 状态改变时

```c++
void stateChange(userState state);
```

## 