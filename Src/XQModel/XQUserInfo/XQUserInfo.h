﻿#ifndef XQUSERINFO_H
#define XQUSERINFO_H
#include<QObject>
#include<QPixmap>
#include<QDateTime>
#include"XQUserInfoEnum.h"
//用户信息
class XQUserInfo :public QObject
{
	Q_OBJECT
public:
	XQUserInfo(QObject* parent = nullptr);
	~XQUserInfo();
	//获取头像
	QPixmap headPortrait()const;
	//获取头像路径
	QString headPortrait_toPath()const;
	//获取账号
	QString account()const;
	//获取密码
	QString password()const;
	//获取邮箱
	QString email()const;
	//获取手机号
	QString phone()const;
	//获取名字
	QString name()const;
	//获取性别
	genderType gender()const;
	//获取权限
	Permission permission()const;
	//获取用户当前状态
	userState state()const;
	//获取创建时间
	QDateTime ctime()const;
	//获取修改时间
	QDateTime mtime()const;
public slots:
	//设置头像路径
	void setheadPortrait(const QString&path);
	//设置头像
	void setheadPortrait(const QPixmap& pixmap);
	//设置账号
	void setAccount(const QString& account);
	//设置密码
	void setPassword(const QString&password );
	//设置邮箱
	void setEmail(const QString& email);
	//设置手机号
	void setPhone(const QString& phone);
	//设置名字
	void setName(const QString& name);
	//设置性别
	void setGender(const genderType& gender);
	//设置权限
	void setPermission(const Permission& permission);
	//设置用户当前状态
	void setState(userState state);
	//设置创建时间
	void setCTime(const QDateTime& time);
	//设置修改时间
	void setMTime(const QDateTime& time);
signals://信号
	//头像缓存路径改变
	void headPortraitPathChange(const QString& path);
	//头像改变
	void headPortraitChange(const QPixmap& pixmap);
	//账号改变
	void accountChange(const QString& account);
	//密码改变
	void passwordChange(const QString& password);
	//邮箱改变
	void emailChange(const QString& email);
	//手机号改变
	void phoneChange(const QString& phone);
	//名字改变
	void nameChange(const QString& name);
	//性别改变
	void genderChange(const genderType& gender);
	//权限改变
	void permissionChange(const Permission& permission);
	//状态改变时
	void stateChange(userState state);
protected://初始化
	//初始化
	void init();
protected://成员变量
	QDateTime m_ctime;//创建时间
	QDateTime m_mtime;//修改时间
	QPixmap m_headPortrait;//头像
	QString m_headPortraitPath;//头像在本地的缓存路径
	QString m_account;//账号
	QString m_password;//密码
	QString m_email;//邮箱
	QString m_phone;//手机号码
	QString m_name;//名字
	genderType m_gender=boy;//性别
	Permission m_permission;//权限
	userState m_state = userState::Logout;//用户当前状态
};
#endif // !XQUserInfo_h
