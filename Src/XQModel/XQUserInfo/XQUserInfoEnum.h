﻿#ifndef XQUSERINFOENUM_H
#define XQUSERINFOENUM_H
//用户状态
enum userState
{
	Login,//登录状态
	Logout//注销状态
};
//性别类型
enum genderType
{
	boy,//男
	girl,//女
	Hideyoshi//女装大佬
};
//权限
enum  Permission
{
	root,//最高权限
	user//普通用户
};

#endif // !XQUSERINFOENUM_H
