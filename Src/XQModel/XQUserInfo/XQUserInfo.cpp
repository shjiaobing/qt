﻿#include "XQUserInfo.h"

XQUserInfo::XQUserInfo(QObject* parent)
	:QObject(parent)
{
	init();
}

XQUserInfo::~XQUserInfo()
{
}

QPixmap XQUserInfo::headPortrait()const
{
	return m_headPortrait;
}

QString XQUserInfo::headPortrait_toPath()const
{
	return m_headPortraitPath;
}

QString XQUserInfo::account()const
{
	return m_account;
}

QString XQUserInfo::password() const
{
	return m_password;
}

QString XQUserInfo::email() const
{
	return m_email;
}

QString XQUserInfo::phone() const
{
	return m_phone;
}

QString XQUserInfo::name() const
{
	return m_name;
}

genderType XQUserInfo::gender() const
{
	return m_gender;
}

Permission XQUserInfo::permission() const
{
	return m_permission;
}

userState XQUserInfo::state() const
{
	return m_state;
}

QDateTime XQUserInfo::ctime() const
{
	return m_ctime;
}

QDateTime XQUserInfo::mtime() const
{
	return m_mtime;
}

void XQUserInfo::setheadPortrait(const QString& path)
{
	setheadPortrait(QPixmap(path));
	m_headPortraitPath = path;
	emit headPortraitPathChange(path);
}

void XQUserInfo::setheadPortrait(const QPixmap& pixmap)
{
	m_headPortrait = pixmap;
	m_headPortraitPath.clear();
	emit headPortraitChange(pixmap);
}

void XQUserInfo::setAccount(const QString& account)
{
	m_account = account;
	emit accountChange(account);
}

void XQUserInfo::setPassword(const QString& password)
{
	m_password = password;
	emit passwordChange(password);
}

void XQUserInfo::setEmail(const QString& email)
{
	m_email = email;
	emit emailChange(email);
}

void XQUserInfo::setPhone(const QString& phone)
{
	m_phone = phone;
	emit phoneChange(phone);
}

void XQUserInfo::setName(const QString& name)
{
	m_name = name;
	emit nameChange(name);
}

void XQUserInfo::setGender(const genderType& gender)
{
	m_gender = gender;
	emit genderChange(gender);
}

void XQUserInfo::setPermission(const Permission& permission)
{
	m_permission = permission;
	emit permissionChange(m_permission);
}

void XQUserInfo::setState(userState state)
{
	m_state = state;
	emit stateChange(m_state);
}

void XQUserInfo::setCTime(const QDateTime& time)
{
	m_ctime = time;
}

void XQUserInfo::setMTime(const QDateTime& time)
{
	m_mtime = time;
}

void XQUserInfo::init()
{
}
