# Algorithm(算法)

## C++

> 打开:c++ [文档](Algorithm/CPP/Algorithm.md)

### XAlgorithm(c++算法)

### XSymbolOverload(符号重载)

### XMultiWayTree(多叉树)

#### 	XBinaryTree(二叉树)

##### 		XHuffmanTree(哈夫曼树)

#### 	XDirectory(目录)

##### 		XFileStruct(文件)

### XZip(哈夫曼压缩)

## QT

> 打开:qt [文档](Algorithm/Qt/Algorithm.md)

### XQAlgorithm(基于qt)

### QCoding(qt编码转换)

### XQArguments(命令行参数解析)

### XQRCode(二维码)

> 二维码生成

### XQLog(日志)

> 可自定义输出日志信息到控制台或文件，内置单独线程处理，开启后可在多线程环境下保持顺序输出

### XQPandas(表格数据处理)

> 现只支持读写 csv格式的表格数据处理

### XQSettingsInfo(配置文件)

### XQTimerGroup(定时器任务)

# XQModel(模型)

打开: [文档](XQModel/XQModel.md)

## XQTimeDoubleDataModel(时间/浮点)

> 键:QDateTime 
>
> 值:Double

## XQUserInfo(用户信息)

> 用户信息类

# XQNetwork(网络)

> 打开: [文档](XQNetwork/XQNetwork.md)

## XQFtp

> Qt4 QFtp源代码修改,[原项目](https://github.com/qt/qtftp)

## XQHttp(http)

### XQHttpObject(基类)

#### XQHttpClient(客户端)

#### XQHttpServer(服务器)

### XQHttpHeadObject(头部解析)

> 解析http 的头部数据

#### XQHttpHeadRequest(请求头)

> 解析http 请求头的数据

#### XQHttpHeadReply(响应头)

> 解析 http响应头数据

### XQHttpRequest(爬虫类)

> 支持https 主要用作爬虫

## XQMail(邮件)

> 电子邮件发送，支持附件

## XQMySql(mysql数据库)

> 基于qt 再次包装，更方便的使用数据库

##  XQRobot(机器人)

> 主要用作信息推送服务

### XQRobotDing(钉钉机器人)

> 钉钉群聊机器人

