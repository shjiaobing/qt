﻿#include "XZip.h"

XZipData& XZip::gzip()
{
	m_Type = GZIP | GZIPUSER;//定义当前为用户压缩
	setGZipDictionaries();//设置字典
	if (tree.dictionariesCount() == 0)//字典是空的
		return m_zipData;
	tree.creationTree();//建立哈夫曼树
	tree.setCode();//设置哈夫曼编码
	m_zipData.clear();//清空
	writeGZipDictionaries();//写入字典数据
	writeGZipDirectoryInof();//写入目录信息及目录名字
	m_file_nSel = 0;
	for (auto& file:m_file)
	{
		writeGZipFileInfo(m_file_nSel);//写入文件信息和名字
		writeGZipFileData(m_file_nSel);//写入文件数据
		++m_file_nSel;
	}
	return m_zipData;
}

size_t XZip::gzip(std::string gzipfile)
{
	m_Type = GZIP | GZIPUSER;//定义当前为用户压缩
	//打开压缩后的文件
	std::fstream writeFile(gzipfile, std::ios::out | std::ios::binary | std::ios::trunc);
	if (!writeFile.is_open())
	{
		std::cout << "create the compressed file failure!" << std::endl;
		return 0;
	}
	setGZipDictionaries();//设置字典
	if (tree.dictionariesCount() == 0)//字典是空的
		return 0;
	tree.creationTree();//建立哈夫曼树
	tree.setCode();//设置哈夫曼编码
	m_zipData.clear();//清空
	writeGZipDictionaries();//写入字典数据到内存
	writeGZipDirectoryInof();//写入目录信息及目录名字到内存
	writeFile.write(&m_zipData[0], m_zipData.size());//将字典数据和目录信息名字写入压缩文件
	
	m_file_nSel = 0;
	for (auto& file : m_file)
	{
		m_zipData.clear();//清空
		writeGZipFileInfo(m_file_nSel);//写入文件信息和名字到内存
		writeFile.write(&m_zipData[0], m_zipData.size());//将文件信息和名字写入压缩文件
		writeGZipFileData(m_file_nSel,writeFile);//写入文件数据
		++m_file_nSel;
	}
	size_t gzipSize = writeFile.tellg();
	writeFile.close();
	return gzipSize;
}
