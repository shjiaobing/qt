﻿#include"XZip.h"
#include <direct.h>
size_t XZip::unzip(const XZipData& data)
{
	return unzip(&data[0],data.size());
}
//内存数据解压到文件目录
size_t XZip::unzip(const char* data, const size_t size, const std::string& writePath)
{
	//if (m_file.isEmpty())//没有需要压缩的文件
	//	return 0;
	size_t unZipsize = 0;//全部文件大小
	size_t& offset = m_readOffset;//偏移
	clear();
	offset = readGZipDictionaries(data, size, offset);
	if (offset==0)//待解压的文件有问题
		return 0;
	tree.creationTree();
	offset = readGZipDirectoryInof(data, size, offset);
	std::string filePath = writePath;
	if(!directory().empty())//存在目录创建目录
	{
		filePath += "/" + directory();//文件路径
		mkdir(filePath.c_str());//创建目录
	}
	for (auto& file : m_file)
	{
		m_zipData.clear();
		offset = readGZipFileInfo(data, size, offset);
		offset = readUnZipData(data, size, offset, m_file[m_file_nSel].fileSize, m_zipData);
		//创建文件写入解压后的数据
		std::string file= filePath+"/" + m_file[m_file_nSel].name;//拼接路径
		std::fstream writeFile(file, std::ios::out | std::ios::binary | std::ios::trunc);
		if (!writeFile.is_open())
		{
			std::cout << "create the compressed file failure!" << std::endl;
			return 0;
		}
		writeFile.write(&m_zipData[0], m_zipData.size());//写入解压后的文件数据
		writeFile.close();
		unZipsize += m_file[m_file_nSel].unZipfile.size();
		++m_file_nSel;
	}
	return unZipsize;
}

size_t XZip::unzip(const XZipData& data, const std::string& writePath)
{
	return unzip(&data[0], data.size(), writePath);
}
//文件解压到内存数据
size_t XZip::unzip(const std::string& File)
{
	clear();
	std::fstream readFile(File, std::ios::in | std::ios::binary | std::ios::ate);
	if (!readFile.is_open())
	{
		std::cout << "File opening failure!" << std::endl;
		return 0;
	}
	size_t size = readFile.tellg();//获取文件大小
	readFile.seekg(0,std::ios::beg);
	char* buff = new char[size];
	readFile.read(buff,size);//读取文件到内存
	size = unzip(buff, size);
	delete[] buff;
	return size;
}
//文件解压到目录
size_t XZip::unzip(const std::string& File, std::string path)
{
	clear();
	std::fstream readFile(File, std::ios::in | std::ios::binary);
	if (!readFile.is_open())
	{
		std::cout << "File opening failure!" << std::endl;
		return 0;
	}
	size_t& offset = m_readOffset;//偏移
	offset = readGZipDictionaries(readFile);
	if (offset == 0)//待解压的文件有问题
	{
		return 0;
		readFile.close();
	}
	tree.creationTree();
	readGZipDirectoryInof(readFile);

	std::string filePath = path;
	if (!directory().empty())//存在目录创建目录
	{
		filePath += "/" + directory();//文件路径
		mkdir(filePath.c_str());//创建目录
	}
	for (auto& file : m_file)
	{
		offset = readGZipFileInfo(readFile);
		//创建文件写入解压后的数据
		std::string file = filePath + "/" + m_file[m_file_nSel].name;//拼接路径
		std::fstream writeFile(file, std::ios::out | std::ios::binary | std::ios::trunc);
		if (!writeFile.is_open())
		{
			std::cout << "create the compressed file failure!" << std::endl;
			return 0;
		}
		readUnZipData(readFile, m_file[m_file_nSel].fileSize,writeFile);//写入解压后的文件数据
		writeFile.close();
		//unZipsize += m_file[m_file_nSel].unZipfile.size();
		++m_file_nSel;
	}





	readFile.close();
	return offset;
}

size_t XZip::unzip(const char* data, const size_t size)
{
	size_t unZipsize = 0;//全部文件大小
	size_t& offset = m_readOffset;//偏移
	clear();
	offset = readGZipDictionaries(data, size, offset);
	if (offset == 0)//待解压的文件有问题
		return 0;
	tree.creationTree();
	offset = readGZipDirectoryInof(data, size, offset);
	for (auto& file : m_file)
	{
		offset = readGZipFileInfo(data, size, offset);
		offset = readUnZipData(data, size, offset, m_file[m_file_nSel].fileSize, m_file[m_file_nSel].unZipfile);
		unZipsize += m_file[m_file_nSel].unZipfile.size();
		++m_file_nSel;
	}
	
	return unZipsize;
}