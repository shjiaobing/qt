﻿#ifndef XENUM_H
#define XENUM_H
//定时器类型
enum class TimerType
{
	time,//时间
	date//日期
};
//时间类型
enum class TimeType
{
	day,//天
	week,//周
	month//月
};
//任务类型
enum class TaskType
{
	root,//预设
	user//用户
};
#endif // !XEnum_H
