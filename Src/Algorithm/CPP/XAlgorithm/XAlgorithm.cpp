﻿#include"XAlgorithm.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include <fstream>
bool isNULL(const void* args/*参数数值*/, const char* argsName/*参数名字*/, const char* str/*附加参数*/, const char* funcName/*函数名字*/, const char* filePath/*所在文件路径*/, int line/*所在行号*/)
{
	if (args == NULL)
	{
		printf("%s\n参数:%s是NULL\t函数名:%s\n文件路径:%s\n正在编译文件的行号:%d\n", str, argsName, funcName, filePath, line);
		return true;
	}
	return false;
}

void XDelay(const size_t msec)
{
	clock_t  time_front = clock();
	while (true)
	{
		clock_t time_after = clock();
		if (time_after - time_front > msec)
			break;
	}
}

bool IsFileExist(const std::string& name)
{
	std::ifstream f(name);
	return f.good();
}

double round(double number, int decimals)
{
	return std::round(number * std::pow(10, decimals)) * std::pow(10, -decimals);
}


//
/// 下面是例子：writer: icemen 2019.03.31. Huizhou City,China
/// qDebug() << readableFileSize(42000517331067);
/// qDebug() << readableFileSize(1200051733);
/// qDebug() << readableFileSize(15522272);
/// qDebug() << readableFileSize(123456);
/// qDebug() << readableFileSize(1201);
/// qDebug() << readableFileSize(1021);
/// qDebug() << readableFileSize(22223567842000517331067);
/// qDebug() << readableFileSize(567842000517331067);
/// qDebug() << readableFileSize(1024);
/// qDebug() << readableFileSize(1024*34);
/// qDebug() << readableFileSize(1024*1024*34);
/// qDebug() << readableFileSize(1024.0*1024*1024*34); // 注意 大整数计算时超过GB 应用浮数计算，否则无法得出正确答案。
/// qDebug() << readableFileSize(1024.0*1024*1024*1024*34 );
