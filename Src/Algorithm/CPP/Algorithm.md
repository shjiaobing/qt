> 纯C++编写不含一点qt

# XAlgorithm

## 延迟毫秒

```c++
void XDelay(const size_t msec);
```

## 判断文件是否存在

```c++
bool IsFileExist(const std::string& name);
```

## 四舍五入

```c++
double round(double number, int decimals);
```

# XMD5

> 获取数据的md5值

##  获取数据或文本的md5值

```c++
//获得32位md5值
static std::string String(const void* input, size_t length);
static std::string String(const string& str);
```

## 获取文件的md5值

```c++
static std::string file(ifstream& in);
static std::string file(const string& path);
```

# XSymbolOverload(符号重载)

> 单位符号重载

```c++
//时间类
//小时
unsigned long long operator""_h(unsigned long long data);
//分钟
unsigned long long operator""_mins(unsigned long long data);
//秒
unsigned long long operator""_s(unsigned long long data);
//毫秒
unsigned long long operator""_ms(unsigned long long data);
//储存单位类
unsigned long long operator""_B(unsigned long long data);
unsigned long long operator""_KB(unsigned long long data);
unsigned long long operator""_MB(unsigned long long data);
unsigned long long operator""_GB(unsigned long long data);
unsigned long long operator""_TB(unsigned long long data);
```

## 