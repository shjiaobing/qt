﻿#include"XSymbolOverload.h"
unsigned long long operator""_h(unsigned long long data)
{
	return data * 1000 * 60*60;
}

unsigned long long operator""_mins(unsigned long long data)
{
	return data *1000*60;
}

unsigned long long operator""_s(unsigned long long data)
{
	return data * 1000;
}

unsigned long long operator""_ms(unsigned long long data)
{
	return data;
}

unsigned long long operator""_B(unsigned long long data)
{
	return data;
}

unsigned long long operator""_KB(unsigned long long data)
{
	return data*1024;
}

unsigned long long operator""_MB(unsigned long long data)
{
	return data * 1024*1024;
}

unsigned long long operator""_GB(unsigned long long data)
{
	return data * 1024 * 1024*1024;
}

unsigned long long operator""_TB(unsigned long long data)
{
	return data * 1024 * 1024 * 1024*1024;
}
