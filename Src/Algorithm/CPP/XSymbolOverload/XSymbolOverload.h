﻿#ifndef XSYMBOLOVERLOAD_H
#define XSYMBOLOVERLOAD_H
//单位符号重载
//时间类
//小时
unsigned long long operator""_h(unsigned long long data);
//分钟
unsigned long long operator""_mins(unsigned long long data);
//秒
unsigned long long operator""_s(unsigned long long data);
//毫秒
unsigned long long operator""_ms(unsigned long long data);
//储存单位类
unsigned long long operator""_B(unsigned long long data);
unsigned long long operator""_KB(unsigned long long data);
unsigned long long operator""_MB(unsigned long long data);
unsigned long long operator""_GB(unsigned long long data);
unsigned long long operator""_TB(unsigned long long data);
#endif // !XSymbolOverload_h
