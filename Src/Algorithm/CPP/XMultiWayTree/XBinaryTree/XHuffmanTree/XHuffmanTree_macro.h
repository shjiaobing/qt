﻿#ifndef XHUFFMANTREE_MACRO_H
#define XHUFFMANTREE_MACRO_H
#include<vector>
#include"XBinaryTree.hpp"

//哈夫曼节点
//字典数据(写入压缩的)
struct DictionaryData
{
	char ch;//字符
	size_t count;//出现次数
	unsigned char codeSize;//编码大小
};
//字典值(内容)
struct DictionariesValue
{
	size_t count;//出现次数
	std::vector<char>* code;//哈夫曼编码
};


//哈夫曼树
//哈夫曼节点数据
struct XHfmNodeData
{
	char ch;//字符
	size_t count;//出现次数
	std::vector<char>* code;//哈夫曼编码   value:char类型
};

using XHfmNode = XBinaryTree<XHfmNodeData>;//哈夫曼树节点
using LPXHfmNode = XHfmNode*;//哈夫曼树节点指针
using XDictionaries = std::map<char, DictionariesValue>;//字典
#endif // !XHUFFMANTREE_MACRO_H
