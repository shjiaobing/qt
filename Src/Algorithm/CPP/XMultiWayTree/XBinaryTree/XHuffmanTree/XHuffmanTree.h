﻿#ifndef XHUFFMANTREE_H
#define XHUFFMANTREE_H
#include<map>
#include<vector>
#include"XHuffmanTree_macro.h"

//哈夫曼树
class XHuffmanTree
{
public:
	friend class XZip;
	XHuffmanTree();
	~XHuffmanTree();
	//压缩数据
	//static std::vector<char> gzip(const char* data, const size_t size, const std::string& name="");
	//static bool gzip(const std::string sourceFile/*打开的源文件*/, const const std::string targetFile/*保存的目标文件*/);
	//解压数据
	//static XHuffmanTree unzip(const char* data, const size_t size);
	//static bool unzip(const std::string sourceFile/*打开的源文件*/, const const std::string targetFile/*保存的目标文件*/);
	//清空哈夫曼树
	void clearTree();
	//清空哈夫曼编码
	void clearCode();
	//清空哈夫曼树和字典
	void clear();
	//遍历哈夫曼树
	static void printTree(XHfmNode* root);
	//遍历字典
	static void printDictionaries(XDictionaries& dictionaries);
public:
	//创建哈夫曼树初始化
	void init();
	//根据数据生成字典(不带编码)
	void addDictionaries(const char* data, const size_t size);
	//打开文件读取数据构增加字典(不带编码自定义缓冲区)
	void addDictionaries(const std::string fileName, size_t buffer = 1024);
	//返回字典数量
	size_t dictionariesCount();
	//返回字典
	XDictionaries& dictionaries();
	//根据字典创建树
	void creationTree();
	//返回哈夫曼树
	XHfmNode* XHfmTree();
	//根据哈夫曼树设置哈夫曼编码
	void setCode();
protected:
	XHfmNode* root=nullptr;//树的根节点
	std::map<char, DictionariesValue> m_dictionaries;//字典  key:char value: DictionariesValue
};
#endif // !XHuffman_h
