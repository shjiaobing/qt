﻿#include "StringMap.h"
#include<functional>
StringMap::StringMap()
{
}

StringMap::~StringMap()
{
}

void StringMap::insert(const string& name, const string& val)
{
	auto data = find_if(GetParamStr.begin(), GetParamStr.end(), [=](pair<string, string>& data) {return data.first == name; });
	if (data == GetParamStr.end())
		GetParamStr.push_back(pair<string, string>(name, val));
	else
		data->second = val;
}

void StringMap::erase(const string& name)
{
	auto data = find_if(GetParamStr.begin(), GetParamStr.end(), [=](pair<string, string>& data) {return data.first == name; });
	if (data != GetParamStr.end())
		GetParamStr.erase(data);
}
const string StringMap::getString(const string& prefix, const string& separator, const string& suffix)
{
	string str;
	for (auto& data: GetParamStr)
	{
		str += prefix;
		str += data.first;
		str += separator;
		str += data.second;
		str += suffix;
	}
	return str;
}

string& StringMap::operator[](const string& name)
{
	auto data = find_if(GetParamStr.begin(), GetParamStr.end(), [=](pair<string, string>& data) {return data.first == name; });
	if (data == GetParamStr.end())
	{
		GetParamStr.push_back(pair<string, string>(name, ""));
		return GetParamStr.back().second;
	}
	else
	{
		return data->second;
	}
}
vector<pair<string, string>>& StringMap::operator()()
{
	return GetParamStr;
}
void StringMap::clear()
{
	GetParamStr.clear();
}
