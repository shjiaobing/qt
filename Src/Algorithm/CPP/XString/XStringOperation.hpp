﻿#ifndef XSTRINGOPERATION_H
#define XSTRINGOPERATION_H
#include<string>
#include<vector>
#include<regex>
#include<QString>
#include<QList>
#include<QDebug>
using namespace std;
//字符串操作类
class XStringOperation
{
public:
	XStringOperation() = delete;
	//分割字符串
#ifdef QT_VERSION
	static const vector<QString> strtok(const QString& Text, const QString& Delimiter);
    static  QStringList strtok(const QString& Text, const QString& Delimiter,const bool space);

	static const vector<string> strtok(const string& Text, const string& Delimiter);
//正则提取
	static const QList<QStringList> regex_extract(const QString& regex,const QString& PlainText);
	static QList<QStringList>& regex_extract( const QString& regex, const QString& PlainText, QList<QStringList>& list);
	template <typename ...Args>//传size个QStringList用作接收结果
	static const size_t regex_extract(const QString& regex, const QString& PlainText, Args&& ...args);
//正则替换
    static QString regex_replace(const QString& regex, const QString& Text, const QStringList& replace);
#endif // QT_VERSION
};

#endif // !StringOperation_H
#ifdef QT_VERSION
template<typename ...Args>
inline const size_t XStringOperation::regex_extract(const QString& regex, const QString& PlainText, Args && ...args)
{
	QList<QStringList*>list = { (&args)...};
   /* if (list.size() != size)
    {
        qWarning() << "regex_extract:参数数量不匹配";
        return 0;
    }*/
    std::string Text = PlainText.toStdString();
    smatch mat;//用作匹配的对象
    string::const_iterator start = Text.begin();//起始位置
    string::const_iterator end = Text.end();//结束位置
    size_t count = 0;//提取个数
    while (std::regex_search(start, end, mat, std::regex(regex.toStdString())))
    {
        for (size_t i = 0; i < mat.size() - 1; i++)
        {
            string temp(mat[i + 1].first, mat[i + 1].second);
            list[i]->append(temp.c_str());
        }
        start = mat[0].second;//改变起始位置
        count++;
    }
    return count;
}
#endif // QT_VERSION
