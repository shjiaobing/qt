﻿#ifndef QCODING_H
#define QCODING_H
#include<string>
#include<QString>
class QCoding 
{
public:
	QCoding() = delete;
	static const std::string Utf8ToGbk(const std::string& str);
	static QByteArray Utf8ToGbk(const QByteArray& str);
	static const std::string GbkToUtf8(const std::string& str);
	static QByteArray GbkToUtf8(const QByteArray& str);
};
#endif // !QCoding_H
