﻿#include"XQAlgorithm.h"
#include<QFile>
#include<QRegExp>
#include<QFileInfoList>
#include<QDir>
#include<QEventLoop>
#include<QTimer>
#include<QDebug>
bool isQNULL(const void* args, const char* argsName, const char* str, const char* funcName, const char* filePath, int line)
{
    if (args == NULL)
    {
        qInfo()<<QString().asprintf("%s\n参数:%s是NULL\t函数名:%s\n文件路径:%s\n正在编译文件的行号:%d\n", str, argsName, funcName, filePath, line);
        return true;
    }
    return false;
}
void deleteDirectory(const QString& dirPath)
{
    QFileInfoList fileList = QDir(dirPath).entryInfoList(QDir::NoDotAndDotDot | QDir::Files);
    for(auto& fileInfo: fileList) {
        QFile::remove(fileInfo.filePath());
    }

    QFileInfoList dirList = QDir(dirPath).entryInfoList(QDir::NoDotAndDotDot | QDir::Dirs);
    for(auto& dirInfo: dirList) {
        deleteDirectory(dirInfo.absoluteFilePath());
    }

    QDir().rmdir(dirPath);
}
bool moveDir(const QString& srcPath, const QString& dstPath)
{
    QDir sourceDir(srcPath);
    QDir destDir(dstPath);
    QDir dir; dir.mkpath(dstPath);
    if (!destDir.exists()) {
        if (!destDir.mkdir(dstPath)) {
            qWarning() << "Cannot create directory: " << dstPath;
            return false;
        }
    }

    QFileInfoList files = sourceDir.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);
    for (int i = 0; i < files.size(); ++i) {
        QFileInfo fileInfo = files.at(i);
        QString fileName = fileInfo.fileName();
        if (fileInfo.isFile()) {
            destDir.rename(fileInfo.absoluteFilePath(), destDir.absolutePath() + "/" + fileName);
        }
        else if (fileInfo.isDir()) {
            if (!destDir.mkdir(fileName)) {
                qWarning() << "Cannot create directory: " << destDir.absolutePath() + "/" + fileName;
                return false;
            }
            if (!moveDir(fileInfo.absoluteFilePath(), destDir.absolutePath() + "/" + fileName)) {
                return false;
            }
        }
    }

    sourceDir.rmdir(srcPath);
    return true;
}
// 文件大小转换成可读的字符串，后加B,KB,....
QString readableFileSize(quint64 filesize)
{
    QStringList units;
    units << "B" << "KB" << "MB" << "GB" << "TB" << "PB";
    double mod = 1024.0;
    double size = filesize;
    //qDebug() << size;
    int i = 0;
    long rest = 0;
    while (size >= mod && i < units.count() - 1)
    {
        rest = (long)size % (long)mod;
        size /= mod;
        i++;
    }
    QString szResult = QString::number(floor(size));
    if (rest > 0)
    {
        szResult += QString(".") + QString::number(rest).left(2);
    }
    szResult += units[i];
    return  szResult;
}
void setCssFile(QWidget* widget, const QString& css)
{
    if (widget == nullptr)
        return;
    // 从文件中加载样式表
  QFile file(css);
  file.open(QFile::ReadOnly | QFile::Text);
  QString stylesheet = file.readAll();
  file.close();

  // 设置样式表
  widget->setStyleSheet(stylesheet);
}

bool RegexMatch(const QString& test, const QString& pattern)
{
    // 创建 QRegExp 对象，并设置模式字符串和匹配选项
    QRegExp rx(pattern);
    //rx.setCaseSensitivity(Qt::CaseInsensitive);  // 不区分大小写
    // 进行正则表达式匹配，并输出结果
    return rx.exactMatch(test);
}

QStringList strtok(const QString& Text, const QString& Delimiter, const bool space)
{
    QStringList list;
    size_t size = Text.size();
    size_t start = 0;
    size_t nSel = 0;
    //qInfo() << Text;
    do
    {
        nSel = Text.indexOf(Delimiter, start);
        if (nSel == -1)//没找到
        {
           break;
        }
        if (nSel == start )
        {
            if(space)
                list << QString();
        }
        else
        {
            list << Text.mid(start, nSel - start);
        }
        start = nSel + Delimiter.size();
    } while (start < size);
    if(size > start)
    {
        list << Text.mid(start);
    }
    return std::move(list);
}

void centerShow(QWidget* showWidget, QWidget* refer)
{
    if (showWidget == nullptr || refer == nullptr)
        return;
    //置顶窗口判断
    if (refer->windowFlags() & Qt::WindowStaysOnTopHint)
        showWidget->setWindowFlag(Qt::WindowStaysOnTopHint);
    else
        showWidget->setWindowFlag(Qt::WindowStaysOnTopHint,false);
    //窗口显示
    showWidget->show();
    size_t w = showWidget->width(), h = showWidget->height();
    QPoint pos = refer->mapToGlobal(QPoint(0, 0)) - (QPoint(w - refer->width(), h - refer->height()) / 2);
    if(pos.x()>0&&pos.y()>0)
        showWidget->move(pos);//移动新窗口居中显示
    //居中显示
    showWidget->activateWindow();
}

void XQDelayEventLoop(qint64 msec)
{
    // 创建一个事件循环对象
    QEventLoop Loop;
    // 创建一个定时器
    QTimer timer;
    timer.setInterval(msec); 
    timer.setSingleShot(true); // 只触发一次
    // 连接定时器的超时信号到事件循环的退出槽函数
    QObject::connect(&timer, &QTimer::timeout, &Loop, &QEventLoop::quit);
    timer.start();
    // 启动事件循环
    Loop.exec();
}
