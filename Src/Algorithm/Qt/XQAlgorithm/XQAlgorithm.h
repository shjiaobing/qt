﻿#ifndef XQALGORITHM_H
#define XQALGORITHM_H
#include<QString>
#include<QList>
#include<QWidget>
#include<qrandom.h>
#include"XAlgorithm.h"
#define isQNULLInfo(args,str) args,#args,str ,__FUNCTION__,__FILE__,__LINE__
#define ISQNULL(args,str)(isQNULL(isQNULLInfo(args,str)))
bool isQNULL(const void* args/*参数数值*/, const char* argsName/*参数名字*/, const char* str/*附加参数*/, const char* funcName/*函数名字*/, const char* filePath/*所在文件路径*/, int line/*所在行号*/);
//qt随机数
#define random(min,max) QRandomGenerator::global()->bounded(min, max)
//递归删除文件
void deleteDirectory(const QString& dirPath);
//递归移动目录
bool moveDir(const QString& srcPath, const QString& dstPath);
// 文件大小转换成可读的字符串，后加B,KB,....
QString readableFileSize(quint64 filesize);
//设置css样式表文件到QWidget
void setCssFile(QWidget* widget, const QString& css);
//正则匹配
bool RegexMatch(const QString& test, const QString& pattern);
//字符串分割
QStringList strtok(const QString& Text, const QString& Delimiter, const bool space=false);
//QWidget居中显示
void centerShow(QWidget* showWidget/*需要显示的*/, QWidget* refer/*参照物*/);
//事件循环暂停
void XQDelayEventLoop(qint64 msec);
#endif
