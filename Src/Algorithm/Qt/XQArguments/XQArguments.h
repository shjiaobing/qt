﻿#ifndef XQARGUMENTS_H
#define XQARGUMENTS_H
//开机启动方式
enum AutoStartType
{
	show,//显示
	hide,//隐藏
	minimized//最小化
};

#include<QObject>
class XQArguments:public QObject
{
	Q_OBJECT
public:
	XQArguments(QObject* parent = nullptr);
	~XQArguments();
public:
	//转命令行参数
	static QString toArgs();
	//查看参数键是否存在
	static bool isArgsMapExist(const QString& key);
	//插入一个一对键值
	static void insert(const QString& key, const QString& value);
	//删除一堆键值
	static void remove(const QString& key);
	//设置开机自启类型
	static void setAutoStartType(AutoStartType type);
	//设置 设置参数文件的路径
	static void setSettingsPath(const QString& path);
public:
	//获取启动软件的路径
	static QString appPath();
	//获取自启类型转字符串
	static QString autoStartType_toString(AutoStartType type);
	//获取 设置参数文件的路径
	static QString settingsPath();
	//获取自启类型
	static AutoStartType autoStartType();
public:
	//设置自启方式的小部件
	static void setAutoStartWidget(QWidget* widget);
	template <typename Func>
	void connect(const QString& args,typename QtPrivate::FunctionPointer<Func>::Object* receiver, Func slot);
	void connect(const QString& args,std::function<void()> func);
protected:
	//运行那时候解析
	static void parse_arguments();
	//检查参数是否存在
	static bool isArgsListExist(const QString& args);
protected:
	static XQArguments* m_Global;
	static QStringList m_argsList;//命令行参数
	static QMap<QString, QString> m_argsMap;
};
template<typename Func>
inline void XQArguments::connect(const QString& args,typename QtPrivate::FunctionPointer<Func>::Object* receiver, Func slot)
{
	if (isArgsMapExist(args))
	{
		(receiver->*slot)();
	}
}
#endif


