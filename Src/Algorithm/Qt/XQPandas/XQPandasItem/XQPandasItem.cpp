﻿#include "XQPandasItem.h"

XQPandasItem::XQPandasItem()
{
}

XQPandasItem::XQPandasItem(const QStringList& list, QHash<QString, int>* title)
	:m_titleIndex(title)
{
	for (auto&item:list)
	{
		m_data << item;
	}
}

XQPandasItem::XQPandasItem(const QVariantList& list)
	:m_data(list)
{
	
}

XQPandasItem::XQPandasItem(const XQPandasItem& other)
	:m_data(other.m_data),
	m_titleIndex(other.m_titleIndex)
{
}

QStringList XQPandasItem::toStringList()const
{
	QStringList list;
	for (auto& data:m_data)
	{
		list << data.toString();
	}
	return std::move (list);
}

QVariantList& XQPandasItem::data()
{
	return m_data;
}

const QVariantList& XQPandasItem::data()const
{
	return m_data;
}

const QVariant& XQPandasItem::at(qsizetype i) const
{
	return m_data[i];
}

qsizetype XQPandasItem::size() const
{
	return m_data.size();
}

QVariantList::iterator XQPandasItem::begin()
{
	return m_data.begin();
}

QVariantList::const_iterator XQPandasItem::begin() const
{
	return m_data.begin();
}

QVariantList::reverse_iterator XQPandasItem::rbegin()
{
	return m_data.rbegin();
}

QVariantList::const_reverse_iterator XQPandasItem::rbegin() const
{
	return m_data.rbegin();
}

QVariantList::iterator XQPandasItem::end()
{
	return m_data.end();
}

QVariantList::const_iterator XQPandasItem::end() const
{
	return m_data.end();
}

QVariantList::reverse_iterator XQPandasItem::rend()
{
	return m_data.rend();
}

QVariantList::const_reverse_iterator XQPandasItem::rend() const
{
	return m_data.rend();
}

bool XQPandasItem::isEmpty() const
{
	return m_data.isEmpty();
}

QVariant& XQPandasItem::front()
{
	return m_data.front();
}

const QVariant& XQPandasItem::front() const
{
	return m_data.front();
}

QVariant& XQPandasItem::back()
{
	return m_data.back();
}

const QVariant& XQPandasItem::back() const
{
	return m_data.back();
}

void XQPandasItem::setTitle(QHash<QString, int>* title)
{
	m_titleIndex = title;
}

void XQPandasItem::clear()
{
	m_data.clear();
}

XQPandasItem& XQPandasItem::operator<<(QVariant&& other)
{
	m_data.append(other);
	return *this;
}

XQPandasItem& XQPandasItem::operator<<(const QVariant& other)
{
	m_data.append(other);
	return *this;
}

XQPandasItem& XQPandasItem::operator<<(QVariantList&& other)
{
	m_data.append(other);
	return *this;
}

XQPandasItem& XQPandasItem::operator<<(const QVariantList& other)
{
	m_data.append(other);
	return *this;
}

XQPandasItem& XQPandasItem::operator<<(XQPandasItem&& other)
{
	m_data.append(other.m_data);
	return *this;
}

XQPandasItem& XQPandasItem::operator<<(const XQPandasItem& other)
{
	m_data.append(other.m_data);
	return *this;
}

QVariant& XQPandasItem::operator[](qsizetype i)
{
	return m_data[i];
}

const QVariant& XQPandasItem::operator[](qsizetype i) const
{
	return m_data[i];
}

QVariant& XQPandasItem::operator[](const QString& i)
{
	if (m_titleIndex->find(i) == m_titleIndex->end())
		return m_Variant;
	return m_data[(*m_titleIndex)[i]];
}

const QVariant& XQPandasItem::operator[](const QString& i) const
{
	if (m_titleIndex->find(i) == m_titleIndex->end())
		return m_Variant;
	return m_data[(*m_titleIndex)[i]];
}

QDebug operator<<(QDebug debug, const XQPandasItem& Obj)
{
	debug << Obj.toStringList().join("");
	return debug;
}
