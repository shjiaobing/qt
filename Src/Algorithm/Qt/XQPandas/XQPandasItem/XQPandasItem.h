﻿#ifndef XQPANDASITEM_H
#define XQPANDASITEM_H
#include"XQHead.h"
#include<QOBject>
#include<QVariant>
#include<QDebug>
class XQPandasItem
{
public:
	XQPandasItem();
	XQPandasItem(const QStringList& list, QHash<QString, int>* title=nullptr);
	XQPandasItem(const QVariantList& list);
	XQPandasItem(const XQPandasItem& other);
public:
	//获取一行数据转字符串
	QStringList toStringList()const ;
	//获取一行数据转QVariant
	QVariantList& data();
	const QVariantList& data()const;
	//获取索引处的数据
	const QVariant& at(qsizetype i) const;
	//获取数据数量
	qsizetype size() const;
	//获取是否为空
	bool isEmpty()const;
	//获取第一个数据
	QVariant& front();
	const QVariant& front()const;
	//获取最后一个数据
	QVariant& back();
	const QVariant& back()const;
public:
	//设置标题指针
	void setTitle(QHash<QString, int>*title);
	//清空数据
	void clear();
public:
	QVariantList::iterator begin();
	QVariantList::const_iterator begin()const;
	QVariantList::reverse_iterator rbegin();
	QVariantList::const_reverse_iterator rbegin()const;
	QVariantList::iterator end();
	QVariantList::const_iterator end()const;
	QVariantList::reverse_iterator rend();
	QVariantList::const_reverse_iterator rend()const;
public:
	friend QDebug operator<<(QDebug debug, const XQPandasItem& Obj);
	XQPandasItem& operator<<(QVariant&& other);
	XQPandasItem& operator<<(const QVariant& other);
	XQPandasItem& operator<<(QVariantList&& other);
	XQPandasItem& operator<<(const QVariantList& other);
	XQPandasItem& operator<<(XQPandasItem&& other);
	XQPandasItem& operator<<(const XQPandasItem& other);
	QVariant& operator[](qsizetype i);
	const QVariant& operator[](qsizetype i)const;
	QVariant& operator[](const QString& i);//根据标题获取
	const QVariant& operator[](const QString& i)const;//根据标题获取
protected:
	QVariant m_Variant;
	QVariantList m_data;//数据
	QHash<QString, int>* m_titleIndex=nullptr;//标题
};
#endif // ! XQPandasItem_H
