﻿#ifndef XQPANDAS_H
#define XQPANDAS_H
#include"XQPandasItem/XQPandasItem.h"
class XQPandas
{
public:
	~XQPandas();
	//获取标题列表
	QStringList title()const;
	//获取信息(第一行的信息)
	QString info()const;
	//获取数据是否为空
	bool isEmpty()const;
	//获取数据项数(行数)
	qsizetype size() const;
	//是否读取到结尾了
	bool isReadAtEnd();
	//读取一行数据
	XQPandasItem readLine();
	//获取指定行数的数据
	const XQPandasItem& at(qsizetype i) const;
	//获取第一行的数据
	XQPandasItem& front();
	const XQPandasItem& front()const;
	//获取尾行数据
	XQPandasItem& back();
	const XQPandasItem& back()const;
public:
	//读取csv格式的文本跳过前skipRow行
	void read_csv(const QString& text,int skipRow=0,bool title=true);
	void read_csv(int skipRow = 0, bool title = true);
	//读取文件到缓冲区
	void readfile(const QString& path,const QString& coding="utf8");
	//读取文件开始
	void readCsvFile_begin(const QString& path, int skipRow = 0, bool title = true, const QString& coding = "utf8");
	//读取文件结束
	void readfile_end();
	//写入到文件
	void writefile(const QString& path);
	//写入到文件开始
	void writeCsvFile_begin(const QString& path);
	//写入到文件结束
	void writefile_end();
	//设置标题
	void setTitle(const QStringList& titles);
	void setTitle(int column ,const QString& title);
	//清除标题
	void clearTitle();
	//设置信息
	void setInfo(const QString& info);
	//设置索引列
	//void setIndexColumn(int column);
	//void setIndexColumn(const QString& title);
	//清空
	void clear();
public:
	QList<XQPandasItem>::iterator begin();
	QList<XQPandasItem>::const_iterator begin()const;
	QList<XQPandasItem>::reverse_iterator  rbegin();
	QList<XQPandasItem>::const_reverse_iterator rbegin()const;
	QList<XQPandasItem>::iterator end();
	QList<XQPandasItem>::const_iterator end()const;
	QList<XQPandasItem>::reverse_iterator rend();
	QList<XQPandasItem>::const_reverse_iterator rend()const;
public:
	XQPandas& operator<<(XQPandasItem&& other);
	XQPandas& operator<<(const XQPandasItem& other);
	XQPandas& operator<<(XQPandas&& other);
	XQPandas& operator<<(const XQPandas& other);
	XQPandasItem& operator[](qsizetype i);
	const XQPandasItem& operator[](qsizetype i)const;
protected:
	QFile* m_file_write=nullptr;
	QFile* m_file_read = nullptr;
	QTextCodec* m_codec = nullptr;
	QString m_info;
	QList<XQPandasItem> m_data;//数据
	QString m_buff;//读取缓冲数据
	QHash<QString,int> m_titleIndex;//标题
	QHash<int, QString> m_titleText;//标题
	//int m_indexColumn=0;//索引
};
#endif // ! XQPandas_H