﻿#include"XQLogInfo.h"
#include"XQLog.hpp"
#include<iostream>
#include<QFile>
#include<QDir>
XQLogDate::XQLogDate(const QString& key, LogType type, const QVariantList& data, const QString& info, QDateTime time)
	:key(key), logType(type), data(data), time(time), info(info)
{

}

XQLogDate::XQLogDate(const QVariantMap& map)
{
	setData(map);
}

XQLogOut::XQLogOut(LogOutType outType, XQLog* log, const QString& timeFormat)
	:outType(outType),timeFormat(timeFormat)
{
	key = log->key();
	flags = log->logShowFlags();
	logType = log->logType();
	SaveType = log->saveLocalType();
	data = log->data_toString().toUtf8();
	localPath = log->localPath();
}
XQLogOut::XQLogOut(LogOutType outType, XQLog* log, const QByteArray& data, const QString& timeFormat)
	:outType(outType), timeFormat(timeFormat)
{
	key = log->key();
	flags = log->logShowFlags();
	logType = log->logType();
	SaveType = log->saveLocalType();
	this->data = data;
	localPath = log->localPath();
}

void LogDefaultCMDOut(XQLog* log)
{
	XQLogOut logOut(LogOutType::CMD, log);
	if (log->logShowFlag(XQLog::cmd_queue))
	{
		XQLog::m_queue.push(logOut);
		XQLog::m_wait.notify_one();
	}
	else if (log->logShowFlag(XQLog::cmd_cpp))
	{
		LogCMDOut_Cpp(logOut);
	}
	else if (log->logShowFlag(XQLog::cmd_qt))
	{
		LogCMDOut_Qt(logOut);
	}

}

void LogCMDOut_Qt(const XQLogOut& log)
{
	auto out = [=](const QString& text) {
		switch (log.logType)
		{
		case LogType::Info:qInfo() << text;
			break;
		case LogType::Debug:qDebug() << text;
			break;
		case LogType::Warning:qWarning() << text;
			break;
		case LogType::Critical:qCritical() << text;
			break;
		case LogType::Fatal:qFatal(text.toUtf8());
			break;
		default:
			break;
		};
		};
	QString text;
	auto flags = log.flags;
	if (flags & XQLog::showKey)
	{
		text += log.key + " ";
	}
	if (flags & XQLog::showLogType)
	{
		text += (XQLog::logType_toString(log.logType) + " ");
	}
	if (flags & XQLog::showTime)
	{
		text += (QDateTime::currentDateTime().toString(log.timeFormat) + " ");
	}
	if (flags & XQLog::dataLineFeed)
	{
		out(text);
		text.clear();
	}
	if (!text.isEmpty())
		text += " ";
	text += log.data.trimmed();
	out(text);
}

void LogCMDOut_Cpp(const XQLogOut& log)
{
	auto flags = log.flags;
	if (flags & XQLog::showKey)
	{
		std::cout << (log.key + " ").toLocal8Bit().data();
	}
	if (flags & XQLog::showLogType)
	{
		std::cout << (XQLog::logType_toString(log.logType) + " ").toLocal8Bit().data();
	}
	if (flags & XQLog::showTime)
	{
		std::cout << (QDateTime::currentDateTime().toString(log.timeFormat) + " ").toLocal8Bit().data();
	}
	if (flags & XQLog::dataLineFeed)
	{
		std::cout << std::endl;
	}
	std::cout << QString(log.data).toLocal8Bit().data() << std::endl;
}

void LogLocalOut(const XQLogOut& log)
{
	auto& data = log.data;
	if (!QFile(log.localPath).exists())
	{//文件不存在
		auto dirPath = QFileInfo(log.localPath).dir().path();
		QDir().mkpath(dirPath);//递归创建目录
	}
	QWriteLocker lcok(&XQLog::m_lock);
	QFile file(log.localPath);
	if (!file.open(((log.SaveType == LogSaveType::append) ? QIODeviceBase::Append : QIODeviceBase::NotOpen) | QIODeviceBase::ReadWrite))
		return;//打开失败
	if (file.size() != 0)
		file.write(",");
	file.write(data);
	file.close();
}
