﻿#include"XQLog.hpp"
#include<QDateTime>
#include<QJsonObject>
#include<QJsonArray>
#include<QJsonDocument>
#include<QThread>
#include<QFile>
#include<QFileInfo>
#include<QDir>
#include<QDebug>
//显示格式转化
QString XQLog::variant_to_string(const QVariant& data)
{
	if (data.type() == QVariant::Type::DateTime)
		return data.toDateTime().toString("yyyy-MM-dd hh:mm:ss");
	else if (data.type() == QVariant::Type::Time)
		return data.toTime().toString("hh:mm:ss");
	else
		return data.toString();
	//QDebug::toString("");
}

QJsonObject XQLog::variant_to_json(const QVariant& data)
{
	QJsonObject json;
	if (data.type() == QVariant::DateTime)
	{
		json["DateTime"] = data.toDateTime().toMSecsSinceEpoch();
	}
	else if (data.type() == QVariant::Date)
	{
		json["Date"] = data.toDate().toJulianDay();
	}
	else if (data.type() == QVariant::Time)
	{
		json["Time"] = data.toTime().msecsSinceStartOfDay();
	}
	else
	{
		json["Value"] = data.toJsonValue();
	}
	return std::move(json);
}
QVariant XQLog::json_to_variant(const QJsonObject& json)
{
	auto keys = json.keys();
	if (keys.isEmpty())
		return QVariant();
	auto key = keys.front();
	auto value = json[key];
	if (key == "DateTime")
		return QDateTime::fromMSecsSinceEpoch(value.toInteger());
	else if (key == "Date")
		return QDate::fromJulianDay(value.toInteger());
	else if (key == "Time")
		return QTime::fromMSecsSinceStartOfDay(value.toInt());
	else if (key == "Value")
		return value.toVariant();
	return QVariant();
}