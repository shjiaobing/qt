﻿#ifndef XQLOGINFO_H
#define XQLOGINFO_H
#include<functional>
#include<QString>
#include<QDateTime>
#define XQInfo XQLog("XQInfo")
#define XQDebug XQLog("XQDebug")
#define XQWarning XQLog("XQWarning")
#define XQCritical XQLog("XQCritical")
#define XQFatal XQLog("XQFatal")
class XQLog;
struct XQLogOut;
struct XQLogDate;
struct XQLogSettings;
typedef QList<XQLogDate> XQLogLocalDateList;

//日志的默认控制台输出方式
void LogDefaultCMDOut(XQLog* log);
//日志的默认控制台Qt输出方式
void LogCMDOut_Qt(const XQLogOut& data);
//日志的默认控制台C++输出方式
void LogCMDOut_Cpp(const XQLogOut& data);
//日志的默认保存文件输出方式
void LogDefaultLocalOut(XQLog* log);
//日志的默认保存文件
void LogLocalOut(const XQLogOut& data);

//日志类型
enum class LogType
{
	null,//空的
	Info,//信息
	Debug,//调试信息
	Warning,//警告
	Critical,//严重错误
	Fatal//致命错误
};
//日志输出类型
enum class LogOutType
{
	CMD,//cmd
	Local//本地
};
//日志本地保存类型
enum class LogSaveType
{
	null,
	append,//追加
	replace//替换覆盖
};

//日志设置
struct XQLogSettings
{
	XQLog* Global = nullptr;
	LogType logType = LogType::Debug;
	int flags = 0;
	std::function<void(XQLog*)> CmdOutFunc = LogDefaultCMDOut;//输出方式
	//日志
	LogSaveType saveLocalType = LogSaveType::append;//保存类型
	QString localPath;//保存到本地路径
	std::function<void(XQLog*)> LocalOutFunc = LogDefaultLocalOut;//输出方式
	//时间
	QString timeFormat = "yyyy-MM-dd hh:mm:ss";//时间格式
};
//日志输出
struct XQLogOut
{
	XQLogOut() = default;
	XQLogOut(LogOutType outType, XQLog* log, const QString& timeFormat= "yyyy-MM-dd hh:mm:ss");
	XQLogOut(LogOutType outType, XQLog* log, const QByteArray& data,const QString& timeFormat = "yyyy-MM-dd hh:mm:ss");
	QString key;//键值-名字
	LogOutType outType = LogOutType::CMD;
	int flags = 0;
	LogType logType = LogType::Debug;
	LogSaveType SaveType = LogSaveType::append;
	QByteArray data;//数据
	QString localPath;//保存到本地路径
	//时间
	QString timeFormat = "yyyy-MM-dd hh:mm:ss";//时间格式
};
//日志数据
struct XQLogDate
{
	XQLogDate() = default;
	XQLogDate(const QString& key, LogType type, const QVariantList& data, const QString& info, QDateTime time = QDateTime::currentDateTime());
	XQLogDate(const QVariantMap& map);
	void setData(const QVariantMap& map);
	QString key;//键值-名字
	LogType logType = LogType::Debug;//日志类型
	QDateTime time;//时间
	QVariantList data;//数据
	QString info;//信息
};
#endif
