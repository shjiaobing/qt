# XQAlgorithm

## qt随机数

> **#define random(min,max) QRandomGenerator::global()->bounded(min, max)**

```c++
random(min,max) ;
```

## 递归删除文件

```c++
void deleteDirectory(const QString& dirPath);
```

## 递归移动目录

```c++
bool moveDir(const QString& srcPath, const QString& dstPath);
```

## 文件大小转换成可读的字符串，后加B,KB,....

```c++
QString readableFileSize(quint64 filesize);
```

## 设置css样式表文件到QWidget

```c++
void setCssFile(QWidget* widget, const QString& css);
```

## 正则匹配

```c++
bool RegexMatch(const QString& test, const QString& pattern);
```

## QWidget居中显示

> 将一个QWidget以另一个QWidget为参照物居中显示到其之上

```c++
void centerShow(QWidget* showWidget/*需要显示的*/, QWidget* refer/*参照物*/);
```

## 事件循环暂停

```c++
void XQDelayEventLoop(qint64 msec);
```

# QCoding

文本编码转化

## Utf8转Gbk

```c++
static const std::string Utf8ToGbk(const std::string& str);
static QByteArray Utf8ToGbk(const QByteArray& str);
```

## Gbk转Utf8

```c++
static const std::string GbkToUtf8(const std::string& str);
static QByteArray GbkToUtf8(const QByteArray& str);
```

# XQArguments

# XQRCode

> 二维码生成 
>
> 这里使用了某位大佬的纯C源码生成二维码
>
> 一般只要用静态成员函数就够了

## public(构造)

### 文本构造

```c++
XQRCode(const QString& text);
```

## public(静态)

### 获取二维码Pixmap对象

```c++
static QPixmap to_Pixmap(const QString& text, int width = 10, const QColor& backColor = Qt::black, const QColor& color = Qt::white);
```

### 获取二维码Pixmap对象黑色背景

```c++
static QPixmap to_Pixmap_black(const QString& text, int width = 10);
```

### 获取二维码Pixmap对象白色背景

```c++
static QPixmap to_Pixmap_white(const QString& text, int width = 10);
```

## public(获取)

### 获取二维码数组的大小(边长)

```c++
int data_size()const;
```

### 获取二维码数据

```c++
QVector<QVector<unsigned char>> data()const;
```

### 转QPixmap 

> 一个点的宽度,背景颜色,颜色

```c++
QPixmap to_Pixmap(int width = 10, const QColor& backColor = Qt::black, const QColor& color = Qt::white);
```

### 获取黑色背景二维码

```c++
QPixmap to_Pixmap_black(int width=10);
```

### 获取白色背景二维码

```c++
QPixmap to_Pixmap_white(int width=10);
```

### 编码数据转控制台输出 01数据

```c++
void data_outCmd()const;
```

### 编码数据

```c++
bool qrcode_encode(const QString& text);
```



# XQLog



# XQPandas

> 现只支持csv格式的表格数据读写

## public (读取信息)

### 获取标题列表

```c++
QStringList title()const;
```

### 获取信息

> 首行的信息，读取时需要跳过这行

```c++
QString info()const;
```

### 获取数据是否为空

```c++
bool isEmpty()const;
```

### 获取数据项数(行数)

```c++
qsizetype size() const;
```

### 是否读取到结尾了

```c++
bool isReadAtEnd();
```

### 读取一行数据

```c++
XQPandasItem readLine();
```

### 获取指定行数的数据

```c++
const XQPandasItem& at(qsizetype i) const;
```

### 获取第一行的数据

```c++
XQPandasItem& front();
const XQPandasItem& front()const;
```

### 获取尾行数据

```c++
XQPandasItem& back();
const XQPandasItem& back()const;
```

## public (设置)

### 读取csv格式的文本跳过前skipRow行

```c++
void read_csv(const QString& text,int skipRow=0,bool title=true);
void read_csv(int skipRow = 0, bool title = true);
```

### 读取文件到缓冲区

```c++
void readfile(const QString& path,const QString& coding="utf8");
```

### 读取文件开始

```c++
void readCsvFile_begin(const QString& path, int skipRow = 0, bool title = true, const QString& coding = "utf8");
```

### 读取文件结束

```c++
void readfile_end();
```

### 写入到文件

> 从缓冲区写入到文件

```c++
void writefile(const QString& path);
```

### 写入到文件开始

```c++
void writeCsvFile_begin(const QString& path);
```

### 写入到文件结束

```c++
void writefile_end();
```

### 设置标题

```c++
void setTitle(const QStringList& titles);
void setTitle(int column ,const QString& title);
```

### 清除标题

```c++
void clearTitle();
```

### 设置信息

```c++
void setInfo(const QString& info);
```

### 清空

```c++
void clear();
```

## public (迭代器)

```c++
QList<XQPandasItem>::iterator begin();
QList<XQPandasItem>::const_iterator begin()const;
QList<XQPandasItem>::reverse_iterator  rbegin();
QList<XQPandasItem>::const_reverse_iterator rbegin()const;
QList<XQPandasItem>::iterator end();
QList<XQPandasItem>::const_iterator end()const;
QList<XQPandasItem>::reverse_iterator rend();
QList<XQPandasItem>::const_reverse_iterator rend()const;
```

## public (运算符重载)

```c++
XQPandas& operator<<(XQPandasItem&& other);
XQPandas& operator<<(const XQPandasItem& other);
XQPandas& operator<<(XQPandas&& other);
XQPandas& operator<<(const XQPandas& other);
XQPandasItem& operator[](qsizetype i);
const XQPandasItem& operator[](qsizetype i)const;
```

# XQPandasItem

> XQPandas的一项(一行数据处理)

## public (构造函数)

```c++
//list 一列数据   title 标题
XQPandasItem(const QStringList& list, QHash<QString, int>* title=nullptr);
XQPandasItem(const QVariantList& list);
XQPandasItem(const XQPandasItem& other);
```

## public (读取信息)

### 获取一行数据转字符串

```c++
QStringList toStringList()const ;
```

### 获取一行数据转QVariant

```c++
QVariantList& data();
const QVariantList& data()const;
```

### 获取索引处的数据

```c++
const QVariant& at(qsizetype i) const;
```

### 获取是否为空

```c++
bool isEmpty()const;
```

### 获取第一个数据

```c++
QVariant& front();
const QVariant& front()const;
```

### 获取最后一个数据

```c++
QVariant& back();
const QVariant& back()const;
```

## public (设置)

### 设置标题指针

```c++
void setTitle(QHash<QString, int>*title);
```

### 清空数据

```c++
void clear();
```

## public (迭代器)

```c++
QVariantList::iterator begin();
QVariantList::const_iterator begin()const;
QVariantList::reverse_iterator rbegin();
QVariantList::const_reverse_iterator rbegin()const;
QVariantList::iterator end();
QVariantList::const_iterator end()const;
QVariantList::reverse_iterator rend();
QVariantList::const_reverse_iterator rend()const;
```

## public (运算符重载)

```c++
friend QDebug operator<<(QDebug debug, const XQPandasItem& Obj);
XQPandasItem& operator<<(QVariant&& other);
XQPandasItem& operator<<(const QVariant& other);
XQPandasItem& operator<<(QVariantList&& other);
XQPandasItem& operator<<(const QVariantList& other);
XQPandasItem& operator<<(XQPandasItem&& other);
XQPandasItem& operator<<(const XQPandasItem& other);
QVariant& operator[](qsizetype i);
const QVariant& operator[](qsizetype i)const;
QVariant& operator[](const QString& i);//根据标题获取
const QVariant& operator[](const QString& i)const;//根据标题获取
```

# XQSettingsInfo

# imerGroup