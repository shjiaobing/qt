﻿#ifndef XQSTRETCHEVENTFILTER_H
#define XQSTRETCHEVENTFILTER_H
#include<QWidget>
#include<QRect>
//窗口拉伸过滤器
class XQStretchEventFilter : public QObject
{
    Q_OBJECT
public:
    explicit XQStretchEventFilter(QObject* parent = nullptr);
    explicit XQStretchEventFilter(QWidget* parent);
    //设置要控制的QWidget
    void setWidget(QWidget* widget);
    //设置边框宽度
    void borderWidth(int width);
    enum class Edge {
        None,//无状态
        Top,//上
        Bottom,//下
        Left,//左
        Right,//右
        TopLeft,//左上
        TopRight,//右上
        BottomLeft,//左下
        BottomRight//右下
    };
protected:
    bool eventFilter(QObject* obj, QEvent* event) override;
private:
    Edge m_edge= Edge::None;
    QWidget* m_widget=nullptr;  // 保存窗口指针
    int m_resizeBorderWidth=4;  // 窗口边框宽度
    QRect m_rect;  // 鼠标按下后的窗口位置信息
};
#endif // !RESIZEEVENTFILTER_H

