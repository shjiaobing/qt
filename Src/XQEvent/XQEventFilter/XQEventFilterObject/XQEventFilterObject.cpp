﻿#include"XQEventFilterObject.h"
#include<QEvent>
XQEventFilterObject::XQEventFilterObject(const int EventType, QObject* parent)
	:QObject(parent),
	m_EventType(EventType)
{
}
XQEventFilterObject::XQEventFilterObject(std::function<void(QEvent* ev)> func, const int EventType, QObject* parent)
	:QObject(parent),
	m_eventFilterFunc(func),
	m_EventType(EventType)
{
}
XQEventFilterObject::XQEventFilterObject(std::function<void()> func, const int EventType, QObject* parent)
	:QObject(parent),
	m_eventFilterFunc([=](QEvent* ev) {func(); }),
	m_EventType(EventType)
{

}

void XQEventFilterObject::setFilterFunc(std::function<void(QEvent* ev)> func)
{
	m_eventFilterFunc = func;
}
void XQEventFilterObject::setFilterFunc(std::function<void()> func)
{
	m_eventFilterFunc = [=](QEvent* ev) {func(); };
}
bool XQEventFilterObject::eventFilter(QObject* object, QEvent* ev)
{
    if(ev->type()== m_EventType)
	{
		ev->ignore();
		m_eventFilterFunc(ev);  
		return ev->isAccepted();
	}
    return QObject::eventFilter(object, ev);
}
