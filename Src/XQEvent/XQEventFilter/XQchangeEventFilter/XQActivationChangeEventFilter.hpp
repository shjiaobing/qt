﻿#ifndef XQACTIVATIONCHANGEEVENTFILTER_HPP
#define XQACTIVATIONCHANGEEVENTFILTER_HPP
#include"XQEventFilterObject.h"
#include<QEnterEvent>
//部件或窗口激活状态改变过滤器
class XQActivationChangeEventFilter :public XQEventFilterObject
{
public:
    template <typename Ty>
    XQActivationChangeEventFilter(Ty func, QObject* parent = nullptr)
        :XQEventFilterObject(func, QEvent::ActivationChange, parent) {}
};
#endif // !MouseEventFilter_enter_H