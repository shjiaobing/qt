﻿#ifndef XQFOCUSLOSEEVENTFILTER_HPP
#define XQFOCUSLOSEEVENTFILTER_HPP
#include"XQEventFilterObject.h"
#include<QEnterEvent>
//部件或窗口焦点失去事件过滤器
class XQFocusLoseEventFilter :public XQEventFilterObject
{
public:
    XQFocusLoseEventFilter(std::function<void()> func, QObject* parent = nullptr)
        :XQEventFilterObject(func, QEvent::FocusOut, parent) {}

    XQFocusLoseEventFilter(std::function<bool(QFocusEvent*)> func, QObject* parent = nullptr)
        :XQEventFilterObject(QEvent::FocusOut, parent)
    {
        auto eventFilterFunc = [=](QEvent* ev)
        {
            func(static_cast<QFocusEvent*>(ev));        //该事件已经被处理
        };
        setFilterFunc(eventFilterFunc);
    }
};
#endif // !MouseEventFilter_enter_H
