﻿#ifndef XQRETURNEVENTFILTER_HPP
#define XQRETURNEVENTFILTER_HPP
#include"XQEventFilterObject.h"
#include<QKeyEvent>
//回车按键事件过滤器
class XQReturnEventFilter :public XQEventFilterObject
{
public:
    XQReturnEventFilter(std::function<void()> func, QObject* parent = nullptr)
        :XQEventFilterObject(QEvent::KeyPress, parent)
    {
        auto ke = [=](QEvent* ev)
        {
            QKeyEvent* ke = static_cast<QKeyEvent*>(ev);
            if (ke->key() == Qt::Key_Enter || ke->key() == Qt::Key_Return)
                func();        
        };
        setFilterFunc(ke);
    }
    XQReturnEventFilter(std::function<void(QKeyEvent*)> func, QObject* parent = nullptr)
        :XQEventFilterObject([=](QEvent* ev)
            
    {
        QKeyEvent* ke = static_cast<QKeyEvent*>(ev);
        if (ke->key() == Qt::Key_Enter || ke->key() == Qt::Key_Return)
        {
             func(ke);        
        }
    }, QEvent::KeyPress, parent) { }
};
#endif 