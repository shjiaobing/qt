﻿#ifndef XQMOUSEEVENTFILTER_RELEASE_HPP
#define XQMOUSEEVENTFILTER_RELEASE_HPP
#include"XQEventFilterObject.h"
#include<QMouseEvent>
//鼠标松开事件过滤器
class XQMouseEventFilter_Release :public XQEventFilterObject
{
public:
    XQMouseEventFilter_Release(std::function<void()> func, QObject* parent = nullptr)
        :XQEventFilterObject(func,QEvent::MouseButtonRelease, parent) {}
    XQMouseEventFilter_Release(std::function<void(QMouseEvent*)> func, QObject* parent = nullptr)
        :XQEventFilterObject(QEvent::MouseButtonRelease, parent)
    {
        auto eventFilterFunc = [=](QEvent* ev)
        {
            func(static_cast<QMouseEvent*>(ev));        //该事件已经被处理
        };
        setFilterFunc(eventFilterFunc);
    }
};
#endif // !MouseEventFilter_enter_H