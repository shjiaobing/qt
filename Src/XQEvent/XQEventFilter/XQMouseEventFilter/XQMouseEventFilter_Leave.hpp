﻿#ifndef XQMOUSEEVENTFILTER_LEAVE_HPP
#define XQMOUSEEVENTFILTER_LEAVE_HPP
#include"XQEventFilterObject.h"
#include<QEvent>
//鼠标离开事件过滤器
class XQMouseEventFilter_Leave :public XQEventFilterObject
{
public:
    template <typename Ty>
    XQMouseEventFilter_Leave(Ty func, QObject* parent = nullptr)
        :XQEventFilterObject(func, QEvent::Leave, parent) {}
};
#endif // !MouseEventFilter_enter_H