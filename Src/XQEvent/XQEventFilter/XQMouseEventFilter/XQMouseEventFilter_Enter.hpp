﻿#ifndef XQMOUSEEVENTFILTER_ENTER_HPP
#define XQMOUSEEVENTFILTER_ENTER_HPP
#include"XQEventFilterObject.h"
#include<QEnterEvent>
//鼠标进入事件过滤器
class XQMouseEventFilter_Enter:public XQEventFilterObject
{
public:
    XQMouseEventFilter_Enter(std::function<void()> func, QObject* parent = nullptr)
        :XQEventFilterObject(func,QEvent::Enter, parent){}
    XQMouseEventFilter_Enter(std::function<void(QEnterEvent*)> func, QObject* parent = nullptr)
        :XQEventFilterObject(QEvent::Enter, parent)
    {
        auto eventFilterFunc = [=](QEvent* ev)
        {
            func(static_cast<QEnterEvent*>(ev)); 
        };
        setFilterFunc(eventFilterFunc);
    }
};
#endif // !MouseEventFilter_enter_H
