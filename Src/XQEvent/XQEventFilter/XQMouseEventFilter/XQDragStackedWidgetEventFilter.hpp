﻿#ifndef XQDRAGSTACKEDWIDGETEVENTFILTER_H
#define XQDRAGSTACKEDWIDGETEVENTFILTER_H
//XQStackedWidget 给添加的Qwidget窗口增加会布局拖动
#include <QMouseEvent>
#include"XQStackedTitleWidget.h"
class XQDragStackedWidgetEventFilter :public QObject
{
public:
    XQDragStackedWidgetEventFilter(XQStackedTitleWidget* StackedTitle,QObject* parent = nullptr)
        :QObject(parent), m_TitleWidget(StackedTitle)
    {

    }
    bool eventFilter(QObject* object, QEvent* ev)override
    {
        auto w = qobject_cast<QWidget*>(object);
        if (!w)
            return false;

        if (ev->type() == QEvent::MouseButtonPress)
        {
            auto me = static_cast<QMouseEvent*>(ev);
            pressPos = me->pos();
            m_selectWidgetPos = w->pos();
            //qInfo() << "窗口被按下";
        }
        else if (ev->type() == QEvent::MouseMove)
        {
            auto me = static_cast<QMouseEvent*>(ev);
            auto pos = me->globalPosition().toPoint();
            if(m_TitleWidget->geometry().contains(pos))
            {
                m_TitleWidget->stackedWidget()->addWidget(w);
                w->removeEventFilter(this);
                deleteLater();
            }
            //if (me->buttons() & Qt::MouseButton::LeftButton)
                //w->move(me->globalPosition().toPoint() - pressPos);
                //w->move(me->pos() - pressPos + m_selectWidgetPos);
        }
        return QObject::eventFilter(object, ev);
    }
private:
    QPoint pressPos;
    QPoint m_selectWidgetPos;
    XQStackedTitleWidget* m_TitleWidget;
};
#endif // !__DRAGWIDGETFILTER_H__