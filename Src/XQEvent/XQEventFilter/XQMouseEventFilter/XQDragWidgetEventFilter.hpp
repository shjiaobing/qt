﻿#ifndef XQDRAGWIDGETEVENTFILTER_HPP
#define XQDRAGWIDGETEVENTFILTER_HPP
//无边框窗口拖动
#include <QMouseEvent>
class XQDragWidgetEventFilter :public QObject
{
public:
    XQDragWidgetEventFilter(QObject* parent = nullptr)
        :QObject(parent)
    {

    }
    bool eventFilter(QObject* object, QEvent* ev)override
    {
        auto w = qobject_cast<QWidget*>(object);
        if (!w)
            return false;

        if (ev->type() == QEvent::MouseButtonPress)
        {
            auto me = static_cast<QMouseEvent*>(ev);
            pressPos = me->pos();
            m_selectWidgetPos = w->pos();
        }
        else if (ev->type() == QEvent::MouseMove)
        {
            auto me = static_cast<QMouseEvent*>(ev);
            if (me->buttons() & Qt::MouseButton::LeftButton)
                //w->move(me->globalPosition().toPoint() - pressPos);
                w->move(me->pos() - pressPos + m_selectWidgetPos);
        }
        return QObject::eventFilter(object, ev);
    }
private:
    QPoint pressPos;
    QPoint m_selectWidgetPos;
};
#endif // !__DRAGWIDGETFILTER_H__