﻿#ifndef XQFUNCEVENT_H
#define XQFUNCEVENT_H
#include<functional>
#include<QEvent>
#include<QApplication>
#include"EventType.h"
//发送函数事件
class XQFuncEvent:public QEvent
{
public:
	friend class XQFuncEventFilter;
	//初始化要发送的函数
	XQFuncEvent(std::function<void()> func);
	//将func函数以异步事件的方式发送给receiver
	XQFuncEvent(QObject* receiver,std::function<void()>&& func);
	/*template<typename Fx, typename...Args>
	XQFuncEvent(QObject* receiver, Fx&& f, Args&& ...args);*/
	//异步的方式发送事件必须创建在堆区，自动释放
	void postEvent(QObject* receiver,int priority = Qt::NormalEventPriority);
protected:
	std::function<void()> m_func;
};
//template<typename Fx, typename...Args>
//inline XQFuncEvent::XQFuncEvent(QObject* receiver, Fx&& f, Args&& ...args)
//	:QEvent(QEvent::Type(ET_FuncEvent))
//{
//	m_func = [=] {
//		std::bind(f, args...)();
//	};
//	QApplication::postEvent(receiver, this);
//}
#endif 

