﻿#include"XSystemInfo.h"
#include <QtGlobal>
#if defined(_WIN32)
#include "SysInfoWindowsImpl.h"
#elif defined(__APPLE__)
#include "sysinfomacimpl.h"
#elif defined(__linux__)
#include "SysInfoLinuxImpl.h"
#endif

XSystemInfo& XSystemInfo::instance()
{
#if defined(_WIN32)
    static SysInfoWindowsImpl singleton;
#elif defined(__APPLE__)
    static SysInfoMacImpl singleton;
#elif defined(__linux__)
    static SysInfoLinuxImpl singleton;
#endif
    return singleton;
}
XSystemInfo::XSystemInfo() {}
XSystemInfo::~XSystemInfo() {}