﻿//sysinfolinuximpl.h
#ifdef __linux__
#ifndef SYSINFOLINUXIMPL_H
#define SYSINFOLINUXIMPL_H
#include <QtGlobal>
#include <QVector>
#include "XSystemInfo.h"

class SysInfoLinuxImpl : public XSystemInfo
{
public:
    SysInfoLinuxImpl();
    void init() override;
    double cpuLoadAverage() override;
    double memoryUsed() override;
private:
    QVector<qulonglong> cpuRawData();
private:
    QVector<qulonglong> mCpuLoadLastValues;
};

#endif // SYSINFOLINUXIMPL_H
#endif // __linux__

