﻿#include"XQAppInfo.h"
#include<QCoreApplication>
XQAppInfo* XQAppInfo::m_Global =nullptr;
XQMySql* XQAppInfo::m_mysql = nullptr;
XQFtp* XQAppInfo::m_ftp = nullptr;
XQMainWindow* XQAppInfo::m_mainWindow;//主窗口
XQStackedWidget* XQAppInfo::m_StackedWidget = nullptr;//堆栈小部件
XQUserInfo* XQAppInfo::m_userInfo = nullptr;//用户信息
XQSettingsInfo* XQAppInfo::m_settingsInfo = nullptr;//配置信息文件
QThreadPool* XQAppInfo::m_pool = nullptr;//线程池
XQSystemTray* XQAppInfo::m_systemTray = nullptr;//系统托盘
QMap<RobotType, XQRobot*> XQAppInfo::m_Robot;
XQTimerGroup* XQAppInfo::m_timerGroup = nullptr;
QDateTime XQAppInfo::m_startTime= QDateTime::currentDateTime();//启动时间
XQAppInfo::XQAppInfo(QObject* parent)
	:QObject(parent)
{
}

XQAppInfo::~XQAppInfo()
{
}

XQAppInfo* XQAppInfo::Global()
{
	return m_Global;
}

void XQAppInfo::setGlobal(XQAppInfo* info)
{
	if (m_Global != nullptr)
		m_Global->deleteLater();
	m_Global = info;
}

QDateTime XQAppInfo::startTime()
{
	return m_startTime;
}

XQMySql* XQAppInfo::mysql() 
{
	return m_mysql;
}

XQUserInfo* XQAppInfo::userInfo() 
{
	return m_userInfo;
}

XQFtp* XQAppInfo::ftp() 
{
	return m_ftp;
}

XQSettingsInfo* XQAppInfo::settingsInfo() 
{
	return m_settingsInfo;
}

QThreadPool* XQAppInfo::threadPool() 
{
	return m_pool;
}

XQSystemTray* XQAppInfo::systemTray() 
{
	return m_systemTray;
}

XQMainWindow* XQAppInfo::mainWindow()
{
	return m_mainWindow;
}

XQStackedWidget* XQAppInfo::stackedWidget()
{
	return m_StackedWidget;
}

XQTimerGroup* XQAppInfo::timerGroup()
{
	return m_timerGroup;
}

void XQAppInfo::setMySql(XQMySql* mysql)
{
	if (mysql == m_mysql)
		return;
	m_mysql = mysql;
	emit mysqlChange(m_mysql);
}

void XQAppInfo::setUserInfo(XQUserInfo* userInfo)
{
	if (userInfo == m_userInfo)
		return;
	m_userInfo = userInfo;
	emit userInfoChange(m_userInfo);
}

void XQAppInfo::setFtp(XQFtp* ftp)
{
	if (ftp == m_ftp)
		return;
	m_ftp = ftp;
	emit ftpChange(m_ftp);
}

void XQAppInfo::setSettingsInfo(XQSettingsInfo* info)
{
	if (info == m_settingsInfo)
		return;
	m_settingsInfo = info;
	emit settingsInfoChange(m_settingsInfo);
}

void XQAppInfo::setThreadPool(QThreadPool* pool)
{
	if (pool == m_pool)
		return;
	m_pool = pool;
	emit threadPoolChange(m_pool);
}
void XQAppInfo::setSystemTray(XQSystemTray* systemTray)
{
	m_systemTray = systemTray;
}
void XQAppInfo::setMainWindow(XQMainWindow* window)
{
	m_mainWindow = window;
}
void XQAppInfo::setStackedWidget(XQStackedWidget* widget)
{
	m_StackedWidget = widget;
}
void XQAppInfo::addRobot(RobotType type, XQRobot* robot)
{
	if(robot)
		m_Robot[type] = robot;
}
void XQAppInfo::removeRobot(RobotType type, bool del)
{
	auto robot = this->robot(type);
	if (robot&&del)
		robot->deleteLater();
	m_Robot.remove(type);
}
void XQAppInfo::setTimerGroup(XQTimerGroup* group)
{
	m_timerGroup = group;
}
//等待一次事件循环
void XQAppInfo::processEvents(int maxtime)
{
	if(maxtime==0)
		QCoreApplication::processEvents(QEventLoop::AllEvents);
	else
		QCoreApplication::processEvents(QEventLoop::AllEvents, maxtime);
}
