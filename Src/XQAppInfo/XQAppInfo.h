﻿#ifndef XQAPPINFO_H
#define XQAPPINFO_H
#include<QObject>
#include<QThreadPool>
#include<QPixmap>
#include<QMap>
#include<QDateTime>
#include"XQHead.h"
#include"XQRobot.h"
class XQAppInfo:public QObject
{
	Q_OBJECT
public:
	XQAppInfo(QObject* parent = nullptr);
	~XQAppInfo();
	//获取全局唯一的XQAppInfo* 指针
	static XQAppInfo* Global();
	static void setGlobal(XQAppInfo* info);
	//获取程序启动时间
	static QDateTime startTime();
	//获取使用的数据库
	static XQMySql* mysql();
	//获取用户数据
	static XQUserInfo* userInfo();
	//获取ftp
	static XQFtp* ftp();
	//获取配置信息
	static XQSettingsInfo* settingsInfo();
	//获取线程池
	static QThreadPool* threadPool();
	//获取系统托盘类
	static XQSystemTray* systemTray();
	//获取主窗口
	static XQMainWindow* mainWindow();
	//获取堆栈小部件
	static XQStackedWidget* stackedWidget();
	//获取机器人
	template<typename T= XQRobot>
	static T* robot(RobotType type= RobotType::DingDing);
	//获得定时器任务组
	static XQTimerGroup* timerGroup();
public slots:
	//设置数据库
	void setMySql(XQMySql* mysql);//不提供mysql的内存释放，因为可能共享使用
	//设置用户数据类指针存储数据
	void setUserInfo(XQUserInfo* userInfo);
	//设置ftp
	void setFtp(XQFtp* ftp);
	//绑定设置信息
	void setSettingsInfo(XQSettingsInfo* info);
	//设置线程池
	void setThreadPool(QThreadPool* pool);
	//设置系统托盘类
	void setSystemTray(XQSystemTray* systemTray);
	//设置主窗口
	void setMainWindow(XQMainWindow* window);
	//设置堆栈小部件
	void setStackedWidget(XQStackedWidget* widget);
	//添加机器人
	void addRobot(RobotType type, XQRobot* robot);
	//删除机器人
	void removeRobot(RobotType type,bool del);
	//设置定时器任务组
	void setTimerGroup(XQTimerGroup* group);
public slots:
	//等待一次事件循环
	void processEvents(int maxtime=0);
signals://信号
	void mysqlChange(XQMySql* mysql);
	void userInfoChange(XQUserInfo* userInfo);
	void ftpChange(XQFtp* ftp);
	void settingsInfoChange(XQSettingsInfo* info);
	void threadPoolChange(QThreadPool* pool);
signals://信号
	//状态栏显示提示信息
	void showMessage(const QString& text, int timeout = 0);
	//堆栈窗口显示小部件
	void showStackedWidget(int type, QWidget* widget = nullptr, const QString& title = QString(), const QPixmap& icon = QPixmap());
	//设置进度
	void setProgressRange(int min, int max);
	void setProgressValue(int val);
	void setProgressValueSize(size_t val, size_t maxSize, size_t delay = 5000);
	void setProgressValueCount(size_t val, size_t maxSize, size_t delay = 5000);
protected://变量
	static XQAppInfo* m_Global;
	static QDateTime m_startTime;//启动时间
	static XQMySql* m_mysql ;
	static XQFtp* m_ftp ;
	static XQMainWindow* m_mainWindow;//主窗口
	static XQStackedWidget* m_StackedWidget;//堆栈小部件
	static XQUserInfo* m_userInfo ;//用户信息
	static XQSettingsInfo* m_settingsInfo;//配置信息文件
	static QThreadPool* m_pool ;//线程池
	static XQSystemTray* m_systemTray;//系统托盘
	static QMap<RobotType, XQRobot*> m_Robot;//机器人
	static XQTimerGroup* m_timerGroup ;//定时器任务组
};
template<typename T>
inline T* XQAppInfo::robot(RobotType type)
{
	if (m_Robot.find(type) == m_Robot.end())
		return nullptr;
	return static_cast<T*>(m_Robot[type]);
}
#endif


