﻿#ifndef XQFILETHREAD_H
#define XQFILETHREAD_H
#include<QThreadPool>
#include<QFile>
#include"XSymbolOverload.h"
#include<atomic>
#define BUFFERSIZE 4096//默认缓冲区大小
class XQFileThread:public QFile
{
	Q_OBJECT
public:
	XQFileThread(const QString& name, QThreadPool* pool = QThreadPool::globalInstance());
	~XQFileThread();
	//获取缓冲区大小
	size_t bufferSize()const;
	//获取使用的线程池
	QThreadPool* threadPool()const;
	//获取线拷贝进度信号发送间隔
	size_t progressSignalsTime()const;
	//获取新文件的名字(带路径)
	QString newName()const;
	//获取现在的任务数量
	int tasksCount()const;
public slots:
	//设置线程池，默认全局线程池
	void setThreadPool(QThreadPool* pool = QThreadPool::globalInstance());
	//设置缓冲区
	void setBufferSize(const size_t size);
	//拷贝文件到新的路径
	void copy(const QString& newName);
	//设置进度信号发送时间
	void setProgressSignalsTime(size_t time);
signals://信号
	//任务开始
	void copyStart(const QString& name,const QString& newName,const size_t fileSize);
	//任务进度
	void copyProgress(const QString& name, const QString& newName,const size_t currentSize, const size_t fileSize);
	//任务结束
	void copyFinish(const QString& name, const QString& newName, const size_t fileSize);
protected:
	void init();//初始化
	void _copy(const QString& newName);
	void sendCopyProgress(const QString& newName, size_t sumSize);//发送拷贝进度信号
protected:
	QThreadPool* m_pool=nullptr;//全局线程池
	char* m_buffer=nullptr;//缓冲区
	size_t m_bufferSize=0;//缓冲区大小
	QDateTime* m_startTime=nullptr;//开始的时间记录
	size_t m_progressSignalsTime = 1000;//进度信号间隔
	QString m_newName;//新文件的名字带路径
	static std::atomic<int> m_tasksCount;//任务数量
};
#endif