﻿#ifndef XQRUNNABLE_H
#define XQRUNNABLE_H
#include<QRunnable>
#include<atomic>
#include"XQThreadList.hpp"
class XQTaskPool;
class QEventLoop;
class XQRunnable:public QRunnable
{
public:
	XQRunnable();
	~XQRunnable();
	//创建一个任务
	static XQRunnable* create(std::function<void(XQRunnable*)> functionToRun);
	static XQRunnable* create(std::function<void()> functionToRun);
public:
	//获得当前绑定的任务池
	XQTaskPool* taskPool()const;
	//当前任务是否需要中断
	bool isInterruptionRequested();
	//线程是否结束退出了
	bool isQuit();
public:
	//发送停止此线程请求
	void requestInterruption();
	//进入Qt事件循环等待线程结束
	void wait_event();
	//循环阻塞 毫秒时间自旋检查
	void wait(int msec=100);
	//绑定一个任务
	void setFunc(std::function<void(XQRunnable*)> functionToRun);
	//绑定任务池结束时从任务池释放
	void setTaskPool(XQTaskPool* taskPool);
protected:
	void run()override;
protected:
	std::atomic<bool> m_stop = false;//结束
	std::atomic<bool> m_quit = false;//退出
	std::function<void(XQRunnable*)> m_func;//任务
	XQTaskPool* m_taskPool = nullptr;//加入的任务组
};
#endif