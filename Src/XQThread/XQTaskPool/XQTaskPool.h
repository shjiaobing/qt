﻿#ifndef XQTASKPOOL_H
#define XQTASKPOOL_H
#include"XQHead.h"
#include"XQRunnable.h"
#include<unordered_set>
#include<QThreadPool>
#include<QReadWriteLock>
#include<QWaitCondition>
//任务池
class XQTaskPool : public QThreadPool
{
	Q_OBJECT
public:
	friend  class XQRunnable;
	XQTaskPool(QObject* parent = nullptr);
	~XQTaskPool();
	//获取当前剩余的任务数量
	size_t size();
	bool isEmpty() ;
	bool isExists(XQRunnable* task);
	//获取名字
	QString name()const;
public:
	//设置名字 
	void setName(const QString& name);
	//添加一个多线程任务
	XQRunnable* addTask(XQRunnable* task, int priority = 0);
	XQRunnable* addTask(std::function<void()>&& task, int priority = 0);
	XQRunnable* addTask(std::function<void(XQRunnable*)>&& task, int priority = 0);
	//结束一个多线程任务
	void freeTask(XQRunnable* task);
	//请求中断程序
	void requestInterruption();
public:
	//等待线程结束
	void wait();
	//等待某一个任务结束
	void wait(XQRunnable* task);
	//等待剩余任务数量达标
	void wait(int count);
public:
	//等待线程结束
	void wait_event();
	//等待某一个任务结束
	void wait_event(XQRunnable* task);
	void wait_event(const QList<XQRunnable*>& task);
	//等待剩余任务数量达标
	void wait_event(int count);
	//等待某一个任务以外的全部任务结束
	void waitNot(XQRunnable* task);
signals://信号
	//任务池开始了
	void start();
	void start(const QString& name);
	void start(XQTaskPool* pool);
	//一个任务开始了
	void taskStart(XQRunnable* task);
	//一个任务完成了
	void taskFinish(XQRunnable* task);
	//任务池全部结束了
	void finish();
	void finish(const QString& name);
	void finish(XQTaskPool* pool);
	//请求退出信号
	void requestQuit();
protected:
	void init();
	//一个任务开始运行了
	void runTask(XQRunnable* task);
protected:
	QWaitCondition m_waitOne;//一个线程结束的时候
	QWaitCondition m_waitAll;//全部结束时候的条件变量
	QReadWriteLock m_taskLock;//锁
	std::unordered_set<XQRunnable*> m_taskList;//多线程任务列表
	QString m_name;//名字
};
#endif

