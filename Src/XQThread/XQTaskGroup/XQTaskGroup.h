﻿#ifndef XQTASKGROUP_H
#define XQTASKGROUP_H
#include"XQHead.h"
#include"XQThreadMap.hpp"
#include<QObject>
#include<QReadWriteLock>
//管理任务池的任务组
class XQTaskGroup:public QObject
{
	Q_OBJECT
public:
	XQTaskGroup(QObject* parent = nullptr);
	~XQTaskGroup();
	//获取当前剩余任务池
	size_t size();
	//获取内部的任务池
	XQTaskPool* taskPool(const QString& name);
public:
	XQTaskPool* addTaskPool(const QString& name);
	//释放一个任务池
	void freeTaskPool(const QString& name);
	//删除一个任务池
	void removeTaskPool(const QString& name);
	//任务退出
	void requestInterruption();
	//指定名字的任务池任务退出
	void requestInterruption(const QString& name);
	//等待全部任务池结束
	void wait();
	//等待指定名字的任务池结束
	void wait(const QString& name);
	//等待指定名字的任务池结束
	void wait(const QString& name, XQRunnable* task);
public:
	//等待全部任务池结束
	void wait_event();
	//等待指定名字的任务池结束
	void wait_event(const QString& name);
	//等待指定名字的任务池结束
	void wait_event(const QString& name, XQRunnable* task);
signals://信号
	//一个任务池开始了
	void taskPoolStart(XQTaskPool* pool);
	void taskPoolStart(const QString& name);
	//一个任务池完成了
	void taskPoolFinish(XQTaskPool* pool);
	void taskPoolFinish(const QString& name);
	//一个任务开始了
	void taskStart(XQRunnable* task);
	//一个任务完成了
	void taskFinish(XQRunnable* task);
protected:
	void init();
	void init_taskPool(XQTaskPool* pool);
	void disconnect_taskPool(XQTaskPool* pool);
protected:
	QReadWriteLock m_poolLock;//锁
	std::unordered_map<QString, XQTaskPool*> m_poolMap;//任务池列表
	//XQThreadMap<QString,XQTaskPool*> m_taskPoolList;//任务池列表
};
#endif // !XQTaskGroup_h
