﻿#ifndef XTHREADQUEUE_H
#define XTHREADQUEUE_H
#include<atomic>
#include<QReadWriteLock>
#include<queue>
//线程队列
template<typename T>
class XQThreadQueue
{
public:
	void push(const T& val);
	T pop();
	size_t size();
	bool isEmpty();
	const T& back();
	const T& front() ;
protected:
	QReadWriteLock m_lock;
	std::queue<T> queue;
};
template<typename T>
inline void XQThreadQueue<T>::push(const T& val)
{
	QWriteLocker locker(&m_lock);
	queue.push(val);
}

template<typename T>
inline T XQThreadQueue<T>::pop()
{
	QWriteLocker locker(&m_lock);
	T val = std::move(queue.front());
	queue.pop();
	return std::move(val);
}

template<typename T>
inline size_t XQThreadQueue<T>::size()
{
	QReadLocker locker(&m_lock);
	return queue.size();
}

template<typename T>
inline bool XQThreadQueue<T>::isEmpty()
{
	QReadLocker locker(&m_lock);
	return queue.empty();
}

template<typename T>
inline const T& XQThreadQueue<T>::back()
{
	QReadLocker locker(&m_lock);
	return queue.back();
}

template<typename T>
inline const T& XQThreadQueue<T>::front()
{
	QReadLocker locker(&m_lock);
	return queue.front();
}

#endif

