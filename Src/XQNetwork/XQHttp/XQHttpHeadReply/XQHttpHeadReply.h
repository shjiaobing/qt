﻿#ifndef XQHTTPHEADREPLY_H
#define XQHTTPHEADREPLY_H
#include"XQHttpHeadObject.h"
//http响应回复
class XQHttpHeadReply :public XQHttpHeadObject
{
public:
	XQHttpHeadReply(const QByteArray& data = QByteArray());
public:
	//请求头转ByteArray输出
	QByteArray header_toByteArray()const;
	//返回状态码
	int statusCode()const;
	//返回状态信息
	QByteArray statusInfo()const;
public:
	//设置http的头部数据报文
	void setData(const QByteArray& data);
	//设置状态码
	void setStatusCode(int code);
	//设置状态信息
	void setStatusInfo(const QByteArray& data);
	//清空
	void clear();
//protected:
	void init();
protected:
	QByteArray m_statusInfo;//状态信息
	int m_statusCode=200;//状态码
};
#endif // !XQHttpHeadRespond_H
