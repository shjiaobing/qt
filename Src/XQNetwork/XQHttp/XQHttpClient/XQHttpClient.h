﻿#ifndef XQHTTPCLIENT_H
#define XQHTTPCLIENT_H
#include<QJsonDocument>
#include"XSymbolOverload.h"
#include"XQHttpObject.h"
#define XQHttpClientLog XQLog("XQHttpClient") //http客户端日志
//http请求
class XQHttpClient:public XQHttpObject
{
	Q_OBJECT
public:
	XQHttpClient(NetworkType type= NetworkType::Tcp,QObject*parent=nullptr);
	~XQHttpClient();
public:
	//获取url
	QString url()const;
	//获取请求头
	XQHttpHeadRequest* request()const;
	//获取响应头
	XQHttpHeadReply* reply()const;
	//获取其文本
	QString toString()const;
	//获取其内容
	QByteArray toByteArray()const;
	//转数组
	QVariantList toList()const;
public://静态成员阻塞版
	static QByteArray Get(NetworkType type,const QString& url, int timeout = 10_s, bool toUtf8 = false/*是否转码*/);
	static QByteArray Get_Tcp(const QString& url, int timeout = 10_s,bool toUtf8=false/*是否转码*/);
	static QByteArray Get_Local(const QString& url, int timeout = 10_s, bool toUtf8 = false/*是否转码*/);
	static bool Post(NetworkType type,const QString& url, const QByteArray& data, int timeout = 10_s);
	static bool Post(NetworkType type, const QString& url, const QJsonDocument& json, int timeout = 10_s);
	static QByteArray Post_Reply(NetworkType type, const QString& url, const QByteArray& data, int timeout = 10_s, bool toUtf8 = false/*是否转码*/);
	static QByteArray Post_Reply(NetworkType type, const QString& url, const QJsonDocument& json, int timeout = 10_s, bool toUtf8 = false/*是否转码*/);
	static bool Post_Tcp(const QString& url, const QByteArray& data, int timeout = 10_s);
	static bool Post_Tcp(const QString& url, const QJsonDocument& json, int timeout = 10_s);
	static bool Post_Local(const QString& url, const QByteArray& data, int timeout = 10_s);
	static bool Post_Local(const QString& url, const QJsonDocument& json, int timeout = 10_s);
public://异步版本
	void setUrl(const QString& url);
	//发送get请求
	bool get(const QString& url);
	bool get();
	//发送pos请求
	bool post(const QString& url, const QByteArray& data);
	bool post(const QByteArray& data);
	bool post(const QString& url, const QJsonDocument& json);
	//等待响应  超时退出
	bool wait_Reply(int timeout = 10_s);
	//发送信号
	void sendSignals(const QString& name, const QVariantList& list);
	//清空初始化请求头
	void clear_request();
public:
	//设置本地服务器名字
	void setServerName_local(const QString& name);
	//连接到服务器
	void connectToServer(QIODeviceBase::OpenMode openMode = QIODeviceBase::ReadWrite);
	void connectToServer(const QString& url, QIODeviceBase::OpenMode openMode = QIODeviceBase::ReadWrite);
	//连接到服务器并等待
	bool connectToServerWait(const QString& url,int msecs = 3_s);
	bool connectToServerWait(int msecs = 3_s);
	//仅等待连接到服务器
	void waitForConnected_local(int msecs = 3_s);
	//断开服务器连接
	void close();
public:

signals://信号
	//连接上服务器
	void connected();
	//断开与服务器连接
	void disconnected();
protected:
	void init();
	//接收处理
	void readyRead();
	//连接上处理
	void readyConnected();
	//断开连接处理
	void readyDisconnected();
protected:
	static XQLog* m_log;//日志服务
	QString m_url;//url
	XQHttpHeadRequest* m_request = nullptr;//网络请求
	XQHttpHeadReply* m_reply = nullptr;//网络回复
	bool m_replyAccept = false;//响应接收
	QLocalSocket* m_localSocket = nullptr;
	QSslSocket* m_tcpSocket = nullptr;
	QByteArray m_buffer;//缓冲区
};
#endif // !XQHttpRequest_H
