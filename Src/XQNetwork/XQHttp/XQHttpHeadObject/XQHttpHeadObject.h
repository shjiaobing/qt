﻿#ifndef XQHTTPHEADOBJECT_H
#define XQHTTPHEADOBJECT_H
#include<QNetworkRequest>
#include<QString>
#include<QHash>
#include<QJsonDocument>
#include"XQNework.h"
using HttpRequestType = Http::RequestType;
using HttpProtocol = Http::Protocol;
//using Header = QHash<QByteArray, QByteArray>;
class  XQHttpHeadObject
{
public:
	XQHttpHeadObject(const QByteArray& header = QByteArray());
	virtual ~XQHttpHeadObject()=default;
public:
	static Http::Versions getVersions(const QString& text);
public:
	virtual QByteArray toByteArray()const;
	//头部转字符串输出
	virtual QString toStrinng()const;
	//http获取版本
	Http::Versions versions()const;
	//协议版本转字符串输出
	QByteArray versions_toByteArray()const;
	QString versions_toString()const;
	//头部数据链接转QByteArray
	virtual QByteArray header_toByteArray()const;
public:
	//返回查看头部
	const QHash<QByteArray, QByteArray>& header()const;
	QHash<QByteArray, QByteArray>& header();
	//头部数据是否为空
	bool headerIsEmpty() const;
	QByteArray header(const QByteArray& headerName)const;
	QByteArray header(QNetworkRequest::KnownHeaders header)const;
	//返回主机地址
	QString host()const;
	//返回主机端口
	quint16 port()const;
	//返回主机地址和端口
	QString hostPort()const;
	//内容是否为空
	bool contentIsEmpty() const;
	//返回的头部记录的长度
	size_t contentLength()const;
	//返回内容
	const QByteArray& content()const;
	//返回内容大小
	quint64 contentSize()const;
	//内容接收完毕
	bool isContentAcceptFinish();
	//内容转QString
	QString content_toString()const;
	//内容转json
	QJsonDocument content_toJson()const;
	//返回缓存
	const QByteArray& buffer()const;
	//返回累计的缓存大小
	quint64 buffSize()const;
	//缓存接收完毕
	bool isBuffAcceptFinish();
public:
	bool isAccepted() const;
	Http::Head type() const;
	void accept();
	void ignore();
	void setAccepted(bool accepted);
public:
	//解析设置http头部数据
	virtual void setData(const QByteArray& header);
	//设置版本信息
	void setVersions(Http::Versions type);
	//设置头部
	void setHeader(const QByteArray& headerName, const QByteArray& headerValue);
	void setHeader(QNetworkRequest::KnownHeaders header, const QByteArray& headerValue);
	void setHeader(const QByteArrayList& data);
	//设置头部Date 日期时间
	void setHeader_Date(const QDateTime& date);
	//头部数据清空
	void clear_header();
	//清空
	virtual void clear();
	//设置内容长度(头部)
	void setContentLength(size_t len);
	//设置内容
	void setContent(const QByteArray& data);
	//添加内容
	void addContent(const QByteArray& data);
	//设置主机(带端口)
	void setHostPort(QString host,quint16 port);
	//设置缓存 缓存长度累计
	void setBuffer(const QByteArray& data);
	//清空缓存
	void clearBuffer();
//protected:
	virtual void init();
protected:
	QHash<QByteArray, QByteArray> m_header;//头部数据
	Http::Versions m_versions;//http版本类型
	QByteArray m_content;//内容
	QByteArray m_buffer;//缓存
	quint64 m_buffSize = 0;//缓存大小
	bool m_accept = false;//是否接受
	Http::Head m_type;//类型
};
#endif