﻿#ifndef XQHTTPHEADREQUEST_H
#define XQHTTPHEADREQUEST_H
#include"XQHttpHeadObject.h"

//http请求头
class XQHttpHeadRequest:public XQHttpHeadObject
{
public:
	XQHttpHeadRequest(const QByteArray& data = QByteArray());
public:
	//请求头转ByteArray输出
	QByteArray header_toByteArray()const;
	//获得请求类型
	HttpRequestType requestType()const;
	//请求方式转字节
	QByteArray request_toByteArray()const;
	//获取协议
	HttpProtocol protocol()const;
	//协议转字节
	QByteArray protocol_toByteArray()const;
	//获取请求资源地址
	QString path()const;
	//获得url
	QString url()const;
	//获取参数转QByteArray
	QByteArray args_toByteArray()const;
public:
	//返回查看参数
	const QHash<QByteArray, QByteArray>& args()const;
	QHash<QByteArray, QByteArray>& args();
	QByteArray args(const QByteArray& argsName)const;
public:
	//设置http的头部数据报文
	void setData(const QByteArray& data);
	//设置请求方式
	void setRequestType(HttpRequestType type);
	//设置协议
	void setProtocol(HttpProtocol protocol);
	//设置参数
	void setArgs(const QByteArray& argsName, const QByteArray& argsValue);
	//参数清空
	void clear_args();
	//清空
	void clear();
	//设置url
	void setUrl(const QByteArray& data);
//protected:
	void init();
protected:
	HttpProtocol m_protocol= HttpProtocol::http;//协议
	HttpRequestType m_requestType= HttpRequestType::GET;//请求方式
	QHash<QByteArray, QByteArray> m_Args;//参数
	QString m_path;//资源路径
};
#endif //
