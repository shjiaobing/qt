﻿#ifndef XQHTTPREQUEST_H
#define XQHTTPREQUEST_H
#include<QNetworkRequest>
#include<QJsonDocument>
#include<QObject>
#include"XSymbolOverload.h"
class QNetworkAccessManager;
class QNetworkReply;
//http请求
class XQHttpRequest:public QObject
{
	Q_OBJECT
public:
	XQHttpRequest(QObject*parent=nullptr);
	~XQHttpRequest();
	//获取url
	QString url()const;
	//返回请求头
	QNetworkRequest* request()const;
	//获取其文本
	QString toString()const;
	//获取其内容
	QByteArray toByteArray()const;
	//转json
	QJsonDocument toJson()const;
	//json转数组
	QVariantList toList()const;
public://静态成员阻塞版
	static QByteArray Get(const QString& url, int timeout = 10_s, bool toUtf8 = false/*是否转码*/);
	static bool Post(const QString& url, const QByteArray& data, int timeout = 10_s);
	static bool Post(const QString& url, const QJsonDocument& json, int timeout = 10_s);
	static QByteArray Post_Reply(const QString& url, const QByteArray& data, int timeout = 10_s, bool toUtf8 = false/*是否转码*/);
	static QByteArray Post_Reply(const QString& url, const QJsonDocument& json, int timeout = 10_s, bool toUtf8 = false/*是否转码*/);
public://异步版本
	//设置url
	void setUrl(const QString& url);
	//发送get请求
	bool get(const QString& url);
	bool get();
	//发送pos请求
	bool post(const QString& url, const QByteArray& data);
	bool post(const QByteArray& data);
	bool post(const QString& url, const QJsonDocument& json);
	//等待超时退出
	bool wait(int timeout = 10_s);
	//清空初始化请求头
	void clear_request();
public:
	//转码
	void GbkToUtf8();
signals://信号
	//请求结束
	void finish();
	void error(const QString& error);
protected:
	void init();
	void readyRead();//接收处理
protected:
	QNetworkRequest* m_request=nullptr;//网络请求
	QNetworkAccessManager*  m_manager=nullptr;//网络访问管理器对象
	QNetworkReply* m_reply = nullptr;//回复
	QByteArray m_data;//接收数据
};
#endif // !XQHttpRequest_H
