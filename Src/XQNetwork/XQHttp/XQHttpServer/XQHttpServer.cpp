﻿#include"XQHttpServer.h"
#include"XQSslServer.h"
#include<QTcpServer>
#include<QTcpSocket>
#include<QLocalServer>
#include<QLocalSocket>
#include<QJsonDocument>
#include<QTimer>
#include"XQFuncEventFilter.hpp"
#include"XQFuncEvent.h"
#include"XQHttpHeadRequest.h"
#include"XQHttpHeadReply.h"
#include"XQTaskPool.h"
#include"XQLog.hpp"
XQLog* XQHttpServer::m_log= XQLog::Create("XQHttpServer", LogType::Debug, XQLog::cmd_out| XQLog::cmd_cpp | XQLog::cmd_queue | XQLog::showTime);
XQHttpServer::XQHttpServer(QObject* parent)
	:XQHttpObject(parent)
{
	init();
}

XQHttpServer::~XQHttpServer()
{
	m_task->requestInterruption();
	//缓存处理
	QWriteLocker lock(&m_lock);
	for (auto data:m_client)
	{
		delete data;
	}
	m_task->wait_event();
}


int XQHttpServer::timeoutConnection() const
{
	return m_timeout;
}

QString XQHttpServer::ip() const
{
	if (m_tcpServer == nullptr)
		return QString();
	QHostAddress ipAddress = m_tcpServer->serverAddress();
	QString ip = ipAddress.toString(); // 转换为字符串形式
	return ip;
}

quint16 XQHttpServer::port() const
{
	if (m_tcpServer == nullptr)
		return m_port;
	return m_tcpServer->serverPort();
}

XQSslServer* XQHttpServer::tcp() const
{
	return m_tcpServer;
}

QString XQHttpServer::serverName_local() const
{
	return m_localServerName;
}

QLocalServer* XQHttpServer::localServer() const
{
	return m_localServer;
}

XQTaskPool* XQHttpServer::taskPool() const
{
	return m_task;
}

QList<QIODevice*> XQHttpServer::clients()
{
	QReadLocker lock(&m_lock);
	return m_client.keys();
}

void XQHttpServer::setModel(NetworkType type)
{
	m_networkType = type;
	if (isTcpNetwork())
	{
		if (m_tcpServer == nullptr)
		{
			m_tcpServer = new  XQSslServer(this);
			connect(m_tcpServer, &XQSslServer::newConnection, this, &XQHttpServer::readyConnection, Qt::QueuedConnection);
		}

		if (m_localServer)
		{
			m_localServer->close();
			m_localServer->deleteLater();
			m_localServer = nullptr;
		}
	}
	else if (isLocalNetwork())
	{
		if (m_localServer == nullptr)
		{
			m_localServer = new  QLocalServer(this);
			connect(m_localServer, &QLocalServer::newConnection, this, &XQHttpServer::readyConnection, Qt::QueuedConnection);
		}
		if (m_tcpServer)
		{
			m_tcpServer->close();
			m_tcpServer->deleteLater();
			m_tcpServer = nullptr;
		}
	}
}

bool XQHttpServer::run(NetworkType type)
{
	setModel(type);
	return run();
}

bool XQHttpServer::run()
{
	if (isTcpNetwork()&& m_tcpServer)
	{
		if (m_tcpServer->isListening())
			return true;
		if (!m_tcpServer->listen(m_address, m_port)) {
			if (m_debug & Http::Debug::error)
				XQHttpServerLog << QString("\t错误\t tcp服务器监听失败,端口:%1").arg(m_port);
			return false;
		}
		if (m_debug & Http::Debug::Run)
			XQHttpServerLog << QString("\ttcp模式\t %1 服务器:%2:%3开始运行")
			.arg(QDebug::toString(m_tcpServer->protocol()))
			.arg(ip()).arg(port());
	}
	else if (isLocalNetwork()&& m_localServer)
	{
		if (m_localServer->isListening())
			return true;
		if (!m_localServer->listen(serverName_local())) {
			if (m_debug & Http::Debug::error)
				XQHttpServerLog << QString("\t错误\t Local服务器监听失败,名字:%1").arg(serverName_local());
			return false;
		}
		if (m_debug & Http::Debug::Run)
			XQHttpServerLog << QString("\tLocal模式\t服务器:%1开始运行").arg(serverName_local());
	}
	return true;
}

void XQHttpServer::close()
{
	if (isTcpNetwork() && m_tcpServer)
	{
		m_tcpServer->close();
	}
	else if (isLocalNetwork() && m_localServer)
	{
		m_localServer->close();
	}
}

void XQHttpServer::setPort(uint16_t port)
{
	if (m_tcpServer == nullptr)
	{
		m_port = port;
		return;
	}
	if (m_tcpServer->isListening())
	{
		m_tcpServer->close();
		if (!m_tcpServer->listen(m_address, port)) {
			if (m_debug & Http::Debug::error)
				XQHttpServerLog << QString("\t错误\t 修改端口:%1,服务器监听失败").arg(port);
			return;
		}
	}
}

void XQHttpServer::setServerName_local(const QString& name)
{
	m_localServerName=name;
}

void XQHttpServer::setTimeoutConnection(int msec)
{
	m_timeout = msec;
	if (m_timeout > 0)
	{//开启定时器
		if (m_timeoutTimer == nullptr)
		{
			m_timeoutTimer = new QTimer(this);
			m_timeoutTimer->callOnTimeout(this, &XQHttpServer::checkTimeoutConnection);
		}
	}
	else
	{//关闭定时器
		if (m_timeoutTimer != nullptr)
		{
			m_timeoutTimer->deleteLater();
			m_timeoutTimer = nullptr;
		}
	}
}

void XQHttpServer::setThreadPool_run(bool run)
{
	
	if (run && m_task == nullptr)
	{
		m_task = new XQTaskPool(this);
	}
	m_thread_run = run;
}

void XQHttpServer::addReplyFunc(const QString& name, std::function<XQHttpHeadReply(QIODevice* socket, XQHttpHeadRequest* header,const QByteArray& data)> func)
{
	QWriteLocker lock(&m_lock);
	m_replyFunc[name] = func;
}

void XQHttpServer::removeReplyFunc(const QString& name)
{
	QWriteLocker lock(&m_lock);
	m_replyFunc.remove(name);
}

void XQHttpServer::clearReplyFunc()
{
	QWriteLocker lock(&m_lock);
	m_replyFunc.clear();
}

void XQHttpServer::sendSignals(const QString& name, const QVariantList& list)
{
	QJsonDocument jsonDoc = QVariantList_toJson(list);
	auto json = jsonDoc.toJson();
	auto send = [this](const QByteArray& name,const QByteArray& data)
		{
			QReadLocker lock(&m_lock);
			XQHttpHeadReply reply;
			reply.setHeader(QNetworkRequest::ContentTypeHeader, SignalsRequestHeader);
			reply.setHeader(SignalsRequestHeader,name);
			reply.setContent(data);
			init_headReply(reply);
			for (auto socket: m_client.keys())
			{
				this->send(socket, reply.toByteArray());
			}
		};
	if (m_thread_run)
		m_task->addTask([=] {send(name.toUtf8(), json); });
	else
		send(name.toUtf8(), json);
}

void XQHttpServer::init()
{
	installEventFilter(new XQFuncEventFilter(this));
	
}

void XQHttpServer::init_headReply(XQHttpHeadReply& reply)
{
	//开启了超时链接断开
	if (timeoutConnection() > 0)
	{
		reply.setHeader("Keep-Alive", QString("timeout=%1").arg(timeoutConnection()/1000).toUtf8());
	}

}

void XQHttpServer::checkTimeoutConnection()
{
	QSet<QIODevice*> remove;
	auto curTime = QDateTime::currentDateTime();
	//检查超时
	{
		QReadLocker lock(&m_lock);
		for (auto it = m_client.begin(); it != m_client.end(); it++)
		{
			auto socket = it.key();
			auto& client = *it.value();
			if(client.lock.tryLockForWrite())
			{
				auto& startTime = client.time;
				if (startTime.msecsTo(curTime) > m_timeout)
				{//超时了
					remove << socket;
				}
				client.lock.unlock();
			}
		}
	}
	//超时断开与客户端的连接
	for (auto socket : remove)
		readyDisconnect(socket);
}
void XQHttpServer::runTimeoutConnectionTimer()
{
	m_timeoutTimer->start(1000);
}
void XQHttpServer::readyConnection()
{
	QIODevice* socket = nullptr;
	if (isTcpNetwork() && m_tcpServer)
	{
		socket = m_tcpServer->nextPendingConnection();
		//socket->waitForReadyRead(3000);
		//QByteArray peekData = socket->peek(1);
		//qDebug() << peekData;
		//bool isSsl = (peekData.size() > 0 && peekData.at(0) == 0x16); // SSL 握手标识
		//if (!static_cast<QSslSocket*>(socket))
		//{
		//	socket->deleteLater();
		//	return;
		//}
	}
	else if (isLocalNetwork() && m_localServer)
	{
		socket = m_localServer->nextPendingConnection();
	}
	if (socket == nullptr)
		return;
	//记录套接字数据
	{
		auto client = new HttpClient;
		client->time = QDateTime::currentDateTime();//记录时间
		QWriteLocker lock(&m_lock);
		if (m_client[socket])
			delete m_client[socket];
		m_client[socket] = client;
	}
	//断开时释放套接字
	if (isTcpNetwork())
		connect(static_cast<QTcpSocket*>(socket), &QTcpSocket::disconnected, [=] {readyDisconnect(socket); });
	else if (isLocalNetwork())
		connect(static_cast<QLocalSocket*>(socket), &QLocalSocket::disconnected, [=] {readyDisconnect(socket); });
	//读取数据
	connect(socket, &QIODevice::readyRead, [=] {readyRead(socket); });
	//检查开启定时器
	if (m_timeoutTimer && !m_timeoutTimer->isActive())
		runTimeoutConnectionTimer();
	//当前是否有可读取的数据
	if(socket->isReadable())
		readyRead(socket);
	if (m_debug&Http::Debug::Connection)
	{
		QString info;
		if (isTcpNetwork() && m_tcpServer)
			info= QString("\tTcp模式\t新的网络客户端连接 %1 当前数量%2").arg(hostPort(static_cast<QTcpSocket*>(socket))).arg(m_client.size());
		else if (isLocalNetwork() && m_localServer)
			info= QString("\tLocal模式\t新的本地客户端连接 当前数量%2").arg(m_client.size());
		XQHttpServerLog << info;
	}
	emit connected(socket);
}
void XQHttpServer::readyDisconnect(QIODevice* socket)
{
	QWriteLocker lock(&m_lock);
	if (socket == nullptr|| !m_client.contains(socket))
		return;
	emit disconnected(socket);
	delete m_client[socket];
	m_client.remove(socket);
	socket->deleteLater();
	//无客户端时关闭超时定时器轮询
	if (m_client.isEmpty() && m_timeoutTimer)
		new XQFuncEvent([=] {if(m_timeoutTimer)m_timeoutTimer->stop(); });
	if (m_debug & Http::Debug::Disconnect)
	{
		if(isTcpNetwork())
			XQHttpServerLog <<"\tTcp模式"<<hostPort(static_cast<QAbstractSocket*>(socket)) << "客户端断开连接当前剩余数量" << m_client.size();
		else if (isLocalNetwork())
			XQHttpServerLog <<"\tLocal模式\t客户端断开连接当前剩余数量" << m_client.size();
	}
}

void XQHttpServer::readyRead(QIODevice* socket)
{
	auto Parse = [=](QIODevice* socket) {
		QReadLocker lock(&m_lock);
		if (!m_client.contains(socket))
			return;
		auto& client = *m_client[socket];
		QWriteLocker clientLock(&client.lock);
		auto& head = client.head;
		QByteArray*	buffer = &client.buff;
		//XQHttpServerLog << "接收数据";
		buffer->append(socket->readAll());

		//判断请求头是否完整
		auto nSel = buffer->indexOf("\r\n\r\n");
		while (nSel != -1)
		{
			auto startIndex = nSel + 4;//内容开始的索引
			if (!client.accept)
			{
				head.clear();
				head.setData(buffer->left(startIndex));
				client.accept = true;
			}
			auto size = head.contentLength() - head.buffSize();//剩余大小
			head.setBuffer(buffer->mid(startIndex, size));//
			buffer->remove(startIndex, size);//从缓存中去除这个内容
			//调用函数处理内容
			if (m_buffFunc)
				m_buffFunc(socket, &head);
			emit buffRead(socket, &head);

			//判断内容是否完全读取处理完成
			if (!head.isBuffAcceptFinish())
				return;//内容不完全继续等待
			//请求头清理
			buffer->remove(0, startIndex);
			emit headRequestRead(head);

			//显示请求头
			if (m_debug & Http::Debug::HeadRequest)
			{
				auto headerReady = head.header_toByteArray();
				QString info;
				if (isTcpNetwork())
					info = QString("Tcp模式 客户端请求 %2\r\nheader:\r\n%1\r\n").arg(headerReady).arg(hostPort(static_cast<QTcpSocket*>(socket)));
				else if (isLocalNetwork())
					info = QString("Local模式 客户端请求\r\nheader:\r\n%1\r\n").arg(headerReady);
				XQHttpServerLog << info;
			}
			auto& content = head.content();
			handlingRequests(socket, &head, content);
			nSel = buffer->indexOf("\r\n\r\n");//更新查找索引
			client.accept = false;
		}
		client.time = QDateTime::currentDateTime();//更新时间
		};
	if (m_thread_run)
		m_task->addTask([=] {Parse(socket);});
	else
		Parse(socket);
}

void XQHttpServer::handlingRequests(QIODevice* socket,XQHttpHeadRequest* header, const QByteArray& data)
{
	//是否是信号
	if (ishandlingSignalsRequest(header))
	{
		auto ret = handlingSignalsRequest(header, data);
		if (!ret.isEmpty())
			send(socket, ret);
	}
	else
	{
		XQHttpHeadReply reply;
		if (!m_replyFunc.isEmpty())
		{
			for (auto& replyFunc:m_replyFunc)
			{
				reply = replyFunc(socket, header, data);
				init_headReply(reply);
				if(header->isAccepted())//是否接受这个请求
				{//发送响应数据
					send(socket, reply.toByteArray());
					break;
				}
			}
		}
		else//默认回复
		{
			QString text = "未设置回复！！！请调用setReadyFunc成员函数设置回复方法\r\n";
			if (isTcpNetwork())
				text+=QString("服务器 %1:%2\r\n客户端 %3").arg(this->ip()).arg(this->port()).arg(hostPort(static_cast<QTcpSocket*>(socket)));
			else if (isLocalNetwork())
				text += QString("服务器 :%1\r\n").arg(this->serverName_local());
			reply.setStatusCode(404);
			reply.setStatusInfo("Not Found");
			reply.setContent(text.toUtf8());
			init_headReply(reply);
			send(socket, reply.toByteArray());
		}

		//套接字处理
		if (QString(header->header("Connection")).contains("close", Qt::CaseInsensitive))
		{
			socket->close();
		}
		if (m_debug & Http::Debug::HeadReply)
		{
			auto headerReady = reply.header_toByteArray();
			QString info;
			if (isTcpNetwork())
				info = QString("Tcp模式 回复客户端 %2\r\nheader:\r\n%1\r\n").arg(headerReady).arg(hostPort(static_cast<QTcpSocket*>(socket)));
			else if (isLocalNetwork())
				info = QString("Local模式 回复客户端\r\nheader:\r\n%1\r\n").arg(headerReady);
			XQHttpServerLog << info;
		}
	}
	
}


void XQHttpServer::send(QIODevice* socket, const QByteArray& data)
{
	new XQFuncEvent(this, [=] {
		auto dataLen = data.size();
		QReadLocker lock(&m_lock);
		if(m_client.contains(socket))
		{
			auto sendLen=socket->write(data);
			if ((sendLen != dataLen) && (m_debug & Http::Debug::error))
				XQHttpServerLog << QString("\t错误\t 数据大小%1字节 实际发送%2字节").arg(dataLen).arg(sendLen);
		}
		});
}

