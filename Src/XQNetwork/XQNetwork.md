# XQFtp

> 使用原QFtp 源代码修改而成

## 测试代码

```c++
#include<QApplication>
#include"XQFtp.h"
#include"XQAlgorithm.h"
#include"XQLog.hpp"
#include<QFile> 
 auto ftp=new XQFtp ;
 using Debug = Ftp::Debug;
 ftp->setDebugModel(Debug::CommandRead| Debug::CommandWrite| Debug::error);
 QObject::connect(qApp, &QApplication::aboutToQuit, ftp, &QObject::deleteLater);
 // 连接到 FTP 服务器
 ftp->connectToHost("192.168.1.3",21); // 替换为你的FTP服务器地址
 // 登录到 FTP 服务器
 ftp->login("FtpServer", "M1aSK4[z"); // 替换为你的FTP服务器登录凭据

 QObject::connect(ftp, &XQFtp::stateChanged, [=](Ftp::State state) { XQDebug << "状态改变" << QDebug::toString(ftp->state()); });

 // 检查连接状态
 if (!ftp->waitForConnected()) 
     XQDebug << "无法连接到FTP服务器：" << ftp->errorString();
 else
     XQDebug << "连接上服务器";

 // 检查登录状态
 if (!ftp->waitForLoggedIn())  
     XQDebug << "无法登录到FTP服务器：" << ftp->errorString();
 else
     XQDebug << "连接上服务器";
 //不是登录状态不进行下一步
 if (ftp->state() != Ftp::State::LoggedIn)
     return;
 ftp->cd("/FtpServer");
 ftp->mkdir("test");
 ftp->rmdir("test");
 //ftp->rename("MyQt.exe","note/MyQt.exe");
 ftp->list();
 ftp->waitForFinished();
 for (auto& info:ftp->listInfo())
 {
     qDebug() << info.name();
 }
 //静态成员方法
 for (auto& info : XQFtp::List("192.168.1.3", 21, "/FtpServer", "FtpServer", "M1aSK4[z"))
 {
     qDebug() << info.name();
 }
 //静态上传
 XQFtp::Put("192.168.1.3", 21, "/FtpServer/MyQt.exe", "MyQt.exe", Ftp::Binary, "FtpServer", "M1aSK4[z");
 //静态下载
 XQFtp::Get("192.168.1.3", 21, "/FtpServer/MyQt.exe", "Download/MyQt.exe", Ftp::Binary, "FtpServer", "M1aSK4[z");
 //进度
 QObject::connect(ftp, &XQFtp::dataTransferProgress, [](qint64 current, qint64 total) 
     {
         XQDebug << "传输中:"<<readableFileSize( current)<<"/"<< readableFileSize(total);
     });
 //上传
 ftp->put(QString("MyQt.exe"), "MyQt.exe");
 ftp->waitForFinished();
 XQDebug << "上传完成";
 //下载
 ftp->get("MyQt.exe","Download/MyQt.exe");
 ftp->waitForFinished();
 XQDebug << "下载完成";
  
 // 断开与FTP服务器的连接
 ftp->close();

```

### 运行结果

Debug 状态改变 Ftp状态(Ftp::State):主机查找中(HostLookup)
Debug 状态改变 Ftp状态(Ftp::State):连接中(Connecting)
Debug 状态改变 Ftp状态(Ftp::State):已连接(Connected)
Debug 连接上服务器
XQFtp Debug 2023-09-27 16:08:40 返回信息: 220 NAS7700T FTP server ready.
XQFtp Debug 2023-09-27 16:08:40 发送命令: USER FtpServer
XQFtp Debug 2023-09-27 16:08:40 返回信息: 331 Password required for FtpServer.
XQFtp Debug 2023-09-27 16:08:40 发送命令: PASS M1aSK4[z
XQFtp Debug Debug 状态改变 Ftp状态(Ftp::State):已登录(LoggedIn)2023-09-27 16:08:40
返回信息: 230 User FtpServer logged in.
Debug 连接上服务器
XQFtp Debug 2023-09-27 16:08:40 发送命令: CWD /FtpServer
XQFtp Debug 2023-09-27 16:08:40 返回信息: 250 CWD command successful.
XQFtp Debug 2023-09-27 16:08:40 发送命令: MKD test
XQFtp Debug 2023-09-27 16:08:40 返回信息: 257 "test" directory created.
XQFtp Debug 2023-09-27 16:08:40 发送命令: RMD test
XQFtp Debug 2023-09-27 16:08:40 返回信息: 250 RMD command successful.
XQFtp Debug 2023-09-27 16:08:40 发送命令: TYPE A
XQFtp Debug 2023-09-27 16:08:40 返回信息: 200 Type set to A.
XQFtp Debug 2023-09-27 16:08:40 发送命令: PASV
XQFtp Debug 2023-09-27 16:08:40 返回信息: 227 Entering Passive Mode (192,168,1,3,217,65)
XQFtp Debug 2023-09-27 16:08:40 发送命令: LIST
"#recycle"
"note"
XQFtp Debug 2023-09-27 16:08:40 返回信息: 150 Opening BINARY mode data connection for 'file list'.
XQFtp Debug 2023-09-27 16:08:40 返回信息: 226 Transfer complete.
"#recycle"
"note"
Debug 传输中: 0B / 6.26MB
XQFtp Debug 2023-09-27 16:08:46 发送命令: TYPE I
XQFtp Debug 2023-09-27 16:08:46 返回信息: 200 Type set to I.
XQFtp Debug 2023-09-27 16:08:46 发送命令: PASV
XQFtp Debug 2023-09-27 16:08:46 返回信息: 227 Entering Passive Mode (192,168,1,3,218,2)
XQFtp Debug 2023-09-27 16:08:46 发送命令: ALLO 6565376
XQFtp Debug 2023-09-27 16:08:46 返回信息: 202 ALLO command ignored.
XQFtp Debug 2023-09-27 16:08:46 发送命令: STOR MyQt.exe
XQFtp Debug 2023-09-27 16:08:46 返回信息: 150 Opening BINARY mode data connection for 'MyQt.exe'.
Debug 传输中: 128KB / 6.26MB

.........

Debug 传输中: 6.26MB / 6.26MB
Debug 上传完成
XQFtp Debug 2023-09-27 16:08:46 返回信息: 226 Transfer complete.
XQFtp Debug 2023-09-27 16:08:46 发送命令: TYPE I
XQFtp Debug 2023-09-27 16:08:46 返回信息: 200 Type set to I.
XQFtp Debug 2023-09-27 16:08:46 发送命令: SIZE MyQt.exe
XQFtp Debug Debug 2023-09-27 16:08:46 传输中: 0B / 6.26MB
返回信息: 213 6565376
XQFtp Debug 2023-09-27 16:08:46 发送命令: PASV
XQFtp Debug 2023-09-27 16:08:46 返回信息: 227 Entering Passive Mode (192,168,1,3,217,36)
XQFtp Debug 2023-09-27 16:08:46 发送命令: RETR MyQt.exe
XQFtp Debug 2023-09-27 16:08:46 返回信息: 150 Opening BINARY mode data connection for 'MyQt.exe' (6565376 bytes).
Debug 传输中: 2.87KB / 6.26MB

..........

Debug 传输中: 6.26MB / 6.26MB
Debug 下载完成
XQFtp Debug Debug 状态改变 Ftp状态(Ftp::State):关闭中(Closing)
2023-09-27 16:08:52 返回信息: 226 Transfer complete.
XQFtp Debug 2023-09-27 16:08:52 发送命令: QUIT
XQFtp Debug 2023-09-27 16:08:52 返回信息: 221 Goodbye. You uploaded 6411.50 KB and downloaded 6411.50 KB.Debug
状态改变 Ftp状态(Ftp::State):未连接(Unconnected)



> 打开: [测试代码](../../Test/ftpTest.cpp)

## enum(枚举)

### debug模式

```c++
namespace Ftp
{
	//debug模式
	enum Debug
	{
		error = 1,//错误
		Run = 2,//运行
		Connection = 4,//连接
		Disconnect = 8,//断开
		CommandWrite = 16,//发送命令
		CommandRead = 32,//接收命令返回信息

	};
}
```

### State状态

```c++
namespace Ftp
{
    //状态
    enum State {
        Unconnected,//未连接
        HostLookup,//主机查找中
        Connecting,//连接中
        Connected,//已连接
        LoggedIn,//已登录
        Closing//关闭中
    };
}
```

### Error错误

```c++
namespace Ftp
{
    //错误
    enum Error {
        NoError,//无错误
        UnknownError,//未知错误
        HostNotFound,//主机未找到
        ConnectionRefused,//连接被拒绝
        NotConnected//未连接
    };
}
```

### Command命令

```c++
namespace Ftp
{
    //命令
    enum Command {
        None,//无指令
        SetTransferMode,//设置传输模式
        SetProxy,//设置代理
        ConnectToHost,//连接到主机
        Login,//登录
        Close,//关闭连接
        List,// 列出文件列表
        Cd,//切换目录
        Get,//下载文件
        Put,//上传文件
        Remove,//删除文件
        Mkdir,//创建目录
        Rmdir,// 删除目录
        Rename,//重命名文件或目录
        RawCommand//原始命令
    };
}
```

### TransferMode传输模式

```c++
namespace Ftp
{
    //传输模式
    enum TransferMode {
        Active,//主动
        Passive//被动
    };
}
```

### TransferType传输类型

```c++
namespace Ftp
{
    //传输类型
    enum TransferType {
        Binary,//二进制
        Ascii//ASCII文本
    };
}
```

## public(静态)

### 查询指定目录的列表信息

```c++
static QList<XQUrlInfo> List(const QString& host, quint16 port = 21, const QString& dir = QString(), const QString& user = QString(), const QString& password = QString());
```

### 下载

```c++
static void Get(const QString& host, quint16 port, const QString& remote_file, const QString& local_file, Ftp::TransferType type = Ftp::Binary, const QString& user = QString(), const QString& password = QString());
```

### 上传

```c++
 static void Put(const QString& host, quint16 port, const QString& remote_file, const QString& local_file, Ftp::TransferType type = Ftp::Binary, const QString& user = QString(), const QString& password = QString());
```

## public(获取)

### 获取可以从套接字中读取的字节数

```c++
qint64 bytesAvailable() const;
```

### 读取数据

```c++
qint64 read(char *data, qint64 maxlen);
QByteArray readAll();
```

### 返回目录列表信息

```c++
const QList<XQUrlInfo>& listInfo()const;
```

### 当前的id

```c++
int currentId() const;
```

### 返回当前的QIODevice

```c++
QIODevice* currentDevice() const;
```

### 返回当前命令

```c++
Ftp::Command currentCommand() const;
```

### 是否还有等待的命令没处理

```c++
bool hasPendingCommands() const;
```

### 清空剩余的等待命令

```c++
void clearPendingCommands();
```

### 获取状态

```c++
Ftp::State state() const;
```

### 获取错误码

```c++
Ftp::Error error() const;
```

### 获取错误信息

```c++
QString errorString() const;
```

### 是否是指定Debug模式

```c++
bool isDebugModel(Ftp::Debug model);
```

## public(设置)

### 设置调试模式输出信息

```c++
void setDebugModel(int model);
void setDebugModel(Ftp::Debug model, bool open = true);
```

### 中断，终止连接

```c++
void abort();
```

## public(发送命令)

> 返回值都是id

### 设置主机地址和端口

```c++
int setProxy(const QString &host, quint16 port);
```

### 连接主机

```c++
int connectToHost(const QString &host, quint16 port=21);
```

### 登录

```c++
int login(const QString &user = QString(), const QString &password = QString());
```

### 关闭连接

```c++
int close();
```

### 设置传输模式

```c++
int setTransferMode(Ftp::TransferMode mode);
```

### 查询指定目录的列表信息

```c++
int list(const QString &dir = QString());
```

### 进入目录

```c++
int cd(const QString &dir);
```

### 下载

```c++
//下载
int get(const QString &file, QIODevice *dev=0, Ftp::TransferType type = Ftp::Binary);
//下载到指定路径
int get(const QString& remote_file, const QString& local_file, Ftp::TransferType type = Ftp::Binary);
```

### 上传

```c++
 //上传
 int put(const QByteArray &data, const QString &file, Ftp::TransferType type = Ftp::Binary);
 //上传
 int put(QIODevice *dev, const QString &file, Ftp::TransferType type = Ftp::Binary);
 //上传指定文件
 int put(const QString& remote_file, const QString& local_file, Ftp::TransferType type = Ftp::Binary);
```

### 删除

```c++
int remove(const QString &file);
```

### 创建目录

```c++
int mkdir(const QString &dir);
```

### 删除目录

```c++
int rmdir(const QString &dir);
```

### 重命名

```c++
int rename(const QString &oldname, const QString &newname);
```

### 移动文件

```c++
int move(const QString& oldfile, const QString& newfile);
```

### 原始命令

```c++
int rawCommand(const QString &command); 
```

## public(等待)

### 等待状态变更

```c++
bool waitForStateChange(Ftp::State state, int msecs = 3000);
```

### 等待连接上服务器

```c++
bool waitForConnected(int msecs = 3000);
```

### 等待登录

```c++
bool waitForLoggedIn(int msecs = 3000);
```

### 等待传输结束 //get put list结束

```c++
bool waitForFinished(int msecs = -1);
```

## signals

### 状态改变信号

```c++
void stateChanged(Ftp::State);
```

### 列表信息信号

```c++
void listInfo(const XQUrlInfo&);
```

### 准备读取信号

```c++
void readyRead();
```

### 数据传输进度

```c++
void dataTransferProgress(qint64 current, qint64 total);
```

### 原始命令准备完毕

```c++
void rawCommandReply(int, const QString&);
```

### 命令开始

```c++
void commandStarted(int id);
```

### 命令结束

```c++
void commandFinished(int id, bool);
```

### 是否都处理完成了

```c++
void done(bool);
```

# XQUrlInfo

## public(获取)

### 如果 XQUrlInfo 对象是有效的，则返回 true

```c++
bool isValid() const;
```

### 返回文件或目录的名称

```c++
QString name() const;
```

### 返回文件或目录的权限信息，如读、写和执行权限

```c++
int permissions() const;
```

### 获取文件或目录的所有者信息

```c++
QString owner() const;
```

### 获取文件或目录的所属组信息

```c++
QString group() const;
```

### 返回文件的大小（以字节为单位）。如果目标为目录，则返回-1

```c++
qint64 size() const;
```

### 返回文件或目录的最后修改时间

```c++
QDateTime lastModified() const;
```

### 获取文件或目录的最后读取时间

```c++
QDateTime lastRead() const;
```

### 如果文件或目录是一个目录，则返回 true

```c++
bool isDir() const;
```

### 如果文件或目录是一个文件，则返回 true

```c++
bool isFile() const;
```

### 判断是否是符号链接

```c++
bool isSymLink() const;
```

### 如果文件或目录可写，则返回 true

```c++
bool isWritable() const;
```

### 如果文件或目录可读，则返回 true

```c++
bool isReadable() const;
```

### 检查文件是否可执行

```c++
bool isExecutable() const;
```

# XQHttp(http)

## enum(枚举)

### 网络类型 

```c++
enum class NetworkType
{
	Tcp,
	Local
};
```

### debug模式

```c++
namespace Http
{
	//debug模式
	enum Debug
	{
		error=1,//错误
		Run=2,//运行
		Connection =4,//连接
		Disconnect=8,//断开
		HeadRequest=16,//请求头
		HeadReply=32,//响应头
		
	};
}
```

### Versions协议版本

```c++
namespace Http
{
	//http协议版本
	enum Versions
	{
		HTTP1,//HTTP/1.0：最早的 HTTP 协议版本，提供了基本的请求和响应功能。它使用短暂的连接，在每个请求/响应后都会关闭连接。
		HTTP11,//HTTP/1.1 是当前广泛使用的 HTTP 协议版本，引入了持久连接（keep - alive）机制，允许多个请求 / 响应通过同一连接复用，从而减少了连接建立的开销。它还支持管道化（pipelining）技术，允许客户端同时发送多个请求而无需等待响应。
		HTTP2,//HTTP/2
		HTTP3,//HTTP/3
	};
}
```

### RequestType请求类型

```c++
namespace Http
{
	//请求类型
	enum RequestType
	{
		GET,
		POST
	};
}
```

### Protocol协议类型

```c++
namespace Http
{
	//协议
	enum Protocol
	{
		http,
		https
	};
}
```

### Head头部类型

```c++
namespace Http
{
	//头部类型
	enum Head
	{
		Request,//请求头
		Reply//响应头
	};
}
```

## XQHttpHeadObject

> **子类@[XQHttpHeadReply](##XQHttpHeadReply)**响应头
>
> **子类@[XQHttpHeadRequest](##XQHttpHeadRequest)**请求头

> 类型设置别名 

```c++
using HttpRequestType = Http::RequestType;
using HttpProtocol = Http::Protocol;
```

### public(构造)

#### 头部数据构造

> 可以带内容或不带内容

```c++
XQHttpHeadObject(const QByteArray& header = QByteArray());
```

### public(静态)

#### 获取协议版本

```c++
static Http::Versions getVersions(const QString& text);
```

### public(获取)

#### 获取头部的原始数据

> 可以用来进行网络发送

```c++
virtual QByteArray toByteArray()const;
```

#### 获取头部转字符串

> toByteArray() 转QString

```c++
virtual QString toStrinng()const;
```

#### 获取http获取版本

```c++
Http::Versions versions()const;
```

#### 获取协议版本 QByteArray

```c++
QByteArray versions_toByteArray()const;
```

#### 获取协议版本 QString

```c++
QString versions_toString()const;
```

#### 获取头部数据链接转QByteArray

```c++
virtual QByteArray header_toByteArray()const;
```

#### 获取头部数据QHash

```c++
const QHash<QByteArray, QByteArray>& header()const;
QHash<QByteArray, QByteArray>& header();
```

#### 获取头部数据是否为空

```c++
bool headerIsEmpty() const;
```

#### 获取指定头部名称的值

```c++
QByteArray header(const QByteArray& headerName)const;
QByteArray header(QNetworkRequest::KnownHeaders header)const;
```

#### 获取主机地址

```c++
QString host()const;
```

#### 获取主机端口

```c++
quint16 port()const;
```

#### 获取主机地址和端口

```c++
QString hostPort()const;
```

#### 获取内容是否为空

```c++
bool contentIsEmpty() const;
```

#### 获取头部记录的内容长度

```c++
size_t contentLength()const;
```

#### 获取内容

```c++
const QByteArray& content()const;
```

#### 获取内容大小

```c++
quint64 contentSize()const;
```

#### 获取内容是否完全接收完毕

```c++
bool isContentAcceptFinish();
```

#### 获取内容转QString

```c++
QString content_toString()const;
```

#### 获取内容转json

```c++
QJsonDocument content_toJson()const;
```

#### 获取缓存

```c++
const QByteArray& buffer()const;
```

#### 获取缓存的大小

```c++
quint64 buffSize()const;
```

#### 获取缓存接收完毕

```c++
bool isBuffAcceptFinish();
```

#### 获取当前头部是否接受

```c++
bool isAccepted() const;
```

#### 获取当前头部类型

```c++
Http::Head type() const;
```

### public(设置)

####  接受当前的头部

```c++
void accept();
```

#### 拒绝当前头部

```c++
void ignore();
```

#### 设置是否接受当前头部

```c++
void setAccepted(bool accepted);
```

#### 解析设置http头部数据

```c++
virtual void setData(const QByteArray& header);
```

#### 设置版本信息

```c++
void setVersions(Http::Versions type);
```

#### 设置头部

```c++
void setHeader(const QByteArray& headerName, const QByteArray& headerValue);
void setHeader(QNetworkRequest::KnownHeaders header, const QByteArray& headerValue);
void setHeader(const QByteArrayList& data);
```

#### 设置头部Date 日期时间

```c++
void setHeader_Date(const QDateTime& date);
```

#### 头部数据清空

```c++
void clear_header();
```

#### 清空

```c++
virtual void clear();
```

#### 设置内容长度(头部)

```c++
void setContentLength(size_t len);
```

#### 设置内容

> 会自动设置在头部中的长度

```c++
void setContent(const QByteArray& data);
```

#### 添加内容

> 仅向后追加，不会修改头部中记录大小

```c++
void addContent(const QByteArray& data);
```

#### 设置主机(带端口)

```c++
void setHostPort(QString host,quint16 port);
```

#### 设置缓存 缓存长度累计

```c++
void setBuffer(const QByteArray& data);
```

#### 清空缓存

```c++
void clearBuffer();
```

## XQHttpHeadReply

> Http响应头
>
> **继承自@[XQHttpHeadObject](##XQHttpHeadObject)**


### public(构造)

#### 头部数据构造

> 可以带内容或不带内容

```c++
XQHttpHeadReply(const QByteArray& data = QByteArray());
```

### public(获取)

#### 获取请求头转ByteArray

```c++
QByteArray header_toByteArray()const;
```

#### 获取状态码

```c++
int statusCode()const;
```

#### 获取状态信息 

```c++
QByteArray statusInfo()const;
```

### public(设置)

#### 设置http的头部数据解析

```c++
void setData(const QByteArray& data);
```

#### 设置状态码

```c++
void setStatusCode(int code);
```

#### 设置状态信息

```c++
void setStatusInfo(const QByteArray& data);
```

#### 清空

```c++
void clear();
```

## XQHttpHeadRequest

> Http请求头
>
> **继承自@[XQHttpHeadObject](##XQHttpHeadObject)**


### public(构造)

#### 头部数据构造

> 可以带内容或不带内容

```c++
XQHttpHeadRequest(const QByteArray& data = QByteArray());
```

### public(获取)

#### 获取请求头转ByteArray

```c++
QByteArray header_toByteArray()const;
```

#### 获得请求类型

```c++
HttpRequestType requestType()const;
```

#### 获取请求方式转QByteArray

```c++
QByteArray request_toByteArray()const;
```

#### 获取协议

```c++
HttpProtocol protocol()const;
```

#### 协议转QByteArray

```c++
QByteArray protocol_toByteArray()const;
```

#### 获取请求资源地址

```c++
QString path()const;
```

#### 获取url

```c++
QString url()const;
```

#### 获取参数转QByteArray

```c++
QByteArray args_toByteArray()const;
```

#### 获取参数映射QHash

```c++
const QHash<QByteArray, QByteArray>& args()const;
QHash<QByteArray, QByteArray>& args();
```

#### 获取参数名对应的值

```c++
QByteArray args(const QByteArray& argsName)const;
```

### public(设置)

#### 设置http的头部数据解析

```c++
void setData(const QByteArray& data);
```

#### 设置请求方式

```c++
void setRequestType(HttpRequestType type);
```

#### 设置协议

```c++
void setProtocol(HttpProtocol protocol);
```

#### 设置参数

```c++
void setArgs(const QByteArray& argsName, const QByteArray& argsValue);
```

#### 参数清空

```c++
void clear_args();
```

#### 清空

```c++
void clear();
```

#### 设置url

```c++
void setUrl(const QByteArray& data);
```

## XQHttpObject

> 继承自QObject
>
> **子类@[XQHttpServer](##XQHttpServer)**
>
> **子类@[XQHttpClient](##XQHttpClient)**

### public(静态)

#### QVariantList转json

```c++
static QJsonDocument QVariantList_toJson(const QVariantList& list);
```

#### 默认的缓存处理方式

```c++
static void defaultBuffFunc(QIODevice* socket, XQHttpHeadObject* head);
```

### public(获取)

#### 获取主机地址和端口

```c++
QString hostPort(QAbstractSocket* socket);
```

#### 获取网络类型

```c++
NetworkType networkType()const;
```

#### 是否是tcp网络

```c++
bool isTcpNetwork()const;
```

#### 是否是本地网络

```c++
bool isLocalNetwork()const;
```

#### 是否是指定Debug模式

```c++
bool isDebugModel(Http::Debug model);
```

### public(获取)

#### 设置Debug模式

```c++
void setDebugModel(int model);
void setDebugModel(Http::Debug model,bool open=true);
```

#### 发送信号  客户端时向服务器发送信息 服务器时向全体客户端发送信息

```c++
virtual void sendSignals(const QString& name, const QVariantList& data);
```

#### 添加槽方法

```c++
void addSlotFunc(const QString& name, std::function<QVariantList(const QVariantList&)> slotFunc);
```

#### 删除槽方法

```c++
void removeSlotFunc(const QString& name);
```

#### 清空槽方法

```c++
void clearSlotFunc();
```

#### 设置缓存处理函数

```c++
void setBuffFunc(std::function<void(QIODevice* socket, XQHttpHeadObject*head)>&&Func);
```

### signals

#### 发送错误

```c++
void error(const QString& error);
```

#### 来自其他主机的请求

```c++
void signalsRequest(const QString& name,const QVariantList& data);
```

#### 请求头可读 用在服务端读取客户端请求

```c++
void headRequestRead(const XQHttpHeadRequest& head);
```

#### 响应头可读 用在客户端读取服务端的响应

```c++
void headReplyRead(const XQHttpHeadReply& head);
```

#### 缓存读取

```c++
void buffRead(QIODevice* socket, XQHttpHeadObject* head);
```

## XQHttpServer

> Http服务器 
>
> **继承自@[XQHttpObject](##XQHttpObject)**

#### http服务器日志

```c++
#define XQHttpServerLog XQLog("XQHttpServer") //http服务器日志
```

### public(获取)

#### 获取超时连接时间(客户端)

```c++
int timeoutConnection()const;
```

#### 获取服务器ip

```c++
QString ip()const;
```

#### 获取服务器端口

```c++
quint16 port()const;
```

#### 获取QTcpServer

```c++
QTcpServer* tcp()const;
```

#### 获取本地模式时的名字

```c++
QString serverName_local()const;
```

#### 获取QLocalServer

```c++
QLocalServer* localServer()const;
```

#### 获取内置的线程池

```c++
XQTaskPool* taskPool()const;
```

#### 获取客户端套接字列表

```c++
QList<QIODevice*> clients();
```

### public(设置)

#### 设置运行模式

```c++
bool run(NetworkType type);
```

#### 设置端口

```c++
void setPort(uint16_t port);
```

#### 设置本地模式名字

```c++
void setServerName_local(const QString& name);
```

#### 设置超时连接时间(客户端)

```c++
void setTimeoutConnection(int msec);
```

#### 设置线程池运行

```c++
void setThreadPool_run(bool run);
```

#### 添加回复函数 业务

```c++
void addReplyFunc(const QString& name, std::function<XQHttpHeadReply(QIODevice* socket, XQHttpHeadRequest* header, const QByteArray& data)>func);
```

#### 删除回复函数 业务

```c++
void removeReplyFunc(const QString& name);
```

#### 清空回复函数 业务

```c++
void clearReplyFunc();
```

#### 发送信号(向客户端发送响应头)

```c++
void sendSignals(const QString& name, const QVariantList& list);
```

### signals

#### 连接上客户端

```c++
void connected(QIODevice* socket);
```

#### 断开与客户端连接

```c++
void disconnected(QIODevice* socket);
```

## XQHttpClient

> Http客户端
>
> **继承自@[XQHttpObject](##XQHttpObject)**

#### http客户端日志

```c++
#define XQHttpClientLog XQLog("XQHttpClient") //http客户端日志
```

### public(构造)

#### 运行模式构造

```c++
XQHttpClient(NetworkType type= NetworkType::Tcp,QObject*parent=nullptr);
```

### public(静态)

#### Get请求

```c++
static QByteArray Get(NetworkType type,const QString& url, int timeout = 10_s, bool toUtf8 = false/*是否转码*/);
static QByteArray Get_Tcp(const QString& url, int timeout = 10_s,bool toUtf8=false/*是否转码*/);
static QByteArray Get_Local(const QString& url, int timeout = 10_s, bool toUtf8 = false/*是否转码*/);
```

#### Post请求

```c++
static bool Post(NetworkType type,const QString& url, const QByteArray& data, int timeout = 10_s);
static bool Post(NetworkType type, const QString& url, const QJsonDocument& json, int timeout = 10_s);
static QByteArray Post_Reply(NetworkType type, const QString& url, const QByteArray& data, int timeout = 10_s, bool toUtf8 = false/*是否转码*/);
static QByteArray Post_Reply(NetworkType type, const QString& url, const QJsonDocument& json, int timeout = 10_s, bool toUtf8 = false/*是否转码*/);
static bool Post_Tcp(const QString& url, const QByteArray& data, int timeout = 10_s);
static bool Post_Tcp(const QString& url, const QJsonDocument& json, int timeout = 10_s);
static bool Post_Local(const QString& url, const QByteArray& data, int timeout = 10_s);
static bool Post_Local(const QString& url, const QJsonDocument& json, int timeout = 10_s);
```

### public(获取)

#### 获取url

```c++
QString url()const;
```

#### 获取请求头

```c++
XQHttpHeadRequest* request()const;
```

#### 获取响应头

```c++
XQHttpHeadReply* reply()const;
```

#### 获取其文本

```c++
QString toString()const;
```

#### 获取其内容

```c++
QByteArray toByteArray()const;
```

#### 转数组

```c++
QVariantList toList()const;
```

### public(设置)

#### 设置本地服务器名字

```c++
void setServerName_local(const QString& name);
```

#### 连接到服务器

```c++
void connectToServer(QIODeviceBase::OpenMode openMode = QIODeviceBase::ReadWrite);
void connectToServer(const QString& url, QIODeviceBase::OpenMode openMode = QIODeviceBase::ReadWrite);
```

#### 连接到服务器并等待

```c++
bool connectToServerWait(const QString& url,int msecs = 3_s);
bool connectToServerWait(int msecs = 3_s);
```

#### 仅等待连接到服务器

```c++
void waitForConnected_local(int msecs = 3_s);
```

#### 断开服务器连接

```c++
void close();
```

### signals

#### 连接上服务器

```c++
void connected();
```

#### 断开与服务器连接

```c++
void disconnected();
```

## XQHttpRequest

> 继承自QOject
>
> 主要用作爬虫

### public(静态)

#### Get请求

```c++
static QByteArray Get(const QString& url, int timeout = 10_s, bool toUtf8 = false/*是否转码*/);
```

#### Post请求

```c++
static bool Post(const QString& url, const QByteArray& data, int timeout = 10_s);
static bool Post(const QString& url, const QJsonDocument& json, int timeout = 10_s);
static QByteArray Post_Reply(const QString& url, const QByteArray& data, int timeout = 10_s, bool toUtf8 = false/*是否转码*/);
static QByteArray Post_Reply(const QString& url, const QJsonDocument& json, int timeout = 10_s, bool toUtf8 = false/*是否转码*/);
```

### public(获取)

#### 获取url

```c++
QString url()const;
```

#### 返回请求头

```c++
QNetworkRequest* request()const;
```

#### 获取其文本

```c++
QString toString()const;
```

#### 获取其内容

```c++
QByteArray toByteArray()const;
```

#### 获取内容转json

```c++
QJsonDocument toJson()const;
```

#### 获取内容转json转数组

```c++
QVariantList toList()const;
```

### public(设置)

#### 设置url

```c++
void setUrl(const QString& url);
```

#### 发送get请求

```c++
bool get(const QString& url);
bool get();
```

#### 发送pos请求

```c++
bool post(const QString& url, const QByteArray& data);
bool post(const QByteArray& data);
bool post(const QString& url, const QJsonDocument& json);
```

#### 等待超时退出

```c++
bool wait(int timeout = 10_s);
```

#### 清空初始化请求头

```c++
void clear_request();
```

#### 内容GBK转UTF8

```c++
void GbkToUtf8();
```

### signals

#### 请求结束

```c++
void finish();
```

#### 发生错误

```c++
void error(const QString& error);
```

# XQMail

## public(设置)

#### 设置smtp服务器地址    

```c++
 void setServerName(const QString& server_name);        //smtp服务器地址 
```

#### 设置smtp服务器端口号   

```c++
void setServerPort(short int port);        //smtp服务器端口号    
```

#### 设置邮箱用户名

```c++
void setUserName(const QString& user_name);            //邮箱用户名
```

#### 设置邮箱用户密码

```c++
void setUserPwd(const QString& user_pwd);            //邮箱用户密码
```

#### 设置发送者的名字

```c++
void setSenderName(const QString& sender_name);        //发送者的名字
```

#### 设置发送者的邮箱

```c++
void setSenderAddress(const QString& sender_addr);    //发送者的邮箱(mail form:)
```

#### 设置邮件接收者

```c++
void setReceiver(const QString& name, const QString& mail);
```

#### 添加邮件接收者

```c++
void addReceiver(const QString& name, const QString& mail);   //增加邮件接收者，name是收件人名字，mail是地址
```

#### 清空邮件接收者

```c++
void clearReceiver();                                      //清空邮件接收者
```

#### 添加附件

```c++
void AddFilePath(const QString& szFilePath);                //添加附件路径到附件列表中，一般的smtp服务器处理附件不超过50MB 
```

#### 删除附件路径

```c++
void DeleteFilePath(const QString& szFilePath);              //删除附件路径，如果有的话  
```

#### 删除全部附件的路径  

```c++
void DeleteAllPath();                                       //删除全部附件的路径  
```

#### 连接服务器

```c++
bool connent();
```

#### 发送邮件

```c++
bool SendMail(const QString& mail_title, const QString& send_content);  //发送邮件的函数
```

# XQMySql

> mysql封装

## mysql日志

```c++
#define XQMysqlInfo XQLog("XQMysqlInfo")
```

## public(构造)

#### 连接构造

```c++
XQMySql(QString SqlName,QString host, quint16 port, QString name, QString password, QString DataBase, QObject* parent = nullptr);
XQMySql(QString host,quint16 port,QString name ,QString password,QString DataBase, QObject* parent = nullptr);
```

## public(静态)

#### 获取全局区的mysql数据库

```c++
static XQMySql*globalInstance();//获取
static XQMySql* globalInstance(QString host, quint16 port, QString name, QString password, QString DataBase);//新的连接覆盖并获取
```

## public(获取)

#### 拷贝数据库

```c++
XQMySql* cloneDatabase( const QString& connectionName = QString());
```

#### 拷贝数据库返回智能指针

```c++
QScopedPointer<XQMySql> cloneDatabase_ScopedPointer(const QString& connectionName=QString());
```

#### 获取连接状态成功或者失败

```c++
bool isOpen()const;
```

#### 获取QSqlQuery

```c++
QSqlQuery SqlQuery()const;
```

#### 获取QSqlDatabase数据库

```c++
QSqlDatabase database()const;
```

#### 获取error错误信息

```c++
QString error()const;
```

#### 获取连接失败重连次数

```c++
int reconnectionCount()const;
```

#### 获取延迟时间

```c++
int sleepTime()const;
```

## public(设置)

#### 连接MySql数据库

```c++
void connection(QString SqlName, QString host, quint16 port, QString name, QString password, QString DataBase);
```

#### 添加服务器的主机地址和端口

```c++
void addHostPort(QString host, quint16 port);
```

#### 删除一个主机

```c++
void removeHostPort(QString host);
```

#### 设置连接失败重连次数

```c++
void setReconnectionCount(int count);
```

#### 设置延迟时间

```c++
void setSleepTime(int sleep);
```

## public(Sql)

#### 执行sql命令

```c++
//执行sql命令
bool Query(const QString& query);
//已QString("%1 %2").arg(1).arg(2)形式初始化sql后执行
template <typename ...Args>
bool Query(const QString& query, Args&& ...args);
```

#### 获取32位uuid

```c++
QString uuid(const QString& variate=QString());
```

#### 获取服务器当前时间

```c++
QDateTime ctime(const QString& variate = QString());
```

## public(获取结果)

#### 只判断是否有结果

```c++
bool result();//只判断是否有结果
```

#### 只获取指定列的数据

```c++
bool result(QVariant& ret,int nSel=0);
```

#### 获取一行

```c++
bool result(QVariantList& ret);
```

#### 获取一列进栈

```c++
bool result(QStack<QVariant>& ret);
```

#### 获取返回的所有数据

```c++
//获取返回的所有数据转QString
QList<QStringList> result_all_toString();
//获取返回的所有数据
QList<QVariantList> result_all();
```

#### 获取一列全部数据

```c++
//获取一列全部数据转QString
QStringList  result_oneColumn_toString(int nSel = 0);
//获取一列全部数据
QVariantList result_oneColumn(int nSel = 0);
```

#### 获取第一行全部数据

```c++
//获取第一行全部数据转QString
QStringList  result_oneRow_toString();
//获取第一行全部数据
QVariantList result_oneRow();
```

#### 获取一个数据

```c++
//获取一个数据转QString
QString result_one_toString();
//获取一个数据
QVariant result_one();
```

## public(查询)

#### 查询指定表名是否存在

```c++
bool tabelExists(const QString& tableName);
```

#### 查询表内的行数

```c++
qint64 SELECT_COUNT(const QString& tableName, const QString& Where = QString());
template <typename ...Args>
qint64 SELECT_COUNT(const QString& tableName, const QString& Where, Args&& ...args);//Where QString构造方式
```

#### 查询数据

```c++
bool SELECT(const QString& tableName, const QStringList& fields, const QString& condition = QString());
template <typename ...Args>
bool SELECT(const QString& tableName, const QStringList& fields, const QString& condition, Args&& ...args);
bool SELECT_Where(const QString& tableName, const QStringList& fields, const QString& Where);
template <typename ...Args>
bool SELECT_Where(const QString& tableName, const QStringList& fields, const QString& Where, Args&& ...args);
```

#### 查询表的所有字段

```c++
QStringList fieldList(const QString& tableName);
```

#### 查询指定字段的所有数据

```c++
//查询指定字段的所有数据
QVariantList findData(const QString tableName, const QString field);
//查询指定字段的所有数据转QString
QStringList findData_toString(const QString tableName, const QString field);
```

#### 查询指定字段合集的所有数据

```c++
//查询指定字段合集的所有数据
QList<QVariantList> findDatas(const QString& tableName, const QStringList& fields);
//查询指定字段合集的所有数据转QString
QList<QStringList> findDatas_toString(const QString& tableName, const QStringList& fields);
```

#### 查询表所有数据

```c++
//查询表所有数据
QList<QVariantList> findDataAll(const QString& tableName);
//查询表所有数据转QString
QList<QStringList> findDataAll_toString(const QString& tableName);
```

## public(插入)

#### 插入数据

```c++
	bool INSERT(const QString& tableName, const QStringList& fields,QVariantList&& datas);
	bool INSERTS(const QString& tableName, const QStringList& fields,const QList<QVariantList>& datas);
	template <typename ...Args>
	bool INSERT(const QString& tableName,const QStringList& fields, Args&& ...args);
```

## public(更新)

#### 更新数据

```c++
bool UPDATE(const QString& tableName, const QString& Where, const QStringList& fields,QVariantList&& datas);
template <typename ...Args>
bool UPDATE(const QString& tableName, const QString& Where, const QStringList& fields, Args&& ...args);
```

## public(删除)

#### 删除数据

```c++
//删除一条数据,已相等做判断(1字段1数据，自动链接做&&判断)
template <typename ...Args>
void deleteTableData(const QString& tableName,size_t maxNum, Args&& ...args);
```

## signals

#### 命令错误发生

```c++
void sqlError(const QString& error);
```

#### 连接mysql成功

```c++
void connectionSucceed();
```

# XQRobot

> 机器人推送
>
> 虚基类不能直接构造
>
> **子类@[XQRobotDing](#XQRobotDing)**钉钉机器人

## 枚举

```c++
enum RobotType
{
	DingDing//钉钉
};
```

## public(获取)

#### 是否启用

```c++
bool isEnabled()const;
```

#### 获取当前的url

```c++
virtual QString url() = 0;
```

#### 转钉钉机器人指针(如果可以的话)

```c++
XQRobotDing* toDingDingRobot()const;
```

## public(设置)

#### 发送json数据

```c++
virtual bool send(const QJsonDocument& json);
```

# XQRobotDing

> 钉钉机器人
>
> **继承自@[XQRobot](#XQRobot)**

## public(获取)

#### 获取令牌

```c++
inline QString token()const;
```

#### 获取密钥

```c++
inline QString secret()const;
```

#### 获取当前的标志

```c++
QString sign();
```

#### 获取当前的url

```c++
QString url();
```

## public(设置)

#### 设置令牌

```c++
void setToken(const QString& token);
```

#### 设置密钥

```c++
void setSecret(const QString& secret);
```

#### 发送文本

```c++
//content(内容) atMobiles(@人的手机号)   atUserIds(@人的userid)  isAtAll(是否@所有人)
bool sendText(const QString& content, const QStringList& atMobiles = {}, const QStringList& atUserIds = {}, bool isAtAll = false);
```

#### 发送链接

```c++
//title(标题)   text(文本内容)  messageUrl(跳转链接)   picUrl(图片地址)
bool sendLink(const QString& title, const QString& text, const QString& messageUrl, const QString& picUrl= QString());
```

#### 发送Markdown文本

```c++
//title(标题)   text(文本内容)  atMobiles(@人的手机号)   atUserIds(@人的userid)  isAtAll(是否@所有人)
bool sendMarkdown(const QString& title, const QString& text, const QStringList& atMobiles = {}, const QStringList& atUserIds = {}, bool isAtAll = false);
```

