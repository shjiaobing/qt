﻿#include "XQMail.h"
#include <QtCore>
#include <QtNetwork>
XQMail::XQMail(QObject* parent)
	:QObject(parent)
{
}

XQMail::~XQMail()
{
}

void XQMail::setServerPort(short int port)
{
	m_Port = port;
}

void XQMail::setUserName(const QString& user_name)
{
	m_UserName = user_name;
}

void XQMail::setUserPwd(const QString& user_pwd)
{
	m_UserPwd = user_pwd;
}

void XQMail::setSenderName(const QString& sender_name)
{
	m_SenderName = sender_name;
}

void XQMail::setSenderAddress(const QString& sender_addr)
{
	m_SenderAddr = sender_addr;
}

void XQMail::setReceiver(const QString& name, const QString& mail)
{
	clearReceiver();
	addReceiver(name, mail);
}

void XQMail::addReceiver(const QString& name, const QString& mail)
{
	m_Receivers[name] = mail;
}

void XQMail::clearReceiver()
{
	m_Receivers.clear();
}

void XQMail::AddFilePath(const QString& szFilePath)
{
	m_FilePathList<< szFilePath;
}

void XQMail::DeleteFilePath(const QString& szFilePath)
{
	m_FilePathList.remove(szFilePath);
}

void XQMail::DeleteAllPath()
{
	m_FilePathList.clear();
}

bool XQMail::connent()
{
	//邮件信息设置判断
	if (m_ServerName.isEmpty() || m_UserName.isEmpty() || m_UserPwd.isEmpty())
	{
		//m_logInfo.logInfo("Connect 失败，请先设置邮件登陆信息！");
		return false;
	}

	if (!CReateSocket())//建立连接  
	{
		//m_logInfo.logInfo("建立连接失败！");
		return false;
	}

	if (!Logon())//建立连接  
	{
		//m_logInfo.logInfo("登陆失败！");
		return false;
	}
	return true;
}

bool XQMail::SendMail(const QString& mail_title, const QString& send_content)
{
	//参数赋值
	m_MailTitle = mail_title;
	m_TextBody = send_content;
	if (m_SenderName.isEmpty() || m_SenderAddr.isEmpty() || m_Receivers.isEmpty())
	{
		//m_logInfo.logInfo("[SendMail]邮件参数设置错误，请检查邮件发送设置信息是否完整！");
		return false;
	}
	if (!SendHead())//发送邮件头  
	{
		//m_logInfo.logInfo("发送邮件头失败！");
		return false;
	}

	if (!SendTextBody())//发送邮件文本部分  
	{
		return false;
	}

	if (!SendFileBody())//发送附件  
	{
		return false;
	}

	if (!SendEnd())//结束邮件，并关闭sock  
	{
		return false;
	}

	return true;
}

void XQMail::setServerName(const QString& server_name)
{
	m_ServerName = server_name;
}
