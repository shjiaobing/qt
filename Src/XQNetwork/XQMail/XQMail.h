﻿#ifndef XQMAIL_H
#define XQMAIL_H
#include<QObject>
#include<QMap>
#include<QSet>
class QTcpSocket;
//电子邮件
class XQMail:public QObject
{
	Q_OBJECT
public:
	XQMail(QObject* parent=nullptr);
	~XQMail();
public slots:
    void setServerName(const QString& server_name);        //smtp服务器地址    
	void setServerPort(short int port);        //smtp服务器端口号    
	void setUserName(const QString& user_name);            //邮箱用户名
	void setUserPwd(const QString& user_pwd);            //邮箱用户密码
	/////////////////////////SendMail//////////////////////////////////
	void setSenderName(const QString& sender_name);        //发送者的名字
	void setSenderAddress(const QString& sender_addr);    //发送者的邮箱(mail form:)
	//邮件接收者
	void setReceiver(const QString& name, const QString& mail);
	void addReceiver(const QString& name, const QString& mail);   //增加邮件接收者，name是收件人名字，mail是地址
	void clearReceiver();                                      //清空邮件接收者

	//添加附件
	void AddFilePath(const QString& szFilePath);                //添加附件路径到附件列表中，一般的smtp服务器处理附件不超过50MB 
	void DeleteFilePath(const QString& szFilePath);              //删除附件路径，如果有的话  
	void DeleteAllPath();                                       //删除全部附件的路径  
	//连接
	bool connent();
	//邮件发送
	bool SendMail(const QString& mail_title, const QString& send_content);  //发送邮件的函数
//signals://信号
protected:	//工作函数
	//获取时间
	QByteArray prepareDate();
	//通信recv和send的封装
	int  sendRequest(const QByteArray& content, bool bout = false);                //返回发送了多少字节
	bool rcvResponse(const QByteArray& expected_response);    //返回接收的结果和expected_response是否相同
	bool CReateSocket();                                    //建立socket连接  
	bool Logon();                                            //登录邮箱，主要进行发邮件前的准备工作  
	bool SendHead();                                        //发送邮件头  
	bool SendTextBody();                                    //发送邮件文本正文  
	bool SendFileBody();                                    //发送邮件附件  
	bool SendEnd();                                            //发送邮件结尾 
protected://变量
	QTcpSocket* m_tcp = nullptr;//tcp连接
	QString                   m_ServerName;        //smtp服务器地址
	short int				  m_Port;				//smtp服务器端口号
	QString                   m_UserName;            //邮箱用户名
	QString                   m_UserPwd;            //邮箱用户密码
	/////////////////////////SendMail//////////////////////////////////
	QString                   m_SenderName;        //发送者的名    
	QString                   m_SenderAddr;        //发送者的邮箱(mail form:)
	QString                   m_MailTitle;        //邮件标题(subject)
	QString                   m_TextBody;            //邮件正文
	QMap<QString, QString>	  m_Receivers;        //邮件接收者（name,email_address)
	QSet<QString>			  m_FilePathList;        //附件路径_list
};
#endif // !XQMail_H
