﻿#include"XQNework.h"
QDebug  operator<<(QDebug debug, Http::Protocol value)
{
	debug << "Http协议(Http::Protocol):";
	switch (value)
	{
	case Http::http:debug << "http";
		break;
	case Http::https:debug << "https";
		break;
	default:
		break;
	}
	return debug;
}
QDebug operator<<(QDebug debug, Ftp::State value)
{
	debug << "Ftp状态(Ftp::State):";
	switch (value)
	{
	case Ftp::State::Unconnected:debug << "未连接(Unconnected)";
		break;
	case Ftp::State::HostLookup:debug << "主机查找中(HostLookup)";
		break;
	case Ftp::State::Connecting:debug << "连接中(Connecting)";
		break;
	case Ftp::State::Connected:debug << "已连接(Connected)";
		break;
	case Ftp::State::LoggedIn:debug << "已登录(LoggedIn)";
		break;
	case Ftp::State::Closing:debug << "关闭中(Closing)";
		break;
	default:
		break;
	}
	return debug;
}

QDebug operator<<(QDebug debug, Ftp::Error value)
{
	debug << "Ftp错误(Ftp::Error):";
	switch (value)
	{
	case Ftp::NoError:debug << "无错误(NoError)";
		break;
	case Ftp::UnknownError:debug << "未知错误(UnknownError)";
		break;
	case Ftp::HostNotFound:debug << "主机未找到(HostNotFound)";
		break;
	case Ftp::ConnectionRefused:debug << "连接被拒绝(ConnectionRefused)";
		break;
	case Ftp::NotConnected:debug << "未连接(NotConnected)";
		break;
	default:
		break;
	}
	return debug;
}

QDebug operator<<(QDebug debug, Ftp::Command value)
{
	debug << "Ftp命令(Ftp::Command):";
	switch (value)
	{
	case Ftp::None:debug << "无指令(None)";
		break;
	case Ftp::SetTransferMode:debug << "设置传输模式(SetTransferMode)";
		break;
	case Ftp::SetProxy:debug << "设置代理(SetProxy)";
		break;
	case Ftp::ConnectToHost:debug << "连接到主机(ConnectToHost)";
		break;
	case Ftp::Login:debug << "登录(Login)";
		break;
	case Ftp::Close:debug << "关闭连接(Close)";
		break;
	case Ftp::List:debug << "列出文件列表(List)";
		break;
	case Ftp::Cd:debug << "切换目录(Cd)";
		break;
	case Ftp::Get:debug << "下载文件(Get)";
		break;
	case Ftp::Put:debug << "上传文件(Put)";
		break;
	case Ftp::Remove:debug << "删除文件(Remove)";
		break;
	case Ftp::Mkdir:debug << "创建目录(Mkdir)";
		break;
	case Ftp::Rmdir:debug << "删除目录(Rmdir)";
		break;
	case Ftp::Rename:debug << "重命名文件或目录(Rename)";
		break;
	case Ftp::RawCommand:debug << "原始命令(RawCommand)";
		break;
	default:
		break;
	}
	return debug;
}

QDebug operator<<(QDebug debug, Ftp::TransferMode value)
{
	debug << "Ftp传输模式(Ftp::TransferMode):";
	switch (value)
	{
	case Ftp::Active:debug << "主动(Active)";
		break;
	case Ftp::Passive:debug << "被动(Passive)";
		break;
	default:
		break;
	}
	return debug;
}

QDebug operator<<(QDebug debug, Ftp::TransferType value)
{
	debug << "Ftp传输类型(Ftp::TransferType):";
	switch (value)
	{
	case Ftp::Binary:debug << "二进制(Binary)";
		break;
	case Ftp::Ascii:debug << "ASCII文本(Ascii)";
		break;
	default:
		break;
	}
	return debug;
}
