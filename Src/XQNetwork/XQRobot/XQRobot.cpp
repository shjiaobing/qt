﻿#include"XQRobot.h"
#include"XQRobotDing.h"
#include"XQHttpRequest.h"
XQRobot::XQRobot(RobotType type,QObject* parent)
	:QObject(parent),m_type(type)
{
}
bool XQRobot::isEnabled()const
{
	return m_Enabled;
}
void XQRobot::setEnabled(bool Enabled)
{
	m_Enabled = Enabled;
}
XQRobotDing* XQRobot::toDingDingRobot() const
{
	if(m_type== RobotType::DingDing)
		return (XQRobotDing*)(this);
	return nullptr;
}
bool XQRobot::send(const QJsonDocument& json)
{
	return XQHttpRequest::Post(url(), json);
}