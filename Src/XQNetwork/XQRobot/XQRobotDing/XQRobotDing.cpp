﻿#include"XQRobotDing.h"
#include"XQHttpRequest.h"
#include<QMessageAuthenticationCode>
#include<QUrl>
#include<QJsonObject>
#include<QJsonArray>
#include<QDebug>
XQRobotDing::XQRobotDing(QObject* parent)
	:XQRobot(RobotType::DingDing, parent)
{
	init();
}

XQRobotDing::~XQRobotDing()
{
}

QString XQRobotDing::token() const
{
	return m_token;
}

QString XQRobotDing::secret() const
{
	return m_secret;
}

void XQRobotDing::setToken(const QString& token)
{
	m_token = token;
}

void XQRobotDing::setSecret(const QString& secret)
{
	m_secret = secret;
}

bool XQRobotDing::sendText(const QString& content, const QStringList& atMobiles, const QStringList& atUserIds, bool isAtAll)
{
	QJsonObject json;
	QJsonObject jsonText;
	
	jsonText.insert("content", content);
	addAt(json, atMobiles, atUserIds, isAtAll);
	json.insert("text", jsonText);
	json.insert("msgtype", "text");
	//QString jsonString( QJsonDocument(json).toJson());

	return send(QJsonDocument(json));
}

bool XQRobotDing::sendLink(const QString& title, const QString& text, const QString& messageUrl, const QString& picUrl)
{
	QJsonObject json;
	QJsonObject jsonLink;
	{
		jsonLink.insert("text", text);
		jsonLink.insert("title", title);
		jsonLink.insert("picUrl", picUrl);
		jsonLink.insert("messageUrl", messageUrl);
	}
	json.insert("link", jsonLink);
	json.insert("msgtype", "link");
	//QString jsonString(QJsonDocument(json).toJson());
	return send(QJsonDocument(json));
}

bool XQRobotDing::sendMarkdown(const QString& title, const QString& text, const QStringList& atMobiles, const QStringList& atUserIds, bool isAtAll)
{
	QJsonObject json;
	QJsonObject jsonMarkdown;
	{
		jsonMarkdown.insert("text", text);
		jsonMarkdown.insert("title", title);
	}
	json.insert("msgtype", "markdown");
	json.insert("markdown", jsonMarkdown);
	addAt(json, atMobiles, atUserIds, isAtAll);
	QString jsonString(QJsonDocument(json).toJson());
	return send(QJsonDocument(json));
}

void XQRobotDing::init()
{
}

QString XQRobotDing::sign()
{
	if (secret().isEmpty())
		return QString();
	m_timestamp = QDateTime::currentMSecsSinceEpoch();
	QByteArray stringToSign = QByteArray::number(m_timestamp) + "\n" + secret().toUtf8();

	QByteArray key = secret().toUtf8();
	QByteArray data = stringToSign;
	QByteArray signData = QMessageAuthenticationCode::hash(data, key, QCryptographicHash::Sha256);
	QString sign = QUrl::toPercentEncoding(signData.toBase64());
	return std::move(sign);
}

QString XQRobotDing::url()
{
	QString sign = this->sign();
	QString url = QString("https://oapi.dingtalk.com/robot/send?access_token=%1").arg(token());
	if(!sign.isEmpty())
		url+=QString("&timestamp=%1&sign=%2").arg(m_timestamp).arg(sign);
	return std::move(url);
}

void XQRobotDing::addAt(QJsonObject& parent, const QStringList& atMobiles, const QStringList& atUserIds, bool isAtAll)
{
	QJsonObject jsonAt;
	{
		QJsonArray Json_atMobiles;
		for (auto& data : atMobiles)
		{
			Json_atMobiles.append(data);
		}
		jsonAt.insert("atMobiles", Json_atMobiles);
	}
	{
		QJsonArray Json_atUserIds;
		for (auto& data : atUserIds)
		{
			Json_atUserIds.append(data);
		}
		jsonAt.insert("atUserIds", Json_atUserIds);
	}
	{
		jsonAt.insert("isAtAll", isAtAll);
	}
	parent.insert("at", jsonAt);
}
