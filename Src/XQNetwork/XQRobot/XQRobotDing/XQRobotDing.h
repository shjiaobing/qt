﻿#ifndef XQROBOTDING_H
#define XQROBOTDING_H
#include"XQRobot.h"
//钉钉在线文档https://open.dingtalk.com/document/robots/custom-robot-access
//钉钉机器人
class XQRobotDing:public XQRobot
{
	Q_OBJECT
public:
	XQRobotDing(QObject* parent = nullptr);
	~XQRobotDing();
	//获取令牌
	inline QString token()const;
	//获取密钥
	inline QString secret()const;
	//获取当前的标志
	QString sign();
	//获取当前的url
	QString url();
public:
	//设置令牌
	void setToken(const QString& token);
	//设置密钥
	void setSecret(const QString& secret);
	//content(内容) atMobiles(@人的手机号)   atUserIds(@人的userid)  isAtAll(是否@所有人)
	bool sendText(const QString& content, const QStringList& atMobiles = {}, const QStringList& atUserIds = {}, bool isAtAll = false);
	//title(标题)   text(文本内容)  messageUrl(跳转链接)   picUrl(图片地址)
	bool sendLink(const QString& title, const QString& text, const QString& messageUrl, const QString& picUrl= QString());
	//title(标题)   text(文本内容)  atMobiles(@人的手机号)   atUserIds(@人的userid)  isAtAll(是否@所有人)
	bool sendMarkdown(const QString& title, const QString& text, const QStringList& atMobiles = {}, const QStringList& atUserIds = {}, bool isAtAll = false);
protected:
	void init();
	//添加at相关参数
	void addAt(QJsonObject& parent, const QStringList& atMobiles = {}, const QStringList& atUserIds = {}, bool isAtAll = false);
protected:
	QString m_token;//令牌
	QString m_secret;//密钥
	qint64 m_timestamp = 0;//当前时间戳，单位是毫秒
};
#endif // !XQRobotDing_H
