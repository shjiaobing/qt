﻿#ifndef XQROBOT_H
#define XQROBOT_H
#include<QJsonDocument>
#include<QObject>
#include"XQHead.h"
enum RobotType
{
	DingDing//钉钉
};
//机器人
class XQRobot :public QObject
{
	Q_OBJECT
public:
	XQRobot(RobotType type,QObject* parent = nullptr);
	virtual ~XQRobot()=default;
	//是否启用
	bool isEnabled()const;
	//获取当前的url
	virtual QString url() = 0;
public:
	//设置是否启用
	void setEnabled(bool Enabled);
public:
	//转钉钉机器人指针(如果可以的话)
	XQRobotDing* toDingDingRobot()const;
public:
	//发送json数据
	virtual bool send(const QJsonDocument& json);
protected:
	
protected:
	bool m_Enabled = true;//是否启用
	RobotType m_type;//机器人类型
};
#endif // !XQRobot_H
