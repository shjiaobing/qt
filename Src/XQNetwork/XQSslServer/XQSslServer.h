﻿#ifndef XQSSLSERVER_H
#define XQSSLSERVER_H
#include<QTcpServer>
#include<QSslConfiguration>
#include<QSslKey>
#include<QSet>
#include"XQNework.h"
class XQSslServer:public QTcpServer
{
	Q_OBJECT
public:
	XQSslServer(QObject* parent = nullptr);
	QSslConfiguration* sslConfiguration();
	//获取协议
	Http::Protocol protocol()const;
public:
	//设置证书
	void setLocalCertificate(const QSslCertificate& certificate);
	void setLocalCertificate(const QString& path, QSsl::EncodingFormat format = QSsl::Pem);
	//设置证书
	void setPrivateKey(const QSslKey& key);
	void setPrivateKey(const QString& path, QSsl::KeyAlgorithm algorithm= QSsl::Rsa, QSsl::EncodingFormat encoding = QSsl::Pem, QSsl::KeyType type = QSsl::PrivateKey, const QByteArray& passPhrase = QByteArray());
	//设置协议 http/https
	void setProtocol(Http::Protocol protocol);
	//监听开始运行
	bool listen(const QHostAddress &address = QHostAddress::Any, quint16 port = 0);
signals://信号
	
protected:
	/*当有新的连接可用时，QTopServer调用这个虚函数。
	socketDescriptor参数是已接受连接的本机套接字描述符。
	基本实现创建QTcpSocket，设置套接字描述符，然后将QTcpSocket存储在挂起连接的内部列表中。最后触发newConnection()。重新实现此函数以在连接可用时更改服务器的行为。
	如果此服务器正在使用QNetworkProxy，那么socketDescriptor可能无法与本机套接字函数一起使用，而应该仅与QTcpSocket::setSocketDescriptor()。*/
	void incomingConnection(qintptr socketDescriptor)override;
	bool isSslConnection(qintptr socketDescriptor);
protected:
	QSslConfiguration m_sslConfiguration= QSslConfiguration::defaultConfiguration();
	Http::Protocol m_protocol = Http::Protocol::http;//协议
};
#endif // !XQSslServer_H
