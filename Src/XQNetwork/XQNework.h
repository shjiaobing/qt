﻿#ifndef XQNEWORK_H
#define XQNEWORK_H
class XQLog;
class XQHttpHeadObject;
class QAbstractSocket;
class QNetworkRequest;
class QNetworkAccessManager;
class QNetworkReply;
class XQHttpHeadRequest;
class QTcpServer;
class QTcpSocket;
class QSslSocket;
class QLocalServer;
class QLocalSocket;
class XQSslServer;
class XQTaskPool;
class XQHttpHeadReply;
class XQFtpPI;
class XQFtpCommand;
#include<QDebug>
//网络类型
enum class NetworkType
{
	Tcp,
	Local
};
namespace Http
{
	//debug模式
	enum Debug
	{
		error=1,//错误
		Run=2,//运行
		Connection =4,//连接
		Disconnect=8,//断开
		HeadRequest=16,//请求头
		HeadReply=32,//响应头
		
	};
	//http协议版本
	enum Versions
	{
		HTTP1,//HTTP/1.0：最早的 HTTP 协议版本，提供了基本的请求和响应功能。它使用短暂的连接，在每个请求/响应后都会关闭连接。
		HTTP11,//HTTP/1.1 是当前广泛使用的 HTTP 协议版本，引入了持久连接（keep - alive）机制，允许多个请求 / 响应通过同一连接复用，从而减少了连接建立的开销。它还支持管道化（pipelining）技术，允许客户端同时发送多个请求而无需等待响应。
		HTTP2,//HTTP/2
		HTTP3,//HTTP/3
	};
	//请求类型
	enum RequestType
	{
		GET,
		POST
	};
	//协议
	enum Protocol
	{
		http,
		https
	};
	//头部类型
	enum Head
	{
		Request,//请求头
		Reply//响应头
	};
}
namespace Ftp
{
	//debug模式
	enum Debug
	{
		error = 1,//错误
		Run = 2,//运行
		Connection = 4,//连接
		Disconnect = 8,//断开
		CommandWrite = 16,//发送命令
		CommandRead = 32,//接收命令返回信息

	};
    //状态
    enum State {
        Unconnected,//未连接
        HostLookup,//主机查找中
        Connecting,//连接中
        Connected,//已连接
        LoggedIn,//已登录
        Closing//关闭中
    };
    //错误
    enum Error {
        NoError,//无错误
        UnknownError,//未知错误
        HostNotFound,//主机未找到
        ConnectionRefused,//连接被拒绝
        NotConnected//未连接
    };
    //命令
    enum Command {
        None,//无指令
        SetTransferMode,//设置传输模式
        SetProxy,//设置代理
        ConnectToHost,//连接到主机
        Login,//登录
        Close,//关闭连接
        List,// 列出文件列表
        Cd,//切换目录
        Get,//下载文件
        Put,//上传文件
        Remove,//删除文件
        Mkdir,//创建目录
        Rmdir,// 删除目录
        Rename,//重命名文件或目录
        RawCommand//原始命令
    };
    //传输模式
    enum TransferMode {
        Active,//主动
        Passive//被动
    };
    //传输类型
    enum TransferType {
        Binary,//二进制
        Ascii//ASCII文本
    };
}
//http
QDebug  operator<<(QDebug debug, Http::Protocol value);
//ftp
QDebug operator<<(QDebug debug, Ftp::State value);
QDebug operator<<(QDebug debug, Ftp::Error value);
QDebug operator<<(QDebug debug, Ftp::Command value);
QDebug operator<<(QDebug debug, Ftp::TransferMode value);
QDebug operator<<(QDebug debug, Ftp::TransferType value);
#endif // !XQNework_H
