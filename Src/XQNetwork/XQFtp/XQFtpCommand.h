﻿#ifndef QFTPCOMMAND_H
#define QFTPCOMMAND_H
#include"XQFtp.h"
#include"XQNework.h"
/**********************************************************************
 *
 * XQFtpCommand ftp命令
 *
 *********************************************************************/
class XQFtpCommand
{
public:
    XQFtpCommand(Ftp::Command cmd, QStringList raw, const QByteArray& ba);
    XQFtpCommand(Ftp::Command cmd, QStringList raw, QIODevice* dev = 0);
    ~XQFtpCommand();

    int m_id;
    Ftp::Command m_command;
    QStringList m_rawCmds;

    // 如果is_ba为true，则使用ba;ba不等于0。
    // 否则使用dev;dev可以是0也可以不是。
    union {
        QByteArray* ba;
        QIODevice* dev;
    } m_data;
    bool m_is_ba;//是否是QByteArray

    static QBasicAtomicInt m_idCounter;//id计数器
};
#endif // !QFtpPrivate_H
