﻿#ifndef QURLINFO_H
#define QURLINFO_H

#include<QDateTime>
#include<QString>
#include<QIODevice>

QT_BEGIN_NAMESPACE

class QUrl;
class QUrlInfoPrivate;

class XQUrlInfo
{
public:
    enum PermissionSpec {
        ReadOwner = 00400, WriteOwner = 00200, ExeOwner = 00100,
        ReadGroup = 00040, WriteGroup = 00020, ExeGroup = 00010,
        ReadOther = 00004, WriteOther = 00002, ExeOther = 00001 };

    XQUrlInfo();
    XQUrlInfo(const XQUrlInfo &ui);
    XQUrlInfo(const QString &name, int permissions, const QString &owner,
             const QString &group, qint64 size, const QDateTime &lastModified,
             const QDateTime &lastRead, bool isDir, bool isFile, bool isSymLink,
             bool isWritable, bool isReadable, bool isExecutable);
    XQUrlInfo(const QUrl &url, int permissions, const QString &owner,
             const QString &group, qint64 size, const QDateTime &lastModified,
             const QDateTime &lastRead, bool isDir, bool isFile, bool isSymLink,
             bool isWritable, bool isReadable, bool isExecutable);
    XQUrlInfo &operator=(const XQUrlInfo &ui);
    virtual ~XQUrlInfo();

    virtual void setName(const QString &name);
    virtual void setDir(bool b);
    virtual void setFile(bool b);
    virtual void setSymLink(bool b);
    virtual void setOwner(const QString &s);
    virtual void setGroup(const QString &s);
    virtual void setSize(qint64 size);
    virtual void setWritable(bool b);
    virtual void setReadable(bool b);
    virtual void setPermissions(int p);
    virtual void setLastModified(const QDateTime &dt);
    void setLastRead(const QDateTime &dt);
    //如果 XQUrlInfo 对象是有效的，则返回 true
    bool isValid() const;
    //返回文件或目录的名称
    QString name() const;
    //返回文件或目录的权限信息，如读、写和执行权限
    int permissions() const;
    //获取文件或目录的所有者信息
    QString owner() const;
    //获取文件或目录的所属组信息
    QString group() const;
    //返回文件的大小（以字节为单位）。如果目标为目录，则返回-1
    qint64 size() const;
    //返回文件或目录的最后修改时间
    QDateTime lastModified() const;
    //获取文件或目录的最后读取时间
    QDateTime lastRead() const;
    //如果文件或目录是一个目录，则返回 true
    bool isDir() const;
    //如果文件或目录是一个文件，则返回 true
    bool isFile() const;
    //判断是否是符号链接
    bool isSymLink() const;
    //如果文件或目录可写，则返回 true
    bool isWritable() const;
    //如果文件或目录可读，则返回 true
    bool isReadable() const;
    //检查文件是否可执行
    bool isExecutable() const;

    static bool greaterThan(const XQUrlInfo &i1, const XQUrlInfo &i2,
                             int sortBy);
    static bool lessThan(const XQUrlInfo &i1, const XQUrlInfo &i2,
                          int sortBy);
    static bool equal(const XQUrlInfo &i1, const XQUrlInfo &i2,
                       int sortBy);
    //比较两个 XQUrlInfo 对象是否相等
    bool operator==(const XQUrlInfo &i) const;
    //比较两个 XQUrlInfo 对象是否不相等
    inline bool operator!=(const XQUrlInfo &i) const
    { return !operator==(i); }

private:
    QUrlInfoPrivate *m_d;
};

QT_END_NAMESPACE

#endif // QURLINFO_H
