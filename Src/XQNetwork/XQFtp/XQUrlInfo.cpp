﻿#include"XQUrlInfo.h"
#include<QUrl>
#include<QDir>
#include<limits.h>

QT_BEGIN_NAMESPACE

class QUrlInfoPrivate
{
public:
    QUrlInfoPrivate() :
        permissions(0),
        size(0),
        isDir(false),
        isFile(true),
        isSymLink(false),
        isWritable(true),
        isReadable(true),
        isExecutable(false)
    {}

    QString name;
    int permissions;
    QString owner;
    QString group;
    qint64 size;

    QDateTime lastModified;
    QDateTime lastRead;
    bool isDir;
    bool isFile;
    bool isSymLink;
    bool isWritable;
    bool isReadable;
    bool isExecutable;
};


/*!
    \class XQUrlInfo
    \brief The XQUrlInfo class stores information about URLs.

    \ingroup io
    \ingroup network
    \inmodule QtNetwork

    The information about a URL that can be retrieved includes name(),
    permissions(), owner(), group(), size(), lastModified(),
    lastRead(), isDir(), isFile(), isSymLink(), isWritable(),
    isReadable() and isExecutable().

    You can create your own XQUrlInfo objects passing in all the
    relevant information in the constructor, and you can modify a
    XQUrlInfo; for each getter mentioned above there is an equivalent
    setter. Note that setting values does not affect the underlying
    resource that the XQUrlInfo provides information about; for example
    if you call setWritable(true) on a read-only resource the only
    thing changed is the XQUrlInfo object, not the resource.

    \sa QUrl, {FTP Example}
*/

/*!
    \enum XQUrlInfo::PermissionSpec

    This enum is used by the permissions() function to report the
    permissions of a file.

    \value ReadOwner The file is readable by the owner of the file.
    \value WriteOwner The file is writable by the owner of the file.
    \value ExeOwner The file is executable by the owner of the file.
    \value ReadGroup The file is readable by the group.
    \value WriteGroup The file is writable by the group.
    \value ExeGroup The file is executable by the group.
    \value ReadOther The file is readable by anyone.
    \value WriteOther The file is writable by anyone.
    \value ExeOther The file is executable by anyone.
*/

/*!
    Constructs an invalid XQUrlInfo object with default values.

    \sa isValid()
*/

XQUrlInfo::XQUrlInfo()
{
    m_d = 0;
}

/*!
    Copy constructor, copies \a ui to this URL info object.
*/

XQUrlInfo::XQUrlInfo(const XQUrlInfo &ui)
{
    if (ui.m_d) {
        m_d = new QUrlInfoPrivate;
        *m_d = *ui.m_d;
    } else {
        m_d = 0;
    }
}

/*!
    Constructs a XQUrlInfo object by specifying all the URL's
    information.

    The information that is passed is the \a name, file \a
    permissions, \a owner and \a group and the file's \a size. Also
    passed is the \a lastModified date/time and the \a lastRead
    date/time. Flags are also passed, specifically, \a isDir, \a
    isFile, \a isSymLink, \a isWritable, \a isReadable and \a
    isExecutable.
*/

XQUrlInfo::XQUrlInfo(const QString &name, int permissions, const QString &owner,
                    const QString &group, qint64 size, const QDateTime &lastModified,
                    const QDateTime &lastRead, bool isDir, bool isFile, bool isSymLink,
                    bool isWritable, bool isReadable, bool isExecutable)
{
    m_d = new QUrlInfoPrivate;
    m_d->name = name;
    m_d->permissions = permissions;
    m_d->owner = owner;
    m_d->group = group;
    m_d->size = size;
    m_d->lastModified = lastModified;
    m_d->lastRead = lastRead;
    m_d->isDir = isDir;
    m_d->isFile = isFile;
    m_d->isSymLink = isSymLink;
    m_d->isWritable = isWritable;
    m_d->isReadable = isReadable;
    m_d->isExecutable = isExecutable;
}


/*!
    Constructs a XQUrlInfo object by specifying all the URL's
    information.

    The information that is passed is the \a url, file \a
    permissions, \a owner and \a group and the file's \a size. Also
    passed is the \a lastModified date/time and the \a lastRead
    date/time. Flags are also passed, specifically, \a isDir, \a
    isFile, \a isSymLink, \a isWritable, \a isReadable and \a
    isExecutable.
*/

XQUrlInfo::XQUrlInfo(const QUrl &url, int permissions, const QString &owner,
                    const QString &group, qint64 size, const QDateTime &lastModified,
                    const QDateTime &lastRead, bool isDir, bool isFile, bool isSymLink,
                    bool isWritable, bool isReadable, bool isExecutable)
{
    m_d = new QUrlInfoPrivate;
    m_d->name = QFileInfo(url.path()).fileName();
    m_d->permissions = permissions;
    m_d->owner = owner;
    m_d->group = group;
    m_d->size = size;
    m_d->lastModified = lastModified;
    m_d->lastRead = lastRead;
    m_d->isDir = isDir;
    m_d->isFile = isFile;
    m_d->isSymLink = isSymLink;
    m_d->isWritable = isWritable;
    m_d->isReadable = isReadable;
    m_d->isExecutable = isExecutable;
}


/*!
    Sets the name of the URL to \a name. The name is the full text,
    for example, "http://qt.nokia.com/doc/qurlinfo.html".

    If you call this function for an invalid URL info, this function
    turns it into a valid one.

    \sa isValid()
*/

void XQUrlInfo::setName(const QString &name)
{
    if (!m_d)
        m_d = new QUrlInfoPrivate;
    m_d->name = name;
}


/*!
    If \a b is true then the URL is set to be a directory; if \a b is
    false then the URL is set not to be a directory (which normally
    means it is a file). (Note that a URL can refer to both a file and
    a directory even though most file systems do not support this.)

    If you call this function for an invalid URL info, this function
    turns it into a valid one.

    \sa isValid()
*/

void XQUrlInfo::setDir(bool b)
{
    if (!m_d)
        m_d = new QUrlInfoPrivate;
    m_d->isDir = b;
}


/*!
    If \a b is true then the URL is set to be a file; if \b is false
    then the URL is set not to be a file (which normally means it is a
    directory). (Note that a URL can refer to both a file and a
    directory even though most file systems do not support this.)

    If you call this function for an invalid URL info, this function
    turns it into a valid one.

    \sa isValid()
*/

void XQUrlInfo::setFile(bool b)
{
    if (!m_d)
        m_d = new QUrlInfoPrivate;
    m_d->isFile = b;
}


/*!
    Specifies that the URL refers to a symbolic link if \a b is true
    and that it does not if \a b is false.

    If you call this function for an invalid URL info, this function
    turns it into a valid one.

    \sa isValid()
*/

void XQUrlInfo::setSymLink(bool b)
{
    if (!m_d)
        m_d = new QUrlInfoPrivate;
    m_d->isSymLink = b;
}


/*!
    Specifies that the URL is writable if \a b is true and not
    writable if \a b is false.

    If you call this function for an invalid URL info, this function
    turns it into a valid one.

    \sa isValid()
*/

void XQUrlInfo::setWritable(bool b)
{
    if (!m_d)
        m_d = new QUrlInfoPrivate;
    m_d->isWritable = b;
}


/*!
    Specifies that the URL is readable if \a b is true and not
    readable if \a b is false.

    If you call this function for an invalid URL info, this function
    turns it into a valid one.

    \sa isValid()
*/

void XQUrlInfo::setReadable(bool b)
{
    if (!m_d)
        m_d = new QUrlInfoPrivate;
    m_d->isReadable = b;
}

/*!
    Specifies that the owner of the URL is called \a s.

    If you call this function for an invalid URL info, this function
    turns it into a valid one.

    \sa isValid()
*/

void XQUrlInfo::setOwner(const QString &s)
{
    if (!m_d)
        m_d = new QUrlInfoPrivate;
    m_d->owner = s;
}

/*!
    Specifies that the owning group of the URL is called \a s.

    If you call this function for an invalid URL info, this function
    turns it into a valid one.

    \sa isValid()
*/

void XQUrlInfo::setGroup(const QString &s)
{
    if (!m_d)
        m_d = new QUrlInfoPrivate;
    m_d->group = s;
}

/*!
    Specifies the \a size of the URL.

    If you call this function for an invalid URL info, this function
    turns it into a valid one.

    \sa isValid()
*/

void XQUrlInfo::setSize(qint64 size)
{
    if (!m_d)
        m_d = new QUrlInfoPrivate;
    m_d->size = size;
}

/*!
    Specifies that the URL has access permissions \a p.

    If you call this function for an invalid URL info, this function
    turns it into a valid one.

    \sa isValid()
*/

void XQUrlInfo::setPermissions(int p)
{
    if (!m_d)
        m_d = new QUrlInfoPrivate;
    m_d->permissions = p;
}

/*!
    Specifies that the object the URL refers to was last modified at
    \a dt.

    If you call this function for an invalid URL info, this function
    turns it into a valid one.

    \sa isValid()
*/

void XQUrlInfo::setLastModified(const QDateTime &dt)
{
    if (!m_d)
        m_d = new QUrlInfoPrivate;
    m_d->lastModified = dt;
}

/*!
    \since 4.4

    Specifies that the object the URL refers to was last read at
    \a dt.

    If you call this function for an invalid URL info, this function
    turns it into a valid one.

    \sa isValid()
*/

void XQUrlInfo::setLastRead(const QDateTime &dt)
{
    if (!m_d)
        m_d = new QUrlInfoPrivate;
    m_d->lastRead = dt;
}

/*!
    Destroys the URL info object.
*/

XQUrlInfo::~XQUrlInfo()
{
    delete m_d;
}

/*!
    Assigns the values of \a ui to this XQUrlInfo object.
*/

XQUrlInfo &XQUrlInfo::operator=(const XQUrlInfo &ui)
{
    if (ui.m_d) {
        if (!m_d)
            m_d= new QUrlInfoPrivate;
        *m_d = *ui.m_d;
    } else {
        delete m_d;
        m_d = 0;
    }
    return *this;
}

/*!
    Returns the file name of the URL.

    \sa isValid()
*/

QString XQUrlInfo::name() const
{
    if (!m_d)
        return QString();
    return m_d->name;
}

/*!
    Returns the permissions of the URL. You can use the \c PermissionSpec flags
    to test for certain permissions.

    \sa isValid()
*/

int XQUrlInfo::permissions() const
{
    if (!m_d)
        return 0;
    return m_d->permissions;
}

/*!
    Returns the owner of the URL.

    \sa isValid()
*/

QString XQUrlInfo::owner() const
{
    if (!m_d)
        return QString();
    return m_d->owner;
}

/*!
    Returns the group of the URL.

    \sa isValid()
*/

QString XQUrlInfo::group() const
{
    if (!m_d)
        return QString();
    return m_d->group;
}

/*!
    Returns the size of the URL.

    \sa isValid()
*/

qint64 XQUrlInfo::size() const
{
    if (!m_d)
        return 0;
    return m_d->size;
}

/*!
    Returns the last modification date of the URL.

    \sa isValid()
*/

QDateTime XQUrlInfo::lastModified() const
{
    if (!m_d)
        return QDateTime();
    return m_d->lastModified;
}

/*!
    Returns the date when the URL was last read.

    \sa isValid()
*/

QDateTime XQUrlInfo::lastRead() const
{
    if (!m_d)
        return QDateTime();
    return m_d->lastRead;
}

/*!
    Returns true if the URL is a directory; otherwise returns false.

    \sa isValid()
*/

bool XQUrlInfo::isDir() const
{
    if (!m_d)
        return false;
    return m_d->isDir;
}

/*!
    Returns true if the URL is a file; otherwise returns false.

    \sa isValid()
*/

bool XQUrlInfo::isFile() const
{
    if (!m_d)
        return false;
    return m_d->isFile;
}

/*!
    Returns true if the URL is a symbolic link; otherwise returns false.

    \sa isValid()
*/

bool XQUrlInfo::isSymLink() const
{
    if (!m_d)
        return false;
    return m_d->isSymLink;
}

/*!
    Returns true if the URL is writable; otherwise returns false.

    \sa isValid()
*/

bool XQUrlInfo::isWritable() const
{
    if (!m_d)
        return false;
    return m_d->isWritable;
}

/*!
    Returns true if the URL is readable; otherwise returns false.

    \sa isValid()
*/

bool XQUrlInfo::isReadable() const
{
    if (!m_d)
        return false;
    return m_d->isReadable;
}

/*!
    Returns true if the URL is executable; otherwise returns false.

    \sa isValid()
*/

bool XQUrlInfo::isExecutable() const
{
    if (!m_d)
        return false;
    return m_d->isExecutable;
}

/*!
    Returns true if \a i1 is greater than \a i2; otherwise returns
    false. The objects are compared by the value, which is specified
    by \a sortBy. This must be one of QDir::Name, QDir::Time or
    QDir::Size.
*/

bool XQUrlInfo::greaterThan(const XQUrlInfo &i1, const XQUrlInfo &i2,
                            int sortBy)
{
    switch (sortBy) {
    case QDir::Name:
        return i1.name() > i2.name();
    case QDir::Time:
        return i1.lastModified() > i2.lastModified();
    case QDir::Size:
        return i1.size() > i2.size();
    default:
        return false;
    }
}

/*!
    Returns true if \a i1 is less than \a i2; otherwise returns false.
    The objects are compared by the value, which is specified by \a
    sortBy. This must be one of QDir::Name, QDir::Time or QDir::Size.
*/

bool XQUrlInfo::lessThan(const XQUrlInfo &i1, const XQUrlInfo &i2,
                         int sortBy)
{
    return !greaterThan(i1, i2, sortBy);
}

/*!
    Returns true if \a i1 equals to \a i2; otherwise returns false.
    The objects are compared by the value, which is specified by \a
    sortBy. This must be one of QDir::Name, QDir::Time or QDir::Size.
*/

bool XQUrlInfo::equal(const XQUrlInfo &i1, const XQUrlInfo &i2,
                      int sortBy)
{
    switch (sortBy) {
    case QDir::Name:
        return i1.name() == i2.name();
    case QDir::Time:
        return i1.lastModified() == i2.lastModified();
    case QDir::Size:
        return i1.size() == i2.size();
    default:
        return false;
    }
}

/*!
    Returns true if this XQUrlInfo is equal to \a other; otherwise
    returns false.

    \sa lessThan(), equal()
*/

bool XQUrlInfo::operator==(const XQUrlInfo &other) const
{
    if (!m_d)
        return other.m_d == 0;
    if (!other.m_d)
        return false;

    return (m_d->name == other.m_d->name &&
            m_d->permissions == other.m_d->permissions &&
            m_d->owner == other.m_d->owner &&
            m_d->group == other.m_d->group &&
            m_d->size == other.m_d->size &&
            m_d->lastModified == other.m_d->lastModified &&
            m_d->lastRead == other.m_d->lastRead &&
            m_d->isDir == other.m_d->isDir &&
            m_d->isFile == other.m_d->isFile &&
            m_d->isSymLink == other.m_d->isSymLink &&
            m_d->isWritable == other.m_d->isWritable &&
            m_d->isReadable == other.m_d->isReadable &&
            m_d->isExecutable == other.m_d->isExecutable);
}

/*!
    \fn bool XQUrlInfo::operator!=(const XQUrlInfo &other) const
    \since 4.2

    Returns true if this XQUrlInfo is not equal to \a other; otherwise
    returns false.

    \sa lessThan(), equal()
*/

/*!
    Returns true if the URL info is valid; otherwise returns false.
    Valid means that the XQUrlInfo contains real information.

    You should always check if the URL info is valid before relying on
    the values.
*/
bool XQUrlInfo::isValid() const
{
    return m_d != 0;
}

QT_END_NAMESPACE
