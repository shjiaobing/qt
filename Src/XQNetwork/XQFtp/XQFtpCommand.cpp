﻿#include"XQFtpCommand.h"
QBasicAtomicInt XQFtpCommand::m_idCounter = Q_BASIC_ATOMIC_INITIALIZER(1);
XQFtpCommand::XQFtpCommand(Ftp::Command cmd, QStringList raw, const QByteArray& ba)
    : m_command(cmd), m_rawCmds(raw), m_is_ba(true)
{
    m_id = m_idCounter.fetchAndAddRelaxed(1);
    m_data.ba = new QByteArray(ba);
}

XQFtpCommand::XQFtpCommand(Ftp::Command cmd, QStringList raw, QIODevice* dev)
    : m_command(cmd), m_rawCmds(raw), m_is_ba(false)
{
    m_id = m_idCounter.fetchAndAddRelaxed(1);
    m_data.dev = dev;
}

XQFtpCommand::~XQFtpCommand()
{
    if (m_is_ba)
        delete m_data.ba;
}