﻿#include"XQFtpDTP.h"
#include"XQFtpPI.h"
#include"XQFtp/XQFtp.h"
#include"XQLog.hpp"
#include<QRegExp>
static void _q_fixupDateTime(QDateTime* dateTime)
{
    // Adjust for future tolerance.
    const int futureTolerance = 86400;
    if (dateTime->secsTo(QDateTime::currentDateTime()) < -futureTolerance) {
        QDate d = dateTime->date();
        d.setDate(d.year() - 1, d.month(), d.day());
        dateTime->setDate(d);
    }
}
static void _q_parseUnixDir(const QStringList& tokens, const QString& userName, XQUrlInfo* info)
{
    // Unix style, 7 + 1 entries
    // -rw-r--r--    1 ftp      ftp      17358091 Aug 10  2004 qt-x11-free-3.3.3.tar.gz
    // drwxr-xr-x    3 ftp      ftp          4096 Apr 14  2000 compiled-examples
    // lrwxrwxrwx    1 ftp      ftp             9 Oct 29  2005 qtscape -> qtmozilla
    if (tokens.size() != 8)
        return;

    char first = tokens.at(1).at(0).toLatin1();
    if (first == 'd') {
        info->setDir(true);
        info->setFile(false);
        info->setSymLink(false);
    }
    else if (first == '-') {
        info->setDir(false);
        info->setFile(true);
        info->setSymLink(false);
    }
    else if (first == 'l') {
        info->setDir(true);
        info->setFile(false);
        info->setSymLink(true);
    }

    // Resolve filename
    QString name = tokens.at(7);
    if (info->isSymLink()) {
        int linkPos = name.indexOf(QLatin1String(" ->"));
        if (linkPos != -1)
            name.resize(linkPos);
    }
    info->setName(name);

    // Resolve owner & group
    info->setOwner(tokens.at(3));
    info->setGroup(tokens.at(4));

    // Resolve size
    info->setSize(tokens.at(5).toLongLong());

    QStringList formats;
    formats << QLatin1String("MMM dd  yyyy") << QLatin1String("MMM dd hh:mm") << QLatin1String("MMM  d  yyyy")
        << QLatin1String("MMM  d hh:mm") << QLatin1String("MMM  d yyyy") << QLatin1String("MMM dd yyyy");

    QString dateString = tokens.at(6);
    dateString[0] = dateString[0].toUpper();

    // Resolve the modification date by parsing all possible formats
    QDateTime dateTime;
    int n = 0;
#ifndef QT_NO_DATESTRING
    do {
        dateTime = QLocale::c().toDateTime(dateString, formats.at(n++));
    } while (n < formats.size() && (!dateTime.isValid()));
#endif

    if (n == 2 || n == 4) {
        // Guess the year.
        dateTime.setDate(QDate(QDate::currentDate().year(),
            dateTime.date().month(),
            dateTime.date().day()));
        _q_fixupDateTime(&dateTime);
    }
    if (dateTime.isValid())
        info->setLastModified(dateTime);

    // Resolve permissions
    int permissions = 0;
    QString p = tokens.at(2);
    permissions |= (p[0] == QLatin1Char('r') ? XQUrlInfo::ReadOwner : 0);
    permissions |= (p[1] == QLatin1Char('w') ? XQUrlInfo::WriteOwner : 0);
    permissions |= (p[2] == QLatin1Char('x') ? XQUrlInfo::ExeOwner : 0);
    permissions |= (p[3] == QLatin1Char('r') ? XQUrlInfo::ReadGroup : 0);
    permissions |= (p[4] == QLatin1Char('w') ? XQUrlInfo::WriteGroup : 0);
    permissions |= (p[5] == QLatin1Char('x') ? XQUrlInfo::ExeGroup : 0);
    permissions |= (p[6] == QLatin1Char('r') ? XQUrlInfo::ReadOther : 0);
    permissions |= (p[7] == QLatin1Char('w') ? XQUrlInfo::WriteOther : 0);
    permissions |= (p[8] == QLatin1Char('x') ? XQUrlInfo::ExeOther : 0);
    info->setPermissions(permissions);

    bool isOwner = info->owner() == userName;
    info->setReadable((permissions & XQUrlInfo::ReadOther) || ((permissions & XQUrlInfo::ReadOwner) && isOwner));
    info->setWritable((permissions & XQUrlInfo::WriteOther) || ((permissions & XQUrlInfo::WriteOwner) && isOwner));
}

static void _q_parseDosDir(const QStringList& tokens, const QString& userName, XQUrlInfo* info)
{
    // DOS style, 3 + 1 entries
    // 01-16-02  11:14AM       <DIR>          epsgroup
    // 06-05-03  03:19PM                 1973 readme.txt
    if (tokens.size() != 4)
        return;

    Q_UNUSED(userName);

    QString name = tokens.at(3);
    info->setName(name);
    info->setSymLink(name.toLower().endsWith(QLatin1String(".lnk")));

    if (tokens.at(2) == QLatin1String("<DIR>")) {
        info->setFile(false);
        info->setDir(true);
    }
    else {
        info->setFile(true);
        info->setDir(false);
        info->setSize(tokens.at(2).toLongLong());
    }

    // Note: We cannot use QFileInfo; permissions are for the server-side
    // machine, and QFileInfo's behavior depends on the local platform.
    int permissions = XQUrlInfo::ReadOwner | XQUrlInfo::WriteOwner
        | XQUrlInfo::ReadGroup | XQUrlInfo::WriteGroup
        | XQUrlInfo::ReadOther | XQUrlInfo::WriteOther;
    QString ext;
    int extIndex = name.lastIndexOf(QLatin1Char('.'));
    if (extIndex != -1)
        ext = name.mid(extIndex + 1);
    if (ext == QLatin1String("exe") || ext == QLatin1String("bat") || ext == QLatin1String("com"))
        permissions |= XQUrlInfo::ExeOwner | XQUrlInfo::ExeGroup | XQUrlInfo::ExeOther;
    info->setPermissions(permissions);

    info->setReadable(true);
    info->setWritable(info->isFile());

    QDateTime dateTime;
#ifndef QT_NO_DATESTRING
    dateTime = QLocale::c().toDateTime(tokens.at(1), QLatin1String("MM-dd-yy  hh:mmAP"));
    if (dateTime.date().year() < 1971) {
        dateTime.setDate(QDate(dateTime.date().year() + 100,
            dateTime.date().month(),
            dateTime.date().day()));
    }
#endif

    info->setLastModified(dateTime);

}

XQFtpDTP::XQFtpDTP(XQFtpPI* p, QObject* parent) :
    QObject(parent),
    m_socket(0),
    m_listener(this),
    m_pi(p),
    m_callWriteData(false)
{
    setBlockSize(128 * 1024);
    clearData();
    m_listener.setObjectName(QLatin1String("QFtpDTP active state server"));
    connect(&m_listener, SIGNAL(newConnection()), SLOT(setupSocket()));
}

void XQFtpDTP::setData(QByteArray* ba)
{
    m_is_ba = true;
    m_data.ba = ba;
}

void XQFtpDTP::setDevice(QIODevice* dev)
{
    m_is_ba = false;
    m_data.dev = dev;
}

void XQFtpDTP::setBytesTotal(qint64 bytes)
{
    m_bytesTotal = bytes;
    m_bytesDone = 0;
    emit dataTransferProgress(m_bytesDone, m_bytesTotal);
}

void XQFtpDTP::setDebug(int* debug)
{
    m_debug = debug;
}

bool XQFtpDTP::isDebugModel(Ftp::Debug model)
{
    if (m_debug == nullptr)
        return false;
    return  (*m_debug) & model;
}

void XQFtpDTP::setBlockSize(qint64 size)
{
    if (size <= 0 )
    {
        if(isDebugModel(Ftp::error))
            XQFtpLog << "错误" << QString("setBlockSize(qint64 size) size=%1 参数必须大于0").arg(size);
        return;
    }
    if (size == m_blockSize)
        return;
    if (m_buffer)
        delete[] m_buffer;
    m_buffer = new char[size];
    m_blockSize = size;
}

void XQFtpDTP::connectToHost(const QString& host, quint16 port)
{
    m_bytesFromSocket.clear();

    if (m_socket) {
        delete m_socket;
        m_socket = 0;
    }
    m_socket = new QTcpSocket(this);
#ifndef QT_NO_BEARERMANAGEMENT
    //copy network session down to the socket
    m_socket->setProperty("_q_networksession", property("_q_networksession"));
#endif
    m_socket->setObjectName(QLatin1String("QFtpDTP Passive state socket"));
    connect(m_socket, SIGNAL(connected()), SLOT(socketConnected()));
    connect(m_socket, SIGNAL(readyRead()), SLOT(socketReadyRead()));
    connect(m_socket, SIGNAL(errorOccurred(QAbstractSocket::SocketError)), SLOT(socketError(QAbstractSocket::SocketError)));
    connect(m_socket, SIGNAL(disconnected()), SLOT(socketConnectionClosed()));
    connect(m_socket, SIGNAL(bytesWritten(qint64)), SLOT(socketBytesWritten(qint64)));

    m_socket->connectToHost(host, port);
    if (isDebugModel(Ftp::Connection))
        XQFtpLog << "连接服务器:" << host<<":"<< port;
        
}

int XQFtpDTP::setupListener(const QHostAddress& address)
{
#ifndef QT_NO_BEARERMANAGEMENT
    //将网络会话复制到套接字
    m_listener.setProperty("_q_networksession", property("_q_networksession"));
#endif
    if (!m_listener.isListening() && !m_listener.listen(address, 0))
        return -1;
    return m_listener.serverPort();
}

void XQFtpDTP::waitForConnection()
{
    //此功能仅在主动传输模式下才有意义;
    //它通过阻塞、等待进入的连接来绕过QFtp设计中的限制。
    //对于默认的被动模式，它不做任何事情。
    if (m_listener.isListening())
        m_listener.waitForNewConnection();
}

QTcpSocket::SocketState XQFtpDTP::state() const
{
    return m_socket ? m_socket->state() : QTcpSocket::UnconnectedState;
}

qint64 XQFtpDTP::bytesAvailable() const
{
    if (!m_socket || m_socket->state() != QTcpSocket::ConnectedState)
        return (qint64)m_bytesFromSocket.size();
    return m_socket->bytesAvailable();
}

qint64 XQFtpDTP::read(char* data, qint64 maxlen)
{
    qint64 read;
    if (m_socket && m_socket->state() == QTcpSocket::ConnectedState) {
        read = m_socket->read(data, maxlen);
    }
    else {
        read = qMin(maxlen, qint64(m_bytesFromSocket.size()));
        memcpy(data, m_bytesFromSocket.data(), read);
        m_bytesFromSocket.remove(0, read);
    }

    m_bytesDone += read;
    return read;
}

QByteArray XQFtpDTP::readAll()
{
    QByteArray tmp;
    if (m_socket && m_socket->state() == QTcpSocket::ConnectedState) {
        tmp = m_socket->readAll();
        m_bytesDone += tmp.size();
    }
    else {
        tmp = m_bytesFromSocket;
        m_bytesFromSocket.clear();
    }
    return tmp;
}

void XQFtpDTP::writeData()
{
    if (!m_socket)
        return;

    if (m_is_ba) {
#if defined(QFTPDTP_DEBUG)
        qDebug("QFtpDTP::writeData: write %d bytes", data.ba->size());
#endif
        if (m_data.ba->size() == 0)
            emit dataTransferProgress(0, m_bytesTotal);
        else
            m_socket->write(m_data.ba->data(), m_data.ba->size());

        m_socket->close();
        emit finished();
        clearData();
    }
    else if (m_data.dev) {
        m_callWriteData = false;
        qint64 read = m_data.dev->read(m_buffer, m_blockSize);
#if defined(QFTPDTP_DEBUG)
        qDebug("QFtpDTP::writeData: write() of size %lli bytes", read);
#endif
        if (read > 0) {
            m_socket->write(m_buffer, read);
        }
        else if (read == -1 || (!m_data.dev->isSequential() && m_data.dev->atEnd())) {
            // error or EOF
            if (m_bytesDone == 0 && m_socket->bytesToWrite() == 0)
                emit dataTransferProgress(0, m_bytesTotal);
           
            m_socket->close();
            emit finished();
            clearData();
        }

        // do we continue uploading?
        m_callWriteData = m_data.dev != 0;
    }
}

void XQFtpDTP::dataReadyRead()
{
    writeData();
}



bool XQFtpDTP::hasError() const
{
    return !m_err.isNull();
}

QString XQFtpDTP::errorMessage() const
{
    return m_err;
}

void XQFtpDTP::clearError()
{
    m_err.clear();
}

void XQFtpDTP::abortConnection()
{
#if defined(QFTPDTP_DEBUG)
    qDebug("QFtpDTP::abortConnection, bytesAvailable == %lli",
        socket ? socket->bytesAvailable() : (qint64)0);
#endif
    m_callWriteData = false;
    clearData();

    if (m_socket)
        m_socket->abort();
}
bool XQFtpDTP::parseDir(const QByteArray& buffer, const QString& userName, XQUrlInfo* info)
{
    if (buffer.isEmpty())
        return false;

    QString bufferStr = QString::fromLatin1(buffer).trimmed();

    // Unix style FTP servers
    QRegExp unixPattern(QLatin1String("^([\\-dl])([a-zA-Z\\-]{9,9})\\s+\\d+\\s+(\\S*)\\s+"
        "(\\S*)\\s+(\\d+)\\s+(\\S+\\s+\\S+\\s+\\S+)\\s+(\\S.*)"));
    if (unixPattern.indexIn(bufferStr) == 0) {
        _q_parseUnixDir(unixPattern.capturedTexts(), userName, info);
        return true;
    }

    // DOS style FTP servers
    QRegExp dosPattern(QLatin1String("^(\\d\\d-\\d\\d-\\d\\d\\d?\\d?\\ \\ \\d\\d:\\d\\d[AP]M)\\s+"
        "(<DIR>|\\d+)\\s+(\\S.*)$"));
    if (dosPattern.indexIn(bufferStr) == 0) {
        _q_parseDosDir(dosPattern.capturedTexts(), userName, info);
        return true;
    }

    // Unsupported
    return false;
}
void XQFtpDTP::clearData()
{
    m_is_ba = false;
    m_data.dev = 0;
}

void XQFtpDTP::socketConnected()
{
    m_bytesDone = 0;
#if defined(QFTPDTP_DEBUG)
    qDebug("QFtpDTP::connectState(CsConnected)");
#endif
    emit connectState(XQFtpDTP::CsConnected);
}

void XQFtpDTP::socketReadyRead()
{
    if (!m_socket)
        return;

    if (m_pi->currentCommand().isEmpty()) {
        m_socket->close();
#if defined(QFTPDTP_DEBUG)
        qDebug("QFtpDTP::connectState(CsClosed)");
#endif
        emit connectState(XQFtpDTP::CsClosed);
        return;
    }

    if (m_pi->m_abortState == XQFtpPI::AbortStarted) {
        // discard data
        m_socket->readAll();
        return;
    }

    if (m_pi->currentCommand().startsWith(QLatin1String("LIST"))) {
        //是否有可读的一行数据
        while (m_socket->canReadLine()) {
            XQUrlInfo i;
            QByteArray line = m_socket->readLine();
#if defined(QFTPDTP_DEBUG)
            qDebug("QFtpDTP read (list): '%s'", line.constData());
#endif
            if (parseDir(line, QLatin1String(""), &i)) {
                emit listInfo(i);
            }
            else {
                // some FTP servers don't return a 550 if the file or directory
                // does not exist, but rather write a text to the data socket
                // -- try to catch these cases
                if (line.endsWith("No such file or directory\r\n"))
                    m_err = QString::fromLatin1(line);
            }
        }
        emit finished();
    }
    else {
        if (!m_is_ba && m_data.dev) {
            do {      
                qint64 bytesRead = m_socket->read(m_buffer, m_blockSize);
                if (bytesRead < 0) {
                    // a read following a readyRead() signal will
                    // never fail.
                    return;
                }
                m_bytesDone += bytesRead;
#if defined(QFTPDTP_DEBUG)
                qDebug("QFtpDTP read: %lli bytes (total %lli bytes)", bytesRead, bytesDone);
#endif
                if (m_data.dev)       // make sure it wasn't deleted in the slot
                    m_data.dev->write(m_buffer, bytesRead);
                emit dataTransferProgress(m_bytesDone, m_bytesTotal);
                if (m_bytesDone == m_bytesTotal)
                {//下载完毕
                    emit finished();
                }
                // Need to loop; dataTransferProgress is often connected to
                // slots that update the GUI (e.g., progress bar values), and
                // if events are processed, more data may have arrived.
            } while (m_socket->bytesAvailable());
        }
        else {
#if defined(QFTPDTP_DEBUG)
            qDebug("QFtpDTP readyRead: %lli bytes available (total %lli bytes read)",
                bytesAvailable(), bytesDone);
#endif
            emit dataTransferProgress(m_bytesDone + m_socket->bytesAvailable(), m_bytesTotal);
            emit readyRead();
        }
    }
    
}

void XQFtpDTP::socketError(QAbstractSocket::SocketError e)
{
    if (e == QTcpSocket::HostNotFoundError) {
#if defined(QFTPDTP_DEBUG)
        qDebug("QFtpDTP::connectState(CsHostNotFound)");
#endif
        emit connectState(XQFtpDTP::CsHostNotFound);
    }
    else if (e == QTcpSocket::ConnectionRefusedError) {
#if defined(QFTPDTP_DEBUG)
        qDebug("QFtpDTP::connectState(CsConnectionRefused)");
#endif
        emit connectState(XQFtpDTP::CsConnectionRefused);
    }
}

void XQFtpDTP::socketConnectionClosed()
{
    if (!m_is_ba && m_data.dev) {
        clearData();
    }
    if (m_socket->isReadable())
        m_bytesFromSocket = m_socket->readAll();
    else
        m_bytesFromSocket.clear();
#if defined(QFTPDTP_DEBUG)
    qDebug("QFtpDTP::connectState(CsClosed)");
#endif
    emit connectState(XQFtpDTP::CsClosed);
}

void XQFtpDTP::socketBytesWritten(qint64 bytes)
{
    m_bytesDone += bytes;
#if defined(QFTPDTP_DEBUG)
    qDebug("QFtpDTP::bytesWritten(%lli)", bytesDone);
#endif
    emit dataTransferProgress(m_bytesDone, m_bytesTotal);
    if (m_callWriteData)
        writeData();
}

void XQFtpDTP::setupSocket()
{
    m_socket = m_listener.nextPendingConnection();
    m_socket->setObjectName(QLatin1String("QFtpDTP Active state socket"));
    connect(m_socket, SIGNAL(connected()), SLOT(socketConnected()));
    connect(m_socket, SIGNAL(readyRead()), SLOT(socketReadyRead()));
    connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), SLOT(socketError(QAbstractSocket::SocketError)));
    connect(m_socket, SIGNAL(disconnected()), SLOT(socketConnectionClosed()));
    connect(m_socket, SIGNAL(bytesWritten(qint64)), SLOT(socketBytesWritten(qint64)));

    m_listener.close();
}


