﻿#include"test.h"
#include<QApplication>
#include"XQFtp.h"
#include"XQAlgorithm.h"
#include"XQLog.hpp"
#include<QFile>
void ftpTest()
{
    auto ftp=new XQFtp ;
    using Debug = Ftp::Debug;
    ftp->setDebugModel(Debug::CommandRead| Debug::CommandWrite| Debug::error);
    QObject::connect(qApp, &QApplication::aboutToQuit, ftp, &QObject::deleteLater);
    // 连接到 FTP 服务器
    ftp->connectToHost("192.168.1.3",21); // 替换为你的FTP服务器地址
    // 登录到 FTP 服务器
    ftp->login("FtpServer", "M1aSK4[z"); // 替换为你的FTP服务器登录凭据

    QObject::connect(ftp, &XQFtp::stateChanged, [=](Ftp::State state) { XQDebug << "状态改变" << QDebug::toString(ftp->state()); });

    // 检查连接状态
    if (!ftp->waitForConnected()) 
        XQDebug << "无法连接到FTP服务器：" << ftp->errorString();
    else
        XQDebug << "连接上服务器";

    // 检查登录状态
    if (!ftp->waitForLoggedIn())  
        XQDebug << "无法登录到FTP服务器：" << ftp->errorString();
    else
        XQDebug << "连接上服务器";
    //不是登录状态不进行下一步
    if (ftp->state() != Ftp::State::LoggedIn)
        return;
    ftp->cd("/FtpServer");
    ftp->mkdir("test");
    ftp->rmdir("test");
    //ftp->rename("MyQt.exe","note/MyQt.exe");
    ftp->list();
    ftp->waitForFinished();
    for (auto& info:ftp->listInfo())
    {
        qDebug() << info.name();
    }
    //静态成员方法
    for (auto& info : XQFtp::List("192.168.1.3", 21, "/FtpServer", "FtpServer", "M1aSK4[z"))
    {
        qDebug() << info.name();
    }
    //静态上传
    XQFtp::Put("192.168.1.3", 21, "/FtpServer/MyQt.exe", "MyQt.exe", Ftp::Binary, "FtpServer", "M1aSK4[z");
    //静态下载
    XQFtp::Get("192.168.1.3", 21, "/FtpServer/MyQt.exe", "Download/MyQt.exe", Ftp::Binary, "FtpServer", "M1aSK4[z");
    //进度
    QObject::connect(ftp, &XQFtp::dataTransferProgress, [](qint64 current, qint64 total) 
        {
            XQDebug << "传输中:"<<readableFileSize( current)<<"/"<< readableFileSize(total);
        });
    //上传
    ftp->put(QString("MyQt.exe"), "MyQt.exe");
    ftp->waitForFinished();
    XQDebug << "上传完成";
    //下载
    ftp->get("MyQt.exe","Download/MyQt.exe");
    ftp->waitForFinished();
    XQDebug << "下载完成";
  
    // 断开与FTP服务器的连接
    ftp->close();

}
