﻿#ifndef XQFTRFILEINFO_H
#define XQFTRFILEINFO_H
#include<QString>
enum class FileType//文件类型
{
	file,//文件
	dir//目录
};
class XQFtrFileInfo
{
public:
	XQFtrFileInfo(const QString& text);
	//显示文件大小字符串形式带单位
	QString fileSizeToString();
	//文件大小
	size_t fileSize();
	//文件类型
	FileType type();
	//链接数量
	size_t links();
	//拥有者
	QString& owner();
	//组名 
	QString& group();
	//名字 
	QString& name();
	//最后修改时间
	QString& time();
	//权限
	QString& privilege();
protected:
	//QString m_dirPath;//目录路径
	FileType m_type;//文件类型
	size_t m_links;//链接数量
	QString m_owner;//拥有者
	QString m_group;//组名
	QString m_name;//名字
	QString m_time;//最后修改时间
	QString m_OGH;//权限
	size_t m_fileSize;//文件大小
};
#endif // !XFtrFileInfo_H