﻿#include"XQFtpFileInfo.h"
#include"XStringOperation.hpp"
#include"XQAlgorithm.h"
#include<QDebug>

XQFtrFileInfo::XQFtrFileInfo(const QString& text)
{
	auto infolist = XStringOperation::strtok(text, " ");
	m_OGH = infolist[0];
	if (m_OGH[0] == 'd')//文件夹
		m_type = FileType::dir;
	else
		m_type = FileType::file;
	m_links = infolist[1].toUInt();
	m_owner = infolist[2];
	m_group= infolist[3];
	m_fileSize= infolist[4].toUInt();
	m_time = QString("%1 %2 -%3").arg(infolist[5]).arg(infolist[6]).arg(infolist[7]);
	m_name = infolist[8];
	//qDebug() << infolist;
}

QString XQFtrFileInfo::fileSizeToString()
{
    return std::move(readableFileSize(m_fileSize));
}

size_t XQFtrFileInfo::fileSize()
{
    return m_fileSize;
}

FileType XQFtrFileInfo::type()
{
    return m_type;
}

size_t XQFtrFileInfo::links()
{
    return m_links;
}

QString& XQFtrFileInfo::owner()
{
    return m_owner;
}

QString& XQFtrFileInfo::group()
{
    return m_group;
}

QString& XQFtrFileInfo::name()
{
    return m_name;
}

QString& XQFtrFileInfo::time()
{
    return m_time;
}

QString& XQFtrFileInfo::privilege()
{
    return m_OGH;
}
