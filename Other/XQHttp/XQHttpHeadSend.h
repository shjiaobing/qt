﻿#ifndef XQHTTPHEADSEND_H
#define XQHTTPHEADSEND_H
#include"XQHttpHeadRecv.h"
//http的头部数据报文
class XQHttpHeadSend:public XQHttpHeadRecv
{
public:
	XQHttpHeadSend();
	XQHttpHeadSend(const QString& header);
	virtual void clear();
	virtual bool isEmpty()const;
	//获得GET请求
	QString toString();
	//获得get查询参数
	QString argsToString();
	XStringMap<QString> args;//url中的get请求的查找参数
};
#endif // !XQHttpHeadRecv_H


